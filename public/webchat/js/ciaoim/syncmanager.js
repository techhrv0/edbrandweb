function loadContacts() {
    var contactItem1 = {
        userId: '149126hxxia',
        ciaoId: 'Swati',
        phone: '123',
        name: 'Swati',
        nick: 'Swati',
        status: 'Hello there! I\'m using CiaoIm.',
        selectMode: SELECT_MODE.CHAT,
        blockStatus: BLOCK_STATUS.NO_BLOCK,
        contactType: ''
    };
    phnbookMap['149126hxxia'] = contactItem1;

    // var contactItem1 = {
    //     userId: '149251o01en',
    //     ciaoId: 'vicky',
    //     phone: '123',
    //     name: 'vicky',
    //     nick: 'vicky',
    //     status: 'Hello there! I\'m using CiaoIm.',
    //     selectMode: SELECT_MODE.CHAT,
    //     blockStatus: BLOCK_STATUS.NO_BLOCK,
    //     contactType: ''
    // };
    // phnbookMap['149251o01en'] = contactItem1;


    var contactItem1 = {
        userId: '149122g0i4m',
        ciaoId: 'Divya',
        phone: '123',
        name: 'divya',
        nick: 'divya',
        status: 'Hello there! I\'m using CiaoIm.',
        selectMode: SELECT_MODE.CHAT,
        blockStatus: BLOCK_STATUS.NO_BLOCK,
        contactType: ''
    };
    phnbookMap['149122g0i4m'] = contactItem1;


    var contactItem1 = {
        userId: '149122wfrjm',
        ciaoId: 'Khyati',
        phone: '123',
        name: 'Khyati',
        nick: 'Khyati',
        status: 'Hello there! I\'m using CiaoIm.',
        selectMode: SELECT_MODE.CHAT,
        blockStatus: BLOCK_STATUS.NO_BLOCK,
        contactType: ''
    };
    phnbookMap['149122wfrjm'] = contactItem1;

    var contactItem1 = {
        userId: '149124grb8o',
        ciaoId: 'Ambuj',
        phone: '123',
        name: 'Ambuj',
        nick: 'Ambuj',
        status: 'Hello there! I\'m using CiaoIm.',
        selectMode: SELECT_MODE.CHAT,
        blockStatus: BLOCK_STATUS.NO_BLOCK,
        contactType: ''
    };
    phnbookMap['149124grb8o'] = contactItem1;
}


function handleAgentList(data) {
    console.log('handleAgentList '+JSON.stringify(data));
    var contactItem=null;
    $.each(data,function (key,value) {
        client.subscribe(data[key].agentId+'@'+domain);
        // handleAgentMap(data[key].agentId,data[key].name,data[key].displayName,data[key].email,100,data[key].role,data[key].enable,data[key].online);
        contactItem = {
            userId: data[key].agentId,
            ciaoId: data[key].name,
            phone: '123',
            name: data[key].displayName,
            nick: data[key].displayName,
            status: 'Hello there! I\'m using CiaoIm.',
            selectMode: SELECT_MODE.CHAT,
            blockStatus: BLOCK_STATUS.NO_BLOCK,
            contactType: ''
        };
        phnbookMap[data[key].agentId] = contactItem;
    })
}

function handleSyncResponse(category, body) {
    console.log('handleSyncResponse');
    body ='';// body.replace(/&quot;/g, "'");
    switch (category){
        case SYNC.CONTACT:
            contactSynced = true;
            // sendToDevice({}, SYNC.RECENTS);
            handleContacts();
            break;
        case SYNC.RECENTS:
            recentsSynced = true;
            handleRecentResponse();
            onRecents();
            break;
        case SYNC.LOAD_CHATS:
            handleOldChats(body);
            break;
        case SYNC.PING:
            pingResponse(messageId);
            break;
        case SYNC.CLEAR_CHAT:
            onClearChat(body);
            break;
        case SYNC.UPDATE_VCARD:
            //alert('Profile updated successfully');
            break;
        case SYNC.GROUP_CHAT:
            handleGroupStatus(body);
            break;
        case SYNC.BLOCK:
            handleBlockStatus(body);
            break;
        default:
            break;
    }
}

//[{"messageBody":"Hc","messageCategory":"single","messageId":"1485528860082-16221","messageState":"DELIVERY_SENT",
// "messageType":"text","sentReceivedTs":1485528860178,"userId":"148546du3gb"}]
function handleRecentResponse() {
    console.log('handleRecentResponse');
    // var response = JSON.parse(body);
    // var hasMore = response.hasMore;
    // var messages = response.messages;
    $('#indexHide').hide();
    $('#indexShow').show();
    $.each(phnbookMap, function (key, value) {
        if(phnbookMap[key].userId!=userName)
        addNewRecent(phnbookMap[key].userId, userName, 'hi '+phnbookMap[key].name, phnbookMap[key].name, MSG_TYPE.text, $.now(), $.now(), MESSAGE_STATE.READ, '0', SELECT_MODE.CHAT);
    });
//     if (jQuery.isEmptyObject(messages)) {
//         $(".chat_users").append('<div id="contact2"><img id="recentimg" src="/webchat/assets/images/recentimg.png" alt="" ><h3 id="no_msg" >No Message, yet!</h3><p id="contactText" >To start chat with your friends, Tap contacts.</p></div>');
//     }
// else{
//         $.each(messages, function (key, value) {
//             try {
//                 prepareRecentsWithLastMessage(value);
//             }catch (e){
//                 console.log('Error in preparing recents', e);
//             }
//         });
//     }
}

function prepareRecentsWithLastMessage(message) {
    var userId = message.userId;
    var name = 'Unknown';

    if (!jQuery.isEmptyObject(getPhonebookMap(userId))) {
        name = getPhonebookMap(userId).name;
    } else {
        name = message.displayName;
    }
    if (!isAlreadyInRecents(userId)) {
        addNewRecent(userId, message.fromUserId, message.messageBody, name, message.messageType, message.messageId, message.sentReceivedTs, message.messageState, message.unReadChats, message.selectMode);
        set_profile_pic(userId, PROFILE_TYPE.RECENT);
    }
    if(message.messageBody == undefined || message.messageBody == null) return;
    setLastMessage(userId, message.fromUserId, message.messageBody, message.messageType, message.messageId, message.sentReceivedTs, message.messageState, message.selectMode);
}

function handleOldChats(body) {
    var response = JSON.parse(body);
    var hasMore = response.hasMore;
    var page = response.page;
    var messages = response.messages;
    var tpl = '';

    if (hasMore == true) {
        tpl+=showHasMoreButton((page+1));
    }

    $.each(messages, function (key, message) {
        tpl += createChatBubble(message,TIMETYPE.OLD);
    });
    add_to_view(tpl, TIMETYPE.OLD, page);
    $.each(messages, function (key, message) {
        mayBeMedia(message.messageId, message.messageType, message.fileName);
        if(message.messageType==MSG_TYPE.audio){
            if(isSent(message.messageState)){
                set_profile_pic(userName, PROFILE_TYPE.VOICERECORD_DEVICE);
            }
            else {
                set_profile_pic(message.userId, PROFILE_TYPE.VOICERECORD_DEVICE);
            }
        }
    });
    isLoadingChats = false;
}

function mayBeMedia(messageId, messageType, fileName) {
    switch (messageType) {
        case MSG_TYPE.image:
        case MSG_TYPE.video:
        case MSG_TYPE.audio:
        case MSG_TYPE.file:
            var ext = getExt(fileName);
            var s3key = get_s3_key(messageId, messageType, ext);
            get_signed_url(s3key);
        default:
            return '';
    }
}

function isSent(messageState) {
    if (messageState == MESSAGE_STATE.TO_SEND || messageState == MESSAGE_STATE.SENT || messageState == MESSAGE_STATE.DELIVERED || messageState == MESSAGE_STATE.READ_BY_RECEIVER) {
        return true;
    }
    return false;
}

function loadMoreMsgs(page) {
    $("#ldmoreBtn").remove();
    // $("#load_new").show();
    var selMode = chatWithContact.selectMode == SELECT_MODE.HIDDEN ? SELECT_MODE.CHAT : chatWithContact.selectMode;
    var params = '{"userId":"' + chatWithContact.userId + '", "selectMode": "' +selMode+ '", "page":' + page + '}';
    sendToDevice(params, SYNC.LOAD_CHATS);
}
function loadMoreMsgs1(page) {
    $("#ldmoreBtn").hide();
    $("#load_new").show();
    var selMode = chatWithContact.selectMode == SELECT_MODE.HIDDEN ? SELECT_MODE.CHAT : chatWithContact.selectMode;
    var params = '{"userId":"' + chatWithContact.userId + '", "selectMode": "' +selMode+ '", "page":' + page + '}';
    sendToDevice(params, SYNC.LOAD_CHATS);
}

function publishMyVcard(status, nick, avtar) {
    $('#userNick').html(nick);
    var params = '{"nickName":"' + nick + '", "status": "' + status + '", "avtar":' + avtar + '}';
    sendToDevice(params, SYNC.UPDATE_VCARD);
}

function onClearChat(body) {
    var response = JSON.parse(body);
    var userId = response.userId;
    var status = response.status;
    var isDelete = response.isDelete;
    if(status == 200){
        var ul = document.getElementById("ulchatdemo");
        while (ul.firstChild) ul.removeChild(ul.firstChild);
        $('#' + userId + '_time').html('');
        $('#' + userId + '_last_msg').html('');

        if(isDelete == '1'){
            $('#' + userId + '_divRecentsUL').remove();
            delete recentsMap[userId];

            var ul = document.getElementById("ulchatdemo");
            while (ul.firstChild) ul.removeChild(ul.firstChild);
            $('#user_chat_theme1').hide();
            $('#default_chat_theme').show();
        }
        else{
            $('#load_more').remove();
        }
        onStartChat(userId);
    }else{
       // alert("Some problem occured in clear chat messages!");
    }
}
