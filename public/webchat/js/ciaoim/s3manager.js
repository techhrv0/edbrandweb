function get_signed_url(fileKey) {
    client.sendIq({
        type: 'get',
        opcode: 'get_signed_url',
        xmlns: 'urn:xmpp:s3:1',
        key: fileKey,
    }, function (err, res) {
    });
}

function s3_response(s3Key, url, token, status) {
    if (status) {
        var data = {
            url: url,
            token: token
        };
        s3Map[s3Key] = data;
        set_media(s3Key, data);
    }
}

function set_media(s3Key, data) {
    var dir = get_s3dir(s3Key);
    switch (dir){
        case DIR.PROFILE:
            setProfile(s3Key);
            break;
        // case DIR.IMAGE.toLowerCase():
        //     set_chatMedia(s3Key, data);
        //     break;
        default:
            set_chatMedia(s3Key, data);
            break;
    }
}

function set_chatMedia(s3Key, data) {
    var msgId = get_s3user(s3Key);
    try{
        if(document.getElementById(msgId+"_msg") == undefined || document.getElementById(msgId+"_msg") == null){
            document.getElementById(msgId+"_preview").src= makeUrl(data);
            return;
        }
        document.getElementById(msgId+"_msg").src= makeUrl(data);
        document.getElementById(msgId + "_msg").setAttribute("data-URL", makeUrl(data));
    }
    catch (e){}
}

function get_s3dir(s3Key) {
    var slashIndex = s3Key.indexOf("/");
    return s3Key.substring(0, slashIndex);
}

function get_s3user(s3Key) {
    var slashIndex = s3Key.indexOf('/');
    var dotIdex = s3Key.indexOf('.');
    return s3Key.substring(slashIndex + 1, dotIdex);
}
function setProfile(s3Key) {
    if (mayBeMe(s3Key)) {
        return;
    }
    var userId = get_s3user(s3Key);
    if (currentTab == TAB_CONTACTS) {
        set_profile_pic(userId, PROFILE_TYPE.CONTACT);
        return;
    }
    set_profile_pic(userId, PROFILE_TYPE.RECENT);
    if(userId==chatWithContact.userId){
        console.log('setProfile '+userId);
        set_profile_pic(userId, PROFILE_TYPE.CHATWITH);
        set_profile_pic(userId, PROFILE_TYPE.CHATWITHGROUP);
    }
}

function mayBeMe(s3Key) {
    if (get_profile_key(userName) == s3Key) {
        set_profile_pic(userName, PROFILE_TYPE.ME);
        return true;
    }
    false;
}
