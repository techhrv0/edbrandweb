var textcontent = "";
$(window).load(function () {
    document.onkeydown = checkKey;
    $(".status_setN").click(function () {
        $("#tick").addClass("tick_blue");
    });
    $(".status_setN").focusout(function () {
        $("#tick").addClass("tick_grey");
        $("#tick").removeClass("tick_blue");
    });
    $(".status_set").click(function () {
        $("#tick1").addClass("tick_blue");
    });
    $(".status_set").focusout(function () {
        $("#tick1").addClass("tick_grey");
        $("#tick1").removeClass("tick_blue");
    });
    $("#grpSubject").click(function () {
        $("#tick2").addClass("tick_blue");
    });
    $("#grpSubject").focusout(function () {
        $("#tick2").addClass("tick_grey");
        $("#tick2").removeClass("tick_blue");
    });

    window.onbeforeunload = function () {
        client.sendIq({
            type: 'get',
            opcode: 'close_session',
            xmlns: 'urn:xmpp:s3:1',
        }, function (err, res) {
        });
    };

    $("#Search").keyup(function (e) {
        $(".chat_users").empty();
        var keySearchs = $("#Search").val();
        var keySearch = keySearchs.toLowerCase();
        if (currentTab == TAB_CONTACTS) {
            getContact(phnbookMap, keySearch);
        }
        if (currentTab == TAB_RECENTS) {
            getRecents(recentsMap, keySearch);
        }
    });
});

function checkKey(e) {
    if (e.keyCode == '38') {// up arrow
        e.preventDefault();
        var scroll_to_chatHeight=60+'px';
        $("#ulchatdemo").slimScroll({
            scrollBy: -60+'px'
        });
    }
    if (e.keyCode == '40') {// down arrow
        e.preventDefault();
        var scroll_to_chatHeight=60+'px';
        $("#ulchatdemo").slimScroll({
            scrollBy: scroll_to_chatHeight
        });
    }
}




function getContact(phnbookMap, keySearch) {
    var result = [];
    var sorted = "contacts";
    $.each(phnbookMap, function (key, val) {
        var usernames = phnbookMap[key].name;
        var phone = phnbookMap[key].phone;
        var username = usernames.toLowerCase();
        if ((username.indexOf(keySearch) != -1 )) {
            result.push(val);
        }
    });
    if (!jQuery.isEmptyObject(result)) {//not empty
        $('#nocontact').text("");
        if ($('#Search').value == "") {
            sort_contacts(result, sorted, true);
        }
        else {
            set_contactAnkur(result,sorted, true);
        }
    }
    if (jQuery.isEmptyObject(result)) {// empty
        $('#nocontact').text("No contacts found");
    }
}
function getRecents(recentsMap, keySearch) {
    var result = [];
    $.each(recentsMap, function (key, val) {
        // if (val.type == 'user')
        var usernames =recentsMap[key].name;
        // else
        //   var usernames = val.groupName;
        var username = usernames.toLowerCase();
        if (username.indexOf(keySearch) != -1) {
            result.push(val);
        }
    });
    if (!jQuery.isEmptyObject(result)) {//not empty
        $('#nocontact').text("");
        set_recents(result);
    }
    if (jQuery.isEmptyObject(result)) {// empty
        $('#nocontact').text("No chat or messages found");
    }
}
function set_tab(type) {
    console.log('set_tab '+type);
    var sorted = "contacts";
    currentTab = type;
    $('#nocontact').text("");
    $(".chat_users").empty();
    if (type == TAB_CONTACTS) {
        if (!jQuery.isEmptyObject(phnbookMap)) {
            sort_contacts(phnbookMap, sorted, false);
        }
        return;
    }
    if (jQuery.isEmptyObject(recentsMap)) {
        $(".chat_users").append('<div id="contact2"><img id="recentimg" src="/webchat/assets/images/recentimg.png" alt="" ><h3 id="no_msg" >No Message, yet!</h3><p id="contactText" >To start chat with your friends, Tap contacts.</p></div>');
    } else {
        var msgTimeArr = [];
        msgTimeArr = sortMessageByTime(msgTimeArr, recentsMap);
        for (i = 0; i < msgTimeArr.length; i++) {
            $.each(recentsMap, function (key, value) {
                if ((msgTimeArr[i] != 0 && msgTimeArr[i] == value.messageTime) || (msgTimeArr[i]  == value.userId)) {
                    appendRecentsNew(value);
                    set_profile_pic(value.userId, PROFILE_TYPE.RECENT);
                }
            });
        }
    }
    adjustRecentWith();
    $('#Search').val('');
}
function set_tab_onLoad(type) {
    if (currentTab != '')
        currentTab = type;
    else
        currentTab = TAB_RECENTS;
    $(".chat_users").empty();
    isNoLastMsg = false;
    phnbookMap = {};
    recentsMap = {};
    groupMap = {};
    // getMyPhonebook();
}

function getMyPhonebook() {
    client.sendIq({
        type: 'set',
        opcode: 'get_myphone_list',
        xmlns: 'urn:xmpp:ciaoim:2',
    }, function (err, res) {
    });
}

function getMyGroups() {
    client.sendIq({
        type: 'set',
        opcode: 'get_mygroups',
        xmlns: 'urn:xmpp:ciaoim:2',
    }, function (err, res) {
    });
}

function deleteAccount() {
    client.deleteAccount(userName + "@" + domain, function (err, res) {
        if (err == null || err == '') {
        }
        else {
        }
    });
}

function stopEnter() {
    var keyStop = {
        13: ":not(input:text, textarea, input:file, input:password)", // stop enter
        8: "input:text, input:password",
        end: null
    };
    $("#mynick,#grpSubject").bind("keydown", function (event) {
        var selector = keyStop[event.which];
        if (selector !== undefined && $(event.target).is(selector)) {
            if (chatWithContact.selectMode == SELECT_MODE.GROUP) {
                saveGroupSubject();
                $('#grpSubject').addClass('subject_height');
                $('#grpSubject').removeClass('subject_height_border');
            }
            else {
                saveProfileNick();
                $('#mynick').addClass('status_setN');
                $('#mynick').removeClass('status_set1');
            }
            event.preventDefault(); //stop event
        }
        return true;
    });
    $("#mystatus").bind("keydown", function (event) {
        var selector = keyStop[event.which];
        if (selector !== undefined && $(event.target).is(selector)) {
            saveProfileStatus();
            $('#mystatus').addClass('status_set');
            $('#mystatus').removeClass('status_set1');
            event.preventDefault(); //stop event
        }
        return true;
    });
}

function updateStatus() {
    stopEnter();
    var max = 120;
    var x = document.getElementById("mystatus");
    if (x.contentEditable != "true") {
        x.contentEditable = "true";
        $("#pencil1").hide();
        $("#tick1").show();
        $('#statusCounter').show();
        $("#mystatus").keydown(function (e) {
            var count = $('#statusCounter');
            var characters = $(this).text().length;
            var charact = characters + 1;
            count.text(max - charact);
            if (e.which < 0x20) {
                count.text(max - (characters));
                // e.which < 0x20, then it's not a printable character
                // e.which === 0 - Not a character
                return;     // Do nothing
            }
            if (charact == max) {
                e.preventDefault();
            }

            if (e.ctrlKey) {
                if (e.keyCode == 86) {//Paste
                    if (charact > max - 1) {
                        $('#statusCounter').text('0');
                        e.preventDefault();
                        $('#mystatus').text($('#mystatus').text().substring(0, max - 1));
                    }
                }
            }
        });
    }
    $("#tick1").removeClass("tick_blue");
    $("#tick1").addClass("tick_grey");
}

function updateNick() {
    stopEnter();
    var max = 30;
    var y = document.getElementById("mynick");
    if (y.contentEditable != "true") {
        y.contentEditable = "true";
        $("#pencil").hide();
        $("#tick").show();
        $('#nameCounter').show();
        //$('#nameCounter').text(characters);
        $("#mynick").keydown(function (e) {
            var count = $('#nameCounter');
            var characters = $(this).text().length;
            var charact = characters + 1;
            count.text(max - charact);
            if (e.which < 0x30) {
                count.text(max - (characters));
                return;
            }
            if (charact == max) {
                e.preventDefault();
            }
            if (e.ctrlKey) {
                if (e.keyCode == 86) {//Paste
                    if (charact > max - 1) {
                        $('#nameCounter').text('0');
                        e.preventDefault();
                        $('#mynick').text($('#mynick').text().substring(0, max - 1));
                    }
                }
            }
        });
    }
    $("#tick").removeClass("tick_blue");
    $("#tick").addClass("tick_grey");
}

$("#pencil2").click(function () {
    $('#groupCounter').show();
    $('#grpSubject').removeClass('subject_height');
    $('#grpSubject').addClass('subject_height_border');
});
$("#pencil1").click(function () {
    $('#statusCounter').show();
    $('#mystatus').removeClass('status_set');
    $('#mystatus').addClass('status_set1');
});
$("#pencil").click(function () {
    $('#nameCounter').show();
    $('#mynick').removeClass('status_setN');
    $('#mynick').addClass('status_set1');
});
$("#tick").click(function () {
    $('#mynick').addClass('status_setN');
    $('#mynick').removeClass('status_set1');
});
$("#tick1").click(function () {
    $('#mystatus').addClass('status_set');
    $('#mystatus').removeClass('status_set1');
});
$("#tick2").click(function () {
    $('#grpSubject').addClass('subject_height');
    $('#grpSubject').removeClass('subject_height_border');
});

function saveProfileNick() {
    $("#pencil").show();
    $("#tick").hide();
    $('#nameCounter').hide();
    $('#mynick').blur();
    
    var y = document.getElementById("mynick");
    y.contentEditable = "false";

    if ($('#mynick').text() == "") {
        alert("Your name can't be empty.");
        $('#mynick').text(userDetail.userNickName);
        return false;
    }
    if ($('#mynick').text() == userDetail.userNickName) {
        return false;
    }
    if (($('#mystatus').text()) != "" && ($('#mynick').text()) != "")
        publishMyVcard(null, $('#mynick').text(), null);
}
function saveProfileStatus() {
    $("#pencil1").show();
    $("#tick1").hide();
    $('#statusCounter').hide();
    $('#mystatus').blur();

    var x = document.getElementById("mystatus");
    x.contentEditable = "false";

    if ($('#mystatus').text() == "") {
        alert("Your status can't be empty.");
        $('#mystatus').text(userDetail.userStatus);
        return false;
    }
    if ($('#mystatus').text() == userDetail.userStatus) {
        return false;
    }
    if (($('#mystatus').text()) != "" && ($('#mynick').text()) != "")
        publishMyVcard($('#mystatus').text(),$('#mynick').text(), null);
}

function plusIconClick() {
    $('#demo').show();
    $('#plusIcon').hide();
    $('#crossIcon').show();
    var scrollTo_val = $('#chats .slimScrollDiv').prop('scrollHeight') + 'px';
    $("#ulchatdemo").slimScroll({
        scrollTo: scrollTo_val,
        start: 'bottom'
    });

    /* var heightslim = $(window).height() - $(".page-header").height() - $(".portlet-title").height() - $("#blocking_msg").height();
     $("#chats .slimScrollDiv").css("height", heightslim);
     $("#ulchatdemo").css("height", $(".chatbody-main").find(".slimScrollDiv").height());

     var scrollTo_val = $('#chats .slimScrollDiv').prop('scrollHeight') + 'px';
     $("#ulchatdemo").slimScroll({
     scrollTo: scrollTo_val,
     start: 'bottom'
     });*/
}
function crossIconClick() {
    $('#demo').hide();
    $('#plusIcon').show();
    $('#crossIcon').hide();
    /*  var heightslim = $(window).height() - $(".page-header").height() - $(".portlet-title").height() - $("#blocking_msg").height();
     $("#chats .slimScrollDiv").css("height", heightslim);
     $("#ulchatdemo").css("height", $(".chatbody-main").find(".slimScrollDiv").height());

     var scrollTo_val = $('#chats .slimScrollDiv').prop('scrollHeight') + 'px';
     $("#ulchatdemo").slimScroll({
     scrollTo: scrollTo_val,
     start: 'bottom'
     });*/
}

function setSub() {
    $('#grpSubArea').html('<p id="grpname" onfocus="grpnameclear()" contentEditable="true" class="subject_height_grp" spellcheck="false"></p><div id="groupCounter1" class="counter_set"> </div><div id ="groupSubject" onclick="getGrpNameFocus()">Group Subject</div>');
    userId = userName + '_' + $.now();
}

$(function () {
    $("#fileupload1").change(function () {
        $("#f").html($("#fileupload1").val().substring($("#fileupload1").val().lastIndexOf('\\') + 1));
    });
});

function sort_contacts(phnbookMap, type, isSearched) {
    console.log('sort_contacts '+type);
    var contactNames = [];
    var i = 0;
    var isMember=false;
    $.each(phnbookMap, function (key, value) {
        console.log('phnbook map '+phnbookMap[key].name);
        //only sort user message
        contactNames[i] = phnbookMap[key].name;
        i++;
    });
    contactNames.sort(function (a, b) {
        var nameA = a.toLowerCase(), nameB = b.toLowerCase();
        if (nameA < nameB) //sort string ascending
            return -1;
        if (nameA > nameB)
            return 1;
        return 0; //default return value (no sorting)
    });
    $.each(contactNames, function (key1, value) {
        $.each(phnbookMap, function (key, value) {
            if (contactNames[key1] == phnbookMap[key].name) {
                //for user only
                if ((type == "contacts") && (phnbookMap[key].selectMode == SELECT_MODE.CHAT)&&(phnbookMap[key].userId != userName)){
                    set_contactAppend(value, isSearched);
                    // set_profile_pic(value.userId, PROFILE_TYPE.CONTACT);
                    // set_profile_pic(value.userId, PROFILE_TYPE.CONTACTSHARING);
                }
                else if(((type=='addGrpMember')||(type=='addGrpMemberNew')||(type=='users_list_for_grp'))&& (phnbookMap[key].selectMode == SELECT_MODE.CHAT)&&(phnbookMap[key].userId != userName)) {
                    set_grp_list(phnbookMap[key], type, false);
                    // set_profile_pic(phnbookMap[key].userId, PROFILE_TYPE.DISPLAYGRPMEMBERS);
                    // set_profile_pic(phnbookMap[key].userId, PROFILE_TYPE.CONTACTSHARE);
                }
                else if(type=='showGrpMembers') {
                    if(phnbookMap[key].userId == userName){
                        isMember=true;
                    }
                    else{
                        displayGroupMembers(phnbookMap[key]);
                    }
                }
            }
        });
    });
    if(isMember){
        displayGroupMembers(phnbookMap[userName]);
    }
}

function sortMembers(phnbookMap, type, isSearched){
    var allContacts = [];
    var i = 0;
    $.each(phnbookMap,function(key,value){
        allContacts[i] = phnbookMap[key].name;
        i++;
    });
    allContacts.sort(function (a, b) {
        var nameA = a.toLowerCase(), nameB = b.toLowerCase();
        if (nameA < nameB) //sort string ascending
            return -1;
        if (nameA > nameB)
            return 1;
        return 0;
    });
    $.each(allContacts, function (key1, value) {
        $.each(phnbookMap, function (key, value) {
            if (allContacts[key1] == phnbookMap[key].name) {
                if((type=='addToNewGroup')&& (phnbookMap[key].selectMode == SELECT_MODE.CHAT)&&(phnbookMap[key].userId != userName)) {
                    set_membersAppend(value,type, isSearched);
                    // set_profile_pic( phnbookMap[key].userId, PROFILE_TYPE.DISPLAYGRPMEMBERS);
                }
            }
        });
    });
}

function set_membersAppend(data,type, isSearched) {
    //show sorted contacts
    var tpl = '';
    tpl += '<ul class="media-list list-items unselect" id="' + data.userId + '_divContactUL">';
    tpl += "<li class='" + data.userId + "_divMember media' id='' onclick='addToGroupArr(\"" + data.userId + "\", \"" + data.name + "\",\"" + type + "\");'>";
    tpl += "<div class='contact_list' style='width:100%;height:66px;padding: 5px 0 ;'>";
    tpl += "<div class='compatible' id='" + data.userId + "_avtar_contact_div' style='width:73px;float:left;padding: 5px 10px !important;'>";
    if (isSearched)
        tpl += "<img class='" + data.userId + "_avtar_contact_share media-object ' id='' src='" + getPhonebookMap(data.userId).avatarURL + "' alt='' onerror='imgError(this,\"" + data.userId + "\");'>";
    else
        tpl += "<img class='" + data.userId + "_avtar_contact_share media-object ' id='' src='" + userPic + "' alt='' onerror='imgError(this,\"" + data.userId + "\");'>";
    tpl += "</div>";
    tpl += "<div class='chat_body'>";
    tpl += "<div class='contactlist_panal'>";
    tpl += "<h4 id='" + data.userId + "_contact_name' class='contact_name' title='" + data.name + "'>" + data.name + "</h4>";
    tpl += "</div>";
    tpl += "<div>";
    tpl += "<p id='" + data.userId + "_contact_message_text' class='message_text' title='' >" + showLimitedChar1(data.status) + "</p>";
    tpl += "</div>";
    tpl += "</div>";
    tpl += "</div>";
    tpl += '</li>';
    tpl += "</ul>";
    $(".chat_usersc").append(tpl);
    $('#' + data.userId + '_contact_message_text').attr('title', data.status);
}

function set_contactAppend(data, isSearched) {
    //show sorted contacts
    var tpl = '';
    tpl += '<ul class="media-list list-items unselect" id="' + data.userId + '_divContactUL">';
    tpl += "<li class='media' id='" + data.userId + "_divContact' onclick='onStartChat(\"" + data.userId + "\");'>";
    tpl += "<div class='contact_list' style='width:100%;height:66px;padding: 5px 0 ;'>";
    tpl += "<div class='compatible' id='" + data.userId + "_avtar_contact_div' style='width:73px;float:left;padding: 5px 10px !important;'>";
    if (isSearched)
        tpl += "<img class='media-object ' id='" + data.userId + "_avtar_contact' src='/webchat/assets/images/profile.jpg' alt='' onerror='imgError(this,\"" + data.userId + "\");'>";
    else
        tpl += "<img class='media-object ' id='" + data.userId + "_avtar_contact' src='/webchat/assets/images/profile.jpg' alt='' onerror='imgError(this,\"" + data.userId + "\");'>";
    tpl += "</div>";
    tpl += "<div class='chat_body'>";
    tpl += "<div class='contactlist_panal'>";
    tpl += "<h4 id='" + data.userId + "_contact_name' class='contact_name' title='" + data.name + "'>" + data.name + "</h4>";
    tpl += "</div>";
    tpl += "<div>";
    tpl += "<p id='" + data.userId + "_contact_message_text' class='message_text' title='' >" + showLimitedChar1(data.status) + "</p>";
    // tpl += "<p id='" + data.userId + "_contact_message_text' class='message_text' >"+decodeBase64(showLimitedChar1(data.status))+"</p>";
    tpl += "</div>";
    tpl += "</div>";
    tpl += "</div>";
    tpl += '</li>';
    tpl += "</ul>";
    $(".chat_users").append(tpl);
    $('#' + data.userId + '_contact_message_text').attr('title', data.status);
}

function set_contactAnkur(data,sorted,isSearched) {
    sort_contacts(data, sorted, true);
    highlightContact($('#Search').val());
}

function setGroup(data,sorted,isSearched){
    sortMembers(data, sorted, true);
    set_profile_pic(data.userId, PROFILE_TYPE.DISPLAYGRPMEMBERS);
    highlightContact($('#searchContactgrp').val());
}

function showLimitedChar1(str) {
    if (str.length > 27) {
        str = str.substring(0, 27) + "...";
    }
    return str;
}
// function truncateUserName1(str) {
// var msgId = $.now();
// var emoji = new EmojiConvertor();
// var eText = emoji.replace_unified(str);
// if(str.length > 17)
//     str = str.substring(0,17)+'...';
// return str;
// }
function imgError(image, userId) {
    image.onerror = "";
    image.src = "/webchat/assets/images/user.png";
    if (!jQuery.isEmptyObject(getPhonebookMap(userId))) {
        if (getPhonebookMap(userId).userId == userId) {
            $('#' + userId + '_avtar_recent_div').removeClass('compatible');
            $('#' + userId + '_avtar_recent_div').addClass('compatibledefault');
            $('#' + userId + '_avtar_contact_div').removeClass('compatible');
            $('#' + userId + '_avtar_contact_div').addClass('compatibledefault');
            $('#' + userId + '_avtar_contact_shared').removeClass('compatible');
            $('#' + userId + '_avtar_contact_shared').addClass('compatibledefault');
            $('#' + userId + '_avtar').removeClass('profile_setting');
            $('#' + userId + '_avtar').addClass('profile_setting_default');
        }
    }
    return true;
}

function appendRecentsNew(data) {
    //append recents when not searched or searched but returned no result
    var tpl='';
    if (data.selectMode == SELECT_MODE.CHAT || data.selectMode == SELECT_MODE.HIDDEN) {
        console.log('appendRecentsNew '+data.selectMode)
        tpl = '<ul class="media-list list-items unselect" id="' + data.userId + '_divRecentsUL">';
        tpl += "<li class='media' id='" + data.userId + "_divRecents' onclick='onStartChat(\"" + data.userId+ "\");'>";
        tpl += "<div class='contact_list' style='width:100%;height:66px;padding: 5px 0 ;'>";
        tpl += "<div class='compatible' style='width:78px;float:left;'>";
        tpl += "<div class='icon_set'>";
        tpl += "<img class='media-object ' id='" + data.userId + "_avtar_recent' src='/webchat/assets/images/profile.jpg' alt='' onerror='imgError(this,\"" + data.userId + "\");'>";
        tpl += "</div>";
        tpl += "</div>";
        tpl += "<div class='chat_body'>";
        tpl += "<div class='contactlist_panal'>";
        tpl += "<div class='name_time'>";
        tpl += "<h4 class='contact_name' title='" + data.name+ "'>" + data.name + "</h4>";
        tpl += "<div class='online_status'>";
        if(data.messageTime != undefined && data.messageTime != 0){
            tpl += "<p id='" + data.userId + "_time' class='time text'>" + get_date1(TIMETYPE.OLD, data.messageTime) + "</p>";
        }else{
            tpl += "<p id='" + data.userId + "_time' class='time text'></p>";
        }
        if (getrecentsMap(data.userId).unreadMsg != 0) {
            tpl += "<p class='badge badge-danger'  id='" + data.userId + "_cnt' >" + parseInt(getrecentsMap(data.userId).unreadMsg) + "</p>";
        } else {
            tpl += "<p class='badge badge-danger'  id='" + data.userId + "_cnt' ></p>";
        }
        tpl += "</div>";
        tpl += "</div>";
        tpl += "<div class='icon_msg_set'>";
        if(data.body != undefined || data.body != null) {
            tpl += "<p id='" + data.userId + "_last_msg' class='message_text' title='" + data.body + "'>" + data.body + "</p>";
        }
        tpl += "</div>";
        tpl += "</div>";
        tpl += "</div>";
        tpl += "</div>";
        tpl += '</li>';
        $(".chat_users").append(tpl);
        // set_profile_pic(data.userId, PROFILE_TYPE.RECENT);
        setLastMessage(data.userId,data.fromUserId, data.body, data.msgType, data.msgId, data.messageTime, data.chatState,data.selectMode);
    }
    if (data.selectMode == SELECT_MODE.GROUP) {
        tpl += '<ul class="media-list list-items unselect" id="' + data.userId + '_divRecentsUL">';
        tpl += "<li class='media' id='" + data.userId + "_divRecentsGrp' onclick='onStartChat(\"" + data.userId+ "\");'>";
        tpl += "<div class='contact_list style='width:100%; height:66px; padding:5px 0 ;'>";
        tpl += "<div class='compatible' id='" + data.userId + "_avtar_Grp_Recent' style='width: 73px; float: left; padding: 5px 10px ! important;'>";
        tpl += "<img class='media-object' id='" + data.userId + "_avtar_recent' onerror='imgErrorgs(this,\"" + data.userId + "\");' src='" + groupPic + "'>";
        tpl += "</div>";
        tpl += "<div class='chat_body'>";
        tpl += "<div class='contactlist_panal'>";
        tpl += "<div class='name_time'>";
        tpl += "<h4 class='contact_name' id='" + data.userId + "_groupName' title='" + truncateUserName(data.name) + "'>" + truncateUserName(data.name) + "</h4>";
        tpl += "<div class='online_status'>";
        if(data.messageTime != undefined && data.messageTime != 0){
            tpl += "<p id='" + data.userId + "_time' class='time text'>" + get_date1(TIMETYPE.OLD, data.messageTime) + "</p>";
        }else{
            tpl += "<p id='" + data.userId + "_time' class='time text'></p>";
        }
        if (getrecentsMap(data.userId).unreadMsg != 0)
            tpl += "<p class='badge badge-danger'  id='" + data.userId + "_cnt' >" + parseInt(getrecentsMap(data.userId).unreadMsg) + "</p>";
        else
            tpl += "<p class='badge badge-danger'  id='" + data.userId + "_cnt' ></p>";
        tpl += "</div>";
        tpl += "</div>";
        tpl += "<div class='icon_msg_set'>";
        if(data.body != undefined || data.body != null) {
            tpl += "<p id='" + data.userId + "_last_msg' class='message_text' title='" + data.body + "'>" + data.body + "</p>";
        }
        tpl += "</div>";
        tpl += "</div>";
        tpl += "</div>";
        tpl += "</div>";
        tpl += '</li>';
        $(".chat_users").append(tpl);
        // set_profile_pic(data.userId, PROFILE_TYPE.RECENT);
        setLastMessage(data.userId,data.fromUserId, data.body, data.msgType, data.msgId, data.messageTime, data.chatState,data.selectMode);
    }
    /*  /!*FOR NOTIFICATION WIDTH*!/
     var notification_width = parseFloat($('.notifiedMsg').width())-65;
     $(".noti-msg").css("width",notification_width);*/
}
function changeIcon() {
    $('.emoji-wysiwyg-editor').trigger('keydown');
}
function appendRecentsNewAnkur(data) {
    var tpl = '';
    if (data.selectMode == SELECT_MODE.CHAT) {
        appendRecentsNew(data);
        highlightContact($('#Search').val());
    }
    if (data.selectMode == SELECT_MODE.GROUP) {
        tpl += '<ul class="media-list list-items unselect" id="' + data.userId + '_divRecentsUL">';
        tpl += "<li class='media' id='" + data.userId + "_divRecentsGrp' onclick='onStartChat(\"" + data.userId + "\");'>";
        tpl += "<div class='contact_list style='width:100%; height:66px; padding:5px 0 ;'>";
        tpl += "<div class='compatible' id='" + data.userId + "_avtar_Grp_Recent' style='width: 73px; float: left; padding: 5px 10px ! important;'>";
        tpl += "<img class='media-object' id='" + data.userId + "_avtar_recent' onerror='imgErrorgs(this,\"" + data.userId + "\");' src='" + groupPic + "'>";
        tpl += "</div>";
        tpl += "<div class='chat_body'>";
        tpl += "<div class='contactlist_panal'>";
        tpl += "<div class='name_time'>";
        tpl += "<h4 class='contact_name' id='" + data.userId + "_groupName' title='" + truncateUserName(data.name) + "'>" + truncateUserName(data.name) + "</h4>";
        tpl += "<div class='online_status'>";
        tpl += "<p id='" + data.userId + "_time' class='time text'>" + get_date1(TIMETYPE.OLD, data.messageTime) + "</p>";
        if (getrecentsMap(data.userId).unreadMsg != 0)
            tpl += "<p class='badge badge-danger'  id='" + data.userId + "_cnt' >" + parseInt(getrecentsMap(data.userId).unreadMsg) + "</p>";
        else
            tpl += "<p class='badge badge-danger'  id='" + data.userId + "_cnt' ></p>";
        tpl += "</div>";
        tpl += "</div>";
        tpl += "<div class='icon_msg_set'>";
        tpl += "<p id='" + data.userId + "_last_msg' class='message_text'>"+data.body+"</p>";
        tpl += "</div>";
        tpl += "</div>";
        tpl += "</div>";
        tpl += "</div>";
        tpl += '</li>';
        $(".chat_users").append(tpl);
    }
    highlightContact($('#Search').val());
}

function set_recents(data) {
    var msgTimeArr = [];
    var i = 0;
    $.each(data, function (key, value) {
        msgTimeArr[i] = data[key].messageTime;
        i++;
    });
    msgTimeArr.sort(function (a, b) {
        // Turn your strings into dates, and then subtract them
        // to get a value that is either negative, positive, or zero.
        return new Date(b).getTime() - new Date(a).getTime();
    });
    for (i = 0; i < msgTimeArr.length; i++) {
        $.each(data, function (key, value) {
            if (msgTimeArr[i] == data[key].messageTime) {
                if ($('#Search').val() == "") {
                    appendRecentsNew(data[key]);
                }
                if ($('#Search').val() != "") {
                    appendRecentsNewAnkur(data[key]);
                }
            }
        });
    }
}

function set_nonuser(from) {
    var avtar = '/webchat/assets/images/user.jpg';
    var tpl = '<ul class="media-list list-items unselect">';
    tpl += "<li class='media' id='" + from + "_divContact' onclick='startChat(\"" + from + "\", \"" + TAB_CONTACTS + "\" , \"" + from + "\");'>";
    tpl += "<div class='contact_list'>";
    tpl += "<div class='compatible'>";
    tpl += "<img class='media-object img-circle' src='" + avtar + "' alt='...'>";
    tpl += "</div>";
    tpl += "<div class='contactlist_panal'>";
    tpl += "<h4 class='contact_name'>" + from + "</h4>";
    tpl += "<p class='message_text'>" + from + "</p>";
    tpl += "</div>";
    tpl += "</div>";
    tpl += '</li>';
    tpl += "</ul>";
    $(".chat_users").append(tpl);
}

function getLastActivity(opponent_jid) {
    client.sendIq({
        to: opponent_jid + "@" + domain,
        type: 'get',
        IQtype: 'getLastActivity',
        query: '',
        xmlns: 'jabber:iq:last'
    }, function (err, res) {
    });
}

function getPhonebookMap(a) {
    return phnbookMap[a];
}

function getrecentsMap(a) {
    return recentsMap[a];
}

function bind_chatpanel_again() {
    var list = $('.chats');
    var input = $('.emoji-wysiwyg-editor');
    initSlimScrollChats('#ulchatdemo');
    initSlimScrollContacts('#contacts');

    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i
            .test(navigator.userAgent)) {

    } else {
        showViewPortSize(true);
    }
    // Initializes and creates emoji set from sprite sheet
    window.emojiPicker = new EmojiPicker({
        emojiable_selector: '[data-emojiable=true]',
        assetsPath: '/webchat/js/emoji-picker/lib/img/',
        popupButtonClasses: 'fa fa-smile-o'
    });
    window.emojiPicker.discover();
    initChat();
    window.scrollTo = function (el, offeset) {
        var pos = (el && el.size() > 0) ? el.offset().top : 0;
        if (el) {
            if ($('body').hasClass('page-header-fixed')) {
                pos = pos - $('.page-header').height();
            }
            pos = pos + (offeset ? offeset : -1 * el.height());
        }

        $('html,body').animate({
            scrollTop: pos
        }, 'slow');
    };

    image_Upload();
    $('#OpenImageGallery').on("click", function () {
        $('#uploadImage').trigger('click');
    });
    $('#OpenFile_Upload').on("click", function () {
        $('#file_Upload').trigger('click');
    });
    $('#Open_gallery').on("click", function () {
        $('#image_Upload').trigger('click');
    });

    $('#Open_videogallery').on("click", function () {
        $('#video_Upload').trigger('click');
    });
    video_Upload();
    file_Upload();
}

/*function startChat(opponent_jid, tab, contact, selectMode) {
    lastMsgTime = '';
    isFirstMsg = true;
    $("#friendLastSeen").text(' ');
    if (selectMode == SELECT_MODE.CHAT) {
        document.getElementById("friendName").innerHTML = contact;
        $("#drop_option").html(
            '<li data-toggle="modal" data-target="#myModalprofile"><a href="#"> Contact info</a></li>' +
            // '<li><a href="#"> Select messages </a></li>' +
            // '<li><a href="#"> Mute </a></li>' +
            '<li data-target="#myModalClearChatConfrm" onclick="confirm_box()" data-toggle="modal"><a href="#"> Clear messages </a></li>' +
            '<li data-target="#myModalDeleteChatConfrm" onclick="confirm_box()" data-toggle="modal"><a href="#"> Delete chat </a></li>');
        $('#friendProfilePic, #friendprofileimg').attr('src', userPic);
        $("#friendProfilePic").attr('data-target', '#myModalprofile');
        getLastActivity(opponent_jid);
        $("#ulchatdemo").css("height", "auto");
        if ((checkBlockStatus(opponent_jid)) == BLOCK_STATUS.NO_BLOCK) {
            $("#block1").hide();
            $("#blocking_msg").show();
            $("#blocking_msg").html(UserChatPanel());
            bind_chatpanel_again();
        }
        else {
            $("#block1").show();
            $("#blocking_msg").hide();
            $("#block1").html("can't send message to blocked contact " + getPhonebookMap(opponent_jid).nick);
        }
    }
    else if (selectMode == SELECT_MODE.GROUP) {
        $("#drop_option").html(
            '<li data-toggle="modal" data-target="#myModalGroupProfile"><a href="#"> Group info</a></li>' +
            '<li><a href="#"> Mute </a></li>' +
            '<li data-target="#myModalClearChatConfrm" onclick="confirm_box()" data-toggle="modal"><a href="#"> Clear messages </a></li>' +
            '<li data-target="#myModalDeleteChatConfrm" onclick="confirm_box()" data-toggle="modal"><a href="#"> Delete chat </a></li>');

        $('#callIcon').attr('style', 'display:none');
        document.getElementById("friendName").innerHTML = getrecentsMap(opponent_jid).subject;
        $('#friendProfilePic, #friendprofileimg').attr('src', groupPic);
        $('#friendProfilePic').attr('data-target', '#myModalGroupProfile');
        $("#friendName").attr('title', contact);
        $("#blocking_msg").show();
        $("#block1").hide();
        $("#blocking_msg").html(groupChatPanel());
        $('#userCroppedPic').css({'display': 'none'});
        bind_chatpanel_again();
        $("#ulchatdemo").css("height", "auto");
    }

    //reset the unread counter to 0
    if (!jQuery.isEmptyObject(getrecentsMap(opponent_jid))) {
        getrecentsMap(opponent_jid).unreadMsg = 0;
    }
    if (!jQuery.isEmptyObject(getPhonebookMap(opponent_jid))) {
        getPhonebookMap(opponent_jid).unreadMsg = 0;
    }

    // getOldChats(opponent_jid);
    $('#default_chat_theme').hide();
    $('#user_chat_theme1').show();
    $('#' + this.opponent_jid + '_div').removeClass("selectedContact");
    $('#' + opponent_jid + '_div').addClass("selectedContact");

    if (tab == TAB_CONTACTS) {
        $('#' + this.opponent_jid + '_divContact').removeClass("selectedContact");
        $('#' + opponent_jid + '_divContact').addClass("selectedContact");
    }
    else {
        $('#' + this.opponent_jid + 'divRecentsUL').removeClass("selectedContact");
        $('#' + opponent_jid + 'divRecentsUL').addClass("selectedContact");
    }
    $(".chats").empty();
    $('#' + opponent_jid + '_cnt').html('');
    this.opponent_jid = opponent_jid;
    this.opponent_Type = selectMode;

    loadRecents();
}*/

function groupChatPanel() {
    return '<div class="message-type unselect">' +
        '<div class="plusIcon" onclick="plusIconClick()" id="plusIcon"><img src="/webchat/assets/images/icon/plus_icon.png" alt="" height="30px;" width="30px;"></div>' +
        '<div class="crossIcon" onclick="crossIconClick()" id="crossIcon"><img src="/webchat/assets/images/icon/cross_icon.png" alt="" height="30px;" width="30px;"></div>' +
        '<div class="input-cont lead emoji-picker-container" spellcheck="false">' +
        '<input class="form-control textBox lead emoji-picker-container" type="text" id="msgBox" data-emojiable="true" placeholder="Type a message here..." />' +
        '</div>' +
        '<div style="width:5%;float:left;" class="custom_gobtn">' +
        '<button type="button" id="key_board" class="btn btn-default btn-cont1 sendButton" style="display: none;"><img src="/webchat/assets/images/GO.png" class="go_icon"> </button>' +
        '<div class="microphone-buttons">' +
        '<img src="/webchat/assets/images/icon/voice_memo_icon.png" id="record" data-toggle="tooltip" title="Record" class="red-tooltip" onclick="switchButtons(\'record\')" alt="voice_memo" height="25px;" width="25px;">' +
        '<div class="recordDiv"><button  id="base64" onclick="switchButtons(\'base64\')"; style="display:none;" class="btn btn-default btn-cont"></button>' +
        '<div class="recordTimer displayIB"><div id="minute" class="recordMin displayIB">24</div><div class="displayIB">:</div><div id="second" class="recordSec displayIB">00</div></div>' +
        '<button  onclick="recordTerminate()"  class="btn btn-default btn-close"></button></div>' +
        '</div>' +
        '</div>' +
        '</div>' +

        '<div class="attachment_list collapse" id="demo">' +
        '<ul>' +
        '<li><img src="/webchat/assets/images/colored_icon/camera_icon.png" title="Camera" class="red-tooltip"  id="open_webcamera" alt="camara" onclick="openwebcamera()">' +
        '<li><img src="/webchat/assets/images/colored_icon/files_icon.png" data-toggle="tooltip" title="Document" class="red-tooltip"  id="OpenFile_Upload" alt="files" >' +
        '<input type="file" id="file_Upload" data-url="/chat/imageupload" style="display:none"/>' +
        '</li>' +
        '<li><img src="/webchat/assets/images/colored_icon/gallery_icon.png" data-toggle="tooltip" title="Gallery" class="red-tooltip" id="Open_gallery" alt="gallery" >' +
        '<input type="file" id="image_Upload" accept="image/*" data-url="/chat/imageupload" style="display:none"/>' +
        '</li>' +
        '<li><img src="/webchat/assets/images/colored_icon/video_icon.png" data-toggle="tooltip" title="Media" class="red-tooltip" id="Open_videogallery" alt="gallery" >' +
        '<input type="file" id="video_Upload" data-url="/chat/imageupload" style="display:none" accept="video/*"/>' +
        '</li>' +
        '<li><a href="#myModalcontacts" data-toggle="modal"><img src="/webchat/assets/images/colored_icon/contact_icon.png" data-toggle="tooltip" title="Contact" class="red-tooltip"  alt="contact" alt="contact" onClick="share_contact_list()"></a></li>' +
        // '<li><a href="#myModalocation" data-toggle="modal"><img src="/webchat/assets/images/colored_icon/location_icon.png" data-toggle="tooltip"  title="Location" class="red-tooltip"  alt="location" onClick="getMapView()"></a>' +
        // '</li>' +
        '<!-- <li><img src="/webchat/assets/images/colored_icon/doodle_icon.png" data-toggle="tooltip"  title="Doodle" class="red-tooltip" alt="doodle"></li>-->' +
        /*'<li id="callIcon" style="display:none;"><img src="/webchat/assets/images/colored_icon/call_icon.png"  title="Audio/Video call" class="red-tooltip"  alt="video" data-toggle="modal" data-target="#selectCalldiv"></li>' +*/
        '<li data-toggle="modal" data-target="#mystickers"><img src="/webchat/assets/images/colored_icon/stickers_icon.png" data-toggle="tooltip"  title="Sticker" class="red-tooltip" alt="stickers" onclick="showSticker();"></li>' +
        '<!-- <li><img src="/webchat/assets/images/colored_icon/location_icon.png" alt="location" onClick="getUserCurrentLocation()"></li>' +
        '<!--  <li><img src="/webchat/assets/images/colored_icon/contact_icon.png" alt="contact"  onclick="handleClickContactUpload()"></li>-->' +
        '</ul>' +
        '</div>' +
        '</div>';
}
function UserChatPanel() {
    return '<div class="message-type unselect">' +
        '<div class="plusIcon" onclick="plusIconClick()" id="plusIcon"><img src="/webchat/assets/images/icon/plus_icon.png" alt="" height="30px;" width="30px;"></div>' +
        '<div class="crossIcon" onclick="crossIconClick()" id="crossIcon"><img src="/webchat/assets/images/icon/cross_icon.png" alt="" height="30px;" width="30px;"></div>' +
        '<div class="input-cont lead emoji-picker-container">' +
        '<input class="form-control textBox lead emoji-picker-container" type="text" id="msgBox" data-emojiable="true" placeholder="Type a message here..." />' +
        '</div>' +
        '<div style="width:5%;float:left;" class="custom_gobtn">' +
        '<button type="button" id="key_board" class="btn btn-default btn-cont1 sendButton" style="display: none;"><img src="/webchat/assets/images/GO.png" class="go_icon"> </button>' +
        '<div class="microphone-buttons">' +
        '<img src="/webchat/assets/images/icon/voice_memo_icon.png" id="record" data-toggle="tooltip" title="Record" class="red-tooltip" onclick="switchButtons(\'record\')" alt="voice_memo" height="25px;" width="25px;">' +
        '<div class="recordDiv"><button  id="base64" onclick="switchButtons(\'base64\')"; style="display:none;" class="btn btn-default btn-cont"></button>' +
        '<div id="record_timer" class="recordTimer displayIB"><div id="minute" class="recordMin displayIB">00</div><div class="displayIB">:</div><div id="second" class="recordSec displayIB">00</div></div>' +
        '<button  onclick="recordTerminate()"  class="btn btn-default btn-close"></button></div>' +
        '</div>' +
        '</div>' +
        '</div>' +

        '<div class="attachment_list collapse unselect" id="demo">' +
        '<ul>' +
        '<li><img src="/webchat/assets/images/colored_icon/camera_icon.png" title="Camera" class="red-tooltip"  id="open_webcamera" alt="camara" onclick="openwebcamera()" data-toggle="modal" data-target="#capturepopup">' +
        '<li><img src="/webchat/assets/images/colored_icon/files_icon.png" data-toggle="tooltip" title="Document" class="red-tooltip"  id="OpenFile_Upload" alt="files" >' +
        '<input type="file" id="file_Upload" data-url="/chat/imageupload" style="display:none"/>' +
        '</li>' +
        '<li><img src="/webchat/assets/images/colored_icon/gallery_icon.png" data-toggle="tooltip" title="Gallery" class="red-tooltip" id="Open_gallery" alt="gallery" >' +
        '<input type="file" id="image_Upload" accept="image/*" data-url="/chat/imageupload" style="display:none" />' +
        '</li>' +
        '<li><img src="/webchat/assets/images/colored_icon/video_icon.png" data-toggle="tooltip" title="Media" class="red-tooltip" id="Open_videogallery" alt="gallery" >' +
        '<input type="file" id="video_Upload" data-url="/chat/imageupload" style="display:none" accept="video/*"/>' +
        '</li>' +
        '<li><a href="#myModalcontacts" data-toggle="modal"><img src="/webchat/assets/images/colored_icon/contact_icon.png" data-toggle="tooltip" title="Contact" class="red-tooltip"  alt="contact" alt="contact" onClick="share_contact_list()"></a></li>' +
        // '<li><a href="#myModalocation" data-toggle="modal"><img src="/webchat/assets/images/colored_icon/location_icon.png" data-toggle="tooltip"  title="Location" class="red-tooltip"  alt="location" onClick="getMapView()"></a>' +
        // '</li>' +
        '<!-- <li><img src="/webchat/assets/images/colored_icon/doodle_icon.png" data-toggle="tooltip"  title="Doodle" class="red-tooltip" alt="doodle"></li>-->' +
        /*'<li id="callIcon" style="display:inline-block;"><img src="/webchat/assets/images/colored_icon/call_icon.png"  title="Audio/Video call" class="red-tooltip"  alt="video" data-toggle="modal" data-target="#selectCalldiv"></li>' +*/
        '<li data-toggle="modal" data-target="#mystickers"><img src="/webchat/assets/images/colored_icon/stickers_icon.png" data-toggle="tooltip"  title="Sticker" class="red-tooltip" alt="stickers" onclick="showSticker();"></li>' +
        '<!-- <li><img src="/webchat/assets/images/colored_icon/location_icon.png" alt="location" onClick="getUserCurrentLocation()"></li>' +
        '<!--  <li><img src="/webchat/assets/images/colored_icon/contact_icon.png" alt="contact"  onclick="handleClickContactUpload()"></li>-->' +
        '</ul>' +
        '</div>' +
        '</div>';
}

function BottomMsg(){
    if (document.getElementById('ulchatdemo').scrollTop > 50 || document.getElementById('chats').scrollTop > 50) {
        $('#bottom-msg').html(
            '<div id ="bottom-msg" class="bottom-msgs" >'+
            '<span class="bottom-arrow"></span>'+
            '</div>'
        );
    }
}
// Add smooth scrolling to all links
$("#bottom-msg").on('click', function(event) {
    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
        // Prevent default anchor click behavior
        event.preventDefault();
        // Store hash
        var hash = this.hash;
        // Using jQuery's animate() method to add smooth page scroll
        // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
        $('#ulchatdemo').animate({
            scrollTop: $(hash).offset().top
        }, 500, function(){
            // Add hash (#) to URL when done scrolling (default click behavior)
            window.location.hash = hash;
        });
    } // End if
});


function getTplClass(msgType) {
    var receipt = '';
    switch (msgType) {
        case MSG_TYPE.text:
            receipt = '<div id="' + message.messageId + '_msg" class="message">';
            break;
        case MSG_TYPE.sticker:
            receipt = '<div class="message message-sticker" style="background:none !important;box-shadow:none !important; color:#333;">';
            break;
        case MSG_TYPE.contact:
            // receipt = 'background_orangeCall dot_set';
            break;
        case MSG_TYPE.image:
            // receipt = 'background_greenCall dot_set';
            break;
        default:
            break;
    }
    return receipt;
}

function getFriendProfile() {
    $('#grpSubject').empty();
    $('#grpMember').empty();
    if(getPhonebookMap(chatWithContact.userId).selectMode == SELECT_MODE.CHAT || getPhonebookMap(chatWithContact.userId).selectMode == SELECT_MODE.HIDDEN) {
        if(getPhonebookMap(chatWithContact.userId).contactType == 'Friend') {
            document.getElementById("frndphone").innerHTML = getPhonebookMap(chatWithContact.userId).ciaoId;
            document.getElementById("PhoneOrId").innerHTML = 'CiaoId';
        }
        else{
            document.getElementById("frndphone").innerHTML = getPhonebookMap(chatWithContact.userId).phone;
            document.getElementById("PhoneOrId").innerHTML = 'Phone';
        }
        document.getElementById("frndstatus").innerHTML = getPhonebookMap(chatWithContact.userId).status;
    }
    else{
        showGroupMembers(chatWithContact.userId);
    }
}

function getMyProfile() {
    set_profile_pic(userDetail.userId, PROFILE_TYPE.CHATWITHMY);
    // $('.basic-result').css({'display':'none'});
    // $('.croppie-container ').css({'display':'block'});
    // $('#userpic ').css({'display':'block'});
    // $('#userCroppedPic').css({'display':'none'});


    // $('.cr-boundary').css({'display':'none'});
    // $('.cr-slider-wrap').css({'display':'none'});
    //
    // $('#userCroppedPic').css({'display':'none'});
    // $('#userCroppedPic').attr('src','');

    // $('#userpicContainer').html('<img class="img-circle hover_set" id="userpic" src="" alt="" height="175px" width="175px"><img class="img-circle hover_set" id="userCroppedPic" src="http://images.ciaoim.com/profile/'+userName+'.png" alt="" style="display: none;">');
    // $('.basic-result').css({'display':'none'});
    // $('.zoomout').css({'display':'none'});
    // $('.zoomin').css({'display':'none'});
    // $('#userpic').attr('src',avtarUrl+'profile/'+userName+'.png')
    // $('.zoomout').css({'display': 'none'});
    // $('.zoomin').css({'display': 'none'});
    // $("#myModaluser").hide();
    /*  $(".basic-result").hide();
     // $(".cr-slider-wrap").hide();
     // $(".cr-boundary").hide();
     $("#userpic").show();
     $("#userCroppedPic").hide();
     $("#userpicContainer").show();
     $(".croppie-container").hide();*/
    client.sendIq({
        type: 'get',
        opcode: 'get_ciao_vcard',
        xmlns: 'urn:xmpp:ciaoim1:2',
        user: userName
    }, function (err, res) {
    });
}

/*function myViewProfile(userId) {
    $.each(phnbookMap, function (key, val) {
        if (val.userId == userId) {
            $("#userpicView").attr('data-target', '');
            $('#grpPicView').attr('src', groupPic);
        }
    });
}*/
function viewMyProfile() {
    set_profile_pic(userDetail.userId, PROFILE_TYPE.MYPROFILEPIC);
    set_profile_pic(userDetail.userId, PROFILE_TYPE.MYPROFILEPICS);
    $('#userName1').text(decodeBase64( userDetail.userNickName));
    $('#view-image').show();
    var profileHeightDiv=$(window).height()-$(".viewProfileHeader").height();
    $("#viewProfileDiv").css("height",profileHeightDiv);
    var profileHeightDivMargin=($("#viewProfileDiv").height()-$("#myprofileIDS").height())/2;
    if(profileHeightDivMargin>0) {
        $("#myprofileIDS").css("margin-top", profileHeightDivMargin);
    }
    else{
        $("#myprofileIDS").css("margin-top", 0);
    }
}
function viewFrndProfile() {
    set_profile_pic(chatWithContact.userId, PROFILE_TYPE.CONTFRNDPIC);
    set_profile_pic(chatWithContact.userId, PROFILE_TYPE.CONTFRNDPICS);
    $('#userNameFrnd').text(getPhonebookMap(chatWithContact.userId).name);
    $('#view-image-frnd').show();
    var friendProfileHeightDiv=$(window).height()-$("#friendProfileView").height();
    $("#friendProfileViewDiv").css("height",friendProfileHeightDiv);
    var friendProfileHeightDivMargin=($("#friendProfileViewDiv").height()-$("#myprofileFrndIDS").height())/2;
    if(friendProfileHeightDivMargin>0){
        $("#myprofileFrndIDS").css("margin-top",friendProfileHeightDivMargin);
    }
    else {
        $("#myprofileFrndIDS").css("margin-top", 0);
    }
}
function myViewProfileGrp() {
    $('#groupname').text(getPhonebookMap(chatWithContact.userId).name);
    $('#viewimagegrp').show();
    set_profile_pic(chatWithContact.userId, PROFILE_TYPE.GROUPVIEW);
    set_profile_pic(chatWithContact.userId, PROFILE_TYPE.GROUPVIEWS);
    var profileHeightDiv=$(window).height()-$("#grpProfileView").height();
    $("#viewGrpProfileDiv").css("height",profileHeightDiv);
    var profileHeightDivMargin=($("#viewGrpProfileDiv").height()-$("#groupProfile").height())/2;
    if(profileHeightDivMargin>0) {
        $("#groupProfile").css("margin-top", profileHeightDivMargin);
    }
    else{
        $("#groupProfile").css("margin-top", 0);
    }
}
function viewImageCloseFrnd() {
    $('#view-image-frnd').hide();
}
function viewImageClose() {
    $('#view-image').hide();
}
function viewImageCloseGroup() {
    $('#viewimagegrp').hide();
}
// opening file upload window on attachment icon click
$('#OpenImageGallery').on("click", function () {
    $('#uploadImage').trigger('click');
});
$('#OpenFile_Upload').on("click", function () {
    $('#file_Upload').trigger('click');
});
$('#Open_gallery').on("click", function () {
    $('#image_Upload').trigger('click');
});
$('#Open_videogallery').on("click", function () {
    $('#video_Upload').trigger('click');
});

$('#profilepicBtn').on("click", function () {
    $('#profilepicUpload').trigger('click');
});
$('#GrprofilepicBtn').on("click", function () {
    $('#GrprofilepicUpload').trigger('click');
});
$('#profilepicgroup').on("click", function () {
    $('#profilepicUploadgroup').trigger('click');

});

$(window).load(function () {
    // Check if ColorBox is available
    var list = $('.chats');
    var input = $('.emoji-wysiwyg-editor');
    initSlimScrollChats('#ulchatdemo');
    initSlimScrollContacts('#contacts');

    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i
            .test(navigator.userAgent)) {

    } else {
        showViewPortSize(true);
    }
    // Initializes and creates emoji set from sprite sheet
    window.emojiPicker = new EmojiPicker({
        emojiable_selector: '[data-emojiable=true]',
        assetsPath: '/webchat/js/emoji-picker/lib/img/',
        popupButtonClasses: 'fa fa-smile-o'
    });
    window.emojiPicker.discover();
    initChat();
    window.scrollTo = function (el, offeset) {
        var pos = (el && el.size() > 0) ? el.offset().top : 0;

        if (el) {
            if ($('body').hasClass('page-header-fixed')) {
                pos = pos - $('.page-header').height();
            }
            pos = pos + (offeset ? offeset : -1 * el.height());
        }

        $('html,body').animate({
            scrollTop: pos
        }, 'slow');
    };
}());

function initSlimScrollChats(el) {
    $(el).each(function () {
        if ($(this).attr("data-initialized")) {
            return; // exit
        }
        var height;
        if ($(this).attr("data-height")) {
            height = $(this).attr("data-height");
        } else {
            height = $(this).css('height');
        }
        $(this).slimScroll({
            allowPageScroll: true, // allow page scroll when the element scroll is ended
            size: '7px',
            color: ($(this).attr("data-handle-color") ? $(this).attr("data-handle-color") : '#bbb'),
            wrapperClass: ($(this).attr("data-wrapper-class") ? $(this).attr("data-wrapper-class") : 'slimScrollDiv'),
            railColor: ($(this).attr("data-rail-color") ? $(this).attr("data-rail-color") : '#eaeaea'),
            position: 'right',
            height: height,
            alwaysVisible: ($(this).attr("data-always-visible") == "1" ? true : false),
            railVisible: ($(this).attr("data-rail-visible") == "1" ? true : false),
            disableFadeOut: true,
            start: 'bottom'
        });
        $(this).attr("data-initialized", "1");
    });
}

function initSlimScrollContacts(el) {
    $(el).each(function () {
        if ($(this).attr("data-initialized")) {
            return; // exit
        }
        var height;
        if ($(this).attr("data-height")) {
            height = $(this).attr("data-height");
        } else {
            height = $(this).css('height');
        }
        $(this).slimScroll({
            allowPageScroll: true, // allow page scroll when the element scroll is ended
            size: '7px',
            color: ($(this).attr("data-handle-color") ? $(this).attr("data-handle-color") : '#bbb'),
            wrapperClass: ($(this).attr("data-wrapper-class") ? $(this).attr("data-wrapper-class") : 'slimScrollDiv'),
            railColor: ($(this).attr("data-rail-color") ? $(this).attr("data-rail-color") : '#eaeaea'),
            position: 'right',
            height: height,
            alwaysVisible: ($(this).attr("data-always-visible") == "1" ? true : false),
            railVisible: ($(this).attr("data-rail-visible") == "1" ? true : false),
            disableFadeOut: true,
        });
        $(this).attr("data-initialized", "1");
    });
}

function initChat() {
    $('.body').on('click', '.message .name .emoji-wysiwyg-editor', function (e) {
        e.preventDefault(); // prevent click event
        var name = $(this).text(); // get clicked user's full name
        input.html('@' + name + ':'); // set it into the input field
        Metronic.scrollTo(input); // scroll to input if needed
    });

    $('.emoji-wysiwyg-editor').attr('spellcheck', 'false');
    $(".microphone-buttons").show();
    var btn = $('.sendButton');
    btn.click(handleClick);

    //for go button
    $('.emoji-wysiwyg-editor').keydown(function (e) {
        $('#OpenFileUpload').show();

        if ((e.keyCode != 86) && (e.keyCode != 8) && (e.keyCode != 9) && (e.keyCode != 27) && (e.keyCode != 112) && (e.keyCode != 113) && (e.keyCode != 114) && (e.keyCode != 115) && (e.keyCode != 116) && (e.keyCode != 117) && (e.keyCode != 118) && (e.keyCode != 119) && (e.keyCode != 120) && (e.keyCode != 121) && (e.keyCode != 122) && (e.keyCode != 123) && (e.keyCode != 124) && (e.keyCode != 125) && (e.keyCode != 126) && (e.keyCode != 46) && (e.keyCode != 16) && (e.keyCode != 17) && (e.keyCode != 18) && (e.keyCode != 20) && (e.keyCode != 93) && (e.keyCode != 33) && (e.keyCode != 34) && (e.keyCode != 35) && (e.keyCode != 36) && (e.keyCode != 144))
            wordCount++;
        if ((e.keyCode == 8) && (wordCount != 0))
            wordCount--;

        if (wordCount == 0) {
            $("#record").show();
            $("#key_board").hide();
        }
        if ((wordCount != 0)) {
            $("#record").hide();
            $("#key_board").show();
        }

        $('.attachment-container').hide('slow');

        // Enter pressed?
        if (e.keyCode == 13 && !e.shiftKey) {
            e.preventDefault();
            $('.sendButton').focus();
            $('.sendButton').trigger('click');
            // handleClick(e);
            $(".microphone-buttons").show();
            $("#record").show();
            $(".sendButton").hide();
            $('.emoji-wysiwyg-editor').focus();
            return false;
        }
    });

    $('.emoji-wysiwyg-editor').bind("paste", function (e) {
        // access the clipboard using the api
        var pastedDatalength = e.originalEvent.clipboardData.getData('text').length;
        wordCount = wordCount + pastedDatalength;
    });

    // activating the left panel list on click
    $("li.media").on("click", function () {
        $("li.media").removeClass("active-list");
        $(this).addClass("active-list");
    });
    // activating the left panel contact & recent button
    $("#recent_tab button").on("click", function () {
        $("button").removeClass("active");
        $(this).addClass("active");
    });
    $("#btnContact").on("click",
        function () {
            $("#contactText").text("CiaoIM doesn't have access to your contacts");
        });
    $("#btnRecnt").on("click",
        function () {
            $("#contactText").text("Tap the contacts tab to start a new chat");
        });
}

function showViewPortSize(display) {
    if (display) {
        var height = window.innerHeight;
        var width = window.innerWidth;
        jQuery(window).resize(
            function () {
                height = window.innerHeight;
                width = window.innerWidth;
                jQuery('#viewportsize').html('Height: ' + height + '<br>Width: ' + width);
            });
        var hh = $(".navbar-fixed-top").outerHeight();
        var pt = $(".portlet-title").outerHeight();
        var nhd = parseInt(hh) + parseInt(pt);
        var nh = parseInt(height) - parseInt(nhd);
        // contacts
        var search_btn = $(".search_btn").outerHeight();
        var contact_tab = $("#contact_tab").outerHeight();
        var tempall = parseInt(hh) + parseInt(search_btn) + parseInt(contact_tab);
        var temp2 = parseInt(height) - parseInt(tempall) - 28;
    }
}

function handleClick(e) {
    e.preventDefault();
    var $input = $('.emoji-wysiwyg-editor');
    // var text='';
    // var text1 = $input.html($.trim($input.html())).text().trim();

    // if(text2.replace(/\s/g, '').length){
    //     console.log('if block');
    //     text=text2;
    // }else if(text1!=0){
    //     // text=text1;
    //     console.log('else block');
    //     text=$input.html();
    //     text = text.replace(/^\s*<br\s*\/?>|<br\s*\/?>\s*$/g,'');
    //     text = text.replace(/^\s*<br\s*\/?>|<br\s*\/?>\s*$/g,'');
    //
    // }
    // var text=$input.html();
    $("#chat_load").hide();
    if (e.type == 'click') {
        text1 = document.getElementById('msgBox').value;//approute's code (use value method only to get value out of msgbox)
        text2 = $('.emoji-wysiwyg-editor').html();//vipin sir code (do not use html() method as it is not giving emoji in server understandable format.)
        if (text1.trim() == '') {
            // $(".emoji-wysiwyg-editor").html("");
            // document.getElementById('msgBox').vaolue='';
        } else {
            var msgId = $.now();
            var emoji = new EmojiConvertor();
            var eText = encodeEmoji(text1);
            text1 = text1.replace(/<br ?\/?>/g, "\n");
            send_message(text1, msgId, 'text');
            // if(send_message(text.replace(/<br ?\/?>/g, "\n"), msgId,'text')){
            add_to_view(get_text_tpl(text1, msgId, msgId, '', MESSAGE_STATE.SENT, chatWithContact.selectMode), TIMETYPE.CURRENT);
            $input.html("");
            $(".emoji-wysiwyg-editor").text("");
            $(".emoji-wysiwyg-editor").html("");
            document.getElementById('msgBox').value = "";
            text2 = '';
            text1 = '';
            wordCount = 0;
            // }
            var scroll_to_chatHeight = $('#ulchatdemo').prop('scrollHeight') + 'px';
            $("#ulchatdemo").slimScroll({
                scrollTo: scroll_to_chatHeight,
                start: 'bottom'
            });
        }
    }
    else {
        //when shift+enter pressed
        $('.emoji-wysiwyg-editor').focus();
        if (text2 == '') {
            //when shift+enter pressed first time
            text2 = document.getElementById('msgBox').value;
        }
        else {
            // console.log('handleClick text2 shift+enter0 ' + text2+' msgbox '+document.getElementById('msgBox').value);
            // text2+='<br/>';
            // console.log('handleClick text2 shift+enter00 ' + text2+' msgbox '+document.getElementById('msgBox').value);
            // text2=document.getElementById('msgBox').value;
            // console.log('handleClick text2 shift+enter000 ' + text2+' msgbox '+document.getElementById('msgBox').value);
            // // text2 = document.getElementById('msgBox').value;
            //
            // document.getElementById('msgBox').value='';
            // document.getElementById('msgBox').value=text2;
            // // text2 +='<br/>'+document.getElementById('msgBox').value;
            // console.log('handleClick text2 shift+enter1 ' + text2+' 2 '+document.getElementById('msgBox').value);
        }
    }
}

function showHasMoreButton(page){
    var btnTpl = '';
    btnTpl+='<div class="notification_bubble" id = "ldmoreBtn">';
    btnTpl+='<div class="loader-bubble-btn">';
    btnTpl+="<div onclick='loadMoreMsgs1(\""+page+"\")' class='notification_body'>";
    btnTpl+='<button class="loader-btn">load more</button>';
    btnTpl+='</span>';
    btnTpl+='</div>';
    btnTpl+='</div><div class="clearfix"></div>';
    btnTpl+='</div>';
    return btnTpl;
}


function encodeEmoji(text) {
    var emoji = new EmojiConvertor();
    var eText=emoji.replace_unified(text);
    emoji.text_mode = true;
    emoji.include_title = true;
    emoji.img_path = "/webchat/js/js-emoji-master/build/emoji-data/img-apple-64/";
    return eText;
}
