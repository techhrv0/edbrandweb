var isLoadingChats = false;
function onStartChat(chatWithId) {
    console.log('onStartChat chatWithId '+chatWithId)
    // setDayNotification();
    $("#chat_load").hide();
    var scroll_to_chatHeight = $('#ulchatdemo').prop('scrollHeight') + 'px';
    $("#ulchatdemo").slimScroll({
        scrollTo: scroll_to_chatHeight,
        start: 'bottom'
    });
   /* try {
        if (isLoadingChats) return;
    } catch (e) {
    }*/
    isLoadingChats = true;
    console.log("isLoadingChats now: "+isLoadingChats);
    setSelection(chatWithContact.userId, chatWithId);
    chatWithContact = getPhonebookMap(chatWithId);

    resetChatView();
    prepareChatView();

    $('#default_chat_theme').hide();
    $('#user_chat_theme1').show();

    // loadMoreMsgs(1);

    $(chatWithId).on('click', function(event) {
        if (this.hash !== "") {
            event.preventDefault();
            var hash = this.hash;
            $('#chats').animate({
                scrollTop: $(hash).offset().top
            }, 200, function(){
                window.location.hash = hash;
            });
        }
    });

}

function resetChatView() {
    lastMsgTime = '';
    isFirstMsg = true;
    $("#friendLastSeen").text(' ');
    $(".chats").empty();
    $('#' + chatWithContact.userId + '_cnt').html('');

    if (!jQuery.isEmptyObject(getrecentsMap(chatWithContact.userId))) {
        getrecentsMap(chatWithContact.userId).unreadMsg = 0;
    }
    if (!jQuery.isEmptyObject(getPhonebookMap(chatWithContact.userId))) {
        getPhonebookMap(chatWithContact.userId).unreadMsg = 0;
    }
}

function prepareChatView() {
    if (chatWithContact.selectMode == SELECT_MODE.CHAT || chatWithContact.selectMode == SELECT_MODE.HIDDEN) {
        document.getElementById("friendName").innerHTML = chatWithContact.name;
        $("#drop_option").html(
            '<li data-toggle="modal" data-target="#myModalprofile" onclick="getFriendProfile()"><a href="#"> Contact info</a></li>' +
            // '<li><a href="#"> Select messages </a></li>' +
            // '<li><a href="#"> Mute </a></li>' +
            '<li data-target="#myModalClearChatConfrm" onclick="confirm_box()" data-toggle="modal"><a href="#"> Clear messages </a></li>' +
            '<li data-target="#myModalDeleteChatConfrm" onclick="confirm_box()" data-toggle="modal"><a href="#"> Delete chat </a></li>');
        //set_profile_pic(chatWithContact.userId, PROFILE_TYPE.CHATWITH);
       // set_profile_pic(chatWithContact.userId, PROFILE_TYPE.CHATWITHI);
        $("#friendProfilePic").attr('data-target', '#myModalprofile');
        $("#friendProfilePic").attr('src', '/webchat/assets/images/profile.jpg');
        $("#ulchatdemo").css("height", "auto");
        if ((checkBlockStatus(chatWithContact.userId) == BLOCK_STATUS.NO_BLOCK)) {
            // getLastActivity(chatWithContact.userId);
            makeBlockUIchanges(chatWithContact.userId,'block');
        }
        else if((checkBlockStatus(chatWithContact.userId) == BLOCK_STATUS.BLOCKED_BY_OTHER)){
            makeBlockUIchanges(chatWithContact.userId,'block');
        }
        else if((checkBlockStatus(chatWithContact.userId) == BLOCK_STATUS.BLOCKED_BY_ME)||(checkBlockStatus(chatWithContact.userId) == BLOCK_STATUS.BLOCKED_BY_BOTH)){
            makeBlockUIchanges(chatWithContact.userId,'unblock');
        }
    }
    else if (chatWithContact.selectMode == SELECT_MODE.GROUP) {
        $("#drop_option").html(
            '<li data-toggle="modal" data-target="#myModalGroupProfile" onclick="getFriendProfile()"><a href="#"> Group info</a></li>' +
            '<li data-target="#myModalClearChatConfrm" onclick="confirm_box()" data-toggle="modal"><a href="#"> Clear messages </a></li>' +
            '<li data-target="#myModalDeleteChatConfrm" onclick="confirm_box()" data-toggle="modal"><a href="#"> Delete chat </a></li>');

        $('#callIcon').attr('style', 'display:none');
        document.getElementById("friendName").innerHTML = getrecentsMap(chatWithContact.userId).name;
        set_profile_pic(chatWithContact.userId, PROFILE_TYPE.CHATWITH);
        set_profile_pic(chatWithContact.userId, PROFILE_TYPE.CHATWITHGROUP);
        $('#friendProfilePic').attr('data-target', '#myModalGroupProfile');
        $("#friendName").attr('title', getrecentsMap(chatWithContact.userId).name);
        $("#friendLastSeen").text(getGroupMembers(getPhonebookMap(chatWithContact.userId).groupMembers).replace(/,\s*$/, ''));
        if (isMember()) {
            $('#leaveGroup').prop('disabled', false);
            $("#block1").hide();
            $("#blocking_msg").show();
            $("#blocking_msg").html(groupChatPanel());
        }
        else {
            $('#leaveGroup').prop('disabled', true);
            $("#block1").show();
            $("#blocking_msg").hide();
            $("#block1").html("You can't send messages to this group because you are no longer a participant");
        }
        $('#userCroppedPic').css({'display': 'none'});
        bind_chatpanel_again();
        $("#ulchatdemo").css("height", "auto");
    }

}

function setSelection(oldUserId, newUserId) {
    $('#' + oldUserId + '_div').removeClass("selectedContact");
    $('#' + newUserId + '_div').addClass("selectedContact");

    if (currentTab == TAB_CONTACTS) {
        $('#' + oldUserId + '_divContact').removeClass("selectedContact");
        $('#' + newUserId + '_divContact').addClass("selectedContact");
    } else {
        $('#' + oldUserId + '_divRecents').removeClass("selectedContact");
        $('#' + newUserId + '_divRecents').addClass("selectedContact");
    }
}

