var domain = 'eventapchat.approutes.com';
var userPic='/webchat/assets/images/user.png';
var groupPic='/webchat/assets/images/group_pic.png';
var url = 'ws://'+domain+':5280/websocket';

var MUC_SERVICE = "muc." + domain;
var MUC_OWNER = 'http://jabber.org/protocol/muc#owner';
var MUC_USER = 'http://jabber.org/protocol/muc#user';

var jid;
var MUC_ROOM='conference.'+domain;
var chatWithContact = {};
var recents=[];
var currentChatUserName;
var currentChatUser;
var broadcast_user = [];
var composing = 0;
var TAB_CONTACTS = "contacts";
var TAB_RECENTS = "recent";

var MESSAGE_TYPE_IMAGE = '1';
var MESSAGE_TYPE_VIDEO = '2';
var MESSAGE_TYPE_AUDIO = '4';
var MESSAGE_TYPE_PDF = '5';
var MESSAGE_TYPE_DOC = '7';
var MESSAGE_TYPE_MAP='11';
var MESSAGE_TYPE_VOICE='13';

var FILE_TYPE_ERROR ="File not supported";
var FILE_SIZE_ERROR = "File size is too large";
var currentTab="";
var isUserBusy=false;
var tpl='';
var userId,phone,nick,status;
var isBroadcast = false;
var phnbookMap = {};
var sharedContactMap=[];
var recentsMap = {};
var groupMember=[];
var newGrpMember=[];
var sessionQueryId='';
var currentSession= '<0.29607.1>';
var isConnected = false;
var isAuthenticated = false;
var getGroupSub=false;
var groupJid='';
var isNoLastMsg=false;
var text2='';
var media;
var menuToggle,showBtn,hideBtn,showBtn1 ;
var lastMsgTime='';
var isFirstMsg=true;
var showMsg=false;
var stopimgAjax=null;
var memberIDtoChange='';

var userDetail={
    userId:userName,
    userStatus:'',
    userNickName:userNickName,
    phone:'',
    avatarHash:''
};
var isNewGroup=false;
var wordCount=0;

var timeCount = null;
var totalSeconds = 0;

var AppConstant= {
    RESOURCE: 'web'
};

var TIMETYPE={
    OLD:'old',
    CURRENT:'current'
}

var SYNC = {
    WEB_CLIENT: 'webclient',
    DEVICE_CLIENT: 'deviceClient',
    CONTACT: 'syncContact',
    RECENTS: 'recents',
    LOAD_CHATS: 'loadchat',
    PING: 'ping',
    CLEAR_CHAT: 'clearChat',
    UPDATE_VCARD: 'updateVcard',
    GROUP_CHAT:'groupChat',
    BLOCK:'blockStatus'
};
var MESSAGE_STATE = {
    //sender
    TO_SEND: 'TO_SEND',
    SENT: 'SENT',
    DELIVERED: 'DELIVERED',
    READ_BY_RECEIVER: 'READ_BY_RECEIVER',
    //receiver
    RECEIVED: 'RECEIVED',
    DELIVERY_SENT: 'DELIVERY_SENT',
    DISPLAYED: 'DISPLAYED',
    READ: 'READ',
    //other
    INVALID: 'INVALID'
};

var SELECT_MODE = {
    HIDDEN:'HIDDEN',
    CHAT:'CHAT',
    SECRET:'SECRET',
    GROUP:'GROUP',
    BROADCAST:'BROADCAST',
    AUDIO_CALL:'AUDIO_CALL',
    VIDEO_CALL:'VIDEO_CALL',
};
var BLOCK_STATUS = {
    NO_BLOCK:'NO_BLOCK',
    BLOCKED_BY_ME:'BLOCKED_BY_ME',
    BLOCKED_BY_OTHER:'BLOCKED_BY_OTHER',
    BLOCKED_BY_BOTH:'BLOCKED_BY_BOTH'
};

var MESSAGE = {
    messageId:'',
    userId: '',
    fromUserId: '',
    messageBody:'',
    messageCategory:'',
    messageState:'',
    messageType:'',
    sentReceivedTs:'',
    fileName: '',
    fileSize: '',
    thumbNail: ''
};

var MSG_CATEGORY ={
    single: 'single',
    groupchat: 'groupchat',
    broadcast: 'broadcast',
    secret: 'secret',
    signal: 'signal'
}

var MSG_TYPE = {
    text: 'text',
    location:'location',
    sticker:'sticker',
    stickerGif:'stickerGif',
    contact:'contact',
    image:'image',
    video:'video',
    audio:'audio',
    voice: 'voice',
    file:'file',
    pdf:'pdf',
    doc:'doc',
    theme:'theme',
    timer:'timer',
    call:'call',
    deleted:'deleted',
    broadcast_create:'broadcast_create',
    group_create:'group_create',
    group_add:'group_add',
    group_join:'group_join',
    group_leave:'group_leave',
    group_remove:'group_remove',
    subject_change:'subject_change',
    pic_change:'pic_change',
    grant_admin:'grant_admin',
    grant_owner:'grant_owner',
    invalid:'invalid',
    webclient:'webclient',
    unknown:'unknown'
}
var s3Map = {}

var PROFILE_TYPE = {
    CONTACT: 'contact',
    RECENT: 'recent',
    ME: 'me',
    CHATWITH: 'chatwith',
    CHATWITHI: 'chatwithi',
    CHATWITHMY: 'chatwithmy',
    CHATWITHGROUP: 'chatwithgroup',
    MYPROFILEPIC: 'myprofilepic',
    MYPROFILEPICS: 'myprofilepics',
    CONTFRNDPIC: 'contactfriendpic',
    CONTFRNDPICS: 'contactfriendpics',
    DISPLAYGRPMEMBERS: 'displaygrpmembers',
    VOICERECORD: 'record11',
    VOICERECORD_DEVICE: 'recordpic_device',
    GROUPVIEW: 'groupview',
    GROUPVIEWS: 'groupviews',
    CONTACTSHARING: 'contactsharing',
    MEMBERADDTOLIST: 'memberaddtolist',
    SHAREDCONTACT: 'sharedcontact'
}

var DIR = {
    PROFILE: 'profile',
    IMAGE: 'captured',
    VIDEOS: 'videos',
    VOICE: 'voice',
    NOTES: 'notes',
    OTHERS: 'file'
}

var pingTs = [];
var pingInterval;
var pingCheckInterval;
var TIME_OUT_INTERVAL = 1000*10;


var CONNCECTION_STATE = {
    CONNECTIING: 'connecting',
    CONNECTED: 'connected',
    DISCONNECTED: 'disconnecetd'
}
var imageData={
    base64:'',
    size:''
};

var OPCODE ={
    createGroup:'createGroup',
    addMember:'addMember',
    removeMember:'removeMember',
    left:'leave',
    makeAdmin:'makeAdmin',
    profilePicChange:'profilePicChange',
    changeSubject:'changeSubject'
}

var contactSynced = false;
var recentsSynced = false;
var RETRY_INTERVAL = 15;
var PING_INTERVAL = 1000*15;
var PING_CHECK_INTERVAL = 1000*3;

var CARBON ={
    DIRECTION_SENT: "sent",
    DIRECTION_RECEIVE: "receive"
}

 document.getElementById('myprofileID').onerror = function () {
     document.getElementById('myprofileID').src = "/webchat/assets/images/user.png";
 };
 document.getElementById('myprofileIDS').onerror = function () {
     document.getElementById('myprofileIDS').src = "/webchat/assets/images/user.png";
 };
 document.getElementById('myprofileFrndID').onerror = function () {
     document.getElementById('myprofileFrndID').src = "/webchat/assets/images/user.png";
 };
 document.getElementById('myprofileFrndIDS').onerror = function () {
     document.getElementById('myprofileFrndIDS').src = "/webchat/assets/images/user.png";
 };
 document.getElementById('grpProfile').onerror = function () {
     document.getElementById('grpProfile').src = "/webchat/assets/images/group_pic.png";
 };
 document.getElementById('groupProfile').onerror = function () {
     document.getElementById('groupProfile').src = "/webchat/assets/images/group_pic.png";
 };
document.getElementById('friendProfileContact').onerror = function () {
    document.getElementById('friendProfileContact').src = "/webchat/assets/images/user.png";
};
document.getElementById('friendProfileContactB').onerror = function () {
    document.getElementById('friendProfileContactB').src = "/webchat/assets/images/user.png";
};
document.getElementById('userpic').onerror = function () {
    document.getElementById('userpic').src = "/webchat/assets/images/user.png";
};
document.getElementById('grpuserpic').onerror = function () {
    document.getElementById('grpuserpic').src = "/webchat/assets/images/group_pic.png";
};
document.getElementById('friendProfilePic').onerror = function () {
    $('#friendProfilePic').removeClass('profile_pic');
    $('#friendProfilePic').addClass('profile_defaultpic');
    if (chatWithContact.selectMode == SELECT_MODE.GROUP)
        document.getElementById('friendProfilePic').src = "/webchat/assets/images/group_pic.png";
    else
        document.getElementById('friendProfilePic').src = "/webchat/assets/images/profile.jpg";
};
document.getElementById('friendprofileimg').onerror = function () {
    $('#friendprofileimg').removeClass('profile_pic_set');
    $('#friendprofileimg').addClass('profile_pic_set_default');
    if (chatWithContact.selectMode == SELECT_MODE.GROUP)
        document.getElementById('friendprofileimg').src = "/webchat/assets/images/group_pic.png";
    else
        document.getElementById('friendprofileimg').src = "/webchat/assets/images/profile.jpg";
};

$(document).ready(function () {
    $('[data-toggle="modal"]').tooltip();

    $('#btn_zoomin').click(
        function (event) {
            //alert('buttomclicked');
            var sliderval = $(".cr-slider").val();
            if (parseFloat(sliderval) >= 0.3788 && parseFloat(sliderval) <= 1.5) {

                var newValue = parseFloat(sliderval) + 0.050;
                $('.cr-slider').val(newValue);
                $('.cr-slider').trigger('change');
            }
        });
    $('#btn_zoomout').click(
        function (event) {
            $('#userpic').width($('#pic').width() / 1.2);
            $('#userpic').height($('#pic').height() / 1.2)
        }
    );

    $('#btn_zoomin_group').click(
        function (event) {
            $('#userpic').width($('#pic').width() * 1.2);
            $('#userpic').height($('#pic').height() * 1.2)
        }
    );
    $('#btn_zoomout_group').click(
        function (event) {
            $('#userpic').width($('#pic').width() / 1.2);
            $('#userpic').height($('#pic').height() / 1.2)
        }
    );
    $('[data-toggle="tooltip"]').tooltip();

    setInterval(alertFunc, 100);
});

function alertFunc() {
    //this is for emoji icon
    $(".emoji-menu").css("transform", "");
    if ($('.emoji-menu').is(":visible")) {
        var emojiheight = $(window).height() - $("#blocking_msg").height() - $(".emoji-menu").height();
        $(".emoji-menu").css("top", emojiheight);
        $(".emoji-menu").css("width", $("#user_chat_theme1").css("width"));
    }
    if ($("#ulchatdemo").height() > $(window).height() - $(".page-header").height() - $(".portlet-title").height() - $("#blocking_msg").height()) {
        var heightslim = $(window).height() - $(".page-header").height() - $(".portlet-title").height() - $("#blocking_msg").height();
        $("#chats").find(".slimScrollDiv").css("height", heightslim);
        $("#ulchatdemo").css("height", $(".chatbody-main").find(".slimScrollDiv").height());
    }
    else {
        var heightslim = $(window).height() - $(".page-header").height() - $(".portlet-title").height() - $("#blocking_msg").height();
        $("#chats").find(".slimScrollDiv").css("height", heightslim);
        //$("#ulchatdemo").css("height", auto);
        var ulchatheight = $("#chats").find(".slimScrollDiv").height() - $("#ulchatdemo").height() - 20;
        $("#ulchatdemo").css("top", ulchatheight);
    }
}

$(window).resize(function () {
    /* for left block*/
 /*   var chatulheight = $(window).height() - $(".page-header").height() - $(".form-group").height() - $("#contact_tab").height();
    $("#contactBodys").css("height", chatulheight);
    $("#contactBodys").find(".slimScrollDiv").css("height", chatulheight);
    $("#contacts").css("height", chatulheight);

    /!* for right block*!/
    var heightset = $(window).height() - $(".page-header").height();
    $(".no_chat_wrapper").css("height", heightset);
    $(".chatbody-main").css("height", "auto");
    $("#chats .slimScrollDiv").css("height", heightset);*/

    setInterval(alertFunc1, 100);
    function alertFunc1() {
        if ($("#ulchatdemo").height() > $(window).height() - $(".page-header").height() - $(".portlet-title").height() - $("#blocking_msg").height()) {
            var heightslim = $(window).height() - $(".page-header").height() - $(".portlet-title").height() - $("#blocking_msg").height();
            $("#chats").find(".slimScrollDiv").css("height", heightslim);
            $("#ulchatdemo").css("height", $(".chatbody-main").find(".slimScrollDiv").height());
        }
        else {
            var heightslim = $(window).height() - $(".page-header").height() - $(".portlet-title").height() - $("#blocking_msg").height();
            $("#chats").find(".slimScrollDiv").css("height", heightslim);
            $("#ulchatdemo").css("height", heightslim);
            var ulchatheight = $("#chats").find(".slimScrollDiv").height() - $("#ulchatdemo").height() - 20;
            $("#ulchatdemo").css("top", ulchatheight);
        }
    }
    adjustRecentWith();
    notificationContent();
});

//zoom library
(function () {
    var $section = $('#userpic');//$('#contain');
    var $panzoom = $section.find('.panzoom').panzoom({
        $zoomIn: $section.find(".zoom-in"),
        $zoomOut: $section.find(".zoom-out"),
        $zoomRange: $section.find(".zoom-range"),
        $reset: $section.find(".reset"),
        startTransform: 'scale(0.9)',
        maxScale: 0.9,
        increment: 0.1,
        contain: true
    }).panzoom('zoom', true);
})();
