function processMessageIn($message) {

    var body = $($message).find("body").text();
    if(jQuery.isEmptyObject(body) || body =='ignore') return;

    var webcorban = $($message).find("webcarbon").text()
    if(webcorban == undefined || webcorban == null || webcorban == '') return;

    var receiver=$($message).find("receiver").text();
    var direction = $($message).find("direction").text();
    var toId = getUserPart($message.attr('to'));
    var messageId = $message.attr('id');
    var bodytype = $($message).find("bodyType").text();
    var nick = decodeBase64($message.find('nick').text());
    var type = $message.attr('type');
    var thumb = $($message).find("thumb").text();
    var fileName = $($message).find("fileName").text();
    var fileSize = $($message).find("fileSize").text();
    var sendTimestamp = $($message).find("delay").attr('stamp');
    var dateOfMsg = sendTimestamp.substring(0, sendTimestamp.indexOf('T'));
    var selectMod = '';
    var userId= getUserPart($message.attr('from'));
    var fromUserId;
    // switch (type){
    //     case 'chat':
    //         selectMod = SELECT_MODE.CHAT;
    //         userId = receiver;
    //         break;
    //     case 'groupchat':
    //         selectMod = SELECT_MODE.GROUP;
    //         userId= receiver.split('/')[0];
    //         fromUserId=receiver.split('/')[1];
    //         break;
    //     default:
    //         break;
    // }
    // maybe phone sent
    // if (!jQuery.isEmptyObject(getPhonebookMap(userId))) {
    //     if (getPhonebookMap(userId).selectMode == SELECT_MODE.CHAT || getPhonebookMap(userId).selectMode == SELECT_MODE.HIDDEN) {
    //         nick = getPhonebookMap(userId).name;
    //     }
    // }
    console.log('processMessageIn '+isAlreadyInRecents(userId)+' chatwith '+chatWithContact.userId+'userid ' +userId);
    if (!isAlreadyInRecents(userId)) {
        addNewRecent(userId, fromUserId , body, nick, bodytype, messageId, sendTimestamp, direction, 0, selectMod);
    }
    if (chatWithContact.userId == userId) {
        console.log('processMessageIn1');
        var tpl = createChatBubble(copyToObject(messageId, userId, fromUserId, body, selectMod, direction, bodytype, sendTimestamp, fileName, fileSize, thumb),TIMETYPE.CURRENT);
        add_to_view(tpl, TIMETYPE.CURRENT);
        if(isSent(direction)){
            set_profile_pic(userName, PROFILE_TYPE.VOICERECORD_DEVICE);
        }
        else {
            set_profile_pic(userId, PROFILE_TYPE.VOICERECORD_DEVICE);
        }
        mayBeMedia(messageId, bodytype, fileName);
        if(userId != userName){
            sendReadReceipt(messageId);
        }
        updateRecentMapMessage(userId, messageId, body, bodytype, direction);
    }
    else {
        console.log('processMessageIn2');
        if (!jQuery.isEmptyObject(getrecentsMap(userId))) {
            // if (!isSent(direction))
            getrecentsMap(userId).unreadMsg++;
            updateRecentMapMessage(userId, messageId, body, bodytype, direction);
            increment_count(userId);
        }
        //sendDeliveryReceipt(messageId);
    }
    /*else {
        console.log('processMessageIn2');
            if (!jQuery.isEmptyObject(getrecentsMap(userId))) {
                // if (!isSent(direction))
                    getrecentsMap(userId).unreadMsg++;
                updateRecentMapMessage(userId, messageId, body, bodytype, direction);
            }
        if (!isSent(direction)) {
            increment_count(userId);
        }
        //sendDeliveryReceipt(messageId);
    }*/
    $('#' + userId + '_divRecentsUL').prependTo("#contactsView");
    updateRecentsMessage(userId, body, bodytype, messageId, sendTimestamp, direction,fileName);
}

function copyToObject(messageId, userId, fromUserId, body, selectMode, direction, bodytype,sendTimestamp,  fileName, fileSize, thumb) {
    var messageDto = {
        messageId:  messageId,
        userId: userId,
        fromUserId: fromUserId,
        messageBody:body,
        selectMode:selectMode,
        messageState:direction,
        messageType:bodytype,
        sentReceivedTs:sendTimestamp,
        fileName: fileName,
        fileSize: fileSize,
        thumbNail: thumb,
    };
    return messageDto;
}

function updateRecentMapMessage(userId, msgId, body, msgType, chatState) {
    var recent = getrecentsMap(userId);
    recent.body = body;
    recent.msgType = msgType;
    recent.msgId = msgId;
    recent.chatState = chatState;
}

function getFriendId(fromId,toId) {
    if(userName==fromId){
        return toId;
    }
    return fromId;
}

function onMessageStateChange(msgId, msgState) {
    console.log('onMessageStateChange '+msgId+ msgState);
    updateMsgStateMAP(msgId, msgState);
    switch (msgState) {
        case MESSAGE_STATE.SENT:
            $('#' + msgId + '_dot').removeClass('fa fa-clock-o clock_status');
            $('#' + msgId + '_dotRecent').removeClass('fa fa-clock-o clock_status_set');
            $('#' + msgId + '_dot').addClass('dot background_gray');
            $('#' + msgId + '_dotRecent').addClass('background_grayCall dot_set');
            $('#' + msgId + '_fwdmsg').html("<img src='/webchat/assets/images/fwdicon.png' class='forward_icon' id='fwdicon'  onclick='fwdpopup(\"" + msgId + "\")'  data-toggle='modal' data-target='#myModalForwardImage' aria-hidden='true'/>");
            break;
        case MESSAGE_STATE.RECEIVED:
            $('#' + msgId + '_dot').removeClass('fa fa-clock-o clock_status');
            $('#' + msgId + '_dotRecent').removeClass('fa fa-clock-o clock_status_set');

            $('#' + msgId + '_dot').removeClass('dot background_gray');
            $('#' + msgId + '_dotRecent').removeClass('background_grayCall dot_set');

            $('#' + msgId + '_dot').addClass('dot background_orange');
            $('#' + msgId + '_dotRecent').addClass('background_orangeCall dot_set');

            break;
        case MESSAGE_STATE.READ:

            $('#' + msgId + '_dot').removeClass('fa fa-clock-o clock_status');
            $('#' + msgId + '_dotRecent').removeClass('fa fa-clock-o clock_status_set');

            $('#' + msgId + '_dot').removeClass('dot background_gray');
            $('#' + msgId + '_dotRecent').removeClass('background_grayCall dot_set');

            $('#' + msgId + '_dot').removeClass('dot background_orange');
            $('#' + msgId + '_dotRecent').removeClass('background_orangeCall dot_set');

            $('#' + msgId + '_dot').addClass('dot background_green');
            $('#' + msgId + '_dotRecent').addClass('background_greenCall dot_set');
            break;
        default:
            break;
    }
}

function updateMsgStateMAP(msgId, chatState) {
    console.log('updateMsgStateMAP0 '+$('#' + msgId + '_dotRecent').attr('data-userID')+currentTab);
    try{
        if(currentTab==TAB_RECENTS){
            var userId= $('#' + msgId + '_dotRecent').attr('data-userID');
            console.log('updateMsgStateMAP '+msgId+ chatState+userId+getrecentsMap(userId).chatState);
            getrecentsMap(userId).chatState= chatState;
            console.log('updateMsgStateMAP1 '+msgId+ chatState+userId+getrecentsMap(userId).chatState);
        }
        else{
            $.each(recentsMap, function (key, val) {
                if(recentsMap[key].msgId==msgId){
                    console.log('updateMsgStateMAP '+msgId+ chatState+recentsMap[key].userId+getrecentsMap(recentsMap[key].userId).chatState);
                    getrecentsMap(recentsMap[key].userId).chatState= chatState;
                    console.log('updateMsgStateMAP1 '+msgId+ chatState+recentsMap[key].userId+getrecentsMap(recentsMap[key].userId).chatState);
                }
            });
        }
    }
    catch (e){}
    
}


