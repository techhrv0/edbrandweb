function get_profile_key(userId) {
    return 'profile/' + userId + '.png';
}

function get_s3_key(messageId, messageType, ext) {
    var key = '';
    switch (messageType) {
        case MSG_TYPE.image:
            key = DIR.IMAGE;
            break;
        case MSG_TYPE.video:
            key = DIR.VIDEOS;
            break;
        case MSG_TYPE.audio:
            key = DIR.VOICE;
            break;
        case MSG_TYPE.file:
            key = DIR.OTHERS;
            break;
        default:
            break;
    }
    return key + "/" + messageId + "." + ext;
}

function getExt(fileName) {
    var dotIndex = fileName.lastIndexOf(".");
    return fileName.substring((dotIndex + 1), fileName.length);
}

function s3MapContains(s3Key) {
    if (!jQuery.isEmptyObject(s3Map[s3Key])) {
        return true;
    }
    return false;
}

function set_profile_pic(userId, profileType) {
    try {
        $("#chat_load").show();
        $("#load_new").hide();
        // var data = s3Map[get_profile_key(userId)];
        // if (!jQuery.isEmptyObject(data)) {
        //     var downloadUrl = makeUrl(data);
        //     $(get_profile_element(userId, profileType)).attr('src',downloadUrl);
        //     return;
        // }
        // get_signed_url(get_profile_key(userId));
    } catch (ignore) {

    }
}

function get_profile_element(userId, profileType) {
    var elementId='';
    switch (profileType) {
        case PROFILE_TYPE.CONTACT:
            elementId = '#'+userId + "_avtar_contact";
            break;
        case PROFILE_TYPE.DISPLAYGRPMEMBERS:
            elementId = '.'+userId + "_avtar_contact_share";
            break;
        case PROFILE_TYPE.MEMBERADDTOLIST:
            elementId = '.'+userId + "_avtar_member_list";
            break;
        case PROFILE_TYPE.CONTACTSHARING:
            elementId = '.'+userId + "_avtar_contact_sharing";
            break;
        case PROFILE_TYPE.SHAREDCONTACT:
            elementId = '.'+userId + "_sharedContactPic";
            break;
        case PROFILE_TYPE.RECENT:
            elementId = '#'+userId + "_avtar_recent";
            break;
        case PROFILE_TYPE.ME:
            elementId = '#'+userId + "_avtar";
            break;
        case PROFILE_TYPE.CHATWITH:
            elementId = "#friendProfilePic";
            break;
        case PROFILE_TYPE.CHATWITHI:
            elementId = "#friendprofileimg";
            break;
        case PROFILE_TYPE.CHATWITHMY:
            elementId = "#userpic";
            break;
        case PROFILE_TYPE.CHATWITHGROUP:
            elementId = "#grpuserpic";
            break;
        case PROFILE_TYPE.MYPROFILEPIC:
            elementId = "#myprofileID";
            break;
        case PROFILE_TYPE.MYPROFILEPICS:
            elementId = "#myprofileIDS";
            break;
        case PROFILE_TYPE.CONTFRNDPIC:
            elementId = "#myprofileFrndID";
            break;
        case PROFILE_TYPE.CONTFRNDPICS:
            elementId = "#myprofileFrndIDS";
            break;
        case PROFILE_TYPE.VOICERECORD:
            elementId = '#'+userId + "_record11";
            break;
        case PROFILE_TYPE.VOICERECORD_DEVICE:
            elementId = '.'+userId + "_recordpic_device";
            break;
        case PROFILE_TYPE.GROUPVIEW:
            elementId= "#grpProfile";
            break;
        case PROFILE_TYPE.GROUPVIEWS:
            elementId= "#groupProfile";
            break;
        default:
            break;
    }
    return elementId;
}

function makeUrl(data) {
    return data.url + "?token=" + data.token;
}
