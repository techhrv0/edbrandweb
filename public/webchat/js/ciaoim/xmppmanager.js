$(window).load(function () {
    console.log("Starting stanza again.. ");
    xmppInit();
    // setInterval(function () {xmppService();}, 10000);
    client.on('connected', function (message) {
        console.log("connection, connected");
        onConnect();
    });

    client.on('disconnected', function (message) {
        console.log("1");
        //onDisconnect();
    });

    client.on('session:end', function () {
        console.log("2");
        //onDisconnect();
    });

    client.on('session:started', function () {
        console.log("connection, session:started");
        onAuthenticated();
    });

    client.on('chat', function (message) {
        //console.log("got chat message " + JSON.stringify(message, null, '  '));
    });

    client.on('raw:incoming', function (xml) {
        var $xmlObj = $.parseXML(xml);
        // if (($xmlObj.getElementsByTagName('message').length > 0)&&($($xmlObj).find("bodyType").text()!='ping'))
        console.log("RECV: "+xml);

    if(!isLoadingChats){
        $("#chat_load").hide();
    }



        //handle rating response
        if(($xmlObj.getElementsByTagName('iq').length > 0)&&($xmlObj.getElementsByTagName('http').length > 0)) {
            //TODO handleclintmap for visitors
            var visitor = $($xmlObj).find("http").text();
            visitor = visitor.replace(/&quot;/g, "'");
            var visitorObj = JSON.parse(visitor);
            console.log('opcode '+visitorObj.opcode+JSON.stringify(visitorObj));
            if($($xmlObj).find("http").attr('code') == 200){
                if(visitorObj.opcode=='set_rating'){
                    // handleRatingResponse(visitorObj);
                }
                else if(visitorObj.message=='client drop status updated'){
                    console.log('client ended chat');
                    // handleDropCall('client');
                }
                else if(visitorObj.status=='success'){
                    // handleSignInResponse(visitorObj);
                }
                else if(visitorObj.opcode=='get_agent_list'){
                    handleAgentList(visitorObj.data);
                    handleSyncResponse(SYNC.CONTACT);
                    handleSyncResponse(SYNC.RECENTS);
                }
                else if(visitorObj.opcode=='get_vcard'){
                    // handleGetVcardResponse(visitorObj.data);
                }

                return;
            }
            else if($($xmlObj).find("http").attr('code') == 400){
                alert(visitorObj.message);
                return;
            }

        }


        //handle get vcard
        if (($($xmlObj).find("query").attr('xmlns') == 'urn:xmpp:ciaoim1:2') && ($xmlObj.getElementsByTagName('http').length > 0)) {
            var vcard = $($xmlObj).find("http").text();
            vcard = vcard.replace(/&apos;/g, "'");
            var vcardObj = JSON.parse(vcard);
            onVcardFetched(vcardObj);
            return;
        }

        if (($xmlObj.getElementsByTagName('vCard').length > 0)) {
            onVcardFetched($xmlObj);
            return;
        }

        //session id xml
        if (($xmlObj.getElementsByTagName('iq').length > 0) && ($($xmlObj).find("http").attr('code') == '200') && ($($xmlObj).find("iq").attr('id') == sessionQueryId)) {
            var sessionId = $($xmlObj).find("http").text();
            sessionId = sessionId.replace(/&lt;/g, '<');
            sessionId = sessionId.replace(/&gt;/g, '>');
            sessionId = sessionId.replace(/&apos;/g, "'");
            var sessionOb = JSON.parse(sessionId);
            currentSession = '';
            currentSession = sessionOb.pid;
            return;
        }

        //handle presence from other users and show online/last seen accordingly
        if ($xmlObj.getElementsByTagName('presence').length > 0) {
            if ($($xmlObj).find("presence").attr('type') == 'available') {
                if (split_jid($($xmlObj).find("presence").attr('from')) == chatWithContact.userId)
                    $("#friendLastSeen").html('<b style=color:green; font-weight: bold>online</b>');
            }

            if ($($xmlObj).find("presence").attr('type') == 'unavailable') {
                if ((split_jid($($xmlObj).find("presence").attr('from')) == chatWithContact.userId) && (chatWithContact.selectMode == SELECT_MODE.CHAT))
                    getLastActivity(chatWithContact.userId);
            }
            return;
        }

        //handle display connecting/connected/Last Active time on UI
        if ($xmlObj.getElementsByTagName('resumed').length > 0) {
            setAvailability(true);
            $("#friendLastSeen").text(' ');
            if (chatWithContact.userId)//get last acivity of selected user when connection is resumed,if no user is selected do not send iq
            {
                if (chatWithContact.selectMode == SELECT_MODE.CHAT)
                    getLastActivity(chatWithContact.userId);
                else if(chatWithContact.selectMode == SELECT_MODE.GROUP)
                    $("#friendLastSeen").text(getGroupMembers(getPhonebookMap(chatWithContact.userId).groupMembers).replace(/,\s*$/, ''));
                else
                    $("#friendLastSeen").text('');
            }
            return;
        }


        //handle sent carbons
        // if ($($xmlObj).find("sent").attr('xmlns') == 'urn:xmpp:carbons:2') {
        //     var $carbonsMessage = $($xmlObj).find("forwarded");
        //     var $message = $($carbonsMessage).find("message");
        //     var msgCategory = $message.find('msgCategory').text();
        //     if (msgCategory == SYNC.DEVICE_CLIENT || msgCategory == SYNC.WEB_CLIENT) {
        //         return;
        //     }
        //     processMessageIn($message);
        //     return;
        // }

        //handle received carbons
        // if ($($xmlObj).find("received").attr('xmlns') == 'urn:xmpp:carbons:2') {
        //     var $carbonsMessage = $($xmlObj).find("forwarded");
        //     var $message = $($carbonsMessage).find("message");
        //     var msgCategory = $message.find('msgCategory').text();
        //     if (msgCategory == SYNC.DEVICE_CLIENT || msgCategory == SYNC.WEB_CLIENT) {
        //         return;
        //     }
        //
        //     //Check for receipt
        //     if(messageReceipt($carbonsMessage)) return;
        //     processMessageIn($message);
        //     return;
        // }

        //Check for receipt
        if(messageReceipt($xmlObj)) return;

        //parse message tag chat XML
        if ($xmlObj.getElementsByTagName('message').length > 0) {//($($xmlObj).find("message") != undefined) {//&&(!readStatus($xmlObj))
            console.log('message rec')
            var bodyType = $($xmlObj).find('bodyType').text();
            if ($($xmlObj).find("message").attr('type') == 'chat' || $($xmlObj).find("message").attr('type') == 'groupchat' || $($xmlObj).find("message").attr('type') == 'normal') {
               console.log('message rec1');
                var $message = $($xmlObj).find("message");
                if ($message != null) {
                    $("#chat_load").hide();
                }
                var messageId = $message.attr("id");
                var bodyType = $message.find('bodyType').text();
                var body = $message.find('body').text();
                var msgCategory = $message.find('msgCategory').text();
                if (msgCategory == SYNC.DEVICE_CLIENT) {
                    handleSyncResponse(messageId, bodyType, body);
                    return;
                }
                if (body == null || body == '') return;
                processMessageIn($message);
            }
            else if (bodyType == 'block') {
                console.log('block ' + xml);
                var body = split_jid($($xmlObj).find('body').text());
                try {
                    var blockStatus = getPhonebookMap(body).blockStatus;
                    if (blockStatus == BLOCK_STATUS.BLOCKED_BY_ME) {
                        getPhonebookMap(body).blockStatus = BLOCK_STATUS.BLOCKED_BY_BOTH;
                        makeBlockUIchanges(body, 'unblock');
                    }
                    else {
                        getPhonebookMap(body).blockStatus = BLOCK_STATUS.BLOCKED_BY_OTHER;
                    }
                }
                catch (e) {
                }
                return;
            }
            else if (bodyType == 'unblock') {
                console.log('unblock ' + xml);
                var body = $($xmlObj).find('body').text();
                try {
                    var blockStatus = getPhonebookMap(body).blockStatus;
                    if (blockStatus == BLOCK_STATUS.BLOCKED_BY_BOTH) {
                        getPhonebookMap(body).blockStatus = BLOCK_STATUS.BLOCKED_BY_ME;
                        makeBlockUIchanges(chatWithContact.userId, 'unblock');
                    }
                    else if (blockStatus == BLOCK_STATUS.BLOCKED_BY_OTHER) {
                        getPhonebookMap(body).blockStatus = BLOCK_STATUS.NO_BLOCK;
                    }
                }
                catch (e) {
                }
                return;
            }
        }

            //Handle Last Activity
            if (($($xmlObj).find("query").attr('xmlns') == 'jabber:iq:last') && (chatWithContact.userId) && (chatWithContact.selectMode == SELECT_MODE.CHAT || chatWithContact.selectMode == SELECT_MODE.HIDDEN)) {
                // var seconds=$($xmlObj).find("query").attr('seconds')
                if (($($xmlObj).find("query").attr('seconds') == 0) || ($($xmlObj).find("query").text() == 'Replaced by new connection')) {
                    $("#friendLastSeen").html('<b style=color:green;font-weight: bold>online</b>');
                }
                else {
                    updateLastAcitvity($($xmlObj).find("query").attr('seconds'), split_jid($($xmlObj).find("iq").attr('from')));
                }
                return;
            }

            // <iq from='14860wijiel@ciaolarge.ciaoim.com' to='14860wijiel@ciaolarge.ciaoim.com/CiaoIM' id='V4SR1-139' type='result'>
            //     <query xmlns='urn:xmpp:s3:1'>
            //    <url>{"urlkey":"http://52.209.17.139:8000/aki/detail/Bj2vQqXKYVgyb6Qry0LGxD0omLW3JpMl/","status":"true"}</url><token>eutp4y2oydcbxieqckle</token></query></iq>

            if ($($xmlObj).find("query").attr('xmlns') == 'urn:xmpp:s3:1') {
                var s3Key = $($xmlObj).find("s3Key").text();
                var token = $($xmlObj).find("token").text();
                var data = $($xmlObj).find("url").text();
                var response = JSON.parse(data);
                var urlkey = response.urlkey;
                var status = response.status;
                s3_response(s3Key, urlkey, token, status);
                return;
            }
    });
    client.on('raw:outgoing', function (xml) {
        // var $xmlObj = $.parseXML(xml);
        // if (($xmlObj.getElementsByTagName('message').length > 0)&&($($xmlObj).find("bodyType").text()!='ping'))
        console.log('SENT: ' + xml);
    });
});


function xmppInit() {
    console.log('user: '+userName + "@" + domain);
    console.log('password '+sid);
    client = XMPP.createClient({
        jid: userName + "@" + domain,
        password: sid,
        resource: AppConstant.RESOURCE,
        wsURL:url,
        transports: ['websocket']
    });
    $('.pw').text('');
    $('.pw').remove();
    //doConnect();
}

function doConnect() {
    onWebStatusUpdate(CONNCECTION_STATE.CONNECTIING);
    client.connect();
}

function onConnect() {
    console.log("hello");
    isConnected = true;
    unBlockOnConnect();
    //setAvailability(true);
    onWebStatusUpdate(CONNCECTION_STATE.CONNECTED);
}

function onAuthenticated() {
    isAuthenticated = true;
    //client.enableCarbons();
    //client.enableKeepAlive();
    //setAvailability(true);
    // pingInterval = setInterval(function (){pingToDevice()}, PING_INTERVAL);
    // pingCheckInterval = setInterval(function (){pingCheck()}, PING_CHECK_INTERVAL);
    // get_signed_url(get_profile_key(userName));

    //send init notification
    // sendToDevice({}, SYNC.CONTACT);
    $('#indexHide').hide();
    $('#indexShow').show();
    //set_tab_onLoad(currentTab);
    // getAgentList();
    loadContacts();
    handleSyncResponse(SYNC.CONTACT);
    handleSyncResponse(SYNC.RECENTS);
    // getMyProfile();

    // getSessionId();
}
function getAgentList() {
    client.sendIq({
        to: 'widget.approutes.com',
        type: 'get',
        opcode : 'get_agent_list',
        xmlns: 'urn:xmpp:webwidget1:2',
        page: '0',
    }, function (err, res) {
    });
}

function onDisconnect() {
    isConnected = false;
    isAuthenticated = false;
    blockOndisconnect();
    onWebStatusUpdate(CONNCECTION_STATE.DISCONNECTED);
    clearInterval(pingInterval);
    clearInterval(pingCheckInterval);
    $("#friendLastSeen").text('Connecting...');
}

function xmppService() {
    if(!isAuthenticated){
        doConnect();
    }
}

function setAvailability(status) {
    var params= '';
    if(status){
        params = {type: 'available'};
    }else{
        params = {type: 'unavailable'};
    }
    client.sendPresence(params, function (err, res) {});
}

function logout() {
    setAvailability(false);
    client.sendIq({
        type: 'get',
        opcode: 'close_session',
        xmlns: 'urn:xmpp:s3:1',
    }, function (err, res) {
    });
    client.disconnect();
    window.location.href = "/application/index/logout";
}

function send_message(message, msgId, msgType) {
    $("#record").show();
    $(".microphone-buttons").show();
    $(".sendButton").hide();
    $('#' + chatWithContact.userId + '_divRecentsUL').prependTo("#contactsView");
    if (chatWithContact.selectMode == SELECT_MODE.CHAT || chatWithContact.selectMode == SELECT_MODE.HIDDEN)
        var msgCategory = 'single';
    if (chatWithContact.selectMode == SELECT_MODE.GROUP)
        var msgCategory = 'group';
    if (chatWithContact.selectMode == SELECT_MODE.BROADCAST)
        var msgCategory = 'broadcast';
    if (this.isBroadcast) {
        for (index = 0; index < broadcast_user.length; index++) {
            chatWithContact.userId = broadcast_user[index].userId;
            send(message, msgId, msgType, msgCategory);
        }
    } else {
        send(message, msgId, msgType, msgCategory);
    }
    return true;

}
function send(message, msgId, msgType, msgCategory) {
    console.log('send '+message+ msgId+ msgType+ msgCategory +chatWithContact.userId)
    var toJID = '';
    var type = '';
    var fromUserId='';
    var selectMode='';
    if (chatWithContact.selectMode == SELECT_MODE.GROUP) {
        toJID = MUC_ROOM;
        type = 'groupchat';
        fromUserId=userName;
        selectMode=SELECT_MODE.GROUP;
    }
    else {
        toJID = domain;
        type = 'chat';
        fromUserId=chatWithContact.userId;
        selectMode=SELECT_MODE.CHAT;
    }
    if (msgType == MSG_TYPE.text) {
        client.sendMessage({
            to: chatWithContact.userId + "@" + toJID,
            body: message,
            request: '',
            nick: userNickName,
            id: msgId,
            type: type,
            bodyType:msgType,
            msgCategory:msgCategory,
            phone:phone
        });

        updateRecentMap(chatWithContact.userId,fromUserId, message, msgId, MSG_TYPE.text, 'out', 'new',MESSAGE_STATE.TO_SEND);
        // updatLastMessage(chatWithContact.userId, message, 'text', msgId, 'out', 'current', msgId);
        setLastMessage(chatWithContact.userId,fromUserId, message, MSG_TYPE.text, msgId, msgId, MESSAGE_STATE.TO_SEND,selectMode);
    }
    else if (msgType == MSG_TYPE.sticker) {
        client.sendMessage({
            to: chatWithContact.userId + "@" + toJID,
            body: message,
            request: '',
            id: msgId,
            nick: userNickName,
            type: type,
            bodyType:msgType,
            msgCategory:msgCategory,
            phone:phone
        });
        updateRecentMap(chatWithContact.userId,fromUserId, message, msgId, MSG_TYPE.sticker, 'out', 'new',MESSAGE_STATE.TO_SEND);
        setLastMessage(chatWithContact.userId,fromUserId, message, MSG_TYPE.sticker, msgId, msgId, MESSAGE_STATE.TO_SEND,selectMode);
        // updatLastMessage(chatWithContact.userId, '[sticker]', 'sticker', msgId, 'out', 'current');
    }
    else if (msgType == MSG_TYPE.stickerGif) {
        client.sendMessage({
            to: chatWithContact.userId + "@" + toJID,
            body: message,
            request: '',
            id: msgId,
            nick: userNickName,
            type: type,
            bodyType:msgType,
            msgCategory:msgCategory,
            phone:phone
        });
        updateRecentMap(chatWithContact.userId,fromUserId, message, msgId, MSG_TYPE.stickerGif, 'out', 'new',MESSAGE_STATE.TO_SEND);
        setLastMessage(chatWithContact.userId,fromUserId, message, MSG_TYPE.stickerGif, msgId, msgId, MESSAGE_STATE.TO_SEND,selectMode);
        // updatLastMessage(chatWithContact.userId, '[stickerGif]', 'stickerGif', msgId, 'out', 'current');
    }
    else if (msgType == MSG_TYPE.contact) {
        client.sendMessage({
            to: chatWithContact.userId + "@" + toJID,
            nick: userNickName,
            body: message,
            request: '',
            id: msgId,
            type: type,
            phone:phone,
            bodyType:msgType,
            msgCategory:msgCategory
        });
        updateRecentMap(chatWithContact.userId,fromUserId, message, msgId, MSG_TYPE.contact, 'out', 'new',MESSAGE_STATE.TO_SEND);
        setLastMessage(chatWithContact.userId,fromUserId, message, MSG_TYPE.contact, msgId, msgId, MESSAGE_STATE.TO_SEND,selectMode);
        // updatLastMessage(chatWithContact.userId, '[contact]', 'contact', msgId, 'out', 'current');
    }

    else if (msgType == MSG_TYPE.location) {
        client.sendMessage({
            to: chatWithContact.userId + "@" + toJID,
            body: message,
            nick: userNickName,
            request: '',
            id: msgId,
            type: type,
            msgCategory:msgCategory,
            phone:phone,
            bodyType:msgType
        });
        updateRecentMap(chatWithContact.userId,fromUserId, message, msgId, MSG_TYPE.location, 'out', 'new',MESSAGE_STATE.TO_SEND);
        setLastMessage(chatWithContact.userId,fromUserId, message, MSG_TYPE.location, msgId, msgId, MESSAGE_STATE.TO_SEND,selectMode);
        // updatLastMessage(chatWithContact.userId, '[Location]', 'location', msgId, 'out', 'current');
    }
    else if (msgType == MSG_TYPE.image){
        client.sendMessage({
            to: chatWithContact.userId + "@" + toJID,
            //from: userName + "@" + domain+"/web",
            body: msgId.toString(),
            nick: userNickName,
            request: '',
            id: msgId,
            type: type,
            bodyType:msgType,
            fileSize:imageData.size,
            fileName:message,
            thumb:splitBase64(imageData.base64),
            msgCategory:msgCategory,
            phone:phone,
        });
        imageData.base64='';
        imageData.size='';
        updateRecentMap(chatWithContact.userId,fromUserId, message, msgId, MSG_TYPE.image, 'out', 'new',MESSAGE_STATE.TO_SEND);
        setLastMessage(chatWithContact.userId,fromUserId, message, MSG_TYPE.image, msgId, msgId, MESSAGE_STATE.TO_SEND,selectMode);
        // updatLastMessage(chatWithContact.userId, '[image]', 'image', msgId, 'out', 'current');
    }
    else if (msgType == MSG_TYPE.video){
        client.sendMessage({
            to: chatWithContact.userId + "@" + toJID,
            //from: userName + "@" + domain+"/web",
            body: msgId.toString(),
            nick: userNickName,
            request: '',
            id: msgId,
            type: type,
            bodyType:msgType,
            fileSize:imageData.size,
            fileName:message,
            thumb:splitBase64(imageData.base64),
            msgCategory:msgCategory,
            phone:phone
        });
        imageData.base64='';
        imageData.size='';
        updateRecentMap(chatWithContact.userId,fromUserId, message, msgId, MSG_TYPE.video, 'out', 'new',MESSAGE_STATE.TO_SEND);
        setLastMessage(chatWithContact.userId,fromUserId, message, MSG_TYPE.video, msgId, msgId, MESSAGE_STATE.TO_SEND,selectMode);
        // updatLastMessage(chatWithContact.userId, '[video]', 'video', msgId, 'out', 'current');
    }
    else if (msgType == 'file' || msgType == 'audio' || msgType == 'voice') {
        var bodyType;
        if ((msgType == MSG_TYPE.audio)||(msgType == MSG_TYPE.voice)) {
            bodyType = 'audio';
            // updatLastMessage(chatWithContact.userId, '[audio]', 'audio', msgId, 'out', 'current');
            setLastMessage(chatWithContact.userId,fromUserId, '[audio]', 'audio', msgId, msgId, MESSAGE_STATE.TO_SEND,selectMode);
        }
        else if (msgType == MSG_TYPE.pdf) {
            bodyType = 'pdf';
            // updatLastMessage(chatWithContact.userId, '[pdf]', 'pdf', msgId, 'out', 'current');
            setLastMessage(chatWithContact.userId,fromUserId, '[pdf]', 'pdf', msgId, msgId, MESSAGE_STATE.TO_SEND,selectMode);
        }
        else if (msgType == MESSAGE_TYPE_DOC) {
            bodyType = 'doc';
            setLastMessage(chatWithContact.userId,fromUserId, '[doc]', 'doc', msgId, msgId, MESSAGE_STATE.TO_SEND,selectMode);
            // updatLastMessage(chatWithContact.userId, '[doc]', 'doc', msgId, 'out', 'current');
        }
        else {
            bodyType = 'file';
            setLastMessage(chatWithContact.userId,fromUserId, '[file]', 'file', msgId, msgId, MESSAGE_STATE.TO_SEND,selectMode);
            // updatLastMessage(chatWithContact.userId, '[file]', 'file', msgId, 'out', 'current');
        }
        client.sendMessage({
            to: chatWithContact.userId + "@" + toJID,
            //from: userName + "@" + domain+"/web",
            body: msgId.toString(),
            nick: userNickName,
            request: '',
            id: msgId,
            type: type,
            bodyType:bodyType,
            fileSize:imageData.size,
            fileName:message,
            // thumb:splitBase64(imageData.base64),
            msgCategory:msgCategory,
            phone:phone,
        });
        updateRecentMap(chatWithContact.userId,fromUserId, message, msgId, bodyType, 'out', 'new',MESSAGE_STATE.TO_SEND);
    }
    $('#demo').hide();
    $('#plusIcon').show();
    $('#crossIcon').hide();
    return true;
}

function split_jid(full_jid) {
    if (full_jid.toString().indexOf("@") >= 0) {
        return full_jid.toString().split("@")[0];
    }
    return full_jid;
}

function subscribe(userId, type) {
    if (type == 'user')
        client.subscribe(userId + "@" + domain);
    if (type == 'group')
        client.subscribe(userId + "@" + MUC_ROOM);
}

function sendToDevice(body, category) {
    client.sendMessage({
        to: userName + "@" + domain + '/CiaoIM',
        body: body,
        id: $.now(),
        type: 'chat',
        notification: true,
        bodyType: category,
        msgCategory: SYNC.WEB_CLIENT
    });
}
    function sendPing(id) {
        client.sendMessage({
            to: userName + "@" + domain + '/CiaoIM',
            body: {},
            id: id,
            type: 'chat',
            notification: true,
            bodyType:SYNC.PING,
            msgCategory:SYNC.WEB_CLIENT
        });
}

