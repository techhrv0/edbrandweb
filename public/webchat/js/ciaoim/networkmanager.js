var retryInt;
function pingResponse(messageId) {
    console.log("device is connected");
    $('#retryPhoneModel').modal('hide');
    $('#syncLoader').removeClass('hide');
    onDeviceStatusUpdate(true);
    unBlockOnConnect();
}
function pingToDevice() {
    if (!isConnected || !navigator.onLine) return;
    var id = $.now();
    sendPing(id);
    pingTs.push(id);
}

function pingCheck() {
    var failPings = 0;
    updateSystemNet();
    if (!isConnected || !navigator.onLine) {
        blockOndisconnect();
        return;
    }
    pingTs.forEach(function (value) {
        var currentTs = new Date($.now() + TIME_OUT_INTERVAL).getTime();
        if (currentTs >= value) {
            failPings++;
        }
    });
    console.log("failPings: " + failPings);
    if (failPings > 1) {
        console.log("contactSynced " + contactSynced);
        if (!contactSynced) {
            clearInterval(pingInterval);
            clearInterval(pingCheckInterval);
            onContactSyncFail();
            return;
        }
        blockOndisconnect();
        onDeviceStatusUpdate(false);
    }/*$('.emoji-wysiwyg-editor').keypress(function(event){

        if (event.keyCode === 10 || event.keyCode === 13)
            event.preventDefault();
        return false;

    });*/
}


function onContactSyncFail() {
    console.log("inside onContactSyncFail");
    // display retry model
    $('#syncLoader').addClass('hide');
    //$('#IndexRetryPhoneDiv').show();
    $('#retryPhoneModel').modal('show');
    startInterval(true);
}

function startInterval(showTimer) {
    clearInterval(retryInt);
    var interVal = RETRY_INTERVAL;
    retryInt = setInterval(function () {
        if(showTimer) {
            document.getElementById("retryTime").innerHTML = "Retrying in " + interVal;
            interVal--;
        }else{
            document.getElementById("retryTime").innerHTML = "Connecting ...";
        }
        if (interVal <= 0) {
            clearInterval(retryInt);  //if i is 0, then stop the interval
            // retry after RETRY_INTERVAL
            retryDeviceSyncNow();
        }
    }, 1000);
}

function retryDeviceSyncNow() {
    console.log("inside retryDeviceSyncNow");
    startInterval(false);
    pingToDevice();
    pingInterval = setInterval(function (){pingToDevice()}, PING_INTERVAL);
    pingCheckInterval = setInterval(function (){pingCheck()}, PING_CHECK_INTERVAL);
    if (isAuthenticated) {
        sendToDevice({}, SYNC.CONTACT);
        return;
    }
    doConnect();
}

function clearPins() {
    while (pingTs.length > 0) {
        pingTs.pop();
    }
}

function displayConnectStatus(show) {
    if (show) {
        if ($('#deviceConnectStatus').hasClass('hide')) {
            $('#deviceConnectStatus').removeClass('hide');
            $('#deviceConnectStatus').addClass('show');
            notificationContent();
            controlHeight();
        }
    } else if ($('#deviceConnectStatus').hasClass('show')) {
        $('#deviceConnectStatus').removeClass('show');
        $('#deviceConnectStatus').addClass('hide');
        /*notificationContent();*/
        controlHeight();
    }
}

function onDeviceStatusUpdate(isConnected) {
    if (isConnected) {
        displayConnectStatus(false);
        clearPins();
        return;
    }
    console.log("device is disconnected :(");
    displayConnectStatus(true);
    clearPins();
}

function onWebStatusUpdate(status) {
    console.log("onWebStatusUpdate: " + status);
    switch (status) {
        case CONNCECTION_STATE.CONNECTIING:
            $('#xmppState').removeClass('hide');
            $('#xmppState strong').text('connecting');
            // $('#xmppState p:first').html("<strong>Connecting</strong>");
            $('#xmppState').hasClass('show');
            break;
        case CONNCECTION_STATE.CONNECTED:
            $('#xmppState').removeClass('hide');
            $('#xmppState strong').text('connected').hide();
            //$('#xmppState p:first').html("<strong>Connected</strong>");
            $('#xmppState').hasClass('show');
            break;
        case CONNCECTION_STATE.DISCONNECTED:
            $('#xmppState').removeClass('hide');
            $('#xmppState strong').text('Disconnected');
            // $('#xmppState p:first').html("<strong>Disconnected</strong>");
            $('#xmppState').hasClass('show');
            break;
        default:
            break;
    }
}

function updateSystemNet() {
    var ele = $('#systemConnectStatus');
    if (navigator.onLine) {
        if (ele.hasClass('show')) {
            ele.removeClass('show');
            ele.addClass('hide');
        }
    } else if (ele.hasClass('hide')) {
        ele.removeClass('hide');
        ele.addClass('show');
    }
}
function controlHeight() {
    var ctrlhgt = $("#notification-height").height();
    if (ctrlhgt > 0) {
        var chatulheightnoti = $(window).height() - $(".page-header").height() - $(".form-group").height() - $("#contact_tab").height() - ctrlhgt;
        $("#contactBodys").css("height", chatulheightnoti);
        $("#contactBodys").find(".slimScrollDiv").css("height", chatulheightnoti);
        $("#contacts").css("height", chatulheightnoti);
    }
    else {
        var chatulheightnoti = $(window).height() - $(".page-header").height() - $(".form-group").height() - $("#contact_tab").height();
        $("#contactBodys").css("height", chatulheightnoti);
        $("#contactBodys").find(".slimScrollDiv").css("height", chatulheightnoti);
        $("#contacts").css("height", chatulheightnoti);
    }
}
function notificationContent() {
    //for notification width
    var notification_width = parseFloat($('#notification-height').width()) - 86;
    $(".noti-msg").css("width", notification_width);
}

function blockOndisconnect() {
    $('#key_board').prop('disabled',true);
    $('.emoji-wysiwyg-editor').keypress(function(event){

        if (event.keyCode === 10 || event.keyCode === 13)
            event.preventDefault();

    });

    $("#record").prop('disabled', true);
    $(".emoji-picker-icon").css("pointer-events", "none")

    $(".plusIcon").css("pointer-events", "none");

}
function unBlockOnConnect(){
    $("#key_board").removeAttr('disabled');

    $("#record").removeAttr('disabled');
    $(".emoji-picker-icon").css("pointer-events", "auto");

    $(".plusIcon").css("pointer-events", "auto");

}