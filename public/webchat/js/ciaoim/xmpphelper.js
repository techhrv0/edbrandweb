function getUserPart(jid) {
    if (jid.toString().indexOf("@") >= 0) {
        return jid.toString().split("@")[0];
    }
    return jid;
}


function sendDeliveryReceipt(msgId) {
    client.sendMessage({
        to: chatWithContact.userId+"@"+domain,
        id:msgId,
        type:'normal',
        received:true
    });
}

function sendReadReceipt(msgId) {
    client.sendMessage({
        to: chatWithContact.userId+"@"+domain,
        id:msgId,
        type:'normal',
        read:true
    });
}

function messageReceipt($xmlObj) {
    //Check for ack when msg is sent to the server
    var $message = $($xmlObj).find("message");
    var body = $($message).find('body').text();
    console.log("inside messageReceipt, "+body);
    if ($($xmlObj).find("server-received").attr('xmlns') == 'urn:xmpp:receipts') {
        var $serverReceived = $($xmlObj).find("server-received");
        var msgID = $serverReceived.attr('id');
        if(body != undefined && body == 'webcarbon' ){
            onMessageStateChange(msgID, MESSAGE_STATE.SENT);
        }
        return true;
    }

    //check for msg recieved acknowledgment
    if ($($xmlObj).find("received").attr('xmlns') == 'urn:xmpp:receipts') {
        var $serverReceived = $($xmlObj).find("received");
        var msgID = $serverReceived.attr('id');
        if(body != undefined && body == 'webcarbon' ) {
            onMessageStateChange(msgID, MESSAGE_STATE.RECEIVED);
        }
        return true;
    }

    if ($($xmlObj).find("read").attr('xmlns') == 'urn:xmpp:receipts') {
        var $serverReceived = $($xmlObj).find("read");
        var msgID = $serverReceived.attr('id');
        if(body != undefined && body == 'webcarbon' ) {
            onMessageStateChange(msgID, MESSAGE_STATE.READ);
        }
        return true;
    }
    return false;
}

function onVcardFetched(vcardObj) {
    if(vcardObj.userid== chatWithContact.userId){
        console.log('inside friend vcard');
        document.getElementById("frndphone").innerHTML = vcardObj.phone;
        document.getElementById("frndstatus").innerHTML = vcardObj.status;
        document.getElementById("myphone").innerHTML = vcardObj.phone;
        document.getElementById("mystatus").innerHTML = decodeBase64(vcardObj.status);
        document.getElementById("mynick").innerHTML =decodeBase64( vcardObj.nick);
    }
    else{
        $('#statusCounter').text('');
        $('#nameCounter').text('');
        userDetail.userNickName=vcardObj.nick;
        userDetail.phone=vcardObj.phone;
        userDetail.userStatus=vcardObj.status;
        userDetail.avatarHash=vcardObj.avatarHash;
        document.getElementById("myphone").innerHTML = vcardObj.phone;
        document.getElementById("mystatus").innerHTML =  decodeBase64(vcardObj.status);
        document.getElementById("mynick").innerHTML = decodeBase64(vcardObj.nick);
        document.getElementById("userNick").innerHTML = decodeBase64(vcardObj.nick);
        $('#picUpdatedOn').text(vcardObj.avatarHash);
    }
}
