function handleContacts() {
    // contactJosn = contactJosn.replace(/&quot;/g, "'");
    // phnbookMap = {};
    // var contacts = JSON.parse(contactJosn);
    // $.each(contacts, function (key, value) {
    //     copyAndPushContact(value);
    //     //get_signed_url(get_profile_key(value.userId));
    // });
    if (currentTab == TAB_CONTACTS) {
        if (jQuery.isEmptyObject(phnbookMap)) {
            $('#nocontact').text("No contacts");
            return;
        }
        sort_contacts(phnbookMap, 'contacts', false);
    }
}
function copyAndPushContact(value) {
    var contactItem = {};
    if (value.selectMode == SELECT_MODE.GROUP) {
        contactItem = {
            userId: value.userId,
            createdBy: value.createdBy,
            name: value.name,
            createdTs: value.createdTs,
            groupMembers: parseGroupMembersJson(value.groupMembers),
            selectMode: value.selectMode,
        };
    }
    else {
        contactItem = {
            userId: value.userId,
            ciaoId: value.ciaoId,
            phone: value.phone,
            name: value.name,
            nick: value.nick,
            status: value.status,
            selectMode: value.selectMode,
            blockStatus: value.blockStatus,
            contactType: value.contactType
        };
    }
    phnbookMap[contactItem.userId] = contactItem;
}

function parseGroupMembersJson(value) {
    var grpMembers=[];
    $.each(value, function (key, value) {
        grpMembers.push(value);
    });
    return grpMembers;
}

function getContactByPhone(phone) {
    $.each(phnbookMap, function (key, contact) {
        if (contact.phone != undefined && contact.phone == phone) {
            return contact;
        }
    });
    return null;
}

