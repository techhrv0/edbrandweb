
function show_map(text, msgId, timeType, messageTime, pos) {
    var tpl = '';
    var latLang = text;
    var latitude = latLang.split(",")[0];
    var longitude = latLang.split(",")[1];
    var name = latLang.split(",")[2];
    tpl = display_map(latitude, longitude, name, msgId, pos, timeType, messageTime);
    return tpl;
}

function get_text_tpl(text, msgId, messageTime, nick, direction, selectMode) {
    setDayNotification();
    var tpl = '';
    var emoji = new EmojiConvertor();
    var msg = emoji.replace_unified(text);
    emoji.text_mode = true;
    emoji.include_title = true;
    emoji.img_path = "/webchat/js/js-emoji-master/build/emoji-data/img-apple-64/";

    if(direction != MESSAGE_STATE.RECEIVED){
        tpl += '<li class="out">';
    }else{
        $("#chat_load").hide();
        tpl += '<li class="in">';
    }
    tpl += '<div id="' + msgId + '_msg" class="message">';
    if (selectMode == SELECT_MODE.GROUP) {
        tpl += '<div style="padding-bottom: 3px;">';
        tpl += '<span style="font-size: 14px; font-weight:bold; color:#2766ff">' + nick + '</span>';
        tpl += '</div>';
    }
    tpl += '<div class="chatting_body">';
    tpl += '<span class="body">';
    tpl += msg;
    tpl += '</span>';
    tpl += '</div>';
    tpl += '<div class="timeicon_set">';
    tpl += '<span class="datetime unselect">' + get_date(TIMETYPE.CURRENT, messageTime) + '</span>';
    if(direction == MESSAGE_STATE.SENT) {
        tpl += '<span id="' + msgId + '_dot" class="fa fa-clock-o clock_status" aria-hidden="true"/>';
    }
    tpl += '</div>';
    tpl += '</div><div class="clearfix"></div>';
    tpl += '</li>';
    return tpl;
}



function get_date(timeType, messageTime) {
    var time_str = '';
    if (timeType == 'current') {
        var time = new Date();
        if (time.getHours() > 12) {
            if (time.getMinutes() < 10)
                time_str = '0' + time.getHours() - 12 + ':' + '0' + time.getMinutes();
            else
                time_str = '0' + time.getHours() - 12 + ':' + time.getMinutes();
        }
        else {
            if (time.getMinutes() < 10)
                time_str = (time.getHours() + ':' + '0' + time.getMinutes());
            else
                time_str = (time.getHours() + ':' + time.getMinutes());
        }
    }
    else if (timeType == 'old') {
        time = new Date(messageTime);
        if (time.getHours() > 12) {
            if (time.getMinutes() < 10)
                time_str = '0' + time.getHours() - 12 + ':' + '0' + time.getMinutes();
            else
                time_str = '0' + time.getHours() - 12 + ':' + time.getMinutes();
        }
        else {
            if (time.getMinutes() < 10)
                time_str = (time.getHours() + ':' + '0' + time.getMinutes());
            else
                time_str = (time.getHours() + ':' + time.getMinutes());
        }
    }
    return time_str;
}

function get_date1(timeType, messageTime) {
    var time = new Date(messageTime);
    var day1 = time.getDate();
    var month1 = 1 + (time.getMonth());
    var year1 = time.getFullYear();

    //calculate current time
    var currentTime = new Date($.now());
    var day2 = currentTime.getDate();
    var month2 = 1 + (currentTime.getMonth());
    var year2 = currentTime.getFullYear();

    if ((day1 == day2) && (month1 == month2) && (year1 == year2)) {
        var time_str = '';
        if (timeType == TIMETYPE.CURRENT) {
            var time = new Date();
            if (time.getHours() > 12) {
                if (time.getMinutes() < 10)
                    time_str = '0' + time.getHours() - 12 + ':' + '0' + time.getMinutes();
                else
                    time_str = '0' + time.getHours() - 12 + ':' + time.getMinutes();
            }
            else {
                if (time.getMinutes() < 10)
                    time_str = (time.getHours() + ':' + '0' + time.getMinutes());
                else
                    time_str = (time.getHours() + ':' + time.getMinutes());
            }
        }
        else if (timeType == TIMETYPE.OLD) {
            time = new Date(messageTime);
            if (time.getHours() > 12) {
                if (time.getMinutes() < 10)
                    time_str = '0' + time.getHours() - 12 + ':' + '0' + time.getMinutes();
                else
                    time_str = '0' + time.getHours() - 12 + ':' + time.getMinutes();
            }
            else {
                if (time.getMinutes() < 10)
                    time_str = (time.getHours() + ':' + '0' + time.getMinutes());
                else
                    time_str = (time.getHours() + ':' + time.getMinutes());
            }
        }
    }

    else if (((day2 - day1) == 1) && (month1 == month2) && (year1 == year2)) {
            var time_str = '';
            time_str = 'Yesterday';
    }
    else if (((day2 - day1) < 7) && (month1 == month2) && (year1 == year2)) {
            var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
            var dayOfWeek = days[time.getDay()];
            var time_str = '';
            time_str = dayOfWeek;
    }
    else {
            var time_str = '';
            time_str = day1 + '/' + month1 + '/' + year1;
    }
    return time_str;
}

function add_to_view(tpl,timeType,page) {
    var cont = $('#chats');
    var list = $('.chats', cont);
    var scroll_to_chatHeight = $('#ulchatdemo').prop('scrollHeight') + 'px';
    var scroll_to_loadMoreHeight = $("#load_more").prop('scrollHeight') + 'px';
    var scroller = scroll_to_chatHeight - scroll_to_loadMoreHeight;

    if (timeType == TIMETYPE.CURRENT) {
        list.append(tpl);
        $("#ulchatdemo").slimScroll({
            scrollTo: scroll_to_chatHeight,
            start: 'bottom'
        });
    }
    else {
        if(page==1){
            list.append(tpl);
            $("#ulchatdemo").slimScroll({
                scrollTo: scroll_to_chatHeight,
                start: 'bottom'
            });
            //setDayNotification();
        }else {
            //setDayNotification();
            list.prepend(tpl);
            $("#ulchatdemo").slimScroll({
                scrollTo: scroller,
                start: 'bottom'
            });
        }
    }
    $("#load_new").hide();
    addPicEye();
}

// increment
function increment_count(user_jId) {
    var time = new Date();
    var time_str = (time.getHours() + ':' + time.getMinutes());
    var lastChatCount = $("#" + user_jId + '_cnt').text();
    if (lastChatCount == '') {
        lastChatCount = 0;
    }
    lastChatCount = parseInt(lastChatCount) + 1;
    if (!jQuery.isEmptyObject(document.getElementById(user_jId + '_cnt'))) {
        document.getElementById(user_jId + '_cnt').innerHTML = lastChatCount;
        document.getElementById(user_jId + '_time').innerHTML = time_str;
    }
    else {
        document.getElementById(user_jId + '_cnt').innerHTML = lastChatCount;
        document.getElementById(user_jId + '_time').innerHTML = time_str;
    }
}

function updateLastAcitvity(seconds, from) {
    // seconds=198000;
    var timestamp = (Math.round($.now() / 1000) - seconds) * 1000;
    var hours1 = seconds / 3600;
    if (chatWithContact.userId == from) {
        $("#friendLastSeen").text('last seen ' + fromSeconds(hours1, timestamp));
    }
}

function decodeBase64(val) {
    try {
        window.atob(val);
        return window.atob(val);
    } catch (e) {
        return val;
        // something failed
        // if you want to be specific and only catch the error which means
        // the base 64 was invalid, then check for 'e.code === 5'.
        // (because 'DOMException.INVALID_CHARACTER_ERR === 5')
    }
}

function encodeBase64(val) {
    var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9+/=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/rn/g,"n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}
    // Encode the String
    return Base64.encode(val);
}

function fromSeconds(hours1, timestamp) {
    var d = new Date(timestamp);
    var hours = d.getHours();
    var minutes = d.getMinutes();
    var seconds = d.getSeconds();
    hours = (hours < 10) ? "0" + hours : hours;
    minutes = (minutes < 10) ? "0" + minutes : minutes;
    seconds = (seconds < 10) ? "0" + seconds : seconds;
    if (hours1 < 24) {
        return 'today at ' + hours + ':' + minutes;
    }
    else if ((hours1 > 24) && (hours1 < 48)) {
        return 'yesterday at ' + hours + ':' + minutes;
    }
    else if ((hours1 > 48) && (hours1 < 168)) {
        var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        var dayOfWeek = days[d.getDay()];
        return dayOfWeek + ' at ' + hours + ':' + minutes;
    }
    else {
        var year = d.getFullYear();
        var month = 1 + (d.getMonth());
        var day = d.getDate();
        year = (year < 10) ? "0" + year : year;
        month = (month < 10) ? "0" + month : month;
        day = (day < 10) ? "0" + day : day;
        return day + '/' + month + '/' + year + ' at ' + hours + ':' + minutes;
    }
}
function clearChatmessages(isDelete) {
        var params = '{"userId":"' + chatWithContact.userId + '", "isDelete": "' + isDelete + '"}';
        sendToDevice(params, SYNC.CLEAR_CHAT);
        return true;
}
function confirm_box() {
    if (chatWithContact.selectMode == SELECT_MODE.GROUP) {
        if (!jQuery.isEmptyObject(getrecentsMap(chatWithContact.userId))) {
            $("#clearmsg").text('Clear chat with group "' + getPhonebookMap(chatWithContact.userId).name + '" ?');
            $("#delchat").text('Delete chat with group "' + getPhonebookMap(chatWithContact.userId).name + '" ?');
        }
    }
    else {
        if (!jQuery.isEmptyObject(getPhonebookMap(chatWithContact.userId))) {
            $("#clearmsg").text('Clear chat with "' + getPhonebookMap(chatWithContact.userId).name + '" ?');
            $("#delchat").text('Delete chat with "' + getPhonebookMap(chatWithContact.userId).name + '" ?');
            $("#blockname").text('Block "' + getPhonebookMap(chatWithContact.userId).name + '" ?');
            $("#unblockname").text('Unblock "' + getPhonebookMap(chatWithContact.userId).name + '" ?');
        }
    }
}

function showNotification(msgtime,fromWeb,timetype) {
    // isFirstMsg=false;
    var tpl = '';
    var time = new Date(msgtime);
    var date1 = time.getDate();
    var month1 = 1 + (time.getMonth());
    var year1 = time.getFullYear();

    //calculate current time
    var time3 = new Date($.now());
    var tpl = '';

    if (lastMsgTime != '') {
        var time2 = new Date(lastMsgTime);
        var date2 = time2.getDate();
    }

    var showDay = compareDate(time3, time);
    var showDay1=showDay.replace(/ /g,"_");
    if (isFirstMsg) {
        showMsg = false;
        isFirstMsg = false;
        lastMsgTime = time;
        if($('.'+showDay1).length!=0){
            if(fromWeb){
                return tpl;
            }
            else{
                $('.'+showDay1).remove();
            }

        }
            tpl= addNoticationTpl(showDay1,showDay)
    }
    if ((date1 == date2) && (isFirstMsg == false)) {
        isFirstMsg = false;
        lastMsgTime = time;
        showMsg = true;
        if($('.'+showDay1).length!=0){
            if(fromWeb){
                return tpl;
            }
            else{
                if(timetype==TIMETYPE.OLD){
                    $('.'+showDay1).remove();
                    tpl= addNoticationTpl(showDay1,showDay)
                }
            }
        }
    }
    if ((date1 > date2) && (isFirstMsg == false)) {
        isFirstMsg = false;
        lastMsgTime = time;
        showMsg = true;
        if($('.'+showDay1).length!=0){
            if(fromWeb){
                return tpl;
            }
            else{
                $('.'+showDay1).remove();
            }
        }
        if(timetype==TIMETYPE.OLD){
            tpl= addNoticationTpl(showDay1,showDay)
        }
    }
    return tpl;
}

function addNoticationTpl(className,msg) {
    return '<li class="notification_bubble_1 '+className+'"><div class="notification_msgTime"><div class="notification_body">'+
    '<span class="body unselect">'+msg+'</span></div></div><div class="clearfix"></div></li>';
}
function compareDate(time1, time2) {
    var tpl = '';
    var day1 = time1.getDate();     //current date
    var month1 = time1.getMonth() + 1;
    var year1 = time1.getFullYear();

    var day2 = time2.getDate();
    var month2 = time2.getMonth() + 1;
    var year2 = time2.getFullYear();

    var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    var dayOfWeek = days[time2.getDay()];
    if ((day1 == day2) && (month1 == month2) && (year1 == year2)) {
        tpl = 'Today';
    }
    else if (((day1 - day2) == 1) && (month1 == month2) && (year1 == year2)) {
        tpl = 'Yesterday';
    }
    else if (((day1 - day2) < 7) && (month1 == month2) && (year1 == year2)) {
        tpl = dayOfWeek;
    }
    else {
        tpl = day2 + '/' + month2 + '/' + year2;
    }
    return tpl;
}

function isOfflineMsg($xmlObj) {
    //check if msg is an offline msg, since it is received before getting phonebook,group list. it must be handled after getting phonebook and gorup list.
    // or else each offline msg will create its separate entry in recents apart from existing phonebook/group entry
    // return true if not offline msg, else ignore
    if ($($xmlObj).find("forwarded").text() == 'server-received, Offline Storage') {
        return false;
    }
    return true;
}


function isAlreadyInRecents(userId) {
    if (!jQuery.isEmptyObject(getrecentsMap(userId))) {
        return true;
    }
    return false;
}

function addNewRecent(userId, fromUserId, body, name, msgType, msgId, messageTime, chatState, unReadChats, selectMode) {
    console.log('addNewRecent '+userId+ fromUserId+ body+ name+ msgType+ msgId+ messageTime+ chatState+unReadChats+selectMode+' currentTab '+currentTab);
    var recent = {
        userId: userId,
        fromUserId:fromUserId,
        name: name,
        body: body,
        msgType: msgType,
        msgId: msgId,
        messageTime: messageTime,
        unreadMsg: unReadChats,
        chatState: chatState,
        selectMode:selectMode
    };
    recentsMap[userId] = recent;
    if (currentTab == TAB_RECENTS) {
        appendRecentsNew(recentsMap[userId]);
    }
}

function showLastMsg(userId, lastMessage, bodyType, msgId) {
    var msg = '';
   /* if (lastMessage.length > 25)    lastMessage = lastMessage.substring(0, 25) + ' ...';*/
    if (bodyType == 'text') {
        var emoji = new EmojiConvertor();
        msg = emoji.replace_unified(lastMessage);
    }
    else if (bodyType == 'location') {
        msg = 'Location';
    }
    else if (bodyType == 'sticker') {
        msg = ' Sticker';
    }
    else if (bodyType == 'contact') {
        msg = ' Contact';
    }
    else if (bodyType == 'image') {
        msg = ' Image';
    }
    else if (bodyType == 'video') {
        msg = ' Video';
    }
    else if (bodyType == 'audio') {
        msg = ' Audio';
    }
    else if (bodyType == 'voice') {
        msg = ' Audio';
    }
    else if (bodyType == 'file') {
        msg = ' ' + messageTypeByFileName1(lastMessage);
    }
    else {
    }
    return msg;
}
/*
function showLastMsgTooltip(userId, lastMessage, bodyType, msgId) {
    var msg = '';
    /!*if(lastMessage.length > 25)	lastMessage = lastMessage.substring(0,25)+' ...';*!/
    if (bodyType == 'text') {
        var emoji = new EmojiConvertor();
        msg = emoji.replace_unified(lastMessage);
    }
    else if (bodyType == 'location') {
        msg = 'Location';
    }
    else if (bodyType == 'sticker') {
        msg = ' Sticker';
    }
    else if (bodyType == 'contact') {
        msg = ' Contact';
    }
    else if (bodyType == 'image') {
        msg = ' Image';
    }
    else if (bodyType == 'video') {
        msg = ' Video';
    }
    else if (bodyType == 'audio') {
        msg = ' Audio';
    }
    else if (bodyType == 'file') {
        msg = ' ' + messageTypeByFileName1(lastMessage);
    }
    else {
    }
    return msg;
}
*/

function updateRecentMap(userId,fromUserId, lastMessage, msgId, bodyType, msgPos, timeType,chatState) {
    console.log('updateRecentMap '+userId+fromUserId+ lastMessage);
    if (!isAlreadyInRecents(userId)) {
        //if not in recents then create a new entry
        console.log('updateRecentMap1 '+userId+fromUserId+ lastMessage);
        addNewRecent(userId, fromUserId , lastMessage, getPhonebookMap(userId).name, bodyType, msgId, msgId, CARBON.DIRECTION_SENT, 0, chatWithContact.selectMode);
    }
    else{
        try{
            getrecentsMap(userId).body= showLastMsg(userId, lastMessage, bodyType, msgId);
            getrecentsMap(userId).msgType= bodyType;
            getrecentsMap(userId).msgId= msgId;
            getrecentsMap(userId).messageTime= msgId;
            getrecentsMap(userId).unreadMsg= 0;
            getrecentsMap(userId).chatState= chatState;
            getrecentsMap(userId).fromUserId= fromUserId;
        }
        catch (e){}
    }
    console.log('updateRecentMap1 '+userId+fromUserId+ lastMessage+getrecentsMap(userId).body);
}

function setDayNotification() {
    add_to_view(showNotification(new Date($.now()),true,TIMETYPE.CURRENT),TIMETYPE.CURRENT);
}