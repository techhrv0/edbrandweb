function sortMessageByTime(msgTimeArr, recentsMap) {
    console.log('sortMessageByTime '+msgTimeArr);
    var msgTimeArr = [];
    var i = 0;
    $.each(recentsMap, function (key, value) {
        msgTimeArr[i] = (value.messageTime == undefined || value.messageTime == 0 || value.messageTime == null) ? value.userId : value.messageTime;
        i++;
    });
    console.log("msgTimeArr lengh: "+msgTimeArr.length);
    msgTimeArr.sort(function (a, b) {
        // Turn your strings into dates, and then subtract them
        // to get a value that is either negative, positive, or zero.
        return new Date(b).getTime() - new Date(a).getTime();
    });
    return msgTimeArr;
}

function setLastMessage(userId, fromUserId, body, msgType, msgId, messageTime, chatState,selectMode) {
    console.log('setLastMessage '+userId+ fromUserId+ body+ msgType+ msgId+ messageTime+ chatState+selectMode);
    var lastMessage = '';
    var text = '';
    var receiptState = '';
    var extra = '';
    var grpMember='';
    switch (msgType) {
        case MSG_TYPE.text:
            if (body.length > 25)    body = body.substring(0, 25) + ' ...';
            var emoji = new EmojiConvertor();
            text = emoji.replace_unified(body);
            $('#'+userId +'_last_msg').attr('title',body);
            break;
        case MSG_TYPE.location:
            text = 'Location';
            extra = '<div class="background_location dot_sett" ></div>';
            $('#'+userId +'_last_msg').attr('title',text);
            break;
        case MSG_TYPE.sticker:
            text = 'Sticker';
            extra = '<div class="background_sticker dot_sett" ></div>';
            $('#'+userId +'_last_msg').attr('title',text);
            break;
        case MSG_TYPE.stickerGif:
            text = 'StickerGif';
            extra = '<div class="background_sticker dot_sett" ></div>';
            $('#'+userId +'_last_msg').attr('title',text);
            break;
        case MSG_TYPE.contact:
            text = 'Contact';
            extra = '<div class="background_contact dot_sett" ></div>';
            $('#'+userId +'_last_msg').attr('title',text);
            break;
        case MSG_TYPE.image:
            text = 'Image';
            extra = '<div class="background_camera dot_set" ></div>';
            $('#'+userId +'_last_msg').attr('title',text);
            break;
        case MSG_TYPE.video:
            text = 'Video';
            extra = '<div class="background_video dot_set" ></div>';
            $('#'+userId +'_last_msg').attr('title',text);
            break;
        case MSG_TYPE.audio:
            text = 'Audio';
            extra = '<div class="background_audio dot_set" ></div>';
            $('#'+userId +'_last_msg').attr('title',text);
            break;
        case MSG_TYPE.voice:
            text = 'Audio';
            extra = '<div class="background_recording dot_set" ></div>';
            $('#'+userId +'_last_msg').attr('title',text);
            break;
        case MSG_TYPE.file:
            text = messageTypeByFileName1(lastMessage);
            extra = '<div class="background_file dot_setting" ></div>';
            $('#'+userId +'_last_msg').attr('title',text);
            break;
        case MSG_TYPE.group_create:
            text="You created group '" + getPhonebookMap(userId).name + "'.";
            break;
        default:
            text=body;
            break;
    }
    if((selectMode==SELECT_MODE.GROUP)&&(chatState!=MESSAGE_STATE.INVALID)){
        if(fromUserId==userName) {
            grpMember='<div class="pull-left">You:</div> ';
        }
        else if(getPhonebookMap(fromUserId).selectMode==SELECT_MODE.CHAT){
            grpMember='<div class="pull-left">'+getPhonebookMap(fromUserId).name+': </div>';
        }
        else{
            grpMember='<div class="pull-left">'+getPhonebookMap(fromUserId).phone+': </div>';
        }
    }

    var direction = 'in';
    if (isSent(chatState)) {
        receiptState = '<div id="' + msgId + '_dotRecent" class="' + getState(chatState) + '" data-userID="'+userId+'"></div>';
        direction = 'out';
    } else {
        receiptState = '<div id="' + msgId + '_dotRecent" data-userID="'+userId+'"></div>';
    }
    lastMessage = grpMember + receiptState + extra +'&nbsp;'+text;
    console.log('setLastMessage '+msgType+grpMember+lastMessage);
    if(messageTime != undefined && messageTime != 0){
        $('#' + userId + '_time').html(get_date1(TIMETYPE.OLD, messageTime));
    }
    if(lastMessage != undefined && lastMessage != null){
        $('#' + userId + '_last_msg').html(lastMessage);
    }
    //updateRecentMap(chatWithContact.userId, text, msgId, msgType, direction, 'current');
}
//recents msg panel
function getState(msgState) {
    var receipt = '';
    switch (msgState) {
        case MESSAGE_STATE.TO_SEND:
            receipt = 'fa fa-clock-o clock_status_set';
            break;
        case MESSAGE_STATE.SENT:
            receipt = 'background_grayCall dot_set';
            break;
        case MESSAGE_STATE.DELIVERED:
            receipt = 'background_orangeCall dot_set';
            break;
        case MESSAGE_STATE.READ_BY_RECEIVER:
            receipt = 'background_greenCall dot_set';
            break;
        default:
            break;
    }
    return receipt;
}
//previous bubble
function bubbleGetState(msgState) {
    var receipt = '';
    switch (msgState) {
        case MESSAGE_STATE.TO_SEND:
            receipt = 'fa fa-clock-o clock_status';
            break;
        case MESSAGE_STATE.SENT:
            receipt = 'dot background_gray';
            break;
        case MESSAGE_STATE.DELIVERED:
            receipt = 'dot background_orange';
            break;
        case MESSAGE_STATE.READ_BY_RECEIVER:
            receipt = 'dot background_green';
            break;
        default:
            break;
    }
    return receipt;
}

function initTpl(messageState, isFile) {
    if (isSent(messageState)) {
        if (isFile) {
            return '<li class="out unselect">';
        }
        return '<li class="out">';
    } else {
        if (isFile) {
            return '<li class="in unselect">';
        }
        return '<li class="in">';
    }
}

function getMsgPosClass(messageState) {
    if (!isSent(messageState)) {
        return '-in'
    }
    return '';
}

function createChatBubble(message,timetype) {
    var statusTpl  = '';
    if (isFirstMsg) {
        lastMsgTime = '';
        statusTpl += showNotification(message.sentReceivedTs,false,timetype);
    }
    else if (!isFirstMsg) {
        statusTpl += showNotification(message.sentReceivedTs,false,timetype);
    }
    switch (message.messageType){
        case MSG_TYPE.text:
        case MSG_TYPE.sticker:
        case MSG_TYPE.stickerGif:
            return statusTpl+createTpl(message);
        case MSG_TYPE.contact:
            return statusTpl+createContactTpl(message);
        case MSG_TYPE.image:
        case MSG_TYPE.video:
        case MSG_TYPE.audio:
        case MSG_TYPE.file:
            return statusTpl+createAttachmentTpl(message);
        case MSG_TYPE.group_create:
        case MSG_TYPE.group_add:
        case MSG_TYPE.group_join:
        case MSG_TYPE.group_remove:
        case MSG_TYPE.subject_change:
        case MSG_TYPE.pic_change:
        case MSG_TYPE.grant_admin:
        case MSG_TYPE.grant_owner:
        case MSG_TYPE.group_leave:
            return statusTpl+showGroupNotification(message.messageBody);
        default:
            return '';
    }
}

function createTpl(message) {
    var nick = 'a group user';
    var emoji = new EmojiConvertor();
    var body = emoji.replace_unified(message.messageBody);
    emoji.text_mode = true;
    emoji.include_title = true;
    emoji.img_path = "/webchat/js/js-emoji-master/build/emoji-data/img-apple-64/";
    var tpl = initTpl(message.messageState, false);

    if (message.messageType == MSG_TYPE.text) {
        tpl += '<div id="' + message.messageId + '_msg" class="message">';
    tpl += getGrpChatHeader(message.selectMode, message.fromUserId, message.messageState);
    tpl += '<div class="chatting_body">';
    tpl += '<span class="body">';
    tpl += body;
    tpl += '</span>';
    tpl += '</div>';
    tpl += '<div class="timeicon_set">';
    tpl += '<span class="datetime unselect">' + get_date(TIMETYPE.OLD, message.sentReceivedTs) + '</span>';
    tpl += '<span id="' + message.messageId + '_dot" class="' + bubbleGetState(message.messageState) + '" aria-hidden="true"/>';
    tpl += '</div>';
    tpl += '</div><div class="clearfix"></div>';
    tpl += '</li>';
    }
    if (message.messageType == MSG_TYPE.sticker || message.messageType == MSG_TYPE.stickerGif) {
        tpl += '<div class="message message-sticker" style="background:none !important;box-shadow:none !important; color:#333;">' + getSticker(message.messageBody,message);
        body = '';
    }
    return tpl;
}

// <message from='14857ki0tyj@ciaolarge.ciaoim.com/CiaoIM' xml:lang='en' to='148546du3gb@ciaolarge.ciaoim.com' id='1485770493412-11664' type='chat' xmlns='jabber:client'>
// <body>{"birthday":"","company":"","emailList":[],"firstName":"ABAB","isCiaoUser":"0","lastName":"","phoneList":[{"detail":"+917838545654","type":"2"},{"detail":"+917838545654","type":"2"}]}</body>
//  <properties xmlns='http://www.jivesoftware.com/xmlns/xmpp/properties'><msgCategory>single</msgCategory><phone>+919582901708</phone><bodyType>contact</bodyType></properties><nick xmlns='http://jabber.org/protocol/nick'>Pratibha</nick><request xmlns='urn:xmpp:receipts'/><delay xmlns='urn:xmpp:delay' from='14857ki0tyj@ciaolarge.ciaoim.com/CiaoIM' stamp='2017-01-30T10:01:33.384Z'>server-received</delay></message>
function createContactTpl(message) {
    var contactObj = jQuery.parseJSON(message.messageBody);
    var contactName = contactObj.firstName + contactObj.lastName;
    var isCiaoUser = contactObj.isCiaoUser;
    var phoneList = contactObj.phoneList;
    var contactId = random5();

    sharedContactMap[contactId] = contactObj;

    var tpl = initTpl(message.messageState, false);
    tpl += '<div id="' + message.messageId + '_contact" class="bubble-contact' + getMsgPosClass(message.messageState) + '">';
    if (message.messageCategory == MSG_CATEGORY.groupchat) {
        tpl += '<div style="padding-bottom: 3px;">';
        tpl += '<span style="font-size: 14px; font-weight:bold; color:#2766ff">' + nick + '</span>';
        tpl += '</div>';
    }
    tpl += '<div class="body contact" style="position: relative;height: 53px;">';
    tpl += '<div style="width: 63px;float: left;">';
    tpl += '<img id="' + contactId + '_sharedContactPic"  onclick="showContact1(' + contactId +')" class="avtar_contact profile_contact_default" data-target="#myModalContact" data-toggle="modal" alt="" onerror="imgErrors(this)" src="' + userPic + '">';
    tpl += '</div>';
    tpl += '<span class="ContactTextSet" id="' + message.messageId + '_name">' + contactName + '</span>';
    tpl += '<span class="datetime">' + '\n' + get_date(TIMETYPE.OLD, message.sentReceivedTs) + '</span>';
    tpl += '<span id="' + message.messageId + '_dot" class="' + bubbleGetState(message.messageState) + '" aria-hidden="true"/>';
    tpl += '</div>';
    tpl += '<div class="clearfix">';
    tpl += '<div class="contact_bubble">';
    if ( isCiaoUser=='1') {
        tpl += "<span class='contact_footer' onclick='onStartChat(\"" + phoneList[0].userId + "\");'>Message</span>";
    } else if( isCiaoUser=='0'){
        tpl += "<span class='contact_footer' onclick='#'>Invite to CiaoIM</span>";
    }
    tpl += '</div>';
    tpl += '</div>';
    tpl += '</div>';
    tpl += '</li>';
    return tpl;
}

function createAttachmentTpl(message) {
    var scroll_to_chatHeight = $('#ulchatdemo').prop('scrollHeight') + 'px';
    $("#ulchatdemo").slimScroll({
        scrollTo: scroll_to_chatHeight,
        start: 'bottom'
    });

    var fileName = getFileName(message.fileName);
    var tpl = initTpl(message.messageState, true);
    var extss = fileName.substr( fileName.indexOf('.') + 1 );//message.messageBody.split('.').pop();
    if (message.messageType == MSG_TYPE.image) {

        tpl += '<div class="bubble-image'+getMsgPosClass(message.messageState)+'">' + getGrpChatHeader(message.messageCategory,message.userId,message.messageState);
        tpl += '<div class="image-thumb">';
        // tpl += '<div  class="forward_icon_set">';
        // tpl += "<img src='/webchat/assets/images/fwdicon.png' class='forward_icon' id='fwdicon'  onclick='fwdpopup(\"" + message.messageId + "\")'  data-toggle='modal' data-target='#myModalForwardImage' aria-hidden='true'/>";//forward
        // tpl += '</div>';

        tpl += '<span class="body image isimg" id="img_div"><img data-URL="" id="' + message.messageId + '_msg" src="/webchat/assets/images/defaultImage.png" class="image_send_set"></span>';
        tpl += '<div class="shade-in"></div>';
        tpl += '</div>';
        tpl += '<div class="message-meta">';
        tpl += '<span class="datetime_img">' + get_date('old', message.sentReceivedTs) + '</span>';
        tpl += '<span id="' + message.messageId + '_dot" class="' + bubbleGetState(message.messageState) + '" aria-hidden="true"/>';
        tpl += '</div>';
        tpl += '</div><div class="clearfix"></div>';
        tpl += '</li>';
        console.log('createAttachmentTpl '+message.messageType+message.messageId+tpl);
    }
    else if (message.messageType == MSG_TYPE.video) {
        tpl += '<div class="bubble-video' + getMsgPosClass(message.messageState) + '">' + getGrpChatHeader(message.selectMode, message.userId,message.messageState);
        tpl += '<div class="image-thumb">';
        tpl += '<span class="body image" id="img_div"><video id="' + message.messageId + '_msg" src="" poster="' + 'data:video/mp4;base64,' + message.thumbNail + '" controls=""></video></span>';
        tpl += '<div class="shade' + getMsgPosClass(message.messageState) + '"></div>';
        tpl += '</div>';
        tpl += '<div class="message-meta">';
        tpl += '<span class="datetime">' + get_date('old', message.sentReceivedTs) + '</span>';
        tpl += '<span id="' + message.messageId + '_dot" class="' + bubbleGetState(message.messageState) + '" aria-hidden="true"/>';
        tpl += '</div>';
        tpl += '</div>';
        tpl += '</li>';
    }
    // else if (message.messageType == MSG_TYPE.audio) {
    //     var s3key = get_s3_key(message.messageId, message.messageType, extss);
    //     get_signed_url(s3key);
    //     if(isSent(message.messageState)) {
    //         tpl += '<li class="out">';
    //         tpl += '<div class="bubble-contact">';
    //         tpl += '<span class="body image" id="img_div" style="position: relative;height: 53px;">';
    //         tpl += '<div style="width: 63px;float: left;position: relative;">';
    //         tpl += '<img  class="' + userName + '_recordpic_device avtar_contact profile_contact_default" alt="" src="/webchat/assets/images/user.png" onerror="imgError(this,\'' + userName + '\');">';
    //         tpl += '<img id="" class="voiceMemo" alt="" src="/webchat/assets/images/voice_memo.png" width="20px" height="20px" >';
    //         tpl += '</div>';
    //         tpl += '<audio id="' + message.messageId + '_msg" preload="true">';
    //         tpl += '<source src="">';//src="' + 'data:audio/wav;base64,' + message.thumbNail + '"
    //         tpl += '</audio>';
    //         tpl += '<div id="' + message.messageId + 'audioplayer" class="audioplayer">';
    //         tpl += '<button id="' + message.messageId + 'pButton" class="play" onclick="playOutFromDevice(' + message.messageId + ')"></button>';
    //         tpl += '<div id="' + message.messageId + 'timeline" class="timeline">';
    //         tpl += '<div id="' + message.messageId + 'playhead" class="playhead" style="background:#ffffff;"></div>';
    //         tpl += '</div>';
    //         tpl += '</div></span>';
    //         tpl += '<div class="timeicon_set">';
    //         tpl += '<span class="datetime">' + get_date('current', null) + '</span>';
    //         tpl += '<span id="' + message.messageId + '_dot" class="' + bubbleGetState(message.messageState) + '" aria-hidden="true"/>';
    //         tpl += '</div>';
    //         tpl += '</div>';
    //         tpl += '</li>';
    //         addPicEye();
    //         return tpl;
    //     }else{
    //         tpl += '<div class="message">';
    //         tpl += getGrpChatHeader(message.messageCategory, message.userId,message.messageState);
    //         tpl += '<span class="body image" id="img_div" style="position: relative;height: 53px;">';
    //         tpl += '<audio id="' + message.messageId + '_msg">';
    //         tpl += '<source src="">';//src="' + 'data:audio/wav;base64,' + message.thumbNail + '"   id="' + message.messageId + '_msgaudio_in"
    //         tpl += '</audio>';
    //         tpl += '<div id="'+message.messageId+'audioplayer_in" class="audioplayer">';
    //         tpl += '<button id="'+message.messageId+'pButton_in" class="play_in" onclick="playIn('+message.messageId+')"></button>';
    //         tpl += '<div id="'+message.messageId+'timeline_in" class="timeline">';
    //         tpl += '<div id="'+message.messageId+'playhead_in" class="playhead" style="background:#2766ff;"></div>';
    //         tpl += '</div>';
    //         tpl += '</div>';
    //         tpl += '<span class="datetime">' + get_date('old', message.sentReceivedTs) + '</span>';
    //         tpl += '<span id="' + message.messageId + '_dot" class="' + bubbleGetState(message.messageState) + '" aria-hidden="true"/>';
    //         tpl += '</span>';
    //         tpl += '<div style="width: 63px;float: right;position: relative;">';
    //         tpl += '<img class="'+message.userId+'_recordpic_device avtar_contact profile_contact_default" alt="" src="/webchat/assets/images/user.png" onerror="imgError(this,\'' + message.userId + '\');">';
    //         tpl += '<img id="" class="voiceMemoOut" alt="" src="/webchat/assets/images/icon/microphone.png" width="20px" height="20px" >';
    //         tpl += '</div>';
    //         tpl += '</div>';
    //         tpl += '</li>';
    //         addPicEye();
    //         return tpl;
    //     }
    // }
    else {
        tpl += '<div class="file_width'+getMsgPosClass(message.messageState)+'">'+getGrpChatHeader(message.messageCategory,message.userId,message.messageState)+'<div class="document-container'+getMsgPosClass(message.messageState)+'">';
        tpl += '<div class="document-container-padding">';
        tpl += '<div class="file-icon">';
        tpl += '<img id="'+ message.messageId+ '_msg"  src="'+getFileIcon(extss)+'" onerror="imageerrhandle(this,\'' +extss+ '\')" width="30px" height="30px" >';
        tpl += '</div>';
        tpl += '<div class="file-icon1">';
        tpl += '<span><h1 class="fileNameSet">' + fileName + '</h1></span>';
        tpl += '</div>';
        tpl += '<div class="file-icon2">';
        tpl += '<a target="_blank" href="/chat/imagedownload/' + message.messageId + "_" + fileName + '"><img id="' + message.messageId+ '_msgLoader"  src="webchat/assets/images/download_icon.png"  width="30px" height="30px"></a>';
        tpl += '</div>';
        tpl += '</div>';
        tpl += '</div>';
        tpl += '<div class="footer_file">';
        tpl += '<span class="text_size">' + extss + ' - ' + getFileSize(message.fileSize) + '</span>';
        tpl += '<span class="datetime">' + get_date('old', message.sentReceivedTs) + '</span>';
        tpl += '<span id="' + message.messageId + '_dot" class="' + bubbleGetState(message.messageState) + '" aria-hidden="true"/>';
        tpl += '</div>';
        tpl += '</div>';
        tpl += '</li>';
        addPicEye();

        /*  new mp3 UI  */
        // if(isSent(message.messageState)) {
        //     tpl += '<li class="out unselect"><div class="message">';// + grpChatHeader;
        //     tpl += '<span class="body image" id="img_div" style="position: relative;height: 53px;">';
        //     //tpl += '<div id="' + msgId + '_preview" data-fileName="' + fileName + '" data-fileSize="' + fileSize + '" data-fileType="' + fileExtention + '">';
        //     tpl += '<audio id=id="' + message.messageId + '_msgaudio" preload="true"><source id="' + message.messageId + '_msg" src=""></audio>';
        //     tpl += '<div style="width: 63px;float: left;position: relative;">';
        //     tpl += '<img id="" class="avtar_contact profile_contact_default" alt="" src="/webchat/assets/images/mp3.png" height="30px" width="30px">';
        //     tpl += '</div>';
        //     tpl += '<div id="' + message.messageId + 'audioplayer" class="audioplayer">';
        //     tpl += '<button id="' + message.messageId + 'pButton" class="play" onclick="playOut(' + message.messageId + ')"></button>';
        //     tpl += '<div id="' + message.messageId + 'timeline" class="timeline">';
        //     tpl += '<div id="' + message.messageId + 'playhead" class="playhead"></div></div></div></span>';
        //     tpl += '<div class="timeicon_set">';
        //     tpl += '<span class="datetime">' + get_date('current', null) + '</span>';
        //     tpl += '<span id="' + message.messageId + '_dot" class="fa fa-clock-o clock_status" aria-hidden="true"/>';
        //     //tpl += '</div>';
        //     tpl += '</div></li>';
        // }else{
        //     tpl += '<div class="message">';
        //     tpl += getGrpChatHeader(message.messageCategory, '');
        //     tpl += '<span class="body image" id="img_div" style="position: relative;height: 53px;">';
        //     tpl += '<audio id="' + message.messageId + '_msgaudio_in" preload="true">';
        //     tpl += '<source id="' + message.messageId + '_msg" src="">';//src="' + 'data:audio/wav;base64,' + message.thumbNail + '"
        //     tpl += '</audio>';
        //     tpl += '<div id="'+message.messageId+'audioplayer_in" class="audioplayer">';
        //     tpl += '<button id="'+message.messageId+'pButton_in" class="play_in" onclick="playIn('+message.messageId+')"></button>';
        //     tpl += '<div id="'+message.messageId+'timeline_in" class="timeline">';
        //     tpl += '<div id="'+message.messageId+'playhead_in" class="playhead" style="background:#2766ff;"></div>';
        //     tpl += '</div>';
        //     tpl += '</div>';
        //     tpl += '<span class="datetime">' + get_date('old', message.sentReceivedTs) + '</span>';
        //     tpl += '<span id="' + message.messageId + '_dot" class="' + bubbleGetState(message.messageState) + '" aria-hidden="true"/>';
        //     tpl += '</span>';
        //     tpl += '<div style="width: 63px;float: right;position: relative;">';
        //     tpl += '<img id="" class="avtar_contact profile_contact_default" alt="" src="/webchat/assets/images/mp3.png" height="30px" width="30px">';
        //     tpl += '</div>';
        //     tpl += '</div>';
        //     tpl += '</li>';
        // }
    }
    return tpl;
}

function imageerrhandle(image,extss) {
    image.onerror = "";
    if (extss == 'pdf') {
        image.src = "webchat/assets/images/pdf_icon.png";
    }
    else if (extss == 'mp3') {
        image.src = "webchat/assets/images/audio_icon.png";
    }
    else if (extss == 'mp3' || extss == 'wav' || extss == '3gp') {
        image.src = "webchat/assets/images/audio_icon.png";
    }
    else {
        image.src = "webchat/assets/images/doce_icon.png";
    }
}

function getFileName(fileName) {
    if(fileName == null || fileName == undefined) return '';
    var lastIndex = fileName.lastIndexOf('/');
    if(lastIndex == 0) return fileName;
    return fileName.substr(lastIndex+1, fileName.length);
}

function getFileIcon(extss) {
    if (extss == 'pdf') {
        return "webchat/assets/images/pdf_icon.png";
    }
    else if (extss == 'mp3' || extss == 'wav' || extss == '3gp') {
        return "webchat/assets/images/audio_icon.png";
    }
    else {
        return "webchat/assets/images/doce_icon.png";
    }
}
function getGrpChatHeader(selectMode, userId,messageState) {
    if ((selectMode == SELECT_MODE.GROUP)&&(!isSent(messageState))) {
        try {
            if(getPhonebookMap(userId).selectMode==SELECT_MODE.CHAT)
                nick=getPhonebookMap(userId).name;
            else
                nick=getPhonebookMap(userId).phone;
        }
        catch (e){}
        return '<div class="group-member-name">'+nick+'</div>';
    }
    return '';
}

function onRecents() {
    /* for left block*/
    var chatulheight = $( window ).height()-$(".page-header").height()-$(".form-group").height()-$("#contact_tab").height()-$("#notification-height").height();
    $("#contactBodys").css("height", chatulheight);
    $("#contactBodys").find(".slimScrollDiv").css("height", chatulheight);
    $("#contacts").css("height", chatulheight);

    /* for right block*/
    var heightset = $( window ).height()-$(".page-header").height();
    $(".no_chat_wrapper").css("height", heightset);
    $(".chatbody-main").css("height", "auto");
    $("#chats .slimScrollDiv").css("height",heightset);
    adjustRecentWith();
}

function adjustRecentWith() {
    var contact_width = parseFloat($('#contactBodys').width())-82;
    $(".chat_body").css("width",contact_width);
}
function random5() {
    var randomValue = Math.floor(Math.random() * 100000);
    return randomValue;
}