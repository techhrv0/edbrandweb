var msgId = '';
function image_Upload() {
    $('#image_Upload').fileupload(
        {
            add: function (e, data) {
                var goUpload = true;
                var uploadFile = data.files[0];
                var ext = uploadFile.name.split('.').pop();
                if (!(/\.(png|jpg|jpeg|bmp)$/i).test(uploadFile.name)) {
                    printFileError(FILE_TYPE_ERROR);
                    goUpload = false;
                } else if (uploadFile.size > 20000000) { // 20mb
                    printFileError(FILE_SIZE_ERROR);
                    goUpload = false;
                }
                if (goUpload == true) {
                    msgId = $.now();
                    addImageLoader(msgId, uploadFile, uploadFile.name, ext, uploadFile.size);
                }
            },
            success: function (response) {
            }
        });
}

function uploadImage(images, msgId, fileName) {
    stopimgAjax = $.ajax({
        url: '/chat/imageupload',
        type: 'POST',
        cache: false,
        dataType: 'json',
        data: {
            images: images,
            fileName: fileName,
            msgId: msgId,
            sessionId: '<0.29607.1>'
             // sessionId:currentSession
        },
        error: function () {
        },
        success: function (response) {
            $("#" + msgId + "_capture_imageloader").remove();
            var file = response.files;
            if (response.status == "200") {
                addDoc(file, msgId);
            } else {
                for (var index = 0; index < file.length; index++) {
                    $("#" + file[index].msgId + "_msg a").removeAttr('href');
                    document.getElementById(file[index].msgId + "_thumb").src = "webchat/assets/images/error.png";
                }
            }
        }
    });
}

// profile pic upload
var msgId = '';
$(document).ready(function () {
    $('#profilepicUpload').fileupload(
        {
            add: function (e, data) {
                var goUpload = true;
                var uploadFile = data.files[0];
                var ext = uploadFile.name.split('.').pop();
                if (!(/\.(png|jpg|jpeg|bmp)$/i).test(uploadFile.name)) {
                    printFileError(FILE_TYPE_ERROR);
                    goUpload = false;
                } else if (uploadFile.size > 20000000) { // 20mb
                    printFileError(FILE_SIZE_ERROR);
                    goUpload = false;
                }
                if (goUpload == true) {
                    msgId = $.now();
                    cropPic(uploadFile, msgId);
                    return;
                }
            },
            success: function (ignore) {

            }
        });
});


function cropPic(uploadFile, msgId) {
    console.log("inside crop...");
    var imagesize = $('img').width();
    $('.zoomout').on('click', function () {
        imagesize = imagesize - 5;
        $('img').width(imagesize);
    });
    $('.zoomin').on('click', function () {
        imagesize = imagesize + 5;
        $('img').width(imagesize);
    });
    $('.basic-result').css({'display': 'block'});
    $('.croppie-container ').css({'display': 'block'});
    $('#userCroppedPic').html('');
    $('#userCroppedPic').css({'display': 'none'});
    $('.zoomout').css({'display': 'block'});
    $('.zoomin').css({'display': 'block'});
    var oFReader = new FileReader();
    oFReader.readAsDataURL(uploadFile);
    oFReader.onload = function (oFREvent) {
        $('#userpic').attr('src', oFREvent.target.result);
        var basic = $('#userpic').croppie({
            viewport: {
                width: 175,
                height: 175,
            },
            showZoomer: true
        });
        basic.croppie('result', {
            type: 'canvas',
            size: 'viewport',
            circle: 'false',
            format: 'png',
            quality: '1',
            width: 175,
            height: 175
        });
        basic.croppie('bind', {
            url: oFREvent.target.result,
            points: [77, 469, 280, 739]
        });
        var isSubmitted = false;
        $('.basic-result').click(function () {
            basic.croppie('result', 'base64').then(function (profileBase64) {
                if(isSubmitted) return;
                console.log(" basic.croppie");
                $('#userCroppedPic').attr('src', profileBase64);
                $('#userCroppedPic').css({'display': 'block'});
                $('.croppie-container').css({'display': 'none'});
                $('.cr-boundary').html('');
                $('.cr-boundary').css({'display': 'none'});
                $('.cr-slider-wrap').html('');
                $('.cr-slider-wrap').css({'display': 'none'});
                $('.croppie-container').find('.croppie-container').css({'display': 'none'});
                $('.basic-result').css({'display': 'none'});
                //$('.basic-result').remove();
                /*
                 $('.zoomout').css({'display':'none'});
                 $('.zoomin').css({'display':'none'});*/
                upload_profilepic(profileBase64, msgId);
                isSubmitted = true;
            });
        });
    };
}

// group profile pic upload
var msgId = '';
$(document).ready(function () {
    $('#GrprofilepicUpload').fileupload(
        {
            add: function (e, data) {
                var goUpload = true;
                var uploadFile = data.files[0];
                var ext = uploadFile.name.split('.').pop();
                if (!(/\.(png|jpg|jpeg|bmp)$/i).test(uploadFile.name)) {
                    printFileError(FILE_TYPE_ERROR);
                    goUpload = false;
                } else if (uploadFile.size > 20000000) { // 20mb
                    printFileError(FILE_SIZE_ERROR);
                    goUpload = false;
                }
                if (goUpload == true) {
                    msgId = $.now();
                    cropGroupPic(uploadFile, msgId);
                    return;
                }
            },
            success: function (ignore) {

            }
        });
});

function cropGroupPic(uploadFile, msgId) {
    console.log("inside cropGroupPic...");
    var imagesize = $('img').width();
    $('.zoomout').on('click', function () {
        imagesize = imagesize - 5;
        $('img').width(imagesize);
    });
    $('.zoomin').on('click', function () {
        imagesize = imagesize + 5;
        $('img').width(imagesize);
    });
    $('.basic-result').css({'display': 'block'});
    $('.croppie-container ').css({'display': 'block'});
    $('#groupUserCroppedPic').html('');
    $('#groupUserCroppedPic').css({'display': 'none'});
    $('.zoomout').css({'display': 'block'});
    $('.zoomin').css({'display': 'block'});
    var oFReader = new FileReader();
    oFReader.readAsDataURL(uploadFile);
    oFReader.onload = function (oFREvent) {
        $('#grpuserpic').attr('src', oFREvent.target.result);
        var basic = $('#grpuserpic').croppie({
            viewport: {
                width: 175,
                height: 175,
            },
            showZoomer: true
        });
        basic.croppie('result', {
            type: 'canvas',
            size: 'viewport',
            circle: 'false',
            format: 'png',
            quality: '1',
            width: 175,
            height: 175
        });
        basic.croppie('bind', {
            url: oFREvent.target.result,
            points: [77, 469, 280, 739]
        });
        var isSubmitted = false;
        $('.basic-result').click(function () {
            basic.croppie('result', 'base64').then(function (profileBase64) {
                if(isSubmitted) return;
                console.log(" basic.croppie");
                $('#groupUserCroppedPic').attr('src', profileBase64);
                $('#groupUserCroppedPic').css({'display': 'block'});
                $('.croppie-container').css({'display': 'none'});
                $('.cr-boundary').html('');
                $('.cr-boundary').css({'display': 'none'});
                $('.cr-slider-wrap').html('');
                $('.cr-slider-wrap').css({'display': 'none'});
                $('.croppie-container').find('.croppie-container').css({'display': 'none'});
                $('.basic-result').css({'display': 'none'});
                //$('.basic-result').remove();
                /*
                 $('.zoomout').css({'display':'none'});
                 $('.zoomin').css({'display':'none'});*/
                upload_group_profilepic(profileBase64, msgId);
                isSubmitted = true;
            });
        });
    };
}

// group profile pic upload at create group.
var msgId = '';
$(document).ready(function () {
    $('#profilepicUploadgroup').fileupload(
        {
            add: function (e, data) {
                var goUpload = true;
                var uploadFile = data.files[0];
                var ext = uploadFile.name.split('.').pop();
                if (!(/\.(png|jpg|jpeg|bmp)$/i).test(uploadFile.name)) {
                    printFileError(FILE_TYPE_ERROR);
                    goUpload = false;
                } else if (uploadFile.size > 20000000) { // 20mb
                    printFileError(FILE_SIZE_ERROR);
                    goUpload = false;
                }
                if (goUpload == true) {
                    msgId = $.now();
                    cropGroupPic1(uploadFile, msgId, uploadFile.name);
                    return;
                }
            },
            success: function (ignore) {

            }
        });
});

function cropGroupPic1(uploadFile, msgId,fileName) {
    console.log("inside cropGroupPic1...");
    var imagesize = $('img').width();
    $('.zoomout').on('click', function () {
        imagesize = imagesize - 5;
        $('img').width(imagesize);
    });
    $('.zoomin').on('click', function () {
        imagesize = imagesize + 5;
        $('img').width(imagesize);
    });
    $('.basic-result').css({'display': 'block'});
    $('.croppie-container ').css({'display': 'block'});
    $('#groupUserCroppedPic1').html('');
    $('#groupUserCroppedPic1').css({'display': 'none'});
    $('.zoomout').css({'display': 'block'});
    $('.zoomin').css({'display': 'block'});
    var oFReader = new FileReader();
    oFReader.readAsDataURL(uploadFile);
    oFReader.onload = function (oFREvent) {
        $('#grpuserpic1').attr('src', oFREvent.target.result);
        var basic = $('#grpuserpic1').croppie({
            viewport: {
                width: 175,
                height: 175,
            },
            showZoomer: true
        });
        basic.croppie('result', {
            type: 'canvas',
            size: 'viewport',
            circle: 'false',
            format: 'png',
            quality: '1',
            width: 175,
            height: 175
        });
        basic.croppie('bind', {
            url: oFREvent.target.result,
            points: [77, 469, 280, 739]
        });
        var isSubmitted = false;
        $('.basic-result').click(function () {
            basic.croppie('result', 'base64').then(function (profileBase64) {
                if(isSubmitted) return;
                console.log(" basic.croppie");
                $('#groupUserCroppedPic1').attr('src', profileBase64);
                $('#groupUserCroppedPic1').css({'display': 'block'});
                $('.croppie-container').css({'display': 'none'});
                $('.cr-boundary').html('');
                $('.cr-boundary').css({'display': 'none'});
                $('.cr-slider-wrap').html('');
                $('.cr-slider-wrap').css({'display': 'none'});
                $('.croppie-container').find('.croppie-container').css({'display': 'none'});
                $('.basic-result').css({'display': 'none'});
                //$('.basic-result').remove();
                /*
                 $('.zoomout').css({'display':'none'});
                 $('.zoomin').css({'display':'none'});*/
                upload_group_profilepic1(profileBase64, msgId,fileName);
                isSubmitted = true;
            });
        });
    };
}


function getSessionId() {
    sessionQueryId = '';
    sessionQueryId = $.now();
    client.sendIq({
        id: sessionQueryId,
        type: 'set',
        opcode: 'get_sessionid',
        xmlns: 'urn:xmpp:ciaoim:2',
    }, function (err, res) {
    });
}

function file_Upload() {
    $('#file_Upload').fileupload(
        {
            add: function (e, data) {
                stopimgAjax = null;
                var goUpload = true;
                var uploadFile = data.files[0];
                var ext = uploadFile.name.split('.').pop();
                if (!(/\.(doc|docx|pdf|mp3|txt)$/i).test(uploadFile.name)) {
                    printFileError(FILE_TYPE_ERROR);
                    goUpload = false;
                } else if (uploadFile.size > 20000000) { // 20mb
                    printFileError(FILE_SIZE_ERROR);
                    goUpload = false;
                }
                if (goUpload == true) {
                    msgId = $.now();
                    if(ext=='mp3')
                        var filename = uploadFile.name.split('.')[0]+'.3gp';
                    else
                        var filename = uploadFile.name;
                    addFileLoader(msgId, filename, uploadFile.size, ext, uploadFile);
                }
            },
            success: function (response) {
            }
        });
}

function video_Upload() {$('#video_Upload').fileupload(
    {
        add: function (e, data) {
            var goUpload = true;
            var uploadsFile = data.files[0];
            var ext = uploadsFile.name.split('.').pop();
            if (!(/\.(mp4)$/i).test(uploadsFile.name)) {
                printFileError(FILE_TYPE_ERROR);
                goUpload = false;
            } else if (uploadsFile.size > 20000000) { // 20mb
                printFileError(FILE_SIZE_ERROR);
                goUpload = false;
            }
            if (goUpload == true) {
                msgId = $.now();
                if (["mp4"].indexOf(ext) >= 0) {
                    addVideoFileLoader(msgId, uploadsFile.name, uploadsFile);
                } else {
                    addFileLoader(msgId);
                }
            }
        },
        success: function (response) {
        }
    });
}

$(document).ready(function () {
    // var input = document.getElementById("video_Upload");
    // 	input.addEventListener('change', function (event) {
    // 		var file = this.files[0];
    // 		var url = URL.createObjectURL(file);
    // 		console.log("hello in addlistener");
    // 		var video = document.createElement('video');
    // 		video.src = url;
    // 		var snapshot = function () {
    // 			var canvas = document.createElement('canvas');
    // 			var ctx = canvas.getContext('2d');
    // 			ctx.drawImage(video, 0, 0, canvas.width, canvas.height);
    // 			var fileUrlVideo= canvas.toDataURL('image/png');
    // 			imageSrcData.shrinked=fileUrlVideo.split(',').pop();
    // 			imageSrcDataSize=imageSrcData.shrinked.length;
    // 			video.removeEventListener('canplay', snapshot);
    // 		};
    // 		video.addEventListener('canplay', snapshot);
    // 	});
});

function addVideoFileLoader(msgId, fileName, uploadFile) {
    imageData.base64 = '';
    imageData.size='';
    var oFReader = new FileReader();
    oFReader.readAsDataURL(uploadFile);
    oFReader.onload = function (oFREvent) {
        var messageId = msgId + "_msgvideo";
        var cont = $('#chats');
        var list = $('.chats', cont);
        setDayNotification();
        var tpl = '';
        tpl += '<li class="out unselect"><div class="bubble-video">';
        if (chatWithContact.selectMode == SELECT_MODE.GROUP) {
            getGrpChatHeader(MSG_CATEGORY.groupchat, '');
        }
        tpl += '<div class="image-thumb"><span class="body image" id="img_div" >';
        tpl += '<video id="' + msgId + '_msgvideo" src="' + oFREvent.target.result + '"  style="min-height: 200px;min-width: 200px;max-height: 200px;max-width: 200px;cursor: pointer;"  controls="controls"></video>';
        tpl += '<div class="loder_position" id="' + msgId + '_loder_position">';
        tpl += '<p id="' + msgId + '_upload_videoicon" onclick="send_again(' + msgId + ',\'video\')">';
        tpl += '<p id="' + msgId + '_videoloader" class="loader_set_video">';
        tpl += '<button id="' + msgId + '_stopvideo" class="btn_set" onclick="">';
        tpl += '<img src="webchat/assets/images/crosss.png"  width="27px" height="27px" style="vertical-align: middle;"/>';
        tpl += '<svg class="spinner-container" viewBox="0 0 44 44" style="position: absolute;top: -7px;left: -22px;">';
        tpl += '<circle class="path" cx="22" cy="22" r="20" fill="none" stroke-width="3"></circle></svg></button>';
        tpl += '<span class="sr-only">Loading...</span></p></p></div></span>';
        tpl += '<span class="sr-only" >Loading...</span> </span>';
        tpl += '<div class="shade"></div></div>';
        tpl += '<div class="message-meta">';
        tpl += '<span class="datetime_img">' + get_date('current', null) + '</span>';
        tpl += '<span id="' + msgId + '_dot" class="fa fa-clock-o clock_status" aria-hidden="true"/></div></div><div class="clearfix"></div></li>';
        imageData.base64 = oFREvent.target.result;
        imageData.size = uploadFile.size;
        updateRecentMap(chatWithContact.userId, 'Image', msgId, 'video', 'out', 'current');
        add_to_view(tpl, TIMETYPE.CURRENT);
        uploadImage(oFREvent.target.result, msgId, fileName);
        $('#' + msgId + '_stopvideo').attr('onclick', 'stoploader(' + msgId + ')');
        $('#demo').hide();
        $('#plusIcon').show();
        $('#crossIcon').hide();
    };
}

function addFileLoader(msgId, fileName, fileSize, fileExtention, uploadFile) {
    imageData.base64='';
    imageData.size='';
    var oFReader = new FileReader();
    oFReader.readAsDataURL(uploadFile);
    oFReader.onload = function (oFREvent) {
        var cont = $('#chats');
        var list = $('.chats', cont);
        setDayNotification();
        var tpl = '';
        /*  new mp3 UI  */
        // if(fileExtention=='mp3'){
        //     tpl += '<li class="out unselect"><div class="message">';// + grpChatHeader;
        //     if (chatWithContact.selectMode == SELECT_MODE.GROUP) {
        //         tpl += getGrpChatHeader(MSG_CATEGORY.groupchat, '');
        //     }
        //     tpl += '<span class="body image" id="img_div" style="position: relative;height: 53px;">';
        //     tpl += '<div id="' + msgId + '_preview" data-fileName="' + fileName + '" data-fileSize="' + fileSize + '" data-fileType="' + fileExtention + '">';
        //     tpl += '<audio id=id="' + msgId + '_msgaudio" preload="true"><source src="' + oFREvent.target.result + '"></audio>';
        //     tpl += '<div style="width: 63px;float: left;position: relative;">';
        //     tpl += '<img id="" class="avtar_contact profile_contact_default" alt="" src="/webchat/assets/images/mp3.png" >';
        //     tpl += '</div>';
        //     tpl += '<div id="' + msgId + 'audioplayer" class="audioplayer">';
        //     tpl += '<button id="' + msgId + 'pButton" class="play" onclick="playOut(' + msgId + ')"></button>';
        //     tpl += '<div id="' + msgId + 'timeline" class="timeline">';
        //     tpl += '<div id="' + msgId + 'playhead" class="playhead"></div></div></div></span>';
        //     tpl += '<div class="timeicon_set">';
        //     tpl += '<span class="datetime">' + get_date('current', null) + '</span>';
        //     tpl += '<span id="' + msgId + '_dot" class="fa fa-clock-o clock_status" aria-hidden="true"/>';
        //     tpl += '</div>';
        //    tpl += '</div></li>';
        // }else {
        tpl += '<li class="out unselect"><div class="file_width">';
        if (chatWithContact.selectMode == SELECT_MODE.GROUP) {
            tpl += getGrpChatHeader(MSG_CATEGORY.groupchat, '');
        }
        tpl += '<div class="document-container"><div class="document-container-padding"><div class="file-icon">';
        tpl += '<img id="' + msgId + '_msg"  src="' + getFileIcon(fileExtention) + '" onerror="imageerrhandle(this,\'' +fileExtention+ '\')" width="30px" height="30px" ></div><div class="file-icon1">';
        tpl += '<span><h1 class="fileNameSet">' + fileName + '</h1></span></div><div class="file-icon2">';
        tpl += '<div id="' + msgId + '_preview" data-fileName="' + fileName + '" data-fileSize="' + fileSize + '" data-fileType="' + fileExtention + '">';
        tpl += '<div class="loder_position_file" id="' + msgId + '_loder_position">';
        tpl += '<p id="' + msgId + '_uploadiconfile" src="' + oFREvent.target.result + '" onclick="send_again(' + msgId + ',\'file\')"><p id="' + msgId + '_msgLoder" class="loader_set_file"><button id="' + msgId + '_stopfilebutton" class="btn_set" onclick=""><img src="webchat/assets/images/crosss.png" width="27px" height="27px"/>';
        tpl += '<svg class="spinner-container" viewBox="0 0 44 44" style="position: absolute;top: -7px;left: -22px;">';
        tpl += '<circle class="path" cx="22" cy="22" r="20" fill="none" stroke-width="3"></circle></svg></button>';
        tpl += '<span class="sr-only">Loading...</span></p></p></div></div></div></div></div>';
        tpl += '<div class="footer_file ">';
        tpl += '<span class="text_size">' + fileExtention + ' - ' + getFileSize(fileSize) + '</span>';
        tpl += '<span class="datetime">' + get_date('current', null) + '</span>';
        tpl += '<span id="' + msgId + '_dot" class="fa fa-clock-o clock_status" aria-hidden="true"/></div></div><div class="clearfix"></div></li>';
        // }
        imageData.base64 = oFREvent.target.result;
        imageData.size = fileSize;
        updateRecentMap(chatWithContact.userId, 'file', msgId, 'file', 'out', 'current');
        add_to_view(tpl, TIMETYPE.CURRENT);
        uploadImage(oFREvent.target.result, msgId, fileName);
        $('#' + msgId + '_stopfilebutton').attr('onclick', 'stoploader(' + msgId + ')');
        $('#demo').hide();
        $('#plusIcon').show();
        $('#crossIcon').hide();
    };
}

function addImageLoader(msgId, uploadFile, fileName, filetype, filesize) {
    var oFReader = new FileReader();
    oFReader.readAsDataURL(uploadFile);
    oFReader.onload = function (oFREvent) {
        resizedataURL(oFREvent.target.result,msgId, fileName, filetype, filesize);
    };
}

function resizedataURL(datas,msgId, fileName, filetype, filesize)
{
    var img = document.createElement('img');
    img.onload = function()
    {
        var canvas = document.createElement('canvas');
        var ctx = canvas.getContext('2d');
        canvas.width = 10;//wantedWidth;
        canvas.height = 10;//wantedHeight;
        ctx.drawImage(this, 0, 0, canvas.width, canvas.height);
        var dataURI = canvas.toDataURL();

        setDayNotification();

        var tpl = '';
        var cont = $('#chats');
        var list = $('.chats', cont);

        tpl += '<li class="out unselect">';
        tpl += '<div class="bubble-image">';
        // tpl += '<div  class="forward_icon_set">';
        // tpl += "<div id='" + msgId + "_fwdmsg'></div>";//forward
        // tpl += '</div>';
        tpl += '<div class="image-thumb">';
        tpl += '<span class="body image" id="img_div" ><div><img id="' + msgId + '_preview" data-fileName="' + fileName + '" data-fileSize="' + filesize + '" data-fileType="' + filetype + '" data-URL="'+datas+'" src="' + datas + '" style="min-width: 330px;"></div>' +
            '<div class="loder_position" id="' + msgId + '_loder_position"><p id="' + msgId + '_uploadicon" onclick="send_again(' + msgId + ',\'image\')">' +
            '<p id="' + msgId + '_imageloader" class="loader_set"><button id="' + msgId + '_stopbutton" class="btn_set" onclick=""><img src="webchat/assets/images/crosss.png"  width="27px" height="27px" style="vertical-align: middle;"/>';
        tpl += '<svg class="spinner-container" viewBox="0 0 44 44" style="position: absolute;top: -7px;left: -22px;">';
        tpl += '<circle class="path" cx="22" cy="22" r="20" fill="none" stroke-width="3"></circle></svg></button>';
        tpl += '<span class="sr-only">Loading...</span></p></p></div></span>';
        tpl += '<span class="sr-only" >Loading...</span>';
        tpl += '<div class="shade"></div>';
        tpl += '</div>';
        tpl += '<div class="message-meta">';
        tpl += '<span class="datetime_img">' + get_date('current', null) + '</span>';
        tpl += '<span id="' + msgId
            + '_dot" class="fa fa-clock-o clock_status_img" aria-hidden="true"/>';
        tpl += '</div>';
        tpl += '</div><div class="clearfix"></div>';
        tpl += '</li>';
        add_to_view(tpl, TIMETYPE.CURRENT);
        uploadImage(datas, msgId, fileName);
        $('#' + msgId + '_stopbutton').attr('onclick', 'stoploader(' + msgId + ')');
        // });
        $('#demo').hide();
        $('#plusIcon').show();
        $('#crossIcon').hide();
        console.log('old size '+datas.length+' new size '+dataURI.length);
        imageData.base64=dataURI;
        imageData.size=dataURI.length;
    };
    img.src = datas;
}

function splitBase64(base64) {
    return base64.split(",").pop();
}

function stoploader(msgId) {
    $("#" + msgId + "_uploadicon").attr('class', 'upload_Icon_Image');
    $("#" + msgId + "_imageloader").remove();
    $("#" + msgId + "_uploadiconfile").attr('class', 'upload_Icon_File');
    $("#" + msgId + "_msgLoder").remove();
    $("#" + msgId + "_upload_videoicon").attr('class', 'upload_Icon_Video');
    $("#" + msgId + "_videoloader").remove();
    stopimgAjax.abort();
}

function send_again(msgId, filetype) {
    stopimgAjax == null;
    $("#" + msgId + "_uploadicon").remove();
    $("#" + msgId + "_uploadiconfile").remove();
    $("#" + msgId + "_upload_videoicon").remove();
    var filethumb = $("#" + msgId + "_preview").attr('src');
    var type = 'chat';
    var messageTime = $.now();
    if (chatWithContact.selectMode == SELECT_MODE.CHAT)
        var msgCategory = 'single';
    if (chatWithContact.selectMode == SELECT_MODE.GROUP)
        var msgCategory = 'group';
    if (filetype == 'file') {
        var fileName = $("#" + msgId + "_preview").attr('data-fileName');
        var fileSize = $("#" + msgId + "_preview").attr('data-fileSize');
        var fileType = $("#" + msgId + "_preview").attr('data-fileType');
        $("#" + msgId + "_loder_position").html('<p id="' + msgId + '_uploadiconfile"  onclick="send_again(' + msgId + ',\'file\')">' +
            '<p id="' + msgId + '_msgLoder" class="loader_set_file"><button id="' + msgId + '_stopfilebutton" class="btn_set" onclick="stoploader(' + msgId + ')"><img src="webchat/assets/images/crosss.png" width="27px" height="27px"/>'+
            '<svg class="spinner-container" viewBox="0 0 44 44" style="position: absolute;top: -7px;left: -22px;">'+
            '<circle class="path" cx="22" cy="22" r="20" fill="none" stroke-width="3"></circle></svg></button>'+
            '<span class="sr-only">Loading...</span></p></p></div>');
    }
    else if (filetype == 'image') {
        var fileName = $("#" + msgId + "_preview").attr('data-fileName');
        var fileSize = $("#" + msgId + "_preview").attr('data-fileSize');
        var fileType = $("#" + msgId + "_preview").attr('data-fileType');
        $("#" + msgId + "_loder_position").html('<p id="' + msgId + '_uploadicon" onclick="send_again(' + msgId + ',\'image\')">' +
            '<p id="' + msgId + '_imageloader" class="loader_set"><button id="' + msgId + '_stopbutton" class="btn_set" onclick="stoploader(' + msgId + ')"><img src="webchat/assets/images/crosss.png"  width="27px" height="27px" style="vertical-align: middle;"/>'+
            '<svg class="spinner-container" viewBox="0 0 44 44" style="position: absolute;top: -7px;left: -22px;">'+
            '<circle class="path" cx="22" cy="22" r="20" fill="none" stroke-width="3"></circle></svg></button>'+
            '<span class="sr-only">Loading...</span></p></p></div>');
    }
    else if (filetype == 'video') {
        $("#" + msgId + "_loder_position").html('<p id="' + msgId + '_upload_videoicon" onclick="send_again(' + msgId + ',\'video\')">' +
            '<p id="' + msgId + '_videoloader" class="loader_set_video"><button id="' + msgId + '_stopvideo" class="btn_set" onclick="stoploader(' + msgId + ')"><img src="webchat/assets/images/crosss.png"  width="27px" height="27px" style="vertical-align: middle;"/>'+
            '<svg class="spinner-container" viewBox="0 0 44 44" style="position: absolute;top: -7px;left: -22px;">'+
            '<circle class="path" cx="22" cy="22" r="20" fill="none" stroke-width="3"></circle></svg></button>'+
            '<span class="sr-only">Loading...</span></p></p></div>');
    }
    uploadImage(filethumb, msgId, fileName);
    client.sendMessage({
        to: chatWithContact.userId + "@" + domain,
        // from: jid,
        body: messageTime,
        nick: userNickName,
        request: '',
        id: messageTime,
        type: type,
        bodyType:MSG_TYPE.image,
        fileSize:fileSize,
        fileName:fileName,
        thumb:splitBase64(filethumb),
        msgCategory:msgCategory,
        phone:phone
    });
    updatLastMessage(chatWithContact.userId, '[image]', 'image', msgId, 'out', 'current');
}

function fwdpopup(msgId) {
    list_for_fwd(phnbookMap, msgId);
}
function list_for_fwd(data, msgId) {
    var tplc = "";
    if (data.length != 0) {
        tplc += "<div class='select_contact'>";
        tplc += '<ul class="media-list contactList">';
        var i = 1;
        $.each(data, function (key, val) {
            var avtar = "/webchat/assets/images/user.png";
            if (val.avtar != null && val.avtar != '') {
                avtar = val.avtar;
            }
            tplc += "<li class='media '  id='" + data[key].userId + "_divContact' data-target='#myModalFwdImageConfirm' onclick='getNameToFwd(\"" + data[key].nick + "\",\"" + data[key].userId + "\",\"" + msgId + "\")' data-toggle='modal' data-dismiss='modal'>";
            tplc += "<div class='contact_list' style='width:100%;height:66px;padding: 5px 0 ;'>";
            tplc += "<div class='compatible' id='" + data[key].userId + "_avtar_contact_shared' style='width:73px;float:left;padding: 5px 10px !important;'>";
            tplc += "<img class='media-object ' id='" + data[key].userId + "_avtar_contact_share' src='" + getPhonebookMap(data[key].userId).avatarURL + "' alt='' onerror='imgError(this,\"" + data[key].userId + "\");'>";
            tplc += "</div>";
            tplc += "<div class='chat_body_width'>";
            tplc += "<div class='contactlist_panal'>";
            tplc += "<h4 class='contact_name'>" + data[key].nick + "</h4>";
            tplc += "<p class='contact_name_set'>" + showLimitedName(data[key].status) + "</p>";
            tplc += "</div>";
            tplc += "</div>";
            tplc += "</div>";
            tplc += "</li>";
            i++;
        });
    } else {
    }
    tplc += "</ul>";
    tplc += "</div>";
    $(".chat_usersc").empty();
    $(".chat_usersc").append(tplc);
}

function getNameToFwd(nick, idtofwd, msgID) {
    this.idtofwd = idtofwd;
    this.nickfwd = nick;
    $('#confirm-box').html(' <h1 id="fwdname">Forward image to "' + nick + '" ?</h1><div class="btn_float"><button type="submit" class="btn btn-white"  data-dismiss="modal">Cancel</button>' +
        "<button type='submit' class='btn btn-primary'  data-dismiss='modal' onclick='sendImageAgain(\"" + msgID + "\",\"" + idtofwd + "\",\"" + nick + "\");'>OK</button></div>");

}

function sendImageAgain(msgId, idtofwd, nick) {
    var fileName = $("#" + msgId + "_preview").attr('data-fileName');
    var fileSize = $("#" + msgId + "_preview").attr('data-fileSize');
    var fileType = $("#" + msgId + "_preview").attr('data-fileType');
    var filethumb = $("#" + msgId + "_preview").attr('src');
    var type = 'chat';
    var timeType = 'new';
    var messageTime = $.now();
    var thumb = filethumb.split(",").pop();
    if (chatWithContact.selectMode == SELECT_MODE.CHAT)
        var msgCategory = 'single';
    if (chatWithContact.selectMode == SELECT_MODE.GROUP)
        var msgCategory = 'group';
    client.sendMessage({
        to: idtofwd + "@" + domain,
        from: jid,
        body: messageTime,//filekeys.bodytagkey,
        nick: userNickName,
        request: '',
        id: messageTime,
        type: type
    }, 'image', fileSize, fileName, thumb, '', '', msgCategory);
        onStartChat(idtofwd);
    add_to_view(display_file_out(fileName, msgId, '1', thumb, 'out', timeType, messageTime, fileSize));
    updatLastMessage(idtofwd, '[image]', 'image', msgId, 'out', 'current');
}

function showLimitedName(str) {
    if (str.length > 10)
        str = str.substring(0, 10) + ' ...';
    return str;
}

// when message is send
function addDoc(file, msgId) {
    var fileExtt = file.substr(file.lastIndexOf('.') + 1);
    if(fileExtt == '3gp'){
        send_message(file, msgId, getFileType(fileExtt));
        displayThumb(file);
        return;
    }

    var fileKey = file.split(".").pop();
    if (send_message(file, msgId, getFileType(fileExtt))) {
        displayThumb(file);
    }
}

function getFileType(fileExtt) {
    switch (fileExtt){
        case 'jpg':
        case 'bmp':
        case 'jpeg':
        case 'png':
            return MSG_TYPE.image;
        case 'mp3':
            return MSG_TYPE.audio;
        case 'wav':
        case '3gp':
            return MSG_TYPE.voice;
        case 'mp4':
            return MSG_TYPE.video;
        default:
            return MSG_TYPE.file;
    }
}


//for image gallery open
function addPicEye() {
    $('.isimg').picEyes();
}
function displayThumb(file) {
    var scroll_to_chatHeight = $('#ulchatdemo').prop('scrollHeight') + 'px';
    $("#ulchatdemo").slimScroll({
        scrollTo: scroll_to_chatHeight,
        start: 'bottom'
    });
    var fileExts = file.substr(file.lastIndexOf('.') + 1);
    if (['jpg', 'bmp', 'gif', 'jpeg', 'png'].indexOf(fileExts) >= 0) {
        document.getElementById(msgId + "_preview").setAttribute("data-fileName", file);
        document.getElementById(msgId + "_preview").setAttribute("data-fileSize", imageData.size);
        document.getElementById(msgId + "_preview").setAttribute("data-fileType", fileExts);
        document.getElementById(msgId + "_preview").setAttribute("data-URL", 'data:image/png;base64,' + imageData.base64);
        $("#" + msgId + "_preview").wrap('<span id="img_div" class="body image isimg">').parent();
        $("#" + msgId + "_preview").attr('class', 'image_send_set');
        $("#" + msgId + "_imageloader").remove();
        addPicEye();
    }
    else if (fileExts == MSG_TYPE.pdf) {
        document.getElementById(msgId + "_msg").src = "webchat/assets/images/pdf_icon.png";
        $("#" + msgId + "_preview").attr('class', 'download_Icon');
        $("#" + msgId + "_preview").wrap('<a target="_blank" href="/chat/imagedownload/' + msgId + "_" + file + '">').parent();
        $("#" + msgId + "_msgLoder").remove();//remove loader
    }
    else if (["doc", "docx", "odt",'txt'].indexOf(fileExts) >= 0) {
        document.getElementById(msgId + "_msg").src = "webchat/assets/images/doce_icon.png";
        $("#" + msgId + "_preview").attr('class', 'download_Icon');
        $("#" + msgId + "_preview").wrap('<a target="_blank" href="/chat/imagedownload/' + msgId + "_" + file + '">').parent();
        $("#" + msgId + "_msgLoder").remove();//remove loader
    }
    else if (["mp4"].indexOf(fileExts) >= 0) {
        // document.getElementById(msgId + "_msgvideo").poster = 'data:video/mp4;base64,' + imageData.base64;
        document.getElementById(msgId + "_msgvideo").setAttribute("data-URL", 'fileController/download?msgId=' + file.msgId + '&fileName=' + file.fileName + '&thumbUrl=' + file.fileUrl + '');
        $("#" + msgId + "_videoloader").remove();
    }
    else if (["mp3"].indexOf(fileExts) >= 0) {
        document.getElementById(msgId + "_msg").src = "webchat/assets/images/audio_icon.png";
        $("#" + msgId + "_preview").attr('class', 'download_Icon');
        $("#" + msgId + "_preview").wrap('<a target="_blank" href="/chat/imagedownload/' + msgId + "_" + file + '">').parent();
        $("#" + msgId + "_msgLoder").remove();//remove loader
    }
    else if (["3gp"].indexOf(fileExts) >= 0) {
        document.getElementById(msgId + "_msg").src = "webchat/assets/images/audio_icon.png";
        $("#" + msgId + "_preview").attr('class', 'download_Icon');
        $("#" + msgId + "_preview").wrap('<a target="_blank" href="/chat/imagedownload/' + msgId + "_" + file + '">').parent();
        $("#" + msgId + "_msgLoder").remove();//remove loader
    }
    else if (["wav"].indexOf(fileExts) >= 0) {
        document.getElementById(msgId + "_msgaudio").src = 'data:audio/wav;base64,' + imageData.base64;
        document.getElementById(msgId + "_msgaudio").setAttribute("data-URL", 'fileController/download?msgId=' + file.msgId + '&fileName=' + file.fileName + '&thumbUrl=' + file.fileUrl + '');
        $("#" + msgId + "_msgaudio").wrap('<span id="img_div" class="body image">').parent();
    }
}

function printFileError(msg) {
    alert(msg);
}

function updateRecentsMessage(userId, body, bodyType, msgId, messageTime, chatState,fileName) {
    console.log("updateRecentsMessage::"+userId+" "+bodyType);
    var extension = fileName.substr( fileName.indexOf('.') + 1 );
    var msg = '';
    var text = '';
    if (body.length > 25)    bodyType = bodyType.substring(0, 25) + ' ...';
    if (bodyType == 'text') {
        var emoji = new EmojiConvertor();
        text = emoji.replace_unified(body);
        if (!isSent(chatState)) {
            msg = '<div id="' + msgId + '_dotRecent" ></div>' + body;//+'</div>';
        }
        else {
            msg = '<div id="' + msgId + '_dotRecent" class="fa fa-clock-o clock_status_set" ></div>' + body;//+'</div>';
        }
        $('#'+userId +'_last_msg').attr('title',text);
    }
    else if (bodyType == MSG_TYPE.location) {
        text = 'Location';
        if (!isSent(chatState)) {
            msg = '<div id="' + msgId + '_dotRecent" ></div><div class="background_location dot_sett" ></div> ' + text;
        }
        else {
            msg = '<div id="' + msgId + '_dotRecent" class="fa fa-clock-o clock_status_set"></div><div class="background_location dot_sett" ></div> ' + text;
        }
        $('#'+userId +'_last_msg').attr('title',text);
    }
    else if (bodyType == MSG_TYPE.sticker) {
        text = 'Sticker';
        if (!isSent(chatState)) {
            msg = '<div id="' + msgId + '_dotRecent"></div><div class="background_sticker dot_sett" ></div> ' + text;
        }
        else {
            msg = '<div id="' + msgId + '_dotRecent" class="fa fa-clock-o clock_status_set" ></div><div class="background_sticker dot_sett" ></div> ' + text;
        }
        $('#'+userId +'_last_msg').attr('title',text);
    }
    else if (bodyType == MSG_TYPE.stickerGif) {
        text = 'StickerGif';
        if (!isSent(chatState)) {
            msg = '<div id="' + msgId + '_dotRecent"></div><div class="background_sticker dot_sett" ></div> ' + text;
        }
        else {
            msg = '<div id="' + msgId + '_dotRecent" class="fa fa-clock-o clock_status_set" ></div><div class="background_sticker dot_sett" ></div> ' + text;
        }
        $('#'+userId +'_last_msg').attr('title',text);
    }
    else if (bodyType == MSG_TYPE.contact) {
        console.log("update contact");
        text = 'Contact';
        if (!isSent(chatState)) {
            msg = '<div id="' + msgId + '_dotRecent"></div><div class="background_contact dot_sett" ></div> ' + text;
        }
        else {
            msg = '<div id="' + msgId + '_dotRecent" class="fa fa-clock-o clock_status_set" ></div><div class="background_contact dot_sett" ></div> ' + text;
        }
        $('#'+userId +'_last_msg').attr('title',text);
    }
    else if (bodyType == MSG_TYPE.image) {
        text = 'Image';
        if (!isSent(chatState)) {
            msg = '<div id="' + msgId + '_dotRecent"></div><div class="background_camera dot_set" ></div> ' + text;
        }
        else {
            msg = '<div id="' + msgId + '_dotRecent" class="fa fa-clock-o clock_status_set "></div><div class="background_camera dot_set" ></div> ' + text;
        }
        $('#'+userId +'_last_msg').attr('title',text);
    }
    else if (bodyType == MSG_TYPE.video) {
        text = 'Video';
        if (!isSent(chatState)) {
            msg = '<div id="' + msgId + '_dotRecent"></div><div class="background_video dot_set" ></div> ' + text;
        }
        else {
            msg = '<div id="' + msgId + '_dotRecent" class="fa fa-clock-o clock_status_set "></div><div class="background_video dot_set" ></div> ' + text;
        }
        $('#'+userId +'_last_msg').attr('title',text);
    }
    else if ((bodyType == MSG_TYPE.audio)||((extension=='mp3')||(extension=='wav')||(extension=='3gp'))) {
        text = 'Audio';
        if(extension=='mp3') {
            if (!isSent(chatState)) {
                msg = '<div id="' + msgId + '_dotRecent" ></div><div class="background_audio dot_set" ></div> ' + text;
            }
            else {
                msg = '<div id="' + msgId + '_dotRecent" class="fa fa-clock-o clock_status_set "></div><div class="background_audio dot_set" ></div> ' + text;
            }
            $('#'+userId +'_last_msg').attr('title',text);
        }
        else if(extension=='3gp','wav'){
            if (!isSent(chatState)) {
                msg = '<div id="' + msgId + '_dotRecent" ></div><div class="background_recording dot_set" ></div> ' + text;
            }
            else {
                msg = '<div id="' + msgId + '_dotRecent" class="fa fa-clock-o clock_status_set "></div><div class="background_recording dot_set" ></div> ' + text;
            }
            $('#'+userId +'_last_msg').attr('title',text);
        }
    }

    else if (bodyType == MSG_TYPE.voice) {
        text = 'Audio';
        if (!isSent(chatState)) {
            msg = '<div id="' + msgId + '_dotRecent" ></div><div class="background_recording dot_set" ></div> ' + text;
        }
        else {
            msg = '<div id="' + msgId + '_dotRecent" class="fa fa-clock-o clock_status_set "></div><div class="background_recording dot_set" ></div> ' + text;
        }
        $('#'+userId +'_last_msg').attr('title',text);
    }

    else if ((extension=='pdf')||(extension=='doc')||(extension=='txt')) {
        if (!isSent(chatState)) {
            msg = '<div id="' + msgId + '_dotRecent"></div><div class="background_file dot_setting" ></div> ' + messageTypeByFileName1(body);
        }
        else {
            msg = '<div id="' + msgId + '_dotRecent" class="fa fa-clock-o clock_status_set "></div><div class="background_file dot_setting" ></div> ' + messageTypeByFileName1(body);
        }
        $('#'+userId +'_last_msg').attr('title',text);
    }
    $('#' + userId + '_last_msg').html(msg);
    $('#' + userId + '_time').html(get_date('current', messageTime));

}

function updatLastMessage(userId, lastMessage, bodyType, msgId, msgPos, timeType, messageTime) {
    var msg = '';
    var text = '';
    if (lastMessage.length > 25)    lastMessage = lastMessage.substring(0, 25) + ' ...';
    if (bodyType == 'text') {
        var emoji = new EmojiConvertor();
        text = emoji.replace_unified(lastMessage);
        if (msgPos == 'in') {
            msg = '<div id="' + msgId + '_dotRecent" ></div>' + lastMessage;//+'</div>';
        }
        else {
            msg = '<div id="' + msgId + '_dotRecent" class="fa fa-clock-o clock_status_set" ></div>' + lastMessage;//+'</div>';
        }
        $('#'+userId +'_last_msg').attr('title',text);
    }
    else if (bodyType == MSG_TYPE.location) {
        text = 'Location';
        if (msgPos == 'in') {
            msg = '<div id="' + msgId + '_dotRecent" ></div><div class="background_location dot_sett" ></div> ' + text;
        }
        else {
            msg = '<div id="' + msgId + '_dotRecent" class="fa fa-clock-o clock_status_set"></div><div class="background_location dot_sett" ></div> ' + text;
        }
        $('#'+userId +'_last_msg').attr('title',text);
    }
    else if (bodyType == MSG_TYPE.sticker) {
        text = 'Sticker';
        if (msgPos == 'in') {
            msg = '<div id="' + msgId + '_dotRecent"></div><div class="background_sticker dot_sett" ></div> ' + text;
        }
        else {
            msg = '<div id="' + msgId + '_dotRecent" class="fa fa-clock-o clock_status_set" ></div><div class="background_sticker dot_sett" ></div> ' + text;
        }
        $('#'+userId +'_last_msg').attr('title',text);
    }
    else if (bodyType == MSG_TYPE.stickerGif) {
        text = 'StickerGif';
        if (msgPos == 'in') {
            msg = '<div id="' + msgId + '_dotRecent"></div><div class="background_sticker dot_sett" ></div> ' + text;
        }
        else {
            msg = '<div id="' + msgId + '_dotRecent" class="fa fa-clock-o clock_status_set" ></div><div class="background_sticker dot_sett" ></div> ' + text;
        }
        $('#'+userId +'_last_msg').attr('title',text);
    }
    else if (bodyType == MSG_TYPE.contact) {
        text = 'Contact';
        if (msgPos == 'in') {
            msg = '<div id="' + msgId + '_dotRecent"></div><div class="background_contact dot_sett" ></div> ' + text;
        }
        else {
            msg = '<div id="' + msgId + '_dotRecent" class="fa fa-clock-o clock_status_set" ></div><div class="background_contact dot_sett" ></div> ' + text;
        }
        $('#'+userId +'_last_msg').attr('title',text);
    }
    else if (bodyType == MSG_TYPE.image) {
        text = 'Image';
        if (msgPos == 'in') {
            msg = '<div id="' + msgId + '_dotRecent"></div><div class="background_camera dot_set" ></div> ' + text;
        }
        else {
            msg = '<div id="' + msgId + '_dotRecent" class="fa fa-clock-o clock_status_set "></div><div class="background_camera dot_set" ></div> ' + text;
        }
        $('#'+userId +'_last_msg').attr('title',text);
    }
    else if (bodyType == MSG_TYPE.video) {
        text = 'Video';
        if (msgPos == 'in') {
            msg = '<div id="' + msgId + '_dotRecent"></div><div class="background_video dot_set" ></div> ' + text;
        }
        else {
            msg = '<div id="' + msgId + '_dotRecent" class="fa fa-clock-o clock_status_set "></div><div class="background_video dot_set" ></div> ' + text;
        }
        $('#'+userId +'_last_msg').attr('title',text);
    }
    else if (bodyType == MSG_TYPE.audio) {
        text = 'Audio';
        if (msgPos == 'in') {
            msg = '<div id="' + msgId + '_dotRecent" ></div><div class="background_audio dot_set" ></div> ';
        }
        else {
            msg = '<div id="' + msgId + '_dotRecent" class="fa fa-clock-o clock_status_set "></div><div class="background_audio dot_set" ></div> ' + text;
        }
        $('#'+userId +'_last_msg').attr('title',text);
    }

    else if (bodyType == MSG_TYPE.voice) {
        text = 'Audio';
        if (msgPos == 'in') {
            msg = '<div id="' + msgId + '_dotRecent" ></div><div class="background_recording dot_set" ></div> ' + text;
        }
        else {
            msg = '<div id="' + msgId + '_dotRecent" class="fa fa-clock-o clock_status_set "></div><div class="background_recording dot_set" ></div> ' + text;
        }
        $('#'+userId +'_last_msg').attr('title',text);
    }

    else if (bodyType == MSG_TYPE.file) {
        if (msgPos == 'in') {
            msg = '<div id="' + msgId + '_dotRecent"></div><div class="background_file dot_setting" ></div> ' + messageTypeByFileName1(lastMessage);
        }
        else {
            msg = '<div id="' + msgId + '_dotRecent" class="fa fa-clock-o clock_status_set "></div><div class="background_file dot_setting" ></div> ' + messageTypeByFileName1(lastMessage);
        }
        $('#'+userId +'_last_msg').attr('title',text);
    }
    if (timeType != 'old') {
        $('#' + userId + '_time').html(get_date(timeType, messageTime));
        $('#' + userId + '_last_msg').html(msg);
    }
}

function messageTypeByFileName1(text) {
    var ext = text.substr(text.lastIndexOf('.') + 1);
    if (["jpg", "bmp", "gif", "png", "jpeg"].indexOf(ext) >= 0) {
        return ' Image';
    }
    else if (["pdf"].indexOf(ext) >= 0) {
        return ' Pdf';
    }
    else if (["doc", "docx", "odt"].indexOf(ext) >= 0) {
        return ' Doc';
    }
    else if (["mp4"].indexOf(ext) >= 0) {
        return ' Video';
    }
    else if (["mp3"].indexOf(ext) >= 0) {
        return ' Audio';
    }
    else if (["wav","3gp"].indexOf(ext) >= 0) {
        return ' Voice';
    }
    else {
        return ' File';
    }
}
function messageTypeByFileName(text) {
    var ext = text.split('.').pop();
    if (['jpg', 'bmp', 'gif', 'jpeg', 'png'].indexOf(ext) >= 0) {
        return MSG_TYPE.image;
    }
    else if (ext == MSG_CATEGORY.pdf) {
        return MSG_TYPE.pdf;
    }
    else if (["doc", "docx", "odt"].indexOf(ext) >= 0) {
        return MSG_TYPE.doc;
    }
    else if (["mp4"].indexOf(ext) >= 0) {
        return MSG_TYPE.video;
    }
    else if (["mp3"].indexOf(ext) >= 0) {
        return MSG_TYPE.audio;
    }
    else if (ext=='wav') {
        return MSG_TYPE.voice;
    }
    else if (ext=='3gp') {
        return MSG_TYPE.voice;
    }
    else{
        return MSG_TYPE.unknown;
    }
}
function thumbByMessageType(msgType, thumbBase64String) {
    if (msgType == MESSAGE_TYPE_IMAGE) {
        return thumbBase64String;
    } else if (msgType == MESSAGE_TYPE_VIDEO) {
        return thumbBase64String;
    } else if (msgType == MESSAGE_TYPE_PDF) {
        return "webchat/assets/images/pdf_icon.png";
    } else if (msgType == MESSAGE_TYPE_DOC) {
        return "webchat/assets/images/doce_icon.png";
    }
    else if (msgType == MESSAGE_TYPE_AUDIO) {
        return "webchat/assets/images/audio_icon.png";
    }
    else if (msgType == MESSAGE_TYPE_VOICE) {
        return thumbBase64String;
    }
    return  "webchat/assets/images/file_icon.png";
}

function display_file_out(text, msgID, msgType, thumb, out, timeType, messageTime, fileSize,type,nick) {  //out from phone
    var cont = $('#chats');
    var list = $('.chats', cont);
    var tpl = '';
    var grpChatHeader='';
    var extss = text.split('.').pop();
    if (type == 'groupchat') {
        grpChatHeader = getGrpChatHeader(MSG_CATEGORY.groupchat, '');
    }
    tpl += '<li class="out unselect">';
    if (msgType == MSG_TYPE.image) {
        tpl += '<div class="bubble-image">'+grpChatHeader;
        tpl += '<div  class="forward_icon_set">';
        tpl += "<img src='/webchat/assets/images/fwdicon.png' class='forward_icon' id='fwdicon'  onclick='fwdpopup(\"" + msgID + "\")'  data-toggle='modal' data-target='#myModalForwardImage' aria-hidden='true'/></div>";
        tpl += '<div class="image-thumb">';
        tpl += '<span class="body image isimg" id="img_div"><img class="image_send_set" data-fileName="' + text + '" data-fileSize="' + fileSize + '" data-fileType="image" data-URL="' + 'data:image/png;base64,' + thumb + '" id="' + msgID + '_preview" src="' + 'data:image/png;base64,' + thumb + '"></span>';
        tpl += '<div class="shade"></div></div><div class="message-meta">';
        tpl += '<span class="datetime_img">' + get_date(timeType, messageTime) + '</span>';
        tpl += '<span id="' + msgID + '_dot" class="fa fa-clock-o clock_status" aria-hidden="true"></span>';
        tpl += '</div>';
        tpl += '</div><div class="clearfix"></div>';
        tpl += '</li>';
        addPicEye();
    }
    else if (msgType == MSG_TYPE.video) {
        tpl += '<li class="out"><div class="bubble-video">'+grpChatHeader;
        tpl +='<div class="image-thumb"><span id="img_div" class="body image">';
        tpl += '<video id="' + msgID + '_msgvideo" class="image_send_set" src="data:video/mp4;base64,' + thumb + '" poster="data:video/mp4;base64,' + thumb + '" controls=""></video>';
        tpl += '</span><div class="shade"></div></div><div class="message-meta"><span class="datetime_img">' + get_date(timeType, messageTime) + '</span>';
        tpl += '<span id="' + msgID + '_dot" class="fa fa-clock-o clock_status" aria-hidden="true"></span></div></div></li>';
    }
    else if (msgType == MSG_TYPE.voice||msgType == MSG_TYPE.audio) {
        var s3key = get_s3_key(msgID, msgType, extss);
        get_signed_url(s3key);
        tpl += '<li class="out unselect"><div class="message">' + grpChatHeader;
        tpl += '<span class="body image" id="img_div" style="position: relative;height: 53px;">';
        tpl += '<audio id="' + msgID + '_msg"><source src=""></audio>';
        tpl += '<div id="' + msgID + 'audioplayer" class="audioplayer">';
        tpl += '<button id="' + msgID + 'pButton" class="play" onclick="playOut(' + msgID + ')"></button>';
        tpl += '<div id="' + msgID + 'timeline" class="timeline">';
        tpl += '<div id="' + msgID + 'playhead" class="playhead"></div></div></div></span>';
        tpl += '<div class="timeicon_set">';
        tpl += '<span class="datetime">' + get_date(timeType, messageTime) + '</span>';
        tpl += '<span id="' + msgID + '_dot" class="fa fa-clock-o clock_status" aria-hidden="true"/>';
        tpl += '</div>';
        tpl += '<div style="width: 63px;float: left;position: relative;">';
        tpl += '<img id="" class="avtar_contact profile_contact_default" alt="" src="/webchat/assets/images/user.png" >';
        tpl += '<img id="" class="voiceMemo" alt="" src="/webchat/assets/images/voice_memo.png" width="20px" height="20px" >';
        tpl += '</div></div></li>';
    }
    else {
        /*  new mp3 UI  */
        // tpl += '<div class="message">';
        // tpl += getGrpChatHeader(message.messageCategory, '');
        // tpl += '<span class="body image" id="img_div" style="position: relative;height: 53px;">';
        // tpl += '<audio id="' +msgID + '_msgaudio_in" preload="true">';
        // tpl += '<source id="' + msgID + '_msg" src="">';//src="' + 'data:audio/wav;base64,' + message.thumbNail + '"
        // tpl += '</audio>';
        // tpl += '<div id="'+ msgID +'audioplayer_in" class="audioplayer">';
        // tpl += '<button id="'+ msgID +'pButton_in" class="play_in" onclick="playIn('+ msgID +')"></button>';
        // tpl += '<div id="'+ msgID +'timeline_in" class="timeline">';
        // tpl += '<div id="'+ msgID +'playhead_in" class="playhead" style="background:#2766ff;"></div>';
        // tpl += '</div>';
        // tpl += '</div>';
        // tpl += '<span class="datetime">' + get_date(timeType, messageTime) + '</span>';
        // tpl += '<span id="' + msgID + '_dot" class="fa fa-clock-o clock_status" aria-hidden="true"/>';
        // tpl += '</span>';
        // tpl += '<div style="width: 63px;float: right;position: relative;">';
        // tpl += '<img id="" class="avtar_contact profile_contact_default" alt="" src="/webchat/assets/images/mp3.png" height="30px" width="30px">';
        // tpl += '</div>';
        // tpl += '</div>';
        // tpl += '</li>';
        tpl += '<li class="out"><div class="file_width">'+grpChatHeader+'<div class="document-container"><div class="document-container-padding"><div class="file-icon">';
        tpl += '<img id="'+ msgID+ '_msg"  src="' + thumb + '" width="30px" height="30px" ></div><div class="file-icon1">';
        tpl += '<span><h1 style="font-size: 15px;margin-top: 5px;">' + showLimitedName(text) + '</h1></span></div><div class="file-icon2">';
        tpl += '<a target="_blank" href="/chat/imagedownload/' + msgID + "_" + text + '"><img id="'+ msgID+ '_msgLoader"  src="webchat/assets/images/download_icon.png"  width="30px" height="30px"></a>';
        tpl += '</div></div></div><div class="footer_file">';
        tpl += '<span class="text_size">' + extss + ' - ' + getFileSize(fileSize) + '</span>';
        tpl += '<span class="datetime">' + get_date(timeType, messageTime) + '</span>';
        tpl += '<span id="' + msgID + '_dot" class="fa fa-clock-o clock_status" aria-hidden="true"/>';
        tpl += '</div>';
        tpl += '</div><div class="clearfix"></div>';
        tpl += '</li>';
        addPicEye();
    }
    return tpl;
}


function getFileSize(size) {
    var i = Math.floor(Math.log(size) / Math.log(1024));
    return ( size / Math.pow(1024, i) ).toFixed(2) * 1 + ' ' + ['B', 'KB', 'MB', 'GB', 'TB'][i];
}


function updatLastMessageFileReceive(userId, lastMessage, bodyType, msgId, msgPos, timeType) {
    var msg = '';
    if (lastMessage.length > 25) {
        lastMessage = lastMessage.substring(0, 25) + ' ...';
    }
    if (bodyType == MSG_TYPE.image) {
        msg = '<p id="' + msgId + '_dotRecent"></p><p class="background_camera dot_set" ></p> Image';
    }
    else if (bodyType == MSG_TYPE.contact) {
        msg = '<p id="' + msgId + '_dotRecent"></p><p class="background_contact dot_set" ></p> Contact';
    }
    else if (bodyType == MSG_TYPE.video) {
        msg = '<p id="' + msgId + '_dotRecent"></p><p class="background_video dot_set" ></p> Video';
    }
    else if (bodyType == MSG_TYPE.audio) {
        msg = '<p id="' + msgId + '_dotRecent" ></p><p class="background_audio dot_set" ></p> Audio';
    }
    else if (bodyType == MSG_TYPE.voice) {
        msg = '<p id="' + msgId + '_dotRecent" ></p><p class="background_recording dot_set" ></p> Audio';
    }
    else {
        msg = '<p id="' + msgId + '_dotRecent"></p><p class="background_file dot_setting" ></p> ' + messageTypeByFileName1(lastMessage);
    }
    if (timeType != 'old')
        $('#' + userId + '_last_msg').html(msg);
}


