/**
 * Created by khyati on 15/2/17.
 */

function getSticker(sticker,message) {
    var tpl = '';
    if(sticker.split(/_/)[0]=='chipmunk')
    {
        tpl+='<img id="' + message.messageId + '_msg" src="/webchat/assets/images/Stickers/chipmunks/'+sticker+'.png" style="max-height: 120px;max-width: 120px; cursor: pointer;"></span>';
    }
    else if(sticker.split(/_/)[0]=='face'){
        tpl+='<img id="' + message.messageId + '_msg" src="/webchat/assets/images/Stickers/face/'+sticker+'.png" style="max-height: 120px;max-width: 120px; cursor: pointer;"></span>';
    }
    else if(sticker.split(/_/)[0]=='calligraphy'){
        tpl+='<img id="' + message.messageId + '_msg" src="/webchat/assets/images/Stickers/calligraphy/'+sticker+'.png" style="max-height: 120px;max-width: 120px; cursor: pointer;"></span>';
    }
    else if(sticker.split(/_/)[0]=='girl'){
        var src="\'/webchat/assets/images/Stickers/girl/" + sticker + ".gif\'";
        tpl+='<img id="' + message.messageId + '_msg" src="/webchat/assets/images/Stickers/girl/'+sticker+'.gif" onclick="' +
            'moveStickerIn('+message.messageId+','+src+')" style="max-height: 120px;max-width: 120px; cursor: pointer;"></span>';
    }
    else if(sticker.split(/_/)[0]=='love'){
        var src="\'/webchat/assets/images/Stickers/love/" + sticker + ".gif\'";
        tpl+='<img id="' + message.messageId + '_msg" src="/webchat/assets/images/Stickers/love/'+sticker+'.gif" onclick="moveStickerIn('+message.messageId+','+src+')" style="max-height: 120px;max-width: 120px; cursor: pointer;"></span>';
    }
    else if(sticker.split(/_/)[0]=='monkey'){
        var src="\'/webchat/assets/images/Stickers/monkey/" + sticker + ".gif\'";
        tpl+='<img id="' + message.messageId + '_msg" src="/webchat/assets/images/Stickers/monkey/'+sticker+'.gif" onclick="moveStickerIn('+message.messageId+','+src+')" style="max-height: 120px;max-width: 120px; cursor: pointer;"></span>';
    }
    else if(sticker.split(/_/)[0]=='pizza'){
        var src="\'/webchat/assets/images/Stickers/pizza/" + sticker + ".gif\'";
        tpl+='<img id="' + message.messageId + '_msg" src="/webchat/assets/images/Stickers/pizza/'+sticker+'.gif" onclick="moveStickerIn('+message.messageId+','+src+')" style="max-height: 120px;max-width: 120px; cursor: pointer;"></span>';
    }
    else if(sticker.split(/_/)[0]=='zombie'){
        var src="\'/webchat/assets/images/Stickers/zombie/" + sticker + ".gif\'";
        tpl+='<img id="' + message.messageId + '_msg" src="/webchat/assets/images/Stickers/zombie/'+sticker+'.gif" onclick="moveStickerIn('+message.messageId+','+src+')" style="max-height: 120px;max-width: 120px; cursor: pointer;"></span>';
    }
    else{
        var src="\'/webchat/assets/images/Stickers/cat/" + sticker + ".gif\'";
        tpl+='<img id="' + message.messageId + '_msg" src="/webchat/assets/images/Stickers/cat/'+sticker+'.gif" onclick="moveStickerIn('+message.messageId+','+src+')" style="max-height: 120px;max-width: 120px; cursor: pointer;"></span>';
    }
    tpl += '<div class="timeicon_set">';
    tpl += '<span class="datetime unselect">' + get_date(TIMETYPE.OLD, message.sentReceivedTs) + '</span>';
    tpl += '<span id="' + message.messageId + '_dot" class="' + bubbleGetState(message.messageState) + '" aria-hidden="true"/>';
    tpl += '</div>';
    tpl += '</div><div class="clearfix"></div>';
    tpl += '</li>';
    return  tpl;
}

function showSticker() {
    var chipmunkTpl = '';
    //limit value for i depends on no of stickers in each category
    for (var i = 0; i <= 19; i++) {
        if (i % 4 == 0)
            chipmunkTpl += '<div class="contat_attachment"><ul style="padding:0 !important;">';
        chipmunkTpl += '<li data-dismiss="modal"><img src="/webchat/assets/images/Stickers/chipmunks/chipmunk_' + i + '.png"  height="80px" width="80px"alt="" onclick="handleClickSticker(\'chipmunk_' + i + '\')"></li>';
        if (i % 4 == 3)
            chipmunkTpl += '</ul></div>';
        if (i == 19)
            chipmunkTpl += '</div>';
    }

    var stickerTpl = '<div class="modal-dialog modal-sm" style="width:400px;">';
    stickerTpl += '<div class="modal-content" style="width:400px; border-radius: 6px !important;margin:0 auto;">';
    stickerTpl += '<div class="modal-header" style="border-top: none !important; background: #2766ff; min-height: 45px;padding: 12px 15px !important; border-top-left-radius: 4px; border-top-right-radius: 4px;">';
    stickerTpl += '<div>';
    stickerTpl += '<div class="modal_header_text">Stickers</div>';
    stickerTpl += '<div class="fa fa-times close modal_header_icon" data-dismiss="modal"></div>';
    stickerTpl += '</div></div>';
    stickerTpl += '<div class="modal-body" style="padding:0 !important; padding-top:10px !important;">';
    stickerTpl += '<div class="custom-tab-container">';
    stickerTpl += '<div id="tab1" style="height: 368px; overflow-y: auto" >';
    stickerTpl += chipmunkTpl;
    stickerTpl += '</div>';
    stickerTpl += '  </div>';
    stickerTpl += '  <div class="modal-footer custom_footer_stickers">';
    stickerTpl += '  <div class="custom_stickers" >';
    stickerTpl += '  <div class="scroll-arrow" style="left:0;">';
    stickerTpl += '  <div class="left-scroll" onclick="leftScroll()"><i class="fa fa-angle-left" aria-hidden="true"></i></div>';
    stickerTpl += '  </div>';
    stickerTpl += '  <div class="scroll-arrow" style="right:0;">';
    stickerTpl += '  <div class="right-scroll" onclick="rightScroll()"><i class="fa fa-angle-right" aria-hidden="true"></i></div>';
    stickerTpl += '  </div>';
    stickerTpl += '  <ul class="custom-image-tab"  style="display:flex;">';
    stickerTpl += '  <li class="active stickerHide" data-active-tab="1"><img src="/webchat/assets/images/Stickers/teddy.png" onclick="changeStickerTab(\'tab1\')"></li>';
    stickerTpl += '  <li class="stickerHide" data-active-tab="2"><img src="/webchat/assets/images/Stickers/abc.png"onclick="changeStickerTab(\'tab2\')"></li>';
    stickerTpl += '  <li class="stickerHide" data-active-tab="3"><img src="/webchat/assets/images/Stickers/face.png" onclick="changeStickerTab(\'tab3\')"></li>';
    stickerTpl += '  <li class="stickerHide" data-active-tab="4"><img src="/webchat/assets/images/Stickers/cat_inactive.png" onclick="changeStickerTab(\'tab4\')"></li>';
    stickerTpl += '  <li class="stickerHide" data-active-tab="5"><img src="/webchat/assets/images/Stickers/girl_inactive.png" onclick="changeStickerTab(\'tab5\')"></li>';
    stickerTpl += '  <li class="stickerHide" data-active-tab="6"><img src="/webchat/assets/images/Stickers/love_inactive.png" onclick="changeStickerTab(\'tab6\')"></li>';
    stickerTpl += '  <li class="stickerHide" data-active-tab="7"><img src="/webchat/assets/images/Stickers/monkey_inactive.png" onclick="changeStickerTab(\'tab7\')"></li>';
    stickerTpl += '  <li data-active-tab="8"><img src="/webchat/assets/images/Stickers/pizza_inactive.png" onclick="changeStickerTab(\'tab8\')"></li>';
    stickerTpl += '  <li data-active-tab="9"><img src="/webchat/assets/images/Stickers/zombie_inactive.png" onclick="changeStickerTab(\'tab9\')"></li>';
    stickerTpl += '  </ul>';
    stickerTpl += '  </div>';
    stickerTpl += '  </div>';
    stickerTpl += '  </div>';
    stickerTpl += '  </div>';
    stickerTpl += '  </div>';
    $('#mystickers').html(stickerTpl);
bindScroller('tab1');
}
function rightScroll(){
    $(".stickerHide").hide();
}
function leftScroll() {
    $(".stickerHide").show();
}
function changeStickerTab(stickerTab) {
    console.log('changeStickerTab '+stickerTab);
    if (stickerTab == 'tab1') {
        var chipmunkTpl = '';
        chipmunkTpl += '<div id="tab1" class="scroller" style="height: 368px; overflow-y: auto" >';
        for (var i = 0; i <= 19; i++) {
            if (i % 4 == 0)
                chipmunkTpl += '<div class="contat_attachment"><ul style="padding:0 !important;">';
            chipmunkTpl += '<li data-dismiss="modal"><img src="/webchat/assets/images/Stickers/chipmunks/chipmunk_' + i + '.png"  height="80px" width="80px"alt="" onclick="handleClickSticker(\'chipmunk_' + i + '\')"></li>'

            if (i % 4 == 3)
                chipmunkTpl += '</ul></div>';
            if (i == 19)
                chipmunkTpl += '</div>';
        }
        $('.custom-tab-container').html(chipmunkTpl);
        bindScroller('tab1');
    }

    if (stickerTab == 'tab2') {
        var calligraphy = '';
        calligraphy += '  <div id="tab2" class="scroller" style="height: 368px; overflow-y: auto">';
        for (var i = 0; i <= 16; i++) {
            if (i % 4 == 0)
                calligraphy += '<div class="contat_attachment"><ul style="padding:0 !important;">';
            calligraphy += ' <li data-dismiss="modal"><img src="/webchat/assets/images/Stickers/calligraphy/calligraphy_' + i + '.png" height="80px" width="80px" alt="" onclick="handleClickSticker(\'calligraphy_' + i + '\')"></li>';

            if (i % 4 == 3)
                calligraphy += '</ul></div>';
            if (i == 16)
                calligraphy += '</div>';
        }
        $('.custom-tab-container').html(calligraphy);
        bindScroller('tab2');
    }

    if (stickerTab == 'tab3') {
        var face = '';
        face += '<div id="tab3" class="scroller" style="height: 368px; overflow-y: auto">';
        for (var i = 0; i <= 11; i++) {
            if (i % 4 == 0)
                face += '<div class="contat_attachment"><ul style="padding:0 !important;">';
            face += ' <li data-dismiss="modal"><img src="/webchat/assets/images/Stickers/face/face_' + i + '.png" height="80px" width="80px" alt="" onclick="handleClickSticker(\'face_' + i + '\')"></li>';

            if (i % 4 == 3)
                face += '</ul></div>';
            if (i == 11)
                face += '</div>';
        }
        $('.custom-tab-container').html(face);
        bindScroller('tab3');
    }
    if (stickerTab == 'tab4') {
        var catsTpl = '';
        catsTpl += '<div id="tab4" class="scroller" style="height: 368px; overflow-y: auto" >';
        for (var i = 0; i <= 6; i++) {
            if (i % 4 == 0)
                catsTpl += '<div class="contat_attachment"><ul style="padding:0 !important;">';
            catsTpl += '<li data-dismiss="modal"><img src="/webchat/assets/images/Stickers/cat/cat_' + i + '.png"  height="80px" width="80px" alt="" onclick="handleClickStickerGif(\'cat_' + i + '\')"></li>';

            if (i % 4 == 3)
                catsTpl += '</ul></div>';
            if (i == 6)
                catsTpl += '</div>';
        }
        $('.custom-tab-container').html(catsTpl);
        bindScroller('tab4');
    }
    if (stickerTab == 'tab5') {
        var girlTpl = '';
        girlTpl += '<div id="tab5" class="scroller" style="height: 368px; overflow-y: auto" >';
        for (var i = 0; i <= 7; i++) {
            if (i % 4 == 0)
                girlTpl += '<div class="contat_attachment"><ul style="padding:0 !important;">';
            girlTpl += '<li data-dismiss="modal"><img src="/webchat/assets/images/Stickers/girl/girl_' + i + '.png"  height="80px" width="80px" alt="" onclick="handleClickStickerGif(\'girl_' + i + '\')"></li>';

            if (i % 4 == 3)
                girlTpl += '</ul></div>';
            if (i == 7)
                girlTpl += '</div>';
        }
        $('.custom-tab-container').html(girlTpl);
        bindScroller('tab5');
    }
    if (stickerTab == 'tab6') {
        var loveTpl = '';
        loveTpl += '<div id="tab6" class="scroller" style="height: 368px; overflow-y: auto" >';
        for (var i = 0; i <= 7; i++) {
            if (i % 4 == 0)
                loveTpl += '<div class="contat_attachment"><ul style="padding:0 !important;">';
            loveTpl += '<li data-dismiss="modal"><img src="/webchat/assets/images/Stickers/love/love_' + i + '.png"  height="80px" width="80px" alt="" onclick="handleClickStickerGif(\'love_' + i + '\')"></li>';

            if (i % 4 == 3)
                loveTpl += '</ul></div>';
            if (i == 7)
                loveTpl += '</div>';
        }
        $('.custom-tab-container').html(loveTpl);
        bindScroller('tab6');
    }
    if (stickerTab == 'tab7') {
        var monkeyTpl = '';
        monkeyTpl += '<div id="tab7" class="scroller" style="height: 368px; overflow-y: auto" >';
        for (var i = 0; i <= 7; i++) {
            if (i % 4 == 0)
                monkeyTpl += '<div class="contat_attachment"><ul style="padding:0 !important;">';
            monkeyTpl += '<li data-dismiss="modal"><img src="/webchat/assets/images/Stickers/monkey/monkey_' + i + '.png"  height="80px" width="80px" alt="" onclick="handleClickStickerGif(\'monkey_' + i + '\')"></li>';

            if (i % 4 == 3)
                monkeyTpl += '</ul></div>';
            if (i == 7)
                monkeyTpl += '</div>';
        }
        $('.custom-tab-container').html(monkeyTpl);
        bindScroller('tab7');
    }
    if (stickerTab == 'tab8') {
        var pizzaTpl = '';
        pizzaTpl += '<div id="tab8" class="scroller" style="height: 368px; overflow-y: auto" >';
        for (var i = 0; i <= 7; i++) {
            if (i % 4 == 0)
                pizzaTpl += '<div class="contat_attachment"><ul style="padding:0 !important;">';
            pizzaTpl += '<li data-dismiss="modal"><img src="/webchat/assets/images/Stickers/pizza/pizza_' + i + '.png"  height="80px" width="80px" alt="" onclick="handleClickStickerGif(\'pizza_' + i + '\')"></li>';

            if (i % 4 == 3)
                pizzaTpl += '</ul></div>';
            if (i == 7)
                pizzaTpl += '</div>';
        }
        $('.custom-tab-container').html(pizzaTpl);
        bindScroller('tab8');
    }
    if (stickerTab == 'tab9') {
        var zombieTpl = '';
        zombieTpl += '<div id="tab9" class="scroller" style="height: 368px; overflow-y: auto" >';
        for (var i = 0; i <= 7; i++) {
            if (i % 4 == 0)
                zombieTpl += '<div class="contat_attachment"><ul style="padding:0 !important;">';
            zombieTpl += '<li data-dismiss="modal"><img src="/webchat/assets/images/Stickers/zombie/zombie_' + i + '.png"  height="80px" width="80px" alt="" onclick="handleClickStickerGif(\'zombie_' + i + '\')"></li>';

            if (i % 4 == 3)
                zombieTpl += '</ul></div>';
            if (i == 7)
                zombieTpl += '</div>';
        }
        $('.custom-tab-container').html(zombieTpl);
        bindScroller('tab9');
    }

    $("ul.custom-image-tab li").on("click", function () {
        $("ul.custom-image-tab li").removeClass("active");
        $(this).addClass("active");
    });
}

function handleClickSticker(sticker) {
    var msgId = $.now();
    if (send_message(sticker, msgId, 'sticker')) {
        add_to_view(display_sticker_out(sticker, msgId, 'current', null),TIMETYPE.CURRENT);
    }
}
function handleClickStickerGif(sticker) {
    var msgId = $.now();
    if (send_message(sticker, msgId, 'stickerGif')) {
        add_to_view(display_sticker_out(sticker, msgId, 'current', null),TIMETYPE.CURRENT);
    }
}

function moveStickerIn(message,src) {
    $('#' + message+ '_msg').attr('src', '');
    $('#' + message + '_msg').attr('src', src + '?' + Math.random());
}
function moveStickerOut(msgId,src){
    $('#'+msgId+'_msg').attr('src','');
    $('#'+msgId+'_msg').attr('src', src+'?' + Math.random());
}


function display_sticker_out(text, msgId, timeType, messageTime) {
    setDayNotification();
    var tpl = '';
    tpl += '<li class="out unselect">';
    tpl += '<div class="message message-sticker" style="background:none !important;box-shadow:none !important; color:#333;">';
    tpl += '<span class="body"  id="img_div">';
    if (text.split(/_/)[0] == 'chipmunk') {
        tpl += '<img id="' + msgId + '_msg" src="/webchat/assets/images/Stickers/chipmunks/' + text + '.png" style="max-height: 120px;max-width: 120px; cursor: pointer;"></span>';
    }
    else if (text.split(/_/)[0] == 'face') {
        tpl += '<img id="' + msgId + '_msg" src="/webchat/assets/images/Stickers/face/' + text + '.png" style="max-height: 120px;max-width: 120px; cursor: pointer;"></span>';
    }
    else if (text.split(/_/)[0] == 'cat') {
        var src="\'/webchat/assets/images/Stickers/cat/" + text + ".gif\'";
        tpl += '<img id="' + msgId + '_msg" src="/webchat/assets/images/Stickers/cat/' + text + '.gif" onclick="moveStickerOut('+msgId+','+src+')" style="max-height: 120px;max-width: 120px; cursor: pointer;"></span>';
    }
    else if (text.split(/_/)[0] == 'girl') {
        var src="\'/webchat/assets/images/Stickers/girl/" + text + ".gif\'";
        tpl += '<img id="' + msgId + '_msg" src="/webchat/assets/images/Stickers/girl/' + text + '.gif" onclick="moveStickerOut('+msgId+','+src+')" style="max-height: 120px;max-width: 120px; cursor: pointer;"></span>';
    }
    else if (text.split(/_/)[0] == 'love') {
        var src="\'/webchat/assets/images/Stickers/love/" + text + ".gif\'";
        tpl += '<img id="' + msgId + '_msg" src="/webchat/assets/images/Stickers/love/' + text + '.gif" onclick="moveStickerOut('+msgId+','+src+')" style="max-height: 120px;max-width: 120px; cursor: pointer;"></span>';
    }
    else if (text.split(/_/)[0] == 'monkey') {
        var src="\'/webchat/assets/images/Stickers/monkey/" + text + ".gif\'";
        tpl += '<img id="' + msgId + '_msg" src="/webchat/assets/images/Stickers/monkey/' + text + '.gif" onclick="moveStickerOut('+msgId+','+src+')" style="max-height: 120px;max-width: 120px; cursor: pointer;"></span>';
    }
    else if (text.split(/_/)[0] == 'pizza') {
        var src="\'/webchat/assets/images/Stickers/pizza/" + text + ".gif\'";
        tpl += '<img id="' + msgId + '_msg" src="/webchat/assets/images/Stickers/pizza/' + text + '.gif" onclick="moveStickerOut('+msgId+','+src+')" style="max-height: 120px;max-width: 120px; cursor: pointer;"></span>';
    }
    else if (text.split(/_/)[0] == 'zombie') {
        var src="\'/webchat/assets/images/Stickers/zombie/" + text + ".gif\'";
        tpl += '<img id="' + msgId + '_msg" src="/webchat/assets/images/Stickers/zombie/' + text + '.gif" onclick="moveStickerOut('+msgId+','+src+')"style="max-height: 120px;max-width: 120px; cursor: pointer;"></span>';
    }
    else {
        tpl += '<img id="' + msgId + '_msg" src="/webchat/assets/images/Stickers/calligraphy/' + text + '.png" style="max-height: 120px;max-width: 120px; cursor: pointer;"></span>';
    }
    tpl += '<span class="datetime">' + get_date(timeType, messageTime) + '</span>';
    if (timeType == 'current') {
        tpl += '<span id="' + msgId
            + '_dot" class="fa fa-clock-o clock_status" aria-hidden="true"/>';
    }
    if (timeType == 'old') {
        tpl += '<span id="' + msgId
            + '_dot" class="dot background_gray" aria-hidden="true"/>';
    }
    tpl += '</div><div class="clearfix"></div>';
    tpl += '</li>';

    return tpl;
}
//for scroll script
function bindScroller(id){
    $('#'+id).slimScroll({
        height: '368px',
        color: 'rgb(187, 187, 187) none repeat scroll 0% 0%'
    });
}