//<contact pop script in below>
function share_contact_list() {
    $('#searchContacts').val('');
    var sorted = "contacts";
    setShareContactList(phnbookMap,sorted,true);
    $.each(phnbookMap,function (key,val) {
        set_profile_pic(val.userId, PROFILE_TYPE.CONTACTSHARING);
    });
}
function setShareContactList(data) {
    var time = new Date();
    var time_str = (time.getHours() + ':' + time.getMinutes());
    setDayNotification();
    var tplc = "";
    var i = 1;
    $.each(data, function (key, val) {
        if (val.selectMode == SELECT_MODE.CHAT) {
            tplc += "<div class='select_contact'>";
            tplc += '<ul class="media-list contactList">';
            tplc += "<li class='media unselect'  id='" + val.userId + "_divContact' data-target='#myModalContactShareConfrm' data-toggle='modal' data-dismiss='modal' onclick='getContactsId(\"" + val.userId + "\", \"" + val.name + "\")'>";
            tplc += "<div class='contact_list' style='width:100%;height:66px;padding: 5px 0 ;'>";
            tplc += "<div class='compatible' id='" + val.userId + "_avtar_contact_shared' style='width:73px;float:left;padding: 5px 10px !important;'>";
            tplc += "<img class='" + val.userId + "_avtar_contact_sharing media-object ' id='' src='" + userPic + "' alt='' onerror='imgError(this,\"" + val.userId + "\");'>";
            tplc += "</div>";
            tplc += "<div class='chat_body_width'>";
            tplc += "<div class='contactlist_panal'>";
            tplc += "<h4 class='contact_name'>" + truncateUserName(val.name) + "</h4>";
            tplc += "<p class='contact_name_set'>" + truncateString(val.status) + "</p>";
            tplc += "</div>";
            tplc += "</div>";
            tplc += "</div>";
            tplc += '</li>';
            tplc += "</ul>";
            tplc += "</div>";
            i++;
        }
    });
    $(".chat_usersc").empty();
    $(".chat_usersc").append(tplc);
}

var userIdss = '';
var namess = '';


function getContactsId(userId, name) {
    if ((!jQuery.isEmptyObject(getPhonebookMap(chatWithContact.userId))) && (chatWithContact.selectMode == SELECT_MODE.CHAT))
        $("#contactSend").text('Send "' + name + '" to "' + getPhonebookMap(chatWithContact.userId).name + '" ?');
    if ((!jQuery.isEmptyObject(getrecentsMap(chatWithContact.userId))) && (chatWithContact.selectMode == SELECT_MODE.GROUP)) {
        $("#contactSend").text('Send "' + name + '" to "' + getPhonebookMap(chatWithContact.userId).name + '" ?');
    }
    userIdss = userId;
    namess = name;
}
function sendContacts() {
    sendContact(userIdss, namess);
}
function createContactBody(name, phone,userId) {
    var body = '{"birthday":"","company":"","emailList":[],"firstName":"'+name+'","isCiaoUser":"1","lastName":"","phoneList":[{"detail":"'+phone+'","type":"2","userId":"'+userId+'"}]}';
    return body;
}
function sendContact(userId, name) {
    var time = new Date();
    var time_str = (time.getHours() + ':' + time.getMinutes());
    var tpl = "";
    var msgId = $.now();
    $.each(phnbookMap, function (key, val) {
        if (userId == val.userId) {
            if (send_message(createContactBody(val.name, val.phone,val.userId), msgId, 'contact')) {
                tpl = display_contact_out(val, msgId, 'contact', '', '', 'current', null, userId);
                sendContactProfilePic(userId);
            }
        }
    });
    add_to_view(tpl,TIMETYPE.CURRENT);
}

function sendContactProfilePic(userId) {
    $('.' + userId + '_sharedContactPic', '#friendProfileContact').attr('src', getPhonebookMap(userId).avatarURL);
    $("#friendProfileContact").attr('data-target', '#myModalContact');
}

function display_contact_out(text, msgId, msgType, type, timeType, messageTime, userId) {
    var cont = $('#chats');
    var list = $('.chats', cont);
        var contactObj={
            birthday:'',
            company:'',
            emailList:[],
            firstName:text.name,
            isCiaoUser:'1',
            lastName:'',
            phoneList:'{"detail":"'+text.phone+'","type":"2","userId":"'+text.userId+'"}'
        };
       var contactId = '';
    var contactId = random5();
    sharedContactMap[contactId] = contactObj;
    var tpl = '';
    tpl += '<li class="out unselect">';
    tpl += '<div id="' + msgId + '_contact" class="bubble-contact">';
    if (type == MSG_CATEGORY.groupchat) {
        tpl += getGrpChatHeader(MSG_CATEGORY.groupchat, '');
    }
    tpl += '<div class="body contact" style="position: relative;height: 53px;">';
    tpl += '<div style="width: 63px;float: left;">';
    tpl += '<img id=""  onclick="showContact(' + contactId + ',' + text.phone + ',\'' + text.userId + '\')" class="' + text.userId + '_sharedContactPic avtar_contact profile_contact_default" data-target="#myModalContact" data-toggle="modal" alt="" onerror="' +
        'imgErrors(this)" src="' + userPic + '">';
    tpl += '</div>';
    tpl += '<span class="ContactTextSet" id="' + msgId + '_name">' + text.name + '</span>';
    tpl += '<span class="datetime">' + get_date(TIMETYPE.CURRENT, messageTime) + '</span>';
    tpl += '<span id="' + msgId + '_dot" class="fa fa-clock-o clock_status" aria-hidden="true"></span>';
    tpl += '</div>';
    tpl += '<div class="clearfix">';
    tpl += '<div class="contact_bubble">';
    // if (isCiaoUser == '1') {
        tpl += "<span class='contact_footer' onclick='onStartChat(\"" + text.userId + "\")'>Message</span>";
    // } else {
    //     tpl += "<span class='contact_footer' onclick='#'>Invite to CiaoIM</span>";
    // }
    tpl += '</div>';
    tpl += '</div>';
    tpl += '</div>';
    tpl += '</li>';
    set_profile_pic(text.userId, PROFILE_TYPE.SHAREDCONTACT);
    return tpl;
}

function showContact(contactId,phones,userId) {
    var contact = sharedContactMap[contactId];
    var contactName = contact.firstName + contact.lastName;
    var isCiaoUser = contact.isCiaoUser;

    $("#viewContact").html(ViewContact(phones,userId));
    if(isCiaoUser=='1'){
        document.getElementById("contactName").innerHTML=contactName;
        $('#contactViewProfilePic').show();
    }
    else if(isCiaoUser=='0'){
        document.getElementById("contactName").innerHTML=contactName;
        $('#contactViewProfilePic').hide();
    }
}

function ViewContact(phones,userId) {
    var tpl='';
    tpl+='<ul class="view-contact-list">';
        tpl += '<li id="shareContact">';
    $.each(phnbookMap, function (key, val) {
        var phoness = val.phone;
        // var phoneno = phoness.substr(phoness.indexOf("+") + 1);
        if (phones == phoness) {
            var phone=val.phone;
            tpl += '<p id="contactPhone">'+phone+'</p>';
        }
        // if(type=='3')
        //     tpl += '<p id="contactType" class="detail-view">Work</p>';
        // else
    });
    tpl += '<p id="contactType" class="detail-view">Mobile</p>';
    tpl += '<img id="contactViewProfilePic" src="/webchat/assets/images/send-message.png" onclick="onStartChat(\'' + userId + '\')" data-dismiss="modal" class="send-message">';
    tpl += '</li>';
    tpl+='</ul>';
    return tpl;
}

function showContact1(contactId) {
    var contact = sharedContactMap[contactId];
    var contactName = contact.firstName + contact.lastName;
    var isCiaoUser = contact.isCiaoUser;
    var phoneList = contact.phoneList;

    $("#viewContact").html(ViewContact1(phoneList,contactId));
        if(isCiaoUser=='1'){
            document.getElementById("contactName").innerHTML=contactName;
            $('#contactViewProfilePic').show();
        }
        else if(isCiaoUser=='0'){
            document.getElementById("contactName").innerHTML=contactName;
            $('#contactViewProfilePic').hide();
        }
}

function ViewContact1(phoneList,contactId) {
    var contact = sharedContactMap[contactId];
    var tpl='';
    tpl+='<ul class="view-contact-list">';
    $.each(phoneList, function (key, val) {
        var phone = phoneList[key].detail;
        var type = phoneList[key].type;
        tpl += '<li id="shareContact">';
        tpl += '<p id="contactPhone">'+phone+'</p>';
        if(type=='3')
            tpl += '<p id="contactType" class="detail-view">Work</p>';
        else
            tpl += '<p id="contactType" class="detail-view">Mobile</p>';
    });
        tpl += '<img id="contactViewProfilePic" src="/webchat/assets/images/send-message.png" onclick="onStartChat(\'' + phoneList[0].userId + '\')" data-dismiss="modal" class="send-message">';
        tpl += '</li>';

    tpl+='</ul>';
    return tpl;
}

function imgErrors(image, userId) {
    image.onerror = "";
    image.src = "/webchat/assets/images/user.png";
    // if(getPhonebookMap(userId).userId==userId)
    // {
    $('#' + userId + 'sharedContactPic').removeClass('compatible');
    $('#' + userId + 'sharedContactPic').addClass('compatibledefault');
    $('#' + userId + '_avtar_contact_share').removeClass('compatible');
    $('#' + userId + '_avtar_contact_share').addClass('compatibledefault');
    $('.' + userId + '_avtar_contact_share').removeClass('compatible');
    $('.' + userId + '_avtar_contact_share').addClass('compatibledefault');
    // }
    return true;
}
function parseContactJson(text, msgId, phone) {
    var obj = JSON.parse(text);
    // console.log("parseContactJson " + JSON.stringify(obj, null, '  '));
    var tpl = '';
    tpl += '<div id="' + msgId + '_name">' + obj.firstName + '</div>';
    $.each(obj.phoneList, function (key, val) {
        tpl += phone = obj.phoneList[key].number;
    });
    return tpl;
}
function updat_Last_Message(jid, lastMessage) {
    $('#' + jid + '_last_msg').html(lastMessage);
    return true;
}

//below code is search contact in contact share list by divya.

var ContactsTab = 'ContactShareT';
jQuery(document).ready(function () {
    $("#searchContacts").keyup(function () {
        $(".chat_usersc").empty();
        var keySearchss = $("#searchContacts").val();
        var keySearchName = keySearchss.toLowerCase();
        if (ContactsTab == 'ContactShareT') {
            get_search_list(phnbookMap, keySearchName);
        }
    });
});

function get_search_list(phnbookMap, keySearchName) {
    var result = [];
    var sorted ="contacts";
    $.each(phnbookMap, function (key, val) {
        var usernamess = val.name;
        // var phone = val.phone;
        var username = usernamess.toLowerCase();
        // if ((username.indexOf(keySearchName) != -1 ) || (phone.indexOf(keySearchName) != -1)) {
        if ((username.indexOf(keySearchName) != -1 )) {
            result.push(val);
        }
    });
    if (!jQuery.isEmptyObject(result)) {//not empty
        $('#nocontactShare').text("");
        if ($('#searchContacts').value == "") {
          //  sort_contacts(result, sorted, true);
        }else{
            setShareContactList(result, sorted, true);
            $.each(phnbookMap, function (key, val) {
                set_profile_pic(val.userId, PROFILE_TYPE.CONTACTSHARING);
            });
            highlightContact($('#searchContacts').val());
        }

    }
    if (jQuery.isEmptyObject(result)) {// empty
        $('#nocontactShare').text("No contacts found");
    }
}
