/**
 * Created by vicky on 27/8/16.
 */
function getGroupMembers(memberArr) {
    var members = '';
    $.each(memberArr, function (key, value) {
        console.log('getGroupMembers1 '+getPhonebookMap(value.userId).name+getPhonebookMap(value.userId).selectMode);
        try{
            if(value.userId != userName){
                if (getPhonebookMap(value.userId).selectMode == SELECT_MODE.CHAT)
                    members += getPhonebookMap(value.userId).name + ', ';
                else
                    members += getPhonebookMap(value.userId).phone + ', ';
            }
            else {
                members += 'You, ';
            }
        }
        catch (e){}
    });
    return members;
}
function getGroupMembersId(memberArr) {
    var members = [];
    $.each(memberArr, function (key, value) {
        members.push(value.userId);
    });
    return members;
}

function leaveGroup() {
    if (isConnected) {
        var params = '{"opcode":"' + OPCODE.left + '","groupId":"'+chatWithContact.userId+'","memberList":[' + userName + ']}';
        sendToDevice(params, SYNC.GROUP_CHAT);
    }
}
function leave_grpname() {
        $("#leave_confirm").text('Leave "' + getrecentsMap(chatWithContact.userId).name + '" Group ?');
}

function showMembersList() {
    $(".chat_usersc").empty();
    groupMember = [];
    console.log('showMembersList1 '+groupMember);
    var oldMemberObj = getPhonebookMap(chatWithContact.userId).groupMembers;
    var oldMembersArr = [];
    var newMembers = {};
    $.each(oldMemberObj, function (key, val) {
        oldMembersArr.push(val.userId);
    });
    $.each(phnbookMap, function (key, val) {
        if (($.inArray(phnbookMap[key].userId, oldMembersArr) == -1) && (phnbookMap[key].selectMode == SELECT_MODE.CHAT)) {
            newMembers[phnbookMap[key].userId] = phnbookMap[key];
        }
    });
      $('#grpAction').html(' <button type="submit" class="btn btn-default btn-lg button_setting " onclick="addMembersInGroup()" data-dismiss="modal">Add</button>');
    sort_contacts(newMembers, 'addGrpMember', false);
    // addNewMember(newMembers);
    $("#MemberAdd").html('');

}

function sendChatState(jid) {
    client.sendMessage({
        to: jid,
        id: $.now(),
        type: 'normal',
        chatState: 'active'
    });
}

function manageGroupMap(groupJid, groupName, groupMember, opType) {
    switch (opType) {
        case OPCODE.createGroup:
            if (jQuery.isEmptyObject(recentsMap)) {
                //remove no message yet icon
                $('#contactsView').html('');
            }
            var groupContact = {
                userId: groupJid,
                createdBy: userName,
                name: groupName,
                createdTs: $.now(),
                groupMembers: groupMember,
                selectMode: SELECT_MODE.GROUP,
            };
            phnbookMap[groupJid] = groupContact;
            var groupRecent = {
                userId: groupJid,
                name: groupName,
                body: 'You created group "'+groupName+'"',
                msgType: MSG_TYPE.group_create,
                msgId: $.now(),
                messageTime: $.now(),
                unreadMsg: 0,
                chatState: MESSAGE_STATE.SENT,
                selectMode: SELECT_MODE.GROUP
            };
            recentsMap[groupJid] = groupRecent;
            break;

        case OPCODE.addMember:
            Array.prototype.push.apply(getPhonebookMap(groupJid).groupMembers,groupMember);
            break;

        case OPCODE.removeMember:
            var members = getPhonebookMap(groupJid).groupMembers;
            $.each(members, function (key, oldValue) {
               if(oldValue.userId==memberIDtoChange){
                   getPhonebookMap(groupJid).groupMembers = jQuery.grep(members, function(value) {
                       return value != oldValue;
                   });
               }
            });
            break;

        case OPCODE.changeSubject:
            try{
                getPhonebookMap(groupJid).name=groupName;
                getrecentsMap(groupJid).name=groupName;
            }
            catch (e){}
            break;

        case OPCODE.left:
            var members = getPhonebookMap(groupJid).groupMembers;
            $.each(members, function (key, oldValue) {
                if(oldValue.userId==userName){
                    getPhonebookMap(groupJid).groupMembers = jQuery.grep(members, function(value) {
                        return value != oldValue;
                    });
                }
            });
            break;

        case OPCODE.makeAdmin:
            var members = getPhonebookMap(groupJid).groupMembers;
            $.each(members, function (key, oldValue) {
                if(oldValue.userId==memberIDtoChange){
                    oldValue.affiliation='admin';
                    return;
                }
            });
            break;
        default:
            break
    }


}

function GrpParticipants(members) {
    var count = 0;
    for (var i = 0; i < members.length; i++) {
        var user = members[i];
        count++;
    }
}

function removeMembers(memberId) {
    if (isConnected) {
        memberIDtoChange = memberId;
        var params = '{"opcode":"' + OPCODE.removeMember + '","groupId":"' + chatWithContact.userId + '","memberList":[' + memberId + ']}';
        sendToDevice(params, SYNC.GROUP_CHAT);
    }
}

function addMembersInGroup() {
    if (isConnected) {
        var params = '{"opcode":"' + OPCODE.addMember + '","groupId":"' + chatWithContact.userId + '","memberList":['+groupMember+']}';
        sendToDevice(params, SYNC.GROUP_CHAT);
    }
}
function createNewGroup() {
    var groupJid = userName + '_' + $.now();
    if (isConnected) {
        var params = '{"opcode":"' + OPCODE.createGroup + '","groupId":"'+groupJid+'", "subject": "' + $("#grpname").text()+ '","memberList":['+groupMember+']}';
        sendToDevice(params, SYNC.GROUP_CHAT);
    }
}

function convertMembersArr(groupMember) {
    var members = [];
    var affiliation='member';
    $.each(groupMember, function (key, value) {
        if(value==userName){
            affiliation='owner';
        }
        var member = {
            affiliation: affiliation,
            userId: value
        }
        members.push(member);
    });
    return members;
}

function truncateString(str) {
    //if(str.length > 32)
    //  str = str.substring(0,32)+'...';
    return str;
}

function truncateUserName(str) {
    //if(str.length > 20)
    //  str = str.substring(0,20)+' ..';
    return str;
}

function changeGroupSubject(groupSubject) {
    if (isConnected) {
        var params = '{"opcode":"' + OPCODE.changeSubject + '","groupId":"'+chatWithContact.userId+'", "subject": "' + $("#grpSubject").text()+ '"}';
        sendToDevice(params, SYNC.GROUP_CHAT);
    }
}
function make_Admin(memberId) {
    if (isConnected) {
        memberIDtoChange=memberId;
        var params = '{"opcode":"' + OPCODE.makeAdmin + '","groupId":"'+chatWithContact.userId+'", "memberList":[' + memberId + ']}';
        sendToDevice(params, SYNC.GROUP_CHAT);
    }
}
function changeGroupProfilePic() {
    if (isConnected) {
        var params = '{"opcode":"' + OPCODE.profilePicChange + '","groupId":"'+chatWithContact.userId+'"}';
        sendToDevice(params, SYNC.GROUP_CHAT);
    }
}

function isMember() {
   var members=getPhonebookMap(chatWithContact.userId).groupMembers;
    var flg=false;
    $.each(members, function (key, value) {
        if(value.userId==userName){
            flg=  true;
        }
    });
    return flg;
}

function handleGroupStatus(body) {
    var response = JSON.parse(body);
    var status = response.status;
    var requestId = response.requestId;
    var message = response.message;
    var msgRecent='';
    console.log('handleGroupStatus ' + status + requestId + message);
    switch (message){
        case OPCODE.createGroup:
            if (status == 200) {
                groupMember.push(userName);
                manageGroupMap(requestId, $('#grpname').text(), convertMembersArr(groupMember), OPCODE.createGroup);
                append_recentsGroup(getPhonebookMap(requestId));
                onStartChat(requestId);
                msgRecent="You created group '" + getPhonebookMap(requestId).name + "'.";
                $('#' + requestId + '_last_msg').html(msgRecent);
                $('#' + requestId + '_divRecentsUL').prependTo("#contactsView");
                $("#grpname").empty();
                groupMember = [];
            } else {
                alert("Unable to create group!");
            }
            break;
        case OPCODE.addMember:
            if (status == 200) {
                manageGroupMap(requestId, '', convertMembersArr(groupMember), OPCODE.addMember);
                $.each(groupMember, function (key, value) {
                    if (getPhonebookMap(value).selectMode == SELECT_MODE.CHAT)
                        add_to_view(showGroupNotification('you added '+getPhonebookMap(value).name), TIMETYPE.CURRENT,0);
                        $('#' + requestId + '_last_msg').html('you added '+getPhonebookMap(value).name);
                });
                $("#friendLastSeen").text(getGroupMembers(getPhonebookMap(chatWithContact.userId).groupMembers).replace(/,\s*$/, ''));
                $('#' + requestId + '_divRecentsUL').prependTo("#contactsView");
                groupMember = [];
            } else {
                alert("Unable to add members");
            }
            break;
        case OPCODE.removeMember:
            if (status == 200) {
                msgRecent='You removed '+getPhonebookMap(memberIDtoChange).name;
                add_to_view(showGroupNotification(msgRecent), TIMETYPE.CURRENT,0);
                manageGroupMap(requestId,'', memberIDtoChange, OPCODE.removeMember);
                $('#'+memberIDtoChange+'_memberList').remove();
                $("#friendLastSeen").text(getGroupMembers(getPhonebookMap(chatWithContact.userId).groupMembers).replace(/,\s*$/, ''));
                showGroupMembers(chatWithContact.userId);
                $('#' + requestId + '_last_msg').html(msgRecent);
                $('#' + requestId + '_divRecentsUL').prependTo("#contactsView");
                memberIDtoChange='';
            } else {
                alert("Unable to removeMember");
            }
            break;
        case OPCODE.changeSubject:
            if (status == 200) {
                manageGroupMap(requestId, $('#grpSubject').text(),'', OPCODE.changeSubject);
                msgRecent='You changed subject to \"'+$('#grpSubject').text()+'\"';
                add_to_view(showGroupNotification(msgRecent), TIMETYPE.CURRENT,0);
                $('#'+requestId+'_groupName').text($('#grpSubject').text());
                $('#friendName').text($('#grpSubject').text());
                $('#' + requestId + '_last_msg').html(msgRecent);
                $('#' + requestId + '_divRecentsUL').prependTo("#contactsView");
            } else {
                alert("Unable to changeSubject");
            }
            break;
        case OPCODE.left:
            if (status == 200) {
                manageGroupMap(requestId,'', userName, OPCODE.removeMember);
                add_to_view(showGroupNotification('You left'), TIMETYPE.CURRENT,0);
                $('#' + requestId + '_last_msg').html(msgRecent);
                $('#' + requestId + '_divRecentsUL').prependTo("#contactsView");
                $('#leaveGroup').prop('disabled', true);
                $("#friendLastSeen").text(getGroupMembers(getPhonebookMap(chatWithContact.userId).groupMembers).replace(/,\s*$/, ''));
                $("#block1").show();
                $("#blocking_msg").hide();
                $("#block1").html("You can't send messages to this group because you are no longer a participant");
            } else {
                alert("Unable to left");
            }
            break;
        case OPCODE.makeAdmin:
            if (status == 200) {
                manageGroupMap(requestId, '',memberIDtoChange, OPCODE.makeAdmin);
                showGroupMembers(chatWithContact.userId);
                memberIDtoChange ='';
            } else {
                alert("Unable to makeAdmin");
            }
            break;
        case OPCODE.profilePicChange:
            if (status == 200) {
                msgRecent="You changed group\'s icon";
                add_to_view(showGroupNotification(msgRecent), TIMETYPE.CURRENT,0);
                $('#' + requestId + '_last_msg').html(msgRecent);
                $('#' + requestId + '_divRecentsUL').prependTo("#contactsView");
            } else {
                alert("Unable to profilePicChange");
            }
            break;
        default:
            break
    }
}
