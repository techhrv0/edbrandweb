function restore() {
    $("#chat_load").hide();
    $("#record, #live").removeClass("disabled");
    $("#pause").replaceWith('<a class="button one" id="pause">Pause</a>');
    $(".one").addClass("disabled");
    Fr.voice.stop();
}
$(document).ready(function () {
    $(document).on("click", "#record:not(.disabled)", function () {
        console.log("record not");
        elem = $(this);
        Fr.voice.record($("#live").is(":checked"), function () {
            console.log("checked");
            elem.addClass("disabled");
            $("#live").addClass("disabled");
            $(".one").removeClass("disabled");

            /**
             * The Waveform canvas
             */
            analyser = Fr.voice.context.createAnalyser();
            analyser.fftSize = 2048;
            analyser.minDecibels = -90;
            analyser.maxDecibels = -10;
            analyser.smoothingTimeConstant = 0.85;
            Fr.voice.input.connect(analyser);

            var bufferLength = analyser.frequencyBinCount;
            var dataArray = new Uint8Array(bufferLength);

            WIDTH = 500, HEIGHT = 200;
            // canvasCtx = $("#level")[0].getContext("2d");
            // canvasCtx.clearRect(0, 0, WIDTH, HEIGHT);

            /*function draw() {
             drawVisual = requestAnimationFrame(draw);
             analyser.getByteTimeDomainData(dataArray);
             canvasCtx.fillStyle = 'rgb(200, 200, 200)';
             canvasCtx.fillRect(0, 0, WIDTH, HEIGHT);
             canvasCtx.lineWidth = 2;
             canvasCtx.strokeStyle = 'rgb(0, 0, 0)';

             canvasCtx.beginPath();
             var sliceWidth = WIDTH * 1.0 / bufferLength;
             var x = 0;
             for(var i = 0; i < bufferLength; i++) {
             var v = dataArray[i] / 128.0;
             var y = v * HEIGHT/2;

             if(i === 0) {
             canvasCtx.moveTo(x, y);
             } else {
             canvasCtx.lineTo(x, y);
             }

             x += sliceWidth;
             }
             canvasCtx.lineTo(WIDTH, HEIGHT/2);
             canvasCtx.stroke();
             };
             draw();*/
        });
    });

    $(document).on("click", "#pause:not(.disabled)", function () {
        if ($(this).hasClass("resume")) {
            Fr.voice.resume();
            $(this).replaceWith('<a class="button one" id="pause">Pause</a>');
        } else {
            Fr.voice.pause();
            $(this).replaceWith('<a class="button one resume" id="pause">Resume</a>');
        }
    });

    $(document).on("click", "#stop:not(.disabled)", function () {
        restore();
    });

    $(document).on("click", "#sendVoice:not(.disabled)", function () {
        Fr.voice.export(function (url) {
            //voiceAudiourl=url;
            //  imageData.base64=url;
            //$("#audio").attr("src", url);
            //$("#audio")[0].play();
        }, "URL");
        restore();
        //addVoiceMemoLoader();
    });

    $(document).on("click", "#download:not(.disabled)", function () {
        Fr.voice.export(function (url) {
            $("<a href='" + url + "' download='MyRecording.wav'></a>")[0].click();
        }, "URL");
        restore();
    });

    $(document).on("click", "#base64:not(.disabled)", function () {
        Fr.voice.export(function (url) {
            console.log('record ' + url);
            //imageData.base64.clear();
            imageData.base64 = '';
            imageData.size = '';
            imageData.base64 = url;
            imageData.size = imageData.base64.length;
            restore();
            addVoiceMemoLoader();
            $("<a href='" + url + "' target='_blank'></a>")[0].click();
        }, "base64");
    });

    $(document).on("click", "#mp3:not(.disabled)", function () {
        alert("The conversion to MP3 will take some time (even 10 minutes), so please wait....");
        Fr.voice.export(function (url) {
            alert("Check the web console for the URL");
            $("<a href='" + url + "' target='_blank'></a>")[0].click();
        }, "mp3");
        restore();
    });

    $(document).on("click", "#save:not(.disabled)", function () {
        Fr.voice.export(function (blob) {
            var formData = new FormData();
            formData.append('file', blob);

            $.ajax({
                url: "upload.php",
                type: 'POST',
                data: formData,
                contentType: false,
                processData: false,
                success: function (url) {
                    $("#audio").attr("src", url);
                    $("#audio")[0].play();
                    alert("Saved In Server. See audio element's src for URL");
                }
            });
        }, "blob");
        restore();
    });
});


function addVoiceMemoLoader() {
    var scroll_to_chatHeight = $('#ulchatdemo').prop('scrollHeight') + 'px';
    $("#ulchatdemo").slimScroll({
        scrollTo: scroll_to_chatHeight,
        start: 'bottom'
    });
    $("#chat_load").hide();
    var msgId = $.now();
    var cont = $('#chats');
    var list = $('.chats', cont);
    var tpl = '';

    var fileName = msgId+'.3gp';//getFileName(message.fileName);
    var extss = '3gp';//fileName.substr( fileName.indexOf('.') + 1 );//message.messageBody.split('.').pop();

    tpl += '<li class="out unselect">';
    tpl += '<div class="file_width">'+getGrpChatHeader(chatWithContact.selectMode,userName,MESSAGE_STATE.TO_SEND)+'<div class="document-container'+getMsgPosClass(MESSAGE_STATE.TO_SEND)+'">';
    tpl += '<div class="document-container-padding">';
    tpl += '<div class="file-icon">';
    tpl += '<img id="'+ msgId+ '_msg"  src="'+getFileIcon(extss)+'" onerror="imageerrhandle(this,\'' +extss+ '\')" width="30px" height="30px" >';
    tpl += '</div>';
    tpl += '<div class="file-icon1">';
    tpl += '<span><h1 style="font-size: 15px;margin-top: 5px;">' + showLimitedName(fileName) + '</h1></span>';
    tpl += '</div>';
    tpl += '<div class="file-icon2">';
    tpl += '<a target="_blank" href="/chat/imagedownload/' + msgId + "_" + fileName + '"><img id="' + msgId+ '_msgLoader"  src="webchat/assets/images/download_icon.png"  width="30px" height="30px"></a>';
    tpl += '</div>';
    tpl += '</div>';
    tpl += '</div>';
    tpl += '<div class="footer_file">';
    tpl += '<span class="text_size">' + extss + ' - ' + getFileSize(imageData.size) + '</span>';
    tpl += '<span class="datetime">' + get_date('old', msgId) + '</span>';
    tpl += '<span id="' + msgId + '_dot" class="' + bubbleGetState(MESSAGE_STATE.TO_SEND) + '" aria-hidden="true"/>';
    tpl += '</div>';
    tpl += '</div>';
    tpl += '</li>';
    list.append(tpl);

    // tpl += '<li class="out">';
    // tpl += '<div class="bubble-contact">';
    // tpl += '<span class="body image" id="img_div" style="position: relative;height: 53px;">';
    // tpl += '<div style="width: 63px;float: left;position: relative;">';
    // tpl += '<img id="' + userName + '_record11" class="avtar_contact profile_contact_default" alt="" src="/webchat/assets/images/user.png" onerror="imgError(this,\'' + userName + '\');">';
    // tpl += '<img id="" class="voiceMemo" alt="" src="/webchat/assets/images/voice_memo.png" width="20px" height="20px" >';
    // tpl += '</div>' +
    //     '<audio  id="' + msgId + '_msgaudio">' +
    //     '<source src="' + imageData.base64 + '">' +
    //     '</audio>' +
    //     '<div id="'+ msgId +'audioplayer" class="audioplayer">' +
    //     '<button id="'+ msgId +'pButton" class="play" onclick="playOut('+msgId+')"></button>' +
    //     '<div id="'+ msgId +'timeline" class="timeline">' +
    //     '<div id="'+ msgId +'playhead" class="playhead" ></div>' +
    //     '</div>' +
    //     '</div></span>';
    // tpl += '<div class="timeicon_set">';
    // tpl += '<span class="datetime">' + get_date('current', null) + '</span>';
    // tpl += '<span id="' + msgId
    //     + '_dot" class="fa fa-clock-o clock_status" aria-hidden="true"/>';
    // tpl += '</div>';
    // tpl += '</div>';
    // tpl += '</li>';
    // list.append(tpl);
    uploadImage(imageData.base64, msgId, msgId+'.wav');
    set_profile_pic(userName, PROFILE_TYPE.VOICERECORD);
}

/*audio track JS*/
var music ;
var duration;
var pButton ;
var playhead;
var timeline;
var timelineWidth;
var onplayhead=false;

// returns click as decimal (.77) of the total timelineWidth
function clickPercent(e) {
    return (e.pageX - timeline.offsetLeft) / timelineWidth;
}

// mouseDown EventListener
function mouseDown() {
    onplayhead = true;
    window.addEventListener('mousemove', moveplayhead, true);
    music.removeEventListener('timeupdate', timeUpdate, false);
}

// getting input from all mouse clicks
function mouseUp(e) {
    if (onplayhead == true) {
        moveplayhead(e);
        window.removeEventListener('mousemove', moveplayhead, true);
        // change current time
        music.currentTime = duration * clickPercent(e);
        music.addEventListener('timeupdate', timeUpdate, false);
    }
    onplayhead = false;
}

// Moves playhead as user drags
function moveplayhead(e) {
    var newMargLeft = e.pageX - timeline.offsetLeft;
    if (newMargLeft >= 0 && newMargLeft <= timelineWidth) {
        playhead.style.marginLeft = newMargLeft + "px";
    }
    if (newMargLeft < 0) {
        playhead.style.marginLeft = "0px";
    }
    if (newMargLeft > timelineWidth) {
        playhead.style.marginLeft = timelineWidth + "px";
    }
}

// timeUpdate
// Synchronizes playhead position with current point in audio
function timeUpdate() {
    var playPercent = timelineWidth * (music.currentTime / duration);
    playhead.style.marginLeft = playPercent + "px";
    if (music.currentTime == duration) {
        pButton.className = "";
        pButton.className = "play";
        pButton.className = "play_in";
    }
}

//Play and Pause
function playOut(msgId) {
    console.log('playOut');
    music = document.getElementById(msgId + "_msgaudio");
    pButton = document.getElementById(msgId +'pButton');
    playhead = document.getElementById(msgId +'playhead');
    timeline = document.getElementById(msgId +'timeline');
    duration = music.duration;
    timelineWidth = timeline.offsetWidth - playhead.offsetWidth;
    bind_audio_controls();
    if (music.paused) {
        console.log('playOut1');
        music.play();
        pButton.className = "";
        pButton.className = "pause";
    } else {
        console.log('playOut2');
        music.pause();
        pButton.className = "";
        pButton.className = "play";
    }
}

function playOutFromDevice(msgId) {
    music = document.getElementById(msgId + "_msg");
    pButton = document.getElementById(msgId +'pButton_out');
    playhead = document.getElementById(msgId +'playhead_out');
    timeline = document.getElementById(msgId +'timeline_out');
    console.log("value of timeline width///"+duration);
    duration = music.duration;
    timelineWidth = timeline.offsetWidth - playhead.offsetWidth;
    console.log("value of timeline width2///"+duration);
    bind_audio_controls();
    if (music.paused) {
        console.log('playOut '+duration);
        music.play();
        pButton.className = "";
        pButton.className = "pause";
    } else {
        console.log('playOut1 '+duration);
        music.pause();
        pButton.className = "";
        pButton.className = "play";
    }
    console.log("value of timeline width3///"+duration);
}


function playIn(msgId){
    music = document.getElementById(msgId + "_msg");
    pButton = document.getElementById(msgId +'pButton_in');
    playhead = document.getElementById(msgId +'playhead_in');
    timeline = document.getElementById(msgId +'timeline_in');
    duration = music.duration;

    timelineWidth = timeline.offsetWidth - playhead.offsetWidth;
    console.log("value of timeline width///"+timelineWidth);
    bind_audio_controls();
    if (music.paused) {
        music.play();
        pButton.className = "";
        pButton.className = "pause_in";
    } else {
        music.pause();
        pButton.className = "";
        pButton.className = "play_in";
    }
}

function bind_audio_controls(){
// timeupdate event listener
    music.addEventListener("timeupdate", timeUpdate, false);

//Makes timeline clickable
    timeline.addEventListener("click", function (event) {
        moveplayhead(event);
        music.currentTime = duration * clickPercent(event);
    }, false);

// Makes playhead draggable
    playhead.addEventListener('mousedown', mouseDown, false);
    window.addEventListener('mouseup', mouseUp, false);

    music.addEventListener("canplaythrough", function () {
        duration = music.duration;
    }, true);
}

/* audio track end */
