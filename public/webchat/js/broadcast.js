/**
 * Created by arbind on 7/9/16.
 */
function broadcast_list() {
    BroadcastMember=[];
    $("#BroadcastMemAdd").html('');
    set_broadcast_list(phnbookMap);
}
function set_broadcast_list(data)
{
    var time = new Date();
    var time_str = (time.getHours() + ':' + time.getMinutes());
    var tplc="";
    if(data.length!=0){
        tplc+="<div class='select_contact unselect'>";
        tplc+='<ul class="media-list contactList">';
        var i=1;
        $.each(data,function(key,val)
        {
            var avtar = "../assets/images/user.jpg";
            if (val.avtar != null && val.avtar != '') {
                avtar = val.avtar;
            }
            // tplc += "<li class='media media_contact'  id='" + data[key].userId+ "_divContact'>";
            tplc += "<li class='"+ data[key].userId +"media' id='" + data[key].userId+ "_divContact' onclick='addToBroadcast(\"" + data[key].userId + "\", \""+data[key].name+"\")'>";
            tplc += "<div class='contact_list' style='width:100%;height:66px;padding: 5px 0 ;'>";
            tplc += "<div class='compatible' id='" + data[key].userId + "_avtar_contact_Broadcast' style='width: 73px; float: left; padding: 5px 10px ! important;'>";
            tplc += "<img class='media-object ' id='" + data[key].userId + "_avtar_contact_share' src='" + getPhonebookMap(data[key].userId).avatarURL + "' alt='' onerror='imgError(this,\"" + data[key].userId + "\");'>";
            //tplc +='<input type="checkbox" id='+ data[key].userId+' name="contactss" value="cont" />';
            // tplc +='<input type="checkbox" id='+ data[key].userId+' name="contactss" value="cont" style="position: absolute;left: 142px;top: 75px;" />';
            tplc +="</div>";
            tplc += "<div class='chat_body_width'>";
            tplc += "<div class='contactlist_panal'>";
            tplc += "<h4 class='contact_name'>" + truncateUserName2(data[key].name) + "</h4>";
            tplc += "<h4 class='contact_name_set'>"+truncateString(data[key].status)+"</h4>";
            tplc += "</div>";
            tplc +="</div>";
            tplc +="</div>";
            tplc += '</li>';
            i++;
        });

    }else{}
    tplc += "</ul>";
    tplc +="</div>";
    $(".chat_usersc").empty();
    $(".chat_usersc").append(tplc);
}
function truncateUserName2(str) {
    if(str.length > 17)
        str = str.substring(0,17)+' ...';
    return str;
}
function addToBroadcast(userId,name) {
    checkedList = [];
    var count='';
    $('input[type=checkbox][name=contactss]').each(function () {
        count = $("[type='checkbox']:checked").length;
        console.log("count of checked"+count);
        var $this = $(this);
        if ($this.is(":checked")) {
            checkedList.push($this.attr("id"));
        }
    });
    broadcast(checkedList,count);

    /*if ($.inArray(userId, BroadcastMember) === -1) {
     BroadcastMember.push(userId);
     console.log("broaddddddddddddddddddddddddddddd::: "+BroadcastMember);
     }*/
}
function createBroadCast(){
    // checkedList = [];
    // var count='';
    // $('input[type=checkbox][name=contactss]').each(function () {
    //      count = $("[type='checkbox']:checked").length;
    //     console.log("count of checked"+count);
    //     var $this = $(this);
    //     if ($this.is(":checked")) {
    //         checkedList.push($this.attr("id"));
    //     }
    // });
    // broadcast(checkedList,count);

}
function broadcast(checkedList,count) {
    var broadcastName = "";
    var broadcastRecipients='';
    var broadcastId=" ";
    broadcast_user = [];
    for (var i = 0; i < checkedList.length; i++) {
        var selecteduserId = checkedList[i];
        $.each(phnbookMap, function (key, val) {
            if (val.userId == selecteduserId) {
                var bUser = {
                    "userId": selecteduserId,
                    "name": val.name
                };
                broadcastName +=(val.name.split(" ")[0]+ ', ');
                broadcastRecipients =count+" "+'recipients';
                broadcastId=selecteduserId;
                broadcast_user.push(bUser);
            }
        });
    }
    broadcastName = broadcastName.substring(0, broadcastName.length-2);
    createBroadCastTuple(broadcastId, broadcastName,broadcastRecipients);
}
function createBroadCastTuple(broadcastId,broadcastName,broadcastRecipients){
    console.log("inside tuplessssssss"+broadcastName+" "+broadcastId);
    var tpl='<ul class="media-list list-items unselect">';
    var avtar = "/webchat/assets/images/broadcast.png";
    tpl += "<li class='media' id='" +broadcastId + "_divContact' onclick='startBroadcastChat(\"" + broadcastId+ "\", \""+TAB_CONTACTS+"\" , \""+broadcastRecipients+"\",\"broadcast\");'>";
    tpl += "<div class='contact_list'>";
    tpl += "<div class='compatible'>";
    /*tpl += "<div class='icon_set'>";*/
    tpl += "<img class='media-object img-circle' id='" + broadcastId + "_divContact' src='" + avtar + "' alt='...'>";
    /*tpl += "</div>";*/
    tpl += "</div>";
    tpl += "<div class='contactlist_panal'>";
    tpl +="<h4 class='broadcastcontact_name'>"+showLimitedChar(broadcastName)+"</h4>";
    tpl += "<p id='" + broadcastId+ "_last_msg' class='message_textb'></p>";
    tpl += "</div>";
    tpl += "<div class='online_status'>";
    tpl += "<p id='" + broadcastId+ "_time' class='time textsb'>"+get_date("current", null)+"</p>";
    tpl += "<p class='badge badge-danger'  id='" +broadcastId+ "_cnt' ></p>";
    tpl += "</div>";
    tpl += "</div>";
    tpl += '</li>';
    $(".chat_users").append(tpl);
}
function showLimitedChar(str) {
    if(str.length > 19)
        str = str.substring(0,19)+' ...';
    return str;
}

function startBroadcastChat(opponent_jid, tab,contacts,chatWithType) {
    if(chatWithType=='broadcast')
    {
        $('#friendProfilePic, #friendprofileimg').attr('src',"/webchat/assets/images/broadcast.png");
        $( "#friendProfilePic" ).attr('data-target','#broadcastModalprofile');
        // getLastActivity(opponent_jid);
    }
    else{
        $('#friendProfilePic, #friendprofileimg').attr('src',getPhonebookMap(opponent_jid).avatarURL);
        $( "#friendProfilePic" ).attr('data-target','#myModalprofile');
        getLastActivity(opponent_jid);
    }
    document.getElementById("friendName").innerHTML =contacts;
    $('#default_chat_theme').hide();
    $('#user_chat_theme1').show();
    $('#'+this.opponent_jid+'_div').removeClass("selectedContact");
    $('#'+opponent_jid+'_div').addClass("selectedContact");
    $(".chats").empty();
    $('#'+opponent_jid+'_cnt').html('');
    this.opponent_jid = opponent_jid;
    this.opponent_Type = chatWithType;
    this.isBroadcast = true;
    console.log("selected contact tab "+tab+' opponent_jid '+opponent_jid+' opponent_Type '+opponent_Type);
}
var ContactsBroad='broadcastShare';
jQuery(document).ready(function() {
    $("#searchContactb").keyup(function() {
        var searchvalue = $("#searchContactb").val();
        var keysearchvalue=searchvalue.toLowerCase();
        if(ContactsBroad == 'broadcastShare'){
            get_phoneBookList('broadcastShare',keysearchvalue,phnbookMap);
        }
    });
});
function getBroadCastUsers() {
    var data='';
    var tplc="";
    tplc+="<div class='select_contact unselect'>";
    tplc+='<ul class="media-list contactList">';
    var i=1;
    $.each(phnbookMap,function(key,val)
    {
        for (index = 0; index < broadcast_user.length; index++) {
            data = broadcast_user[index].userId;
            if (data == phnbookMap[key].userId) {
                var phone=phnbookMap[key].phone;
                var phones=phone.substr(phone.indexOf("+")+1);
                var avtar = "/webchat/assets/images/user.png";
                if (val.avtar != null && val.avtar != '') {
                    avtar = val.avtar;
                }
                tplc += "<li class='media '  id='" +data+ "_divContact' >";
                tplc += "<div class='contact_list' style='width:227%;height:66px;padding: 5px 0 ;'>";
                tplc += "<div class='compatible' id='" + data+ "_avtar_contact_shared' style='width:73px;float:left;padding: 5px 10px !important;'>";
                tplc += "<div class='icon_set'>";
                tplc += "<img class='media-object ' id='" + data + "_avtar_contact_share' data-target='#myModalContactBroadcast' data-toggle='modal' src='" + getPhonebookMap(data).avatarURL + "'  alt='' onerror='imgErrors(this,\"" + data + "\");' onclick='showContactB("+phones+");'>";
                tplc += "</div>";
                tplc += "</div>";
                tplc += "<div class='chat_body_widthb'>";
                tplc += "<div class='contactlist_panal'>";
                tplc += "<h4 class='contact_name'>" + truncateUserName(phnbookMap[key].name) + "</h4>";
                tplc += "<p class='contact_name_set'>"+truncateString(phnbookMap[key].status)+"</p>";
                tplc += "</div>";
                tplc +="</div>";
                tplc +="</div>";
                tplc += '</li>';
                i++;
            }
        }
    });
    tplc += "</ul>";
    tplc +="</div>";
    $(".chat_usersc").empty();
    $(".chat_usersc").append(tplc);
}
function showContactB(phones){
    console.log("hello contacts "+phones);
    $.each(phnbookMap, function (key, val) {
        console.log("hello val contact"+val.phone);
        var phoness=val.phone;
        var phoneno=phoness.substr(phoness.indexOf("+")+1);
        console.log("how   "+phoneno);
        if(phones==phoneno)
        {
            console.log("hello inside if"+val.phone+ "  "+val.name);
            var phone=val.phone;
            var name=val.name;
            var userIds=val.userId;
            document.getElementById("contactNameB").innerHTML=name;
            document.getElementById("contactPhoneB").innerHTML=phone;
            $('#friendProfileContactB').attr('src',getPhonebookMap(userIds).avatarURL);
            $( '#friendProfileContactB' ).attr('data-target','#myModalContactBroadcast');

        }
    })
}

function get_phoneBookList(broadcastShare,keysearchvalue,phnbookMap){
    if(ContactsBroad == 'broadcastShare') {
        getContactList(phnbookMap,keysearchvalue);
    }
}
function getContactList(phnbookMap,keysearchvalue){
    var result=[];
    $.each(phnbookMap, function (key, val) {
        var usernamess= val.name;
        var phone=val.phone;
        var username=usernamess.toLowerCase();
        if((username.indexOf(keysearchvalue) != -1)||(phone.indexOf(keysearchvalue)!=-1)){
            result.push(val);
        }
    });
    if(!jQuery.isEmptyObject(result)) {//not empty
        console.log("not empty");
        $('#nocontactBroadcast').text("");

    }
    if(jQuery.isEmptyObject(result)) {// empty
        console.log("empty");
        $('#nocontactBroadcast').text("No contacts found");
    }
    set_broadcast_list(result);
}
