/*** Created by khyati on 22/8/16.*/
var gui = {
    $btnBlock: $('.block_btn'),
    $btnUnBLock: $('.unblock_btn'),
    showBlockBtn: function () {
        // console.log('inside showBlockBtn');
        this.$btnUnBLock.addClass('hidden');
        this.$btnBlock.removeClass('hidden');
    },
    showUnBlockBtn: function () {
        //  console.log('inside hideBlockBtn');
        this.$btnUnBLock.removeClass('hidden');
        this.$btnBlock.addClass('hidden');
    }
};

function checkBlockStatus(userId) {
    try{
        return getPhonebookMap(userId).blockStatus;
    }
    catch (e){}
}

function blockUnblockUser() {
    var user="{ userId:"+ chatWithContact.userId+" }";
    sendToDevice(user, SYNC.BLOCK);
}

function handleBlockStatus(body) {
    var response = JSON.parse(body);
    var status = response.status;
    var requestId = response.requestId;
    var message = response.message;
    console.log('handleBlockStatus ' + status + requestId + message);
    if (status == 200) {
        try{
            var blockStatus=getPhonebookMap(requestId).blockStatus;
            if(blockStatus==BLOCK_STATUS.BLOCKED_BY_ME){
                console.log("user unblocked");
                //user is unblocked
                try{
                    getPhonebookMap(requestId).blockStatus = BLOCK_STATUS.NO_BLOCK;
                    makeBlockUIchanges(requestId,'block');
                }
                catch (e){}
            }
            else if(blockStatus==BLOCK_STATUS.BLOCKED_BY_BOTH){
                console.log("blocked by both");
                if(requestId==userName) {
                    try {
                        getPhonebookMap(requestId).blockStatus = BLOCK_STATUS.BLOCKED_BY_ME;
                        makeBlockUIchanges(chatWithContact.userId, 'unblock');
                    }
                    catch (e) {
                    }
                }
                else{
                    try {
                        getPhonebookMap(requestId).blockStatus = BLOCK_STATUS.BLOCKED_BY_OTHER;
                        makeBlockUIchanges(chatWithContact.userId, 'unblock');
                    }
                    catch (e) {
                    }
                }
            }
            else if(blockStatus==BLOCK_STATUS.BLOCKED_BY_OTHER){
                console.log("blocked by other");
                try {
                    getPhonebookMap(requestId).blockStatus = BLOCK_STATUS.BLOCKED_BY_ME;
                    makeBlockUIchanges(chatWithContact.userId, 'unblock');
                }
                catch (e){}
            }
            else {
                console.log("user blocked");
                //user is blocked
                try{
                    getPhonebookMap(requestId).blockStatus = BLOCK_STATUS.BLOCKED_BY_ME;
                    makeBlockUIchanges(requestId,'unblock');
                }
                catch (e){}
            }
        }
        catch (e){}
    }
}

function makeBlockUIchanges(userId,type) {
    console.log('makeBlockUIchanges '+userId+type);
    switch (type){
        case 'block':
            gui.showBlockBtn();
            if (chatWithContact.userId == userId) {
                console.log('makeBlockUIchanges1 '+userId+type);
                $("#blocking_msg").html(UserChatPanel());
                bind_chatpanel_again();
                $("#blocking_msg").show();
                $("#block1").hide();
            }
            break;
        case 'unblock':
            gui.showUnBlockBtn();
            if (chatWithContact.userId == userId) {
                console.log('makeBlockUIchanges2 '+userId+type);
                $("#block1").html("Can't send message to blocked contact " +chatWithContact.name);
                $("#blocking_msg").hide();
                $("#block1").show();
            }
            break;
        default:
            break;

    }
}