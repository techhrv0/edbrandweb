/**
 * Created by arbind on 30/8/16.
 */
$(document)
    .ready(
        function() {
            $('#fileupload1')
                .fileupload(
                    {
                        //dataType : 'json',
                        add : function(e, data) {
                            var goUpload = true;
                            var uploadFile = data.files[0];
                            var ext = uploadFile.name
                                .split('.').pop();
                            console.log("file type:" + ext);
                            if (!(/\.(gif|png|jpg|jpeg|bmp)$/i)
                                    .test(uploadFile.name)) {
                                printFileError(FILE_TYPE_ERROR);
                                goUpload = false;
                            } else if (uploadFile.size > 2000000) { // 2mb
                                printFileError(FILE_SIZE_ERROR);
                                goUpload = false;
                            }
                            if (goUpload == true) {
                                data.formData = {
                                    msgId : msgId,
                                    fileName:uploadFile.name
                                };
                                data.submit();
                            }
                        },
                        success : function(response) {
                            var files = response.files;
                            console.log("files nameeeeeee : "
                                + JSON.stringify(response.files));
                            if (response.status == "200") {
                                addImage(files);
                            } else {
                                for (var index = 0; index < files.length; index++) {
                                    $("#"+ files[index].msgId+ "_msg a").removeAttr('href');
                                    document.getElementById(files[index].msgId+ "_thumb").src = "webchat/assets/images/error.png";
                                }
                            }
                        }
                    });
        });

function addImage(files)
{
    console.log("uploaded image response: "+files);
   // publishMyVcard($('#mystatus').text());
}