/*http://stackoverflow.com/questions/35823655/quickblox-javascript-sdk-no-incoming-call
 */
;(function(window, $) {
    'use strict';
    /** when DOM is ready */
    $(function() {
        //ui setup
        var ui = {

            $btnAudioCall: $('.j-audioCall'),
            $btnAudioHangup: $('.j-audioHangup'),

            $panel: $('.j-pl'),
            $callees: $('.j-callees'),

            $btnCall: $('.j-call'),
            $btnHangup: $('.j-hangup'),

            $ctrlBtn: $('.j-caller__ctrl'),
            filterClassName: '.j-filter',

            modal: {
                'income_call': '#income_call'
            },

            sounds: {
                'call': 'callingSignal',
                'end': 'endCallSignal',
                'rington': 'ringtoneSignal'
            },

            togglePreloadMain: function(action) {
                var $main = $('.j-main'),
                    preloadClassName = 'main-preload';

                if(action === 'show') {
                    $main.addClass( preloadClassName );
                } else {
                    $main.removeClass( preloadClassName );
                }
            },

            showCallBtn: function() {

                console.log('inside showCallBtn');
                this.$btnHangup.addClass('hidden');
                this.$btnCall.removeClass('hidden');
            },
            hideCallBtn: function() {
                console.log('inside hideCallBtn');
                this.$btnHangup.removeClass('hidden');
                this.$btnCall.addClass('hidden');
            },


            showAudioCallBtn: function() {
                console.log('inside showAudioCallBtn');
                this.$btnAudioHangup.addClass('hidden');
                this.$btnAudioCall.removeClass('hidden');
            },
            hideAudioCallBtn: function() {
                console.log('inside hideAudioCallBtn');
                this.$btnAudioHangup.removeClass('hidden');
                this.$btnAudioCall.addClass('hidden');
            },

            toggleRemoteVideoView: function(userId, action) {
                var $video = $('#remote_video_' + userId);

                if(!_.isEmpty(qbApp.currentSession) && $video.length){
                    if(action === 'show') {
                        $video.parents('.j-callee').removeClass('callees__callee-wait');
                    } else if(action === 'hide') {
                        $video.parents('.j-callee').addClass('callees__callee-wait');
                    } else if(action === 'clear') {
                        /** detachMediaStream take videoElementId */
                        qbApp.currentSession.detachMediaStream('remote_video_' + userId);
                        $video.removeClass('fw-video-wait');
                    }
                }
            },

            callTime: 0,
            updTimer: function() {
                this.callTime += 1000;
                $('#timer').removeClass('hidden')
                    .text( new Date(this.callTime).toUTCString().split(/ /)[4] );
            },

            updAudioTimer: function() {
                this.callTime += 1000;
                $('#audioTimer').removeClass('hidden')
                    .text( new Date(this.callTime).toUTCString().split(/ /)[4] );
            }


        };

        
        QB.init(QBApp.appId, QBApp.authKey, QBApp.authSecret, CONFIG);
        jQuery(document).ready(function(e) {
            console.log("Inside user");
            QB.createSession({login: qbApp.user.login, password: qbApp.user.password}, function(err, res) {
                console.log("session res "+res);
                var params = { 'login': qbApp.user.login, 'password': qbApp.user.password};
                QB.login(params, function(err, user){
                    console.log("Inside qb login..");
                    if (user) {
                        // success
                        console.log("qb login success ."+user);
                        QB.chat.connect({
                            jid: QB.chat.helpers.getUserJid(qbApp.user.userId, QBApp.appId),
                            password: qbApp.user.password
                        }, function(err, res) {
                            console.log("connect err : "+err);
                            console.log("connect resposne : "+res);
                            if(err !== null) {
                                QB.chat.disconnect();
                            } else {
                                console.log("chat conncetd...");
                            }
                        });
                    } else  {
                        // error
                        console.log("qb login error ."+err);
                    }
                });
            });
            return false;
        });

        /** send Call */
        $(document).on('click', '.j-call', function(e) {
            var isMediaShared=false;
            var callwith = [qbApp.callees.userId];
            // var callwith = [53171];
            var app = {
                callees : []
            };
            var user = {};
//	 user.id=13745;
//             user.name='Rahul';
             app.callees[callwith] = user.name;

            var mediaParams = {
                audio: true,
                video: true,
                options: {
                    muted: true,
                    mirror: true
                },
                elemId: 'localVideo'
            };
            if(!window.navigator.onLine) {
                console.log("No internet");
            } else {

                console.log("start video call with : "+qbApp.callees.userId);
                qbApp.currentSession = QB.webrtc.createNewSession(Object.keys(app.callees), QB.webrtc.CallType.VIDEO);
                qbApp.currentSession.getUserMedia(mediaParams, function(err, stream) {
                    if (err || !stream.getAudioTracks().length || !stream.getVideoTracks().length) {
                        var errorMsg = '';
                        if(err && err.message) {
                            errorMsg += 'Error: ' + err.message;
                        } else {
                            errorMsg += 'device_not_found';
                        }
                        qbApp.currentSession.stop({});
                        showErrorAccessPermission(err);
                    } else {
                        console.log("starting video call");
                        isMediaShared=true;
                        //Open dialog
                        $("#qbcallingdiv").modal();
                        $('#demo').hide();
                        $('#plusIcon').show();
                        $('#crossIcon').hide();
                        qbApp.currentSession.call({}, function(error) {
                            console.log("Call Resposne : "+error);
                            if(error) {
                                console.warn(error.detail);
                            } else {
                                ui.hideCallBtn();
                                console.log("started video call");
                            }
                        });
                    }
                });
                if(!isMediaShared){
                    qbApp.currentSession.stop({});
                }
            }
        });

        /** send audio call **/
        $(document).on('click', '.j-audioCall', function(e) {
		var isMediaShared=false;
            var callwith = [qbApp.callees.userId];
            // var callwith = [53171];
            var app = {
                callees : []
            };
            var user = {};
//      user.id=13745;
//             user.name='Rahul';
        app.callees[callwith] = user.name;

            var mediaParams = {
                audio: true,
                video: false,
                options: {
                    muted: false,
                    mirror: false
                },
                elemId: 'localAudio'
            };
            if(!window.navigator.onLine) {
                console.log("No internet");
            } else {

                console.log("start audio call with : "+qbApp.callees.userId);
                qbApp.currentSession = QB.webrtc.createNewSession(Object.keys(app.callees), QB.webrtc.CallType.AUDIO);
                qbApp.currentSession.getUserMedia(mediaParams, function(err, stream) {
                    if (err || !stream.getAudioTracks().length) {
                        var errorMsg = '';
                        if(err && err.message) {
                            errorMsg += 'Error: ' + err.message;
                        } else {
                            errorMsg += 'device_not_found';
                        }
                        qbApp.currentSession.stop({});
                        showErrorAccessPermission(err);
                    } else {
                        console.log("starting audio call");
			isMediaShared=true;
                        //Open dialog
                        $("#qbaudiocallingdiv").modal();
                        $('#demo').hide();
                        $('#plusIcon').show();
                        $('#crossIcon').hide();
                        qbApp.currentSession.call({}, function(error) {
                            console.log("Call Resposne : "+error);
                            if(error) {
                                console.warn(error.detail);
                            } else {
                                ui.hideAudioCallBtn();
                                console.log("started audio call");
                            }
                        });
                    }
                });
		if(!isMediaShared){
                    qbApp.currentSession.stop({});
                }
            }
        });


        QB.webrtc.onAcceptCallListener = function onAcceptCallListener(session, userId, extension) {
            console.log('inside onAcceptCallListener');
        };

        QB.webrtc.onRemoteStreamListener = function onRemoteStreamListener(session, userId, stream) {
            console.log('onRemoteStreamListener.');
            console.log('userId :... ' + userId);
            console.log('Session:... ' + session);
            console.log("calltype : "+session.callType);

            qbApp.currentSession.peerConnections[userId].stream = stream;
            if(session.callType=='1'){
            qbApp.currentSession.attachMediaStream('main_video', stream);
                if(!qbApp.callTimer) {
                    qbApp.callTimer = setInterval( function(){ ui.updTimer.call(ui); }, 1000);
                }
            }
            else {
                qbApp.currentSession.attachMediaStream('localAudio', stream);
                if(!qbApp.callTimer) {
                    qbApp.callTimer = setInterval( function(){ ui.updAudioTimer.call(ui); }, 1000);
                }
            }           
        };

        // Call receive

        QB.webrtc.onCallListener = function onCallListener(session, extension) {
            console.group('onCallListener.');
            console.log('Session: ' + session);
            console.log('Extension: ' + JSON.stringify(extension));
            console.groupEnd();

            /** close previous modal if his is exist */
            $(ui.modal.income_call).modal('hide');

            var userInfo = _.findWhere(QBUsers, {id: session.initiatorID});

            qbApp.currentSession = session;

            /** set name of caller */
//       $('.j-ic_initiator').text( userInfo.full_name );

            $(ui.modal.income_call).modal('show');

            //document.getElementById(ui.sounds.rington).play();
        };


        QB.webrtc.onSessionCloseListener = function onSessionCloseListener(session){
            console.log('onSessionCloseListener: ' + session);
            /** pause play call sound */
            // document.getElementById(ui.sounds.call).pause();
            // document.getElementById(ui.sounds.end).play();
            if(qbApp.currentSession.callType == QB.webrtc.CallType.VIDEO) {
                ui.showCallBtn();
                /** delete blob from myself video */
                document.getElementById('localVideo').src = '';
                /** disable controls (mute cam/min) */
                ui.$ctrlBtn.removeClass('active');
                /** delete callee video elements */
                $('.j-callee').remove();
                /** clear main video */
                qbApp.currentSession.detachMediaStream('main_video');
                qbApp.mainVideo = 0;
                if(qbApp.callTimer) {
                    clearInterval(qbApp.callTimer);
                    qbApp.callTimer= 0;
                }
          // remoteStreamCounter = 0;
            }
            else{
                ui.showAudioCallBtn();
            /** delete blob from myself video */
            document.getElementById('localAudio').src = '';
            /** disable controls (mute cam/min) */
            ui.$ctrlBtn.removeClass('active');
            /** delete callee video elements */
            $('.j-callee').remove();
            /** clear main audio */
           qbApp.currentSession.detachMediaStream('localAudio');
           qbApp.mainAudio = 0;
                if(qbApp.callTimer) {
                    clearInterval(qbApp.callTimer);
                    qbApp.callTimer= 0;
                }
          // remoteStreamCounter = 0;
            }
        };

        QB.webrtc.onSessionConnectionStateChangedListener = function onSessionConnectionStateChangedListener(session, userId, connectionState) {
            console.group('onSessionConnectionStateChangedListener.'+isUserBusy);
           // console.log(' onSessionConnectionStateChangedListener app is: ' + app);
            var isCallEnded,takedCallCallee,network;
            console.log('userId: ' + userId);
            console.log('Session: ' + session);
            console.log('Сonnection state: ' + connectionState);
            console.groupEnd();

	if(connectionState =='1'||connectionState =='2'){
  		 isUserBusy=true;
	}
	
            else if(connectionState === QB.webrtc.SessionConnectionState.CLOSED){
		console.log("state 5 enter");
              // ui.toggleRemoteVideoView(userId, 'clear');
                // document.getElementById(ui.sounds.rington).pause();
                if( !_.isEmpty(qbApp.currentSession) ) {
                    if ( Object.keys(qbApp.currentSession.peerConnections).length === 1 || userId === qbApp.currentSession.initiatorID) {
                        $(ui.modal.income_call).modal('hide');
                    }
                }

                isCallEnded = _.every(qbApp.currentSession.peerConnections, function(i) {
                    return i.iceConnectionState === 'closed';
                });
                /** remove filters */
                if( isCallEnded ) {
                    // ui.changeFilter('#localVideo', 'no');
                    // ui.changeFilter('#main_video', 'no');
                    $(ui.filterClassName).val('no');
                    $(ui.modal.income_call).modal('hide');
                    takedCallCallee = [];
                }

                if (qbApp.currentSession.currentUserID === qbApp.currentSession.initiatorID && !isCallEnded) {
                    /** get array if users without user who ends call */
                    takedCallCallee = _.reject(takedCallCallee, function(num){ return num.id === +userId; });

                    qbApp.MsgBoard.update('accept_call', {users: takedCallCallee});
                }

                if( _.isEmpty(qbApp.currentSession) || isCallEnded ) {
		if(qbApp.currentSession.callType == QB.webrtc.CallType.VIDEO){
                    if(qbApp.callTimer) {
                        $('#timer').addClass('hidden');
                        clearInterval(qbApp.callTimer);
                        qbApp.callTimer = null;
                        ui.callTime = 0;
                        network = {};
                    }
		}
		else{
		if(qbApp.callTimer) {
				        $('#audioTimer').addClass('hidden');
				        clearInterval(qbApp.callTimer);
				        qbApp.callTimer = null;
				        ui.callTime = 0;
				        network = {};
				    }
		}
                }
            }
		else{
		console.log("nothing");
		}
	if(isUserBusy==false){
	console.log("user is busy "+isUserBusy);
        //$("#callStatus").text="user is busy";
	alert("user is busy");
	}
        };


        /** Accept */
        $(document).on('click', '.j-accept', function() {
            console.log("accept call");
            var mediaParams = {
                audio: true,
                video: true,
                elemId: 'localVideo',
                options: {
                    muted: true,
                    mirror: true
                }
            };
            if(qbApp.currentSession.callType == QB.webrtc.CallType.AUDIO) {
                console.log("accept call AUDIO CALL ACCEPTED");
                 mediaParams = {
                    audio: true,
                    video: false,
                    elemId: 'localAudio',
                    options: {
                        muted: false,
                        mirror: false
                    }
                }
            }
            $(ui.modal.income_call).modal('hide');
	    if(qbApp.currentSession.callType == QB.webrtc.CallType.VIDEO){		
		$("#qbcallingdiv").modal();
	    }
	    else{
		$("#qbaudiocallingdiv").modal();
	    }
            qbApp.currentSession.getUserMedia(mediaParams, function(err, stream) {
var isDeviceAccess;
                if(err){
                    var errorMsg = '';

                    //for any error in video/audio
                    if (!stream.getAudioTracks().length || !stream.getVideoTracks().length) {

                    if(err && err.message) {
                        errorMsg += 'Error: ' + err.message;
                    } else {
                        errorMsg += 'device_not_found';
                    }
                    showErrorAccessPermission(err);
                    isDeviceAccess = false;
                        qbApp.currentSession.stop({});
                }
                }
                else {
                    qbApp.currentSession.accept({});
                    if(qbApp.currentSession.callType == QB.webrtc.CallType.VIDEO){
                        console.log("accept call AUDIO CALL ACCEPTED stream.getAudioTracks().length "+stream.getAudioTracks().length );
                        console.log("accept call AUDIO CALL ACCEPTED stream.getVideoTracks().length "+stream.getVideoTracks().length);
                        ui.hideCallBtn();
                    }else{
                        console.log("accept call AUDIO CALL ACCEPTED stream.getAudioTracks().length "+stream.getAudioTracks().length );
                        console.log("accept call AUDIO CALL ACCEPTED stream.getVideoTracks().length "+stream.getVideoTracks().length);
                        ui.hideAudioCallBtn();
                    }
                }
            });
        });


        /** Mute / Unmute cam / mic */
        $(document).on('click', '.j-caller__ctrl', function() {
            var $btn = $(this),
                isActive = $btn.hasClass('active');

            if( _.isEmpty( qbApp.currentSession)) {
                return false;
            } 
		
		else {
                if(isActive) {//to Unmute
		console.log("inside MUTE BUTTON 2 "+$btn.data('target'));
                    $btn.removeClass('active');
			 if($btn.data('target')=='audio'){
				 console.log("to mute audio of audio call");
				//document.getElementById("localAudio").muted = false;
				qbApp.currentSession.unmute( 'audio' );			
			}
			else if($btn.data('target')=='video_call_audio')
			{
                                console.log("to mute audio of video call");
				//document.getElementById("localVideo").muted = false;
				qbApp.currentSession.unmute( 'audio' );
			}
			else
			{
				 console.log("to hide video of video call");
				//document.getElementById("localVideo").muted = false;
				qbApp.currentSession.unmute( $btn.data('target') );
			}
		
                  //qbApp.currentSession.unmute( $btn.data('target') );
                } else {
     			//to mute
			console.log("inside MUTE BUTTON 3 "+$btn.data('target'));
			$btn.addClass('active');
			 if($btn.data('target')=='audio'){
				//document.getElementById("localAudio").muted = true;
				qbApp.currentSession.mute('audio');			
			}
			else if($btn.data('target')=='video_call_audio')
			{
                                console.log("yup123");
				//document.getElementById("localVideo").muted = true;
				qbApp.currentSession.mute('audio');
			}
			else
			{
                                console.log("yup");
				//document.getElementById("localVideo").muted = true;
				qbApp.currentSession.mute( $btn.data('target') );
			}
                    //qbApp.currentSession.mute( $btn.data('target') );
                }
              }
        });

        /** Reject */
        $(document).on('click', '.j-decline', function() {
            console.log("inside decline");
            if (!_.isEmpty(qbApp.currentSession)) {
                qbApp.currentSession.reject({});
                // document.getElementById(ui.sounds.rington).pause();
                $(ui.modal.income_call).modal('hide');
                console.log("inside reject");

            }
        });

    });


/** Hangup */
    $(document).on('click', '.j-hangup , .closeVideoCall', function() {
         if(!_.isEmpty(qbApp.currentSession)) {
	console.log("inside video Hangup");
	if(qbApp.currentSession) {
		 qbApp.currentSession.stop({});
            qbApp.currentSession = {};
            }
           
            if(qbApp.callTimer) {
                clearInterval(qbApp.callTimer);
                qbApp.callTimer=0;
            }
         }
    });

    /**audio Hangup */
    $(document).on('click', '.j-audioHangup , .closeAudioCall', function() {
         if(!_.isEmpty(qbApp.currentSession)) {
		console.log("inside audio Hangup");
            if(qbApp.currentSession) {
		 qbApp.currentSession.stop({});
            qbApp.currentSession = {};
            }
            if(qbApp.callTimer) {
                clearInterval(qbApp.callTimer);
            qbApp.callTimer= 0;
            }
         }
    });

}(window, jQuery));


