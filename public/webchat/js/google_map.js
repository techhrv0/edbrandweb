function displayMapIn(msgId,latitude,longitude,timeType,messageTime)
{
	console.log("inside display map in "+latitude+','+longitude);

	var tpl = '';
	var cont = $('#chats');
	var list = $('.chats', cont);

	tpl += '<li class="in unselect">';
	tpl += '<div class="bubble-image-in">';

	tpl +='<div class="location-thumb" href="#myLocation12" data-toggle="modal" onClick="fetchLocation('+latitude+','+longitude+')">';
	tpl += '<span class="body image" id="img_div">' +
		'<img id="' + msgId+ '_preview" data-fileName="" data-fileSize="" data-fileType="" src="https://maps.googleapis.com/maps/api/staticmap?center=' + latitude + "," + longitude + '&zoom=15&size=320x320&sensor=false&scale=2&markers=color:red%7Clabel:c%7C'+ latitude + "," + longitude+'"; style="min-width: 250px;max-width: 250px;">' ;
	/*'<div class="location_loder_position"><p id="'
	 + msgId
	 + '_msg" class="loader_set_location"><button class="btn_set">';
	 tpl +='<svg class="spinner-container" viewBox="0 0 44 44" style="position: absolute;top: -7px;left: -22px;">';
	 tpl +='<circle class="path" cx="22" cy="22" r="20" fill="none" stroke-width="3"></circle></svg></button>';
	 tpl	+='<span class="sr-only">Loading...</span></p></div></span>';*/
	tpl +='<div class="shade-in"></div>';
	tpl += '</div>';
	tpl+='<div class="message-meta">';
	tpl += '<span class="datetime_img">' + get_date(timeType,messageTime) + '</span>';
	tpl += '</div>';
	tpl += '</div>';
	tpl += '</li>';


	// var tpl = '';
	// tpl += '<li class="in"  >';
	// tpl+='<div class="message" href="#myLocation12" data-toggle="modal" onClick="fetchLocation('+latitude+','+longitude+')">';
	// /*tpl += '<div class="message" onClick="fetchLocation('+latitude+','+longitude+')">';*/
	// tpl += '<span class="body image" id="img_div"><img id="'
	// 		+ msgId
	// 		+ '_msg" style="max-width: 200px;max-height: 200px;cursor: pointer;" src="https://maps.googleapis.com/maps/api/staticmap?center=' + latitude + "," + longitude + '&zoom=15&size=320x320&sensor=false&scale=2&markers=color:red%7Clabel:c%7C'+ latitude + "," + longitude+'";></span>';
	// tpl += '<span class="datetime">' + get_date(timeType,messageTime) + '</span>';
	// // if(timeType=='current')
	// // tpl += '<span id="' + msgId
	// // 		+ '_dot" class="fa fa-clock-o clock_status" aria-hidden="true"/>';
	// tpl += '</div>';
	// tpl += '</li>';
	// updatLastMessage(currentChatUserName, lastChatById('location'));
	return tpl;
	}
function displayMapOut(msgId,latitude,longitude,timeType,messageTime)
{
	console.log("inside display map in "+latitude+','+longitude+','+timeType);


	var tpl = '';
	var cont = $('#chats');
	var list = $('.chats', cont);

	tpl += '<li class="out unselect">';
	tpl += '<div class="bubble-image">';

	tpl +='<div class="location-thumb" href="#myLocation12" data-toggle="modal" onClick="fetchLocation('+latitude+','+longitude+')">';
	tpl += '<span class="body image" id="img_div">' +
		'<img id="' + msgId+ '_preview" data-fileName="" data-fileSize="" data-fileType="" src="https://maps.googleapis.com/maps/api/staticmap?center=' + latitude + "," + longitude + '&zoom=15&size=320x320&sensor=false&scale=2&markers=color:red%7Clabel:c%7C'+ latitude + "," + longitude+'"; style="min-width: 250px;max-width: 250px;">' ;
		/*'<div class="location_loder_position"><p id="'
		+ msgId
		+ '_msg" class="loader_set_location"><button class="btn_set">';
	tpl +='<svg class="spinner-container" viewBox="0 0 44 44" style="position: absolute;top: -7px;left: -22px;">';
	tpl +='<circle class="path" cx="22" cy="22" r="20" fill="none" stroke-width="3"></circle></svg></button>';
	tpl	+='<span class="sr-only">Loading...</span></p></div></span>';*/
	tpl +='<div class="shade"></div>';
	tpl += '</div>';
	tpl+='<div class="message-meta">';
	tpl += '<span class="datetime_img">' + get_date('current', null) + '</span>';
	tpl += '<span id="' + msgId
		+ '_dot" class="fa fa-clock-o clock_status_img" aria-hidden="true"/>';
	tpl += '</div>';
	tpl += '</div>';
	tpl += '</li>';



	// var tpl = '';
	// tpl += '<li class="out"  >';
	// /*tpl += '<div class="message" onClick="fetchLocation('+latitude+','+longitude+')">';*/
	// tpl+='<div class="message" href="#myLocation12" data-toggle="modal" onClick="fetchLocation('+latitude+','+longitude+')">';
	// tpl += '<span class="body image" id="img_div"><img id="'
	// 		+ msgId
	// 		+ '_msg" style="max-width: 200px;max-height: 200px;cursor: pointer;" src="https://maps.googleapis.com/maps/api/staticmap?center=' + latitude + "," + longitude + '&zoom=15&size=320x320&sensor=false&scale=2&markers=color:red%7Clabel:c%7C'+ latitude + "," + longitude+'";></span>';
	// tpl += '<span class="datetime">' + get_date(timeType,messageTime) + '</span>';
	// if(timeType=='current'){
	// tpl += '<span id="' + msgId
	// 		+ '_dot" class="fa fa-clock-o clock_status" aria-hidden="true"/>';}
	// else
	// 	tpl += '<span id="' + msgId	+ '_dot" class="dot background_gray" aria-hidden="true"/>';
	// tpl += '</div>';
	// tpl += '</li>';
	
	return tpl;
	}
function displayNearby(msgId,latitude,longitude,name)
{
	console.log("inside display map in "+latitude+','+longitude);
	var location=latitude+','+longitude+','+name;
	if (send_message(location,msgId,'location')) {
		add_to_view(display_map(latitude,longitude,name, msgId,'out','current',null));
		// updatLastMessage(currentChatUser, chatTextByFileName('sticker', sticker));
		// storeChatMessage(sticker, userName, currentChatUser,msgId, 'sticker');

	}

}
function display_map(latitude,longitude,name, msgId,pos,timeType,messageTime)
{
	var tpl='';
	if(pos == 'in'){
		tpl=displayMapIn(msgId,latitude,longitude,timeType,messageTime);

	}else{
		console.log("map out");
		tpl=displayMapOut(msgId,latitude,longitude,timeType,messageTime);
	}
	return tpl;
}

function fetchLocation(latitude,longitude)
{
	/*var mapProp = {
		    center:new google.maps.LatLng(latitude,longitude),
		    zoom:10,
		    mapTypeId:google.maps.MapTypeId.ROADMAP
		    
		  };
		  var map=new google.maps.Map(document.getElementById("map-canvas"),mapProp);
		  //$("#map").css({"left":"100px", "top":"100px","visibility":"visible"});
		  new google.maps.Marker({map:map, position: new google.maps.LatLng(latitude,longitude)});*/
	var map;        
    var myCenter=new google.maps.LatLng(latitude,longitude);
var marker=new google.maps.Marker({
position:myCenter
});

function initialize()
{
var mapProp = {
center:myCenter,
zoom: 16,
draggable: false,
scrollwheel: false,
mapTypeId:google.maps.MapTypeId.ROADMAP
};

map=new google.maps.Map(document.getElementById("map-canvas_large"),mapProp);
marker.setMap(map);

google.maps.event.addListener(marker, 'click', function() {

infowindow.setContent(contentString);
infowindow.open(map, marker);

}); 
}
google.maps.event.addDomListener(window, 'load', initialize());

google.maps.event.addDomListener(window, "resize", resizingMap());

$('#myLocation12').on('show.bs.modal', function() {
//Must wait until the render of the modal appear, thats why we use the resizeMap and NOT resizingMap!! ;-)
resizeMap();
});
function resizeMap() {
	if(typeof map =="undefined") return;
	setTimeout( function(){resizingMap();} , 400);
	}
function resizingMap() {
	if(typeof map =="undefined") return;
	var center = map.getCenter();
	google.maps.event.trigger(map, "resize");
	map.setCenter(center); 
	}
}


function getUserCurrentLocation()
{
var msgId = $.now();
addFileLoader(msgId);
getLocation(msgId);
}

	
function sendMap(name,latitude,longitude,msgId)
{
	var flag=false;
	console.log("inside send map");
	if (connection.connected && connection.authenticated) {
		
		if (name.length > 0) {
			connection.send(sendMapXml(name, latitude, longitude,msgId).tree());
			flag=true;
		}
	} else {
		log("You are log out.");
		flag=false;
	}
	return flag;
}

// function sendMapXml(name, latitude, longitude,msgId)
// {
// 	console.log("inside send map xml "+name+' ,'+latitude+' ,'+longitude);
// 	var reply = $msg({
// 		to : currentChatUser + "@" + domain,
// 		from : connection.jid,
// 		type : "chat",
// 		id : msgId,
// 	}).c("body").t(latitude+','+longitude+','+name).up();
// 	reply.c("request", {
// 		"xmlns" : "urn:xmpp:receipts"
// 	});
// 	reply.c("properties", {
// 		xmlns : 'http://www.jivesoftware.com/xmlns/xmpp/properties'
// 	}).c("edit", "0").up();
// 	reply.c("sendTimestampDate", "1234").up();
// 	reply.c("quickBoxID", "1234").up();
// 	reply.c("bodyType", "location").up();
// 	reply.c("filelocalpath",latitude+','+longitude+','+name).up();
// 	reply.c("fileSize","00").up();
// 	reply.c("fileName","00").up();
//     reply.c("ThumbBase64String","00").up().up();
// 	return reply;
// 	}


function getLocation(msgId)
{
	var name='';
	var latitude='';
	var longitude='';
	console.log("inside updated getLocation");
	var output = document.getElementById(msgId+'_msg');

	  function success(position) {
	    latitude  = position.coords.latitude;
	   longitude = position.coords.longitude;
	   console.log("inside get location "+latitude+','+longitude);
	   var latlng = new google.maps.LatLng(latitude, longitude);
	    var geocoder = geocoder = new google.maps.Geocoder();
	    geocoder.geocode({ 'latLng': latlng }, function (results, status) {
	        if (status == google.maps.GeocoderStatus.OK) {
	            if (results[2]) {
	            	console.log("inside results2");
	            	 address=name=extractFromAdress(results[0].address_components, "route");
	            	 console.log("name is "+name);
	            	 if(sendMap(name,latitude,longitude,msgId))
	            		 {
	            		 console.log("send map");
	            		 storeChatMessage(latitude+','+longitude+','+name, userName, currentChatUser,msgId, MESSAGE_TYPE_MAP);
	            	         }
	            	 else 
	            		 {
	            		 return false;
	            		 
	            		 }
	            	 getNearbyPlaces(latitude,longitude);
	            }
	        }
	    });
	  document.getElementById(msgId + "_msg").src ="https://maps.googleapis.com/maps/api/staticmap?center=" + latitude + "," + longitude + "&zoom=15&size=320x320&sensor=false&scale=2&markers=color:red%7Clabel:c%7C"+ latitude + "," + longitude;
	  $("#"+msgId + "_msg").attr('onClick','fetchLocation('+latitude+','+longitude+')');
	  $("#"+msgId + "_msg").attr('data-toggle','modal');
	 // $("#"+msgId + "_msg").attr('data-target','#myLocation12');
	  $("#"+msgId + "_msg").attr('href','#myLocation12');
	  


}
	function error() {
	    output.innerHTML = "Unable to retrieve your location";
	  }
	navigator.geolocation.getCurrentPosition(success, error);
	 updatLastMessage(currentChatUserName, lastChatById('location'));
	
	}
function extractFromAdress(components, type){
	 for (var i=0; i<components.length; i++)
	  for (var j=0; j<components[i].types.length; j++)
	   if (components[i].types[j]==type) return components[i].long_name;
	  return "";
	}

function getNearbyPlaces(myCentre)
{
	console.log("inside nearby place");
	//console.log("latitude and longitude is "+latitude+' , '+longitude);
	var map;
	var infowindow;
	  //var pyrmont = {lat: latitude, lng: longitude};

	  map = new google.maps.Map(document.getElementById('map-canvas'), {
	    center: myCentre,
	    zoom: 15
	  });

	  infowindow = new google.maps.InfoWindow();
	  var service = new google.maps.places.PlacesService(map);
	  service.nearbySearch({
	    location: myCentre,
	    radius: 2000,
	    type: ['point_of_interest','establishment','food']
	  }, callback);
	}

	function callback(results, status) {
	  if (status === google.maps.places.PlacesServiceStatus.OK) {
		  var tpl='';
	    for (var i = 0; i < results.length; i++) {
	    	var msgId=$.now();
	    	var messageType="current";
	    	var time=null;
	    	
	    	tpl+="<li data-dismiss='modal' onClick='displayNearby(\""+msgId+ "\",\"" +results[i].geometry.location.lat()+ "\",\""+results[i].geometry.location.lng()+ "\",\""+results[i].name+"\")'>"+results[i].name+"<p>132</p></li>";
	      console.log(results[i].geometry.location.lat()+' '+results[i].geometry.location.lng()+','+results[i].name);
	    }
	    var list = $("#myId");
	    console.log(tpl);
	    list.append(tpl);
	  }
	
	  
		  function getPlaces() {
		        var map = new google.maps.Map(document.getElementById('map-canvas'), {
		          center: {lat: -33.8688, lng: 151.2195},
		          zoom: 13,
		          mapTypeId: google.maps.MapTypeId.ROADMAP
		        });

		        // Create the search box and link it to the UI element.
		        var input = document.getElementById('pac-input');
		        var searchBox = new google.maps.places.SearchBox(input);
		        console.log("search box"+searchBox);
		        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

		        // Bias the SearchBox results towards current map's viewport.
		        map.addListener('bounds_changed', function() {
		          searchBox.setBounds(map.getBounds());
		        });

		        var markers = [];
		        // Listen for the event fired when the user selects a prediction and retrieve
		        // more details for that place.
		        searchBox.addListener('places_changed', function() {
		          var places = searchBox.getPlaces();
		          console.log("places is "+places);

		          if (places.length == 0) {
		            return;
		          }

		          // Clear out the old markers.
		          markers.forEach(function(marker) {
		            marker.setMap(null);
		          });
		          markers = [];

		          // For each place, get the icon, name and location.
		          var bounds = new google.maps.LatLngBounds();
		          places.forEach(function(place) {
		            var icon = {
		              url: place.icon,
		              size: new google.maps.Size(71, 71),
		              origin: new google.maps.Point(0, 0),
		              anchor: new google.maps.Point(17, 34),
		              scaledSize: new google.maps.Size(25, 25)
		            };

		            // Create a marker for each place.
		            markers.push(new google.maps.Marker({
		              map: map,
		              icon: icon,
		              title: place.name,
		              position: place.geometry.location
		            }));

		            if (place.geometry.viewport) {
		              // Only geocodes have viewport.
		              bounds.union(place.geometry.viewport);
		            } else {
		              bounds.extend(place.geometry.location);
		            }
		          });
		          map.fitBounds(bounds);
		        });
		      }
		  
	  function getSearchValues()
	  {
          console.log("inside get search");
              var input = document.getElementById('pac-input');
              var autocomplete = new google.maps.places.Autocomplete(input);
            
              google.maps.event.addListener(autocomplete, 'place_changed',
                  function() {
                      var place = autocomplete.getPlace();
                      var lat = place.geometry.location.lat();
                      var lng = place.geometry.location.lng();
                      document.getElementById("output").innerHTML = "Lat: "+lat+"<br />Lng: "+lng;
                  } 
              );
          
        google.maps.event.addDomListener(window, 'load', getSearchValues);
	  }
}
	function getMapView()
	{
		console.log("inside map view");
		var map;        
		 if (navigator.geolocation) {
			 console.log("inside navigator.geolocation");
			    navigator.geolocation.getCurrentPosition(function (position) {
			    	console.log("inside navigator.geolocation123");
	    var myCenter=new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
	    var geocoder = geocoder = new google.maps.Geocoder();
	    geocoder.geocode({ 'latLng': myCenter }, function (results, status) {
	        if (status == google.maps.GeocoderStatus.OK) {
	            if (results[2]) {
	            	/*var latitude=position.coords.latitude;
	            	var longitude=position.coords.longitude;*/
	            	getNearbyPlaces(myCenter);
	            	 address=name=extractFromAdress(results[0].address_components, "route");
	            	 console.log(name);
	            	 var infowindow=new google.maps.InfoWindow();
	            	 infowindow.setContent(name);
	            
	var marker=new google.maps.Marker({
	position:myCenter
	});

	function initialize()
	{
	var mapProp = {
	center:myCenter,
	zoom: 10,
	draggable: false,
	scrollwheel: false,
	mapTypeId:google.maps.MapTypeId.ROADMAP
	};

	map=new google.maps.Map(document.getElementById("map-canvas"),mapProp);
	marker.setMap(map);
	infowindow.open(map, marker);

	
	}
	
			    
	google.maps.event.addDomListener(window, 'load', initialize());

	google.maps.event.addDomListener(window, "resize", resizingMap());

	$('#myModalocation').on('show.bs.modal', function() {
	//Must wait until the render of the modal appear, thats why we use the resizeMap and NOT resizingMap!! ;-)
	resizeMap();
	
	});
	            }
		        }
		    });
			    });
	function resizeMap() {
		if(typeof map =="undefined") return;
		setTimeout( function(){resizingMap();} , 400);
		}
	function resizingMap() {
		if(typeof map =="undefined") return;
		var center = map.getCenter();
		google.maps.event.trigger(map, "resize");
		map.setCenter(center); 
		}
		 }
	
}