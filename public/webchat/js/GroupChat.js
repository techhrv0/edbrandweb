/**
 * Created by khyati on 7/9/16.
 */
function grpnameclear() {
    updateGroupSubject1();
    var elem=document.getElementById('groupSubject');
        elem.style.transition="all 0.2s cubic-bezier(0.1, 0.82, 0.25, 1)";
        elem.style.top="0px";
        elem.style.fontSize="13px";
    $("#grpname").empty();
    stopEnter();
    $('#grpname').keydown(function (event) {
        var keyCode = (event.keyCode ? event.keyCode : event.which);

        if (keyCode == 13) {
            $('#nextbtn').trigger('click');
        }
    });
}
function getGrpNameFocus() {
    $("#grpname").focus();
}

function addToGroupArr(memberId, nick,type) {
    $('.' + memberId + 'media').remove();
    $('.' + memberId + '_divMember').remove();
    if ($.inArray(memberId, groupMember) === -1) {
        groupMember.push(memberId);
        var tpl = '<div id="' + memberId + '_grpMemberSelect" class="selected_member col-sm-12 unselect"><div class="col-sm-3 selected_icon"><img class="' + memberId + '_avtar_member_list media-object" src="' + userPic + '"  onerror="imgError(this,\'' + memberId + '\');" width="35px" height="35px" style="border-radius: 50%;"></div> <div class="col-sm-7 selected_icon2"><p style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">' + (memberId, nick) + '</p></div><div class="col-sm-2 selected_icon1" ><h1 id="' + memberId + '_rmMemberList" onclick="removeMemberList(\'' + memberId + '\',\'' + nick + '\',\'' + type + '\')" class="fa fa-times close modal_header_icons"></h1></div></div>';
        $('#MemberAdd').append(tpl);
        set_profile_pic(memberId, PROFILE_TYPE.MEMBERADDTOLIST);
        var scrollMax = $('#MemberAdd').prop('scrollHeight') + 'px';
        $('#MemberAdd').slimscroll({
            scrollTo: scrollMax
        })
    }
}

function removeMemberList(memberId, nick,type) {
    $('#' + memberId + '_grpMemberSelect').remove();
    groupMember = jQuery.grep(groupMember, function (value) {
        return value != memberId;
    });
    var oldMembers=[];
    try{
        if(type=='addGrpMember')
        oldMembers=getGroupMembersId(getPhonebookMap(chatWithContact.userId).groupMembers);
    }
    catch (e){}
    var newMembers ={};
    $.each(phnbookMap,function(key,val) {
        if(($.inArray(phnbookMap[key].userId, groupMember)==-1)&&(phnbookMap[key].selectMode==SELECT_MODE.CHAT)){
            if(type!='addGrpMemberNew'){
                if(($.inArray(phnbookMap[key].userId, oldMembers)==-1)){
                    newMembers[phnbookMap[key].userId]=phnbookMap[key];
                }
            }
            else{
                newMembers[phnbookMap[key].userId]=phnbookMap[key];
            }

        }
    });
   $(".chat_usersc").empty();
    sort_contacts(newMembers, type,false);
}

function group_list() {
    $("#searchContactgrp").val('');
    $(".chat_usersc").empty();
    var sorted = "users_list_for_grp"
    $('#grpAction').html(' <button type="submit" class="btn btn-default btn-lg button_setting " data-dismiss="modal" onclick="createNewGroup()">Create Group</button>');
    sort_contacts(phnbookMap,sorted,false);
    $("#MemberAdd").html('');
}

function imgErrorgs(image, groupJID) {
    image.onerror = "";
    image.src = "/webchat/assets/images/group_pic.png";
    if (getrecentsMap(groupJID).groupJID == groupJID) {
        $('#' + groupJID + '_avtar_Grp_Recent').removeClass('compatible');
        $('#' + groupJID + '_avtar_Grp_Recent').addClass('compatibledefault');
        $('#' + groupJID + '_avtar_contact_Grp').removeClass('compatible');
        $('#' + groupJID + '_avtar_contact_Grp').addClass('compatibledefault');
        $('#' + groupJID + '_avtar_contact_shareGrp').removeClass('compatible');
        $('#' + groupJID + '_avtar_contact_shareGrp').addClass('compatibledefault');
    }
    return true;
}

function updateGroupSubject1() {
    $('#groupCounter1').show();
    stopEnter();
    var max = 25;
    var z = document.getElementById("grpSubject");
    if (z.contentEditable != "true") {
        z.contentEditable = "true";
        $("#grpname").keydown(function (e) {
            var count = $('#groupCounter1');
            var characters = $(this).text().length;
            var charact = characters + 1;
            count.text(max - charact);
            if (e.which < 0x25) {
                count.text(max - (characters));
                return;
            }
            if (charact == max) {
                e.preventDefault();
            }
            if (e.ctrlKey) {
                if (e.keyCode == 86) {//Paste
                    if (charact > max - 1) {
                        $('#groupCounter1').text('0');
                        e.preventDefault();
                        $('#grpname').text($('#grpname').text().substring(0, max - 1));
                    }
                }
            }
        });
    }
}

function updateGroupSubject() {
    $('#groupCounter').show();
    stopEnter();
    var max = 25;
    var z = document.getElementById("grpSubject");
    if (z.contentEditable != "true") {
        z.contentEditable = "true";
        $("#pencil2").hide();
        $("#tick2").show();
        $("#grpSubject").keydown(function (e) {
            var count = $('#groupCounter');
            var characters = $(this).text().length;
            var charact = characters + 1;
            count.text(max - charact);
            if (e.which < 0x25) {
                count.text(max - (characters));
                return;
            }
            if (charact == max) {
                e.preventDefault();
            }
            if (e.ctrlKey) {
                if (e.keyCode == 86) {//Paste
                    if (charact > max - 1) {
                        $('#groupCounter').text('0');
                        e.preventDefault();
                        $('#grpSubject').text($('#grpSubject').text().substring(0, max - 1));
                    }
                }
            }
        });
    }
    $("#tick2").removeClass("tick_blue");
    $("#tick2").addClass("tick_grey");
}

function saveGroupSubject() {
    $("#groupCounter").hide();
    $('#grpSubject').blur();
    var z = document.getElementById("grpSubject");
    z.contentEditable = "false";

    $("#pencil2").show();
    $("#tick2").hide();
    if ($('#grpSubject').text() == "") {
        alert("Group subject can't be empty.");
        $('#grpSubject').text(getrecentsMap(chatWithContact.userId).subject);
        $("#groupCounter").hide();
        return false;
    }
    if ($('#grpSubject').text() == getrecentsMap(chatWithContact.userId).subject) {
        $("#groupCounter").hide();
        return false;
    }
    if ($('#grpSubject').text() != "") {
        changeGroupSubject( $('#grpSubject').text());
    }
}

function set_grp_list(data,type,isSearched) {
    groupMember = [];
    var msgId = $.now();
    var emoji = new EmojiConvertor();
    // var eText=emoji.replace_unified(text);
    emoji.text_mode = true;
    emoji.include_title = true;
    emoji.img_path = "/webchat/js/js-emoji-master/build/emoji-data/img-apple-64/";
    var time = new Date();
    var time_str = (time.getHours() + ':' + time.getMinutes());
    var tplc = "";
    tplc += "<div class='select_contact unselect'>";
    tplc += '<ul class="media-list contactList">';
    tplc += "<li class='" + data.userId + "media' id='" + data.userId + "_addMemberList' onclick='addToGroupArr(\"" + data.userId + "\", \"" + data.name + "\",\"" + type + "\");'>";
    tplc += "<div class='contact_list' style='width:100%;height:66px;padding: 5px 0 ;'>";
    tplc += "<div class='compatible' id='" + data.userId + "_avtar_contact_shareGrp' style='width: 73px; float: left; padding: 5px 10px ! important;'>";
    tplc += "<img class='" + data.userId + "_avtar_contact_share media-object ' id='' src='" + userPic + "' alt='' onerror='imgErrors(this,\"" + data.userId + "\");'>";
    tplc += "</div>";
    tplc += "<div class='chat_body_width'>";
    tplc += "<div class='contactlist_panal'>";
    tplc += "<h4 class='contact_name'>" +truncateUserName(data.name)+ "</h4>";
    tplc += "<p class='contact_name_set'>" + truncateString(data.status) + "</p>";
    tplc += "</div>";
    tplc += "</div>";
    tplc += "</div>";
    tplc += '</li>';
    tplc += "</ul>";
    tplc += "</div>";
    $(".chat_usersc").append(tplc);
}


function append_recentsGroup(data) {
    //to append new created group in recent chat
    var tpl = '<ul class="media-list list-items unselect" id="' + data.userId + '_divRecentsUL">';
    tpl += "<li class='media' id='" + data.userId + "_divRecentsGrp' onclick='onStartChat(\"" + data.userId + "\");'>";
    tpl += "<div class='contact_list style='width:100%;height:66px;padding: 5px 0 ;'>";
    tpl += "<div class='compatible' id='" + data.userId + "_avtar_Grp_RecentA' style='width:25%;float:left;'>";
    tpl += "<img class='media-object' id='" + data.userId + "_avtar_recent' src=" + groupPic + " alt='' onerror='imgErrorgs(this,\"" + data.userId + "\");'>";
    tpl += "</div>";
    tpl += "<div class='chat_body'>";
    tpl += "<div class='contactlist_panal'>";
    tpl += "<div class='name_time'>";
    tpl += "<h4 class='contact_name'>" + truncateUserName(data.name) + "</h4>";
    tpl += "<div class='online_status'>";
    tpl += "<p id='" + data.userId + "_time' class='time text'>" + get_date("current", null) + "</p>";
    tpl += "<p class='badge badge-danger'  id='" + data.userId + "_cnt' ></p>";
    tpl += "</div>";
    tpl += "</div>";
    tpl += "<div class='icon_msg_set'>";
    tpl += "<p id='" + data.userId + "_last_msg' class='message_text'></p>";
    tpl += "</div>";
    tpl += "</div>";
    tpl += "</div>";
    tpl += "</div>";
    tpl += '</li>';
    $(".chat_users").prepend(tpl);
    $('#' + data.userId + '_divRecentsUL').prependTo("#contactsView");
}

function checkForMsgRepeat($xmlObj) {
    if (($($xmlObj).find("phone").text() == phone) && ($($xmlObj).find("message").attr('type') == 'groupchat'))
        return false;
    else return true;
}

function showGroupMembers(groupId) {
    $('#add_member_icon').html('');
    var group_userId = [];
    var members=parseGroupMembersJson(getPhonebookMap(groupId).groupMembers);
    var memberMap={};
    //create members MAP
    $.each(members, function (key, value) {
        var member={
            userId:value.userId,
            affiliation:value.affiliation,
            name:getPhonebookMap(value.userId).name,
            // selectMode:SELECT_MODE.CHAT
        };
        memberMap[value.userId]=member;
    });

    $('#grpSubject').text(getPhonebookMap(groupId).name);
    // if(getPhonebookMap(groupId).createdBy==userName){
        $('#add_member_icon').html('<h4> Participants<div id="grpParticipants"></div></h4>');
    document.getElementById("grpParticipants").innerHTML = members.length;
    // }
    $(".chat_usersc").empty();
    sort_contacts(memberMap,'showGrpMembers',false);
    try{
        var grpCreateTime =new Date(getPhonebookMap(groupId).createdTs);
        var createBy = getPhonebookMap(groupId).createdBy;
        var hours = grpCreateTime.getHours();
        var minutes = grpCreateTime.getMinutes();
        var year = grpCreateTime.getFullYear();
        var month = 1 + (grpCreateTime.getMonth());
        var day = grpCreateTime.getDate();
        hours = (hours < 10) ? "0" + hours : hours;
        minutes = (minutes < 10) ? "0" + minutes : minutes;
        year = (year < 10) ? "0" + year : year;
        month = (month < 10) ? "0" + month : month;
        day = (day < 10) ? "0" + day : day;
        if(getPhonebookMap(groupId).createdBy==userName){
            document.getElementById("createTime").innerHTML='Created by You : '+ day + '/' + month + '/' + year + ' at ' + hours + ':' + minutes;
        }
        else{
            if (getPhonebookMap(createBy).selectMode == SELECT_MODE.CHAT)
               var names = getPhonebookMap(createBy).name ;
            else
                var names = getPhonebookMap(createBy).phone;

            document.getElementById("createTime").innerHTML='Created by '+names+' : '+ day + '/' + month + '/' + year + ' at ' + hours + ':' + minutes;
        }
    }
    catch (e){}
}

function displayGroupMembers(value) {
        var tplc = '';
        tplc += "<div class='" + value.userId + "_memberListDIV select_contact unselect' id='" + value.userId + "_memberList'>";
        tplc += "<ul class='media-list contactList'>";
        tplc += "<div class='contact_list' style='width:100%;height:66px;padding: 5px 0 ;'>";
        tplc += "<div class='compatible' id='" + value.userId + "_memberLisT' style='width:73px;float:left;padding: 5px 10px !important;'>";
        tplc += "<div class='icon_set'>";
        tplc += "<img class='media-object ' id='" + value.userId + "_avtar_contact_share' data-toggle='modal' src='" + userPic + "' alt='' onerror='imgErrors(this,\"" + value.userId + "\");'>";
        tplc += "</div>";
        tplc += "</div>";
        tplc += "<div class='chat_body_widthG'>";
        tplc += "<div class='contactlist_panal'>";
        if(value.userId==userName){
            tplc += "<h4 class='contact_name' style='width:67%;'>You</h4>";
        }
        else{
            tplc += "<h4 class='contact_name' style='width:67%;'>" + getPhonebookMap(value.userId).name + "</h4>";
        }

        if((getPhonebookMap(chatWithContact.userId).createdBy==value.userId)||(value.affiliation=='admin')){
            tplc +='<div class="chat-marker chat-marker-admin">Group Admin</div>';
        }
        tplc += "<p class='contact_name_set'>" + encodeEmoji(getPhonebookMap(value.userId).status) + "</p>";
        // if(((getPhonebookMap(chatWithContact.userId).createdBy==userName)||(value.affiliation=='admin'))&&(value.userId!=userName)) {
        //     tplc += " <div class='remButton' style='display:block'><span><span class='dropdown'></span>" +
        //         "<a href='#' class='dropdown-toggle' data-toggle='dropdown' data-hover='dropdown' data-close-others='true' id='arrow_down' onclick='dropdownSet()'>" +
        //         "<i class='fa fa-angle-down' aria-hidden='true' ></i></button><ul class='dropdown-menu' aria-labelledby='arrow_down'>" +
        //         "<li onclick='removeMembers(\"" + value.userId + "\")'><a href='#'>Remove</a></li><li onclick='make_Admin(\"" + value.userId + "\")'><a href='#'>Make Admin</a></li></ul></a></span></div>";
        // }
    if(((getPhonebookMap(chatWithContact.userId).createdBy==userName)||(value.affiliation=='admin'))&&(value.userId!=userName)) {
        tplc += " <div class='remButton' style='display:block'><span><span class='dropdown'></span>" +
            "<a href='#' class='dropdown-toggle' data-toggle='dropdown' data-hover='dropdown' data-close-others='true' id='arrow_down' onclick='dropdownSet()'>" +
            "<i class='fa fa-angle-down' aria-hidden='true' ></i></button><ul class='dropdown-menu' aria-labelledby='arrow_down'>" +
            "<li onclick='removeMembers(\"" + value.userId + "\")'><a href='#'>Remove</a></li><li onclick='make_Admin(\"" + value.userId + "\")'><a href='#'>Make Admin</a></li></ul></a></span></div>";
    }
        tplc += "</div></div></div></ul></div>";
        $(".chat_usersc").append(tplc);
        set_profile_pic(value.userId, PROFILE_TYPE.DISPLAYGRPMEMBERS);
}
function dropdownSet() {
        var x = $(".memberIcon").offset();
        //alert("Top: " + x.top + " Left: " + x.left);
    }
/** searching in group by divya**/
jQuery(document).ready(function () {
    $("#searchContactgrp").keyup(function (e) {
        $(".chat_usersc").empty();
        var searchedKey = $("#searchContactgrp").val();
        var searchedKeyL = searchedKey.toLowerCase();
        get_group_con(phnbookMap,  searchedKeyL);
    });
});

function get_group_con(phnbookMap, searchedKeyL) {
    var result = [];
    var sorted = "addToNewGroup";
    $.each(phnbookMap, function (key, val) {
        if(phnbookMap[key].selectMode==SELECT_MODE.CHAT){
            var contactNames = phnbookMap[key].name;
            var contactNamesL = contactNames.toLowerCase();
            if (contactNamesL.indexOf(searchedKeyL) != -1) {
                result.push(val);
            }
        }
    });
    if (!jQuery.isEmptyObject(result)) {//not empty
        $('#nocontactNewGrp').text("");
        if ($('#searchContactgrp').value == "") {

        }
        else {
            setGroup(result,sorted,true);
        }
    }
    if (jQuery.isEmptyObject(result)) {// empty
        $('#nocontactNewGrp').text("No contacts found");
    }
}

function showGroupNotification(message) {
    return '<li class="out notification_bubble"><div class="notification"><div class="notification_body unselect"><span class="body">'+message+'</span></div></div><div class="clearfix"></div></li>';
}

//for scroll script
$(function(){
        $('#grpScrollModal').slimScroll({
                height: '500px',
                color: 'rgb(187, 187, 187) none repeat scroll 0% 0%'
        });
    });