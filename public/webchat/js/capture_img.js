//web capture method
//capture image
var captureimage="";
function addcaptureFileLoader() {
    msgId=$.now();
    var cont = $('#chats');
    var list = $('.chats', cont);
    setDayNotification();
    var tpl = '';
    tpl += '<li class="out unselect">';
    tpl += '<div class="bubble-image">';
    tpl +='<div class="image-thumb">';
    tpl += '<span class="body image" id="img_div" ><div><img id="' + msgId+ '_preview" src="'+captureimage+'" style="min-width: 330px;"/></div><div class="loder_position" id="'+ msgId+ '_loder_position"><p id="' + msgId+ '_uploadicon" onclick="send_again(' + msgId+ ',\'image\')">'  +
        '<p id="'+ msgId+ '_imageloader" class="loader_set"><button id="'+msgId+'_stopbutton"  class="btn_set" onclick=""><img src="webchat/assets/images/crosss.png" width="27px" height="27px"/>';
    tpl +='<svg class="spinner-container" viewBox="0 0 44 44" style="position: absolute;top: -7px;left: -22px;">';
    tpl +='<circle class="path" cx="22" cy="22" r="20" fill="none" stroke-width="3"></circle></svg></button>';
    tpl	+='<span class="sr-only">Loading...</span></p></p></div></span>';
    tpl += '<span class="sr-only" >Loading...</span>';
    tpl +='<div class="shade"></div>';
    tpl += '</div>';
    tpl+='<div class="message-meta">';
    tpl += '<span class="datetime_img">' + get_date('current',null) + '</span>';
    tpl += '<span id="' + msgId
        + '_dot" class="fa fa-clock-o clock_status_img" aria-hidden="true"/>';
    tpl += '</div>';
    tpl += '</div>';
    tpl += '</li>';
    list.append(tpl);
    var scrollTo_val = $('#chats').prop('scrollHeight') + 'px';
    $("#chats").slimScroll({
        scrollTo : scrollTo_val
    });
    uploadImage(captureimage,msgId,msgId+'.png');
    $('#'+ msgId+ '_stopbutton').attr('onclick','stoploader('+msgId+')');
    $('#demo').hide();
    $('#plusIcon').show();
    $('#crossIcon').hide();
    captureImageClose();
}
function openwebcamera()
{
    $(".caputreImageHide").addClass("hide");
    $("#capture-image").show();
    $(".bottom-msgs").addClass("hide");
    $("#image_aftercapture").hide();
    $("#send_image").hide();
    $("#show_web").show();
    $("#capture_button").show();
    Webcam.set({
        width: 640,
        height: 420,
        dest_width: 640,
        dest_height: 480,
        image_format: 'jpeg',
        jpeg_quality: 100,
        enable_flash:true,
        force_flash: false,
        flip_horiz: true,
        fps: 45
    });
    Webcam.init();
    console.log("webcam set");
    Webcam.attach('#show_web');
}

function take_snapshot() {
    $("#show_web").hide();
    $("#send_image").show();
    $("#capture_button").hide();
    $("#image_aftercapture").show();
    // take snapshot and get image data
    console.log("in snapshot method");
    $("#capture_img").show();
    Webcam.snap( function(data_uri) {
        imageData.base64='';
        imageData.size='';
        Webcam.reset();
        // display results in page
        console.log("show web disable");
        $("#image_aftercapture").attr("src", data_uri);
        captureimage=data_uri;
        imageData.base64=captureimage.split(",").pop();
        imageData.size=imageData.base64.shrinked.length;
    } );
    console.log("print show web enable");
}

function captureImageClose() {
    $(".captureImageShow").hide();
    $(".caputreImageHide").removeClass("hide");
    $(".bottom-msgs").removeClass("hide");
}
function send_snapshot(images,msgId){
    console.log("am in methodddd"+" "+images)
    $.ajax({
        url : '/chat/imageuploadcapture',
        type : 'POST',
        cache : false,
        dataType : 'json',
        data : {
            images : images,
            msgId : msgId,
            // sessionId:'<0.29607.1>',
            sessionId:currentSession,// uncomment this line before going live on server in all file upload code
        },
        error : function() {
        },
        success : function(response) {
            console.log("this is responseeeee"+response)
            var file= response.files;
            //console.log("capture image file name: "+file);
            console.log("capture image file name: "+ JSON.stringify(file));

            if(response.status=="200"){
                //var files = response.file;
                console.log("capture image file name: "+ JSON.stringify(file));
                addDoc(file);

            }}
    });
}

function uploadCapturePic() {
    msgId=$.now();
    upload_profilepic(captureimage,msgId);
}

function upload_profilepic(images,msgId){
    console.log("am in methodddd for upload profile")
    $.ajax({
        url : '/chat/profileimageuploadcapture',
        type : 'POST',
        cache : false,
        dataType : 'json',
        data : {
            images : images,
            msgId : msgId,
            // sessionId:'<0.29607.1>'
            sessionId:currentSession,// uncomment this line before going live on server in all file upload code
        },
        error : function() {
        },
        success : function(response) {
            console.log('inside upload_profilepic resposne');
            var file= response.files;
            if(response.status=="200"){
                if(($('#mystatus').text())!=null && ($('#mynick').text())!=null){
                    console.log("upload_profilepic updated ");
                    // $('#' + chatWithContact.userId+ '_avtar_recent').attr('src',images);
                    // $('#' + chatWithContact.userId+ '_avtar_contact').attr('src',images);
                    //$('#friendProfilePic').attr('src',images);
                    $('#userCroppedPic').attr('src',images);
                    $('#userCroppedPic').css({'display':'block'});
                    $('.croppie-container').css({'display':'none'});
                    $('.cr-boundary').html('');
                    $('.cr-boundary').css({'display':'none'});
                    $('.cr-slider-wrap').html('');
                    $('.cr-slider-wrap').css({'display':'none'});
                    $('.croppie-container').find('.croppie-container').css({'display':'none'});
                    $('.basic-result').css({'display':'none'});
                    $('.zoomout').css({'display':'none'});
                    $('.zoomin').css({'display':'none'});
                    publishMyVcard(null, $('#mynick').text(), msgId);
                    reloadimg(images);
                }
            }
        }
    });
}

function upload_group_profilepic(images,msgId){
    $.ajax({
        url : '/chat/GroupProfileimageuploadcapture',
        type : 'POST',
        cache : false,
        dataType : 'json',
        data : {
            images : images,
            groupJid:chatWithContact.userId,
            msgId : msgId,
            // sessionId:'<0.29607.1>',
            sessionId:currentSession,// uncomment this line before going live on server in all file upload code
        },
        error : function() {
        },
        success : function(response) {
            var file= response.files;
            console.log("capture image file name: "+file);
            if(response.status=="200"){
                if(($('#mystatus').text())!=null && ($('#mynick').text())!=null){
                    console.log("upload_profilepic updated ");
                    $('#groupUserCroppedPic').attr('src',images);
                    $('#friendProfilePic').attr('src',images);
                    $('#' + chatWithContact.userId+ '_avtar_recent').attr('src',images);
                    $('#groupUserCroppedPic').css({'display':'block'});
                    $('.croppie-container').css({'display':'none'});
                    $('.cr-boundary').html('');
                    $('.cr-boundary').css({'display':'none'});
                    $('.cr-slider-wrap').html('');
                    $('.cr-slider-wrap').css({'display':'none'});
                    $('.croppie-container').find('.croppie-container').css({'display':'none'});
                    $('.basic-result').css({'display':'none'});
                    $('.zoomout').css({'display':'none'});
                    $('.zoomin').css({'display':'none'});
                    changeGroupProfilePic();
                    // publishMyVcard($('#mystatus').text(),$('#mynick').text(),msgId);
                }
            }}
    });
}

function upload_group_profilepic1(images,msgId,fileName){
    $.ajax({
        url : '/chat/grpCaptureImageUpload',
        // url : '/chat/groupProfileImageupload',
        type : 'POST',
        cache : false,
        dataType : 'json',
        data : {
            filename :fileName ,
            //images :images ,
            // groupJid:chatWithContact.userId,
            groupJid:userName + '_' + msgId,
            msgId : msgId,
            // sessionId:'<0.29607.1>',
            sessionId:currentSession,// uncomment this line before going live on server in all file upload code
        },
        error : function() {
        },
        success : function(response) {
            var file= response.files;
            console.log("capture image file name1: "+file);
            if(response.status=="200"){
                if(($('#mystatus').text())!=null && ($('#mynick').text())!=null){
                    console.log("upload_profilepic updated ");
                    $('#groupUserCroppedPic1').attr('src',images);
                    $('#friendProfilePic1').attr('src',images);
                    $('#' + chatWithContact.userId+ '_avtar_recent').attr('src',images);
                    $('#groupUserCroppedPic1').css({'display':'block'});
                    $('.croppie-container').css({'display':'none'});
                    $('.cr-boundary').html('');
                    $('.cr-boundary').css({'display':'none'});
                    $('.cr-slider-wrap').html('');
                    $('.cr-slider-wrap').css({'display':'none'});
                    $('.croppie-container').find('.croppie-container').css({'display':'none'});
                    $('.basic-result').css({'display':'none'});
                    $('.zoomout').css({'display':'none'});
                    $('.zoomin').css({'display':'none'});
                    changeGroupProfilePic();
                    // publishMyVcard($('#mystatus').text(),$('#mynick').text(),msgId);
                }
            }}
    });
}


function openPhotoWebCamera() {
    $(".bottom-msgs").addClass("hide");
    $("#image_aftercapturep").hide();
    $("#send_imagep").hide();
    $("#show_webp").show();
    $("#capture_buttonp").show();
    Webcam.set({
        width: 322,
        height: 242,
        dest_width: 640,
        dest_height: 480,
        image_format: 'jpeg',
        jpeg_quality: 100,
        enable_flash:true,
        force_flash: false,
        flip_horiz: true,
        fps: 45
    });
    Webcam.init();
    Webcam.attach('#show_webp');
}

function take_photo() {
    console.log("inside take_photo()");
    $("#show_webp").hide();
    $("#send_imagep").show();
    $("#capture_buttonp").hide();
    $("#image_aftercapturep").show();
    // take snapshot and get image data
    console.log("in snapshot method");
    $("#capture_imgp").show();
    Webcam.snap( function(data_uri) {
        // imageData.base64='';
        // imageData.size='';
        Webcam.reset();
        console.log("show web disable");
        $("#image_aftercapturep").attr("src", data_uri);
        captureimage=data_uri;
        // imageData.base64=captureimage.split(",").pop();
        // imageData.size=imageData.base64.shrinked.length;
    } );
    console.log("print show web enable");
}

function openPhotoWebCameraG() {
    $(".bottom-msgs").addClass("hide");
    $("#image_aftercaptureG").hide();
    $("#send_imageG").hide();
    $("#show_webG").show();
    $("#capture_buttonG").show();
    Webcam.set({
        width: 640,
        height: 420,
        dest_width: 640,
        dest_height: 480,
        image_format: 'jpeg',
        jpeg_quality: 100,
        enable_flash:true,
        force_flash: false,
        flip_horiz: true,
        fps: 45
    });
    Webcam.init();
    Webcam.attach('#show_webG');
}


function take_photoG()
{
    console.log("inside take_photoG()");
    $("#show_webG").hide();
    $("#send_imageG").show();
    $("#capture_buttonG").hide();
    $("#image_aftercaptureG").show();
    // take snapshot and get image data
    console.log("in snapshot method");
    $("#capture_imgG").show();
    Webcam.snap( function(data_uri) {
        // imageData.base64='';
        // imageData.size='';
        Webcam.reset();
        console.log("show web disable");
        $("#image_aftercaptureG").attr("src", data_uri);
        captureimage=data_uri;
        // imageData.base64=captureimage.split(",").pop();
        // imageData.size=imageData.base64.shrinked.length;
    } );
}

function openPhotoWebCameraGps()
{
    $(".bottom-msgs").addClass("hide");
    $("#image_aftercaptureGps").hide();
    $("#send_imageGps").hide();
    $("#show_webGps").show();
    $("#capture_buttonGps").show();
    Webcam.set({
        width: 640,
        height: 420,
        dest_width: 640,
        dest_height: 480,
        image_format: 'jpeg',
        jpeg_quality: 100,
        enable_flash:true,
        force_flash: false,
        flip_horiz: true,
        fps: 45
    });
    Webcam.init();
    Webcam.attach('#show_webGps');
}

function take_photoGps()
{
    $("#show_webGps").hide();
    $("#send_imageGps").show();
    $("#capture_buttonGps").hide();
    $("#image_aftercaptureGps").show();

    $("#capture_imgGps").show();
    Webcam.snap( function(data_uri) {
        Webcam.reset();
        $("#image_aftercaptureGps").attr("src", data_uri);
        captureimage=data_uri;
    } );
}

function update_snapshotGps(){
    console.log( "update_snapshotGps()");

    msgId=$.now();
    $.ajax({
        // url:'/chat/grpCaptureImageUpload',
        url:'/chat/GroupProfileimageuploadcapture',
        type:'POST',
        cache:false,
        dataType:'json',
        data:{
            images:captureimage,
            groupJid:chatWithContact.userId,
            msgId:msgId,
            // sessionId:'<0.29607.1>', //this line used for local
            sessionId:currentSession, //this line uncomment before commit live;
        },
        error:function(){
        },
        success:function (response) {

            if(response.status=="200"){
                if(($('#mystatus').text())!=null&&($('#mynick').text())!=null){
                    //reloadImage('http://images.ciaoim.com/profile/'+groupJid+'.png',groupJid);
                    //publishMyVcard(null, null, msgId);
                    changeGroupProfilePic();
                }
                else
                    alert("Error: Status/Name cannot be blank");
            }

        }
    });
}