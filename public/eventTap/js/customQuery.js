var drooly = {
   getSpeakerList: function(){      
        var formData = {};
        formData['search'] = $('#search').val();
         formData['page'] = '1';
        $.ajax({
            type: "POST",
            url: '/users/speaker-data-list',
            datatype:'html',
            data: formData,
            async: true,
            success:function(data,status){
                $('#resultData').html("");
                $('#resultData').html(data);
            },

            failure: (function () { alert("Failure!!"); })
        });
    },
    getAgendaList: function(){
        var formData = {};
        formData['search'] = $('#search').val();
        formData['page'] = '1';
        formData['agendaLocation'] = $("#ddlfilterLocation").val();
        formData['keyword'] = $("#ddlfilterSessionType").val();
        formData['theme'] = $("#ddlfilterTheme").val();
        formData['filterDate'] = $("#filterDate").val();
        $.ajax({
            type: "POST",
            url: '/users/agenda-data',
            datatype:'html',
            data: formData,
            async: true,
            success:function(data,status){
                $('#resultData').html("");
                $('#resultData').html(data);
            },

            failure: (function () { alert("Failure!!"); })
        });
    },
    
    getSpeakerListLogin: function(){
        var formData = {};
        formData['search'] = $('#searchWebinarSpeaker').val();
        formData['page'] = '1';
        $.ajax({
            type: "POST",
            url: '/users/speaker-data-list-login',
            datatype:'html',
            data: formData,
            async: true,
            success:function(data,status){
//                alert(data); return false;
                $('#resultData').html("");
                $('#resultData').html(data);
            },

            failure: (function () { alert("Failure!!"); })
        });
    },
    getAgendaListLogin: function(){
        var formData = {};
        formData['search'] = $('#search').val();
        formData['page'] = '1';
        formData['agendaLocation'] = $("#ddlfilterLocation").val();
        formData['keyword'] = $("#ddlfilterSessionType").val();
        formData['theme'] = $("#ddlfilterTheme").val();
        formData['type'] = $("#agendaType").val();
        var url;
        if(formData['type']==1 || formData['type']==2){
          url = '/users/agenda-data-login'; 
        }else{
          url = '/users/agenda-data-auditorium';  
        }
        $.ajax({
            type: "POST",
            url: url,
            datatype:'html',
            data: formData,
            async: true,
            success:function(data,status){
                $('#resultDataAgenda').html("");
                $('#resultDataAgenda').html(data);
            },

            failure: (function () { alert("Failure!!"); })
        });
    },
    
   getMentorsListLogin: function(){
        var formData = {};
        formData['search'] = $('#searchWebinarMentors').val();
        formData['page'] = '1';
        $.ajax({
            type: "POST",
            url: '/users/mentor-data-list-login',
            datatype:'html',
            data: formData,
            async: true,
            success:function(data,status){
                $('#mentorsresultData').html("");
                $('#mentorsresultData').html(data);
            },

            failure: (function () { alert("Failure!!"); })
        });
    },
   
  getAttendeListLogin: function(){
        var formData = {};
        formData['search'] = $('#searchWebinarAttende').val();
        formData['page'] = '1';
        $.ajax({
            type: "POST",
            url: '/users/attende-data-list-login',
            datatype:'html',
            data: formData,
            async: true,
            success:function(data,status){
                $('#attenderesultData').html("");
                $('#attenderesultData').html(data);
            },

            failure: (function () { alert("Failure!!"); })
        });
    },
    
    
    getImatchList: function(){
        var formData = {};
        //formData['search'] = $('#searchWebinarAttende').val();
        formData['page'] = '1';
        $.ajax({
            type: "POST",
            url: '/users/imatch',
            datatype:'html',
            data: formData,
            async: true,
            success:function(data,status){
                //$('#resultDataImatch').html("");
                $('#resultDataImatch').html(data);
            },

            failure: (function () { alert("Failure!!"); })
        });
   },
      getMyMeetingLogin: function(){
        var formData = {};
        formData['search'] = $('#searchWebinarMyMeeting').val();
        formData['page'] = '1';
        $.ajax({
            type: "POST",
            url: '/users/mymeeting-data-list-login',
            datatype:'html',
            data: formData,
            async: true,
            success:function(data,status){
                $('#mymeetingresultData').html("");
                $('#mymeetingresultData').html(data);
            },

            failure: (function () { alert("Failure!!"); })
        });
    },
    
    
   getAuditoriumBanner: function(){
        var formData = {};
        formData['search'] = $('#searchWebinarAttende').val();
        formData['page'] = '1';
        $.ajax({
            type: "POST",
            url: '/users/auditorium-banner',
            datatype:'html',
            data: formData,
            async: true,
            success:function(data,status){
                $('#AudiBanner').html("");
                $('#AudiBanner').html(data);
            },

            failure: (function () { alert("Failure!!"); })
        });
    },
    
    
 getAttendeListForMeeting: function(){
        $.ajax({
            type: "POST",
            url: '/users/attende-list-for-meeting',
            datatype:'html',
            data: '',
            async: true,
            success:function(data,status){
                $('#attendeForMeeting').html("");
                $('#attendeForMeeting').html(data);
            },

            failure: (function () { alert("Failure!!"); })
        });
    },
   
    
     getQuickPoll: function(){
     var qrCode=$("#qrCode").val();
        $.ajax({
            type: "POST",
            url: '/users/quickPollQr',
            datatype:'json',
            data: {qrCode:qrCode},
            async: true,
            success:function(data){
//                alert(data); return false;
             if(data.send.status==0){   
                alert(data.send.message);
                return false;
            }else{
               $("#qrcode-form").css('display','none'); 
                $('#pollQuestion').html("");
                $('#pollQuestion').html(data);
//                $("#quick-poll-questions").css('display','block'); 
                  
                  //  window.location.href = '/users/quick-poll/'+btoa(qrCode);
                  return true;
            }
            },

            failure: (function () { alert("Failure!!"); })
        });
    },
   getQuickPollQuestions: function(idArr){
        $.ajax({
            type: "POST",
            url: '/users/quick-poll',
            datatype:'html',
            data: {idArr:idArr},
            async: true,
            success:function(data,){                
                $('#quickpolldata').html(data);
            },
            failure: (function () { alert("Failure!!"); })
        });
    },
    
    
  getQuickPollGraph: function(){
      var id = $("#quickpollId").val();
      var chartType = $("#myselect").val();
        $.ajax({
            type: "POST",
            url: '/users/quickpollgraph',
            datatype:'html',
            data: {id:id,chartType:chartType},
            async: true,
            success:function(data){  
               $('#columnchart_values').html(data);
               $("#quick-poll-graph").css('display','block'); 
            },
            failure: (function () { alert("Failure!!"); })
        });
    },  
    
    
    
   getQuickPollModal: function(){
        $.ajax({
            type: "POST",
            url: '/users/show-quick-poll',
            datatype:'json',
            data: {},
            async: true,
            success:function(data,){                
                $('#showPoll').html("");
                $('#showPoll').html(data);
            },
            failure: (function () { alert("Failure!!"); })
        });
    },  
    
    
  getSurveyQuestions: function(idArr){
        $.ajax({
            type: "POST",
            url: '/users/usersessionfeedback',
            datatype:'json',
            data: {idArr:idArr},
            async: true,
            success:function(data){   
//                alert(data); return false;
                $('#surveydata').html("");
                $('#surveydata').html(data);
            },
            failure: (function () { alert("Failure!!"); })
        });
    }
  
    
};      
//method to append data  
function appendUserDetail(username,type){
  $("#usernameonw").val("");
  $("#usernameonw").val(username);
  $.ajax({
            type: "POST",
            url: '/users/user-detail',
            data: {username:username,type:type},
            success: function (data) {
                    if(type=="speaker"){
                     //$('#speakerModal .speakerdetail').html("");
                     $('#speakerModal .speakerdetail').html(data);
                   }else{
                     //$('#EditSpeakerModal .transport1').html("");
                     $('#speakerModalNew .speakerdetail').html(data);  
                   }
            },
                    
               failure: (function () { alert("Failure!!"); })
            });    
     
 }
 //method to append data  
function appendUserDetailNew(username,type){
  $("#usernameonw").val("");
  $("#usernameonw").val(username);
  $.ajax({
            type: "POST",
            url: '/users/user-detail-login',
            data: {username:username,type:type},
            success: function (data) {
//                alert(data); return false;
                    if(type=="speaker"){
                     //$('#speakerModal .speakerdetail').html("");
                     $('#speakerModal .speakerdetail').html(data);
                   }else{
                     //$('#EditSpeakerModal .transport1').html("");
                     $('#speakerModalNew .speakerdetail').html(data);  
                   }
            },
                    
               failure: (function () { alert("Failure!!"); })
            });    
     
 } 
  //method to append data  
function appendAgenda(agendaId){
  $.ajax({
            type: "POST",
            url: '/users/agenda-detail',
            data: {agendaId:agendaId},
            success: function (data) {
                     $('#agendaModal .appendAgenda').html(data);
            },
                    
               failure: (function () { alert("Failure!!"); })
       });    
 }
 //method for book agenda
 function bookAgenda(agendaId,isLike){
    var jobdata;
    if(isLike==1){
       jobdata = {agendaId:agendaId,isLike:isLike}; 
    }else{
      var r = confirm("Are you sure to unbook agenda ?");
        if (r == true) {
             jobdata = {agendaId:agendaId,isLike:isLike}; 
        }  else {
            return false;
        }          
    }
    $.ajax({
            type: "POST",
            url: '/users/book-agenda',
            data: jobdata,
            success: function (data) {
                if(data.send.status=='1'){
                  alert(data.send.message);
                  $('#agendaModal').modal('hide');
                  drooly.getAgendaListLogin();
                }else{
                  alert(data.send.message);
                  return false;
                }   
                
            },
                    
               failure: (function () { alert("Failure!!"); })
       }); 
 }
 //method to append data  
function edituser(type){
  $.ajax({
            type: "POST",
            url: '/users/edit-user',
            data: {agendaId:type},
            success: function (data) {
                     $('#registrationModal .edituser').html(data);
                   
            },
                    
               failure: (function () { alert("Failure!!"); })
       });    
 }
 
 
 function sendMeetingRequest(){
  var imatchUserId = $("#imatchUserId").val();
//  alert(imatchUserId); return false;
  var meetingTitle = $("#meetingTitle").val();
  var meetingDate = $("#meetingDate").val();
  var meetingDescrption = $("#meetingDescrption").val();
  if(meetingTitle==""){
    alert("Title is Required");
    return false;
  }
 if(meetingDate==""){
   alert("Date is Required");
  return false;  
 }
   $.ajax({
            type: "POST",
            url: '/users/add-meeting',
            data: {imatchUserId:imatchUserId,title:meetingTitle,dateTime:meetingDate,description:meetingDescrption},
            success: function (data) {
                if(data.send.status=='1'){
                  //alert(data.send.message);
                  //$("#request-meeting").css('display','none');
                  //$('#requestmeetingdata').html("");
                  //$(".carousel-control-prev").css('display','flex'); 
                  //$(".carousel-control-next").css('display','flex');
                  $("#request-meeting-content").css('display','none');
                  $("#message").html(data.send.message);
                  $("#meeting-schedule-success").css('display','block');
                  return true;
                }else{
                  $("#message").html(data.send.message);
                  $("#meeting-schedule-success").css('display','block');
                  return true;
                }    
            },
                    
               failure: (function () { alert("Failure!!"); })
            });
}
function appendMeetingDetail(meetingId){
  $.ajax({
            type: "POST",
            url: '/users/meeting-detail',
            data: {meetingId:meetingId},
            success: function (data) {
                     $('#speakerModalNew .speakerdetail').html(data);  
            },
                    
               failure: (function () { alert("Failure!!"); })
            });    
     
 }
 function appendAgendaVideo(agendaId){
  $.ajax({
            type: "POST",
            url: '/users/append-agenda-video',
            data: {agendaId:agendaId},
            success: function (data) {
                     $('#videoModal .appendagendavideo').html(data);
                     $('#agenda-video1')[0].play();
            },
                    
               failure: (function () { alert("Failure!!"); })
       });    
 }
 
 function sendMeetingRequestToAttende(){
  var imatchUserId = $("#imatchUserIdnew").val();
  var meetingTitle = $("#meetingTitleNew").val();
  var meetingDate = $("#meeting-date-new").val();
  var meetingDescrption = $("#meetingDescripton").val();
  if(meetingTitle==""){
    alert("Title is Required");
    return false;
  }
 if(meetingDate==""){
   alert("Date is Required");
   return false;  
 }
   $.ajax({
            type: "POST",
            url: '/users/add-meeting-new',
            data: {imatchUserId:imatchUserId,title:meetingTitle,dateTime:meetingDate,description:meetingDescrption},
            success: function (data) {
//                alert(data); return false;
                if(data.send.status=='1'){
                  //alert(data.send.message);
                  //$('#attendeForMeeting').html("");
                  //$("#NetworkScheduleMeetingModal").modal('hide');
                  $('#NetworkScheduleMeetingModal .container').css('display','none');
                  $('#NetworkScheduleMeetingModal #network-sel-list-attendees').css('display','none');
                  $('#NetworkScheduleMeetingModal #message').html(data.send.message);
                  $('#NetworkScheduleMeetingModal #meeting-schedule-successnew').css('display','block');
                  return true;
                }else{
                  $('#NetworkScheduleMeetingModal #message').html(data.send.message);
                  $('#NetworkScheduleMeetingModal #meeting-schedule-successnew').css('display','block');
                  return true;
                }
                    
            },
                    
               failure: (function () { alert("Failure!!"); })
            });
}
function sendMeetingRequestAttende(){
  var imatchUserId = $("#imatchUserId").val();
  var meetingTitle = $("#meetingTitleAttende").val();
  var meetingDate = $("#meetingDateAttende").val();
  var meetingDescrption = $("#meetingDescrptionAttende").val();
  if(meetingTitle==""){
    alert("Title is Required");
    return false;
  }
 if(meetingDate==""){
   alert("Date is Required");
   return false;  
 }
   $.ajax({
            type: "POST",
            url: '/users/add-meeting',
            data: {imatchUserId:imatchUserId,title:meetingTitle,dateTime:meetingDate,description:meetingDescrption},
            success: function (data) {
                if(data.send.status=='1'){
                  //alert(data.send.message);
                  //$('#attendeScheduleMeetingModal .requestmeetingattende').html("");  
                  //$('#attendeScheduleMeetingModal').modal('hide');
                  $('#attendemeetingdata').css('display','none');
                  $('#messageattende').html(data.send.message);
                  $('#meeting-schedule-success-attende').css('display','block');
                  return true;
                }else{
                  //alert(data.send.message);
                  $('#messageattende').html(data.send.message);
                  $('#meeting-schedule-success-attende').css('display','block');
                  return true;
                }
                
                    
            },
                    
               failure: (function () { alert("Failure!!"); })
            });
}
//function isValidEmail(emailText) {
//    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
//    return pattern.test(emailText);
//};   
//function loginUser(){
//     var username = $("#loginemail").val();
//     var pass = $("#exampleInputPassword1").val();
//    if(username==""){
//       alert("Email is required");   
//       return false;
//    }
//    if( !isValidEmail(username) ) {
//       alert("Email is not valid");  
//       return false;
//    }
//    if(pass==""){
//      alert("Password is required");  
//       return false;
//    }
//     $.ajax({
//        type: "POST",
//        url: '/application/index/login',
//       data: {username:username,password:pass},
//       datatype:'json',
//       
//       beforeSend: function (data) {
//                $("#startup").show();
//            },
//       success: function (data) {
//            if(data.send.status==0){   
//                alert(data.send.message);
//                return false;
//            } else {
//                //window.location.href = data.send.url;
//                $("#loginModal").modal("hide");
//                $("#intro-screen").css('display','none');
//                $("#welcomeimage").attr("src",data.send.profilePic);
//                $("#profileimage1").attr("src",data.send.profilePic);
//                $("#profileimage2").attr("src",data.send.profilePic);
//                $("#userfirstname").html(data.send.firstName);
//                $("#main").css('display','block');
//                return true;
//           }
//       },
//       failure: (function () { alert("Failure!!"); })
//      });       
// }
