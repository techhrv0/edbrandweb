$(function() {
    //caches a jQuery object containing the header element
    var header = $("nav");
    $(window).scroll(function() {
        var scroll = $(window).scrollTop();

        if (scroll >= 200) {
            header.addClass("sticky-header bg-light");
			jQuery('.navbar-brand img').attr('src', '/eventTap/images/eventap-logo.svg');
        } else {
            header.removeClass("sticky-header bg-light");
			jQuery('.navbar-brand img').attr('src', '/eventTap/images/eventap-logo-horizontal.svg');
        }
    });
});
//Multistep Form Wizard

$(document).ready(function () {
	var isFS=true;
$("#loginSubmit").click(function(e){
	 e.preventDefault();
	  $("#frmlogin").find("div.form-group").removeClass("has-error");
	  $("#frmlogin").find("div.form-group").find('.invalid-feedback').css('display','none');
	curInputs = $("#frmlogin").find("input[type='email'],input[type='password']");
	isValid = true;
	for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
				$(curInputs[i]).closest(".form-group").find('.invalid-feedback').css('display','block');
            }

        }
        if(isValid){
			$("#frmlogin").submit();
			
		}
});
    var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn'),
			allPrevBtn=$('.prevBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
		
		if(!isFS){
			var visibleId = $('.setup-content:visible').prop('id');
            var curStepBtn = $(".stepwizard-step").find("a.btn-primary").attr("href");
            var nextStepWizard = $('div.setup-panel div a[href="' + curStepBtn + '"]').parent().next().children("a");
            var curInputs = $("#"+visibleId).find("select,input[type='text'],input[type='url'],input[type='email'],input[type='tel'],input[type='password']");
            var isValid = true;
			
        $("#signupFirst").find("div.form-group").removeClass("has-error");
		$("#signupFirst").find("div.form-group").find('.invalid-feedback').css('display','none');
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
				$(curInputs[i]).closest(".form-group").find('.invalid-feedback').css('display','block');
            }
        }
        if (isValid)
		{			
			var $target = $($(this).attr('href'));
              var  $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
		}
		}
		}
		else{
			
			 var $target = $($(this).attr('href'));
              var  $item = $(this);
        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
		isFS=false;
		}
		
       
    });

    allNextBtn.click(function(e){
        var curStep = $(this).closest(".setup-content");
            var curStepBtn = curStep.attr("id");
			
            var nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a");
            var curInputs = curStep.find("select,input[type='text'],input[type='url'],input[type='email'],input[type='tel'],input[type='password']");
            var isValid = true;

        $("#signupFirst").find("div.form-group").removeClass("has-error");
		$("#signupFirst").find("div.form-group").find('.invalid-feedback').css('display','none');
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
				$(curInputs[i]).closest(".form-group").find('.invalid-feedback').css('display','block');
            }
        }
        if (isValid){
			console.log(curStepBtn);
			if(curStepBtn=="step-1"){
				
                            var email = $("#regemail").val();
                            var countyCode = $("#Country-Code").val();
                            var mobile = $("#mobile").val();
                            var type =1;
                            $.ajax({
        type: "POST",
        url: '/application/index/emailcheck',
       data: {email:email,countyCode:countyCode,mobile:mobile,type:type},
       datatype:'json',
       beforeSend: function (data) {
                $("#startup").show();
            },
       success: function (data) {
            if(data.send.status==0){   
                alert(data.send.message);
                return false;
            }
			else{
				//alert('no email')
				nextStepWizard.removeAttr('disabled').trigger('click');
				 return false;
			}
return false;			
        },
       failure: (function () { alert("Failure!!");  return false;})
      });
				
			
		}
		else{
			nextStepWizard.removeAttr('disabled').trigger('click');
			
		}
		}
    });
	
	 allPrevBtn.click(function(){
		 allWells.hide();
		  navListItems.removeClass('btn-primary');
		  var dtid=$(this).attr('data-target-id');
		  $("div.setup-panel div a[href='"+dtid+"']").addClass('btn-primary');
		  $(dtid).css('display','block');
    });

    $('div.setup-panel div a.btn-primary').trigger('click');
});

