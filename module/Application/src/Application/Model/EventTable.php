<?php
namespace Application\Model;
use Zend\Db\Sql\Sql;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Select;
use CustomLib\Model\CustomTable;
use Zend\Db\Sql\Predicate\Expression;

class EventTable extends CustomTable
{
    protected $table = 'eventProgramForAu';
    //method to get list of guests
    public function getAllUsers($data=array(), $pageing = true, $testAccount)
    {
       $searchUser = strtolower($data['search']);
       $filter = $data['filter'];
       //Filters according to the dates
       if($filter=="all") {
            $filterQuery = "and t1.type='attende'";
        } elseif ($filter=="vip") {
            $filterQuery = "and t1.type='attende' and t1.attendeType='VIP'";
        } elseif ($filter=="hrprac") {
            $filterQuery = "and t1.type='attende' and t1.attendeType='HRPRAC' ";
        } elseif ($filter=="serviceprovider") {
            $filterQuery = "and t1.type='attende' and t1.attendeType='SERVICEPRO'";
        }elseif ($filter=="totalLogin") {
            $filterQuery = "and t1.type='attende' and t1.fb_verfiy= 1";
        } elseif ($filter=="iosLogin") {
             $filterQuery = "and t1.type='attende' and t3.device_type='iOS' ";
        } elseif ($filter=="androidLogin") {
             $filterQuery = "and t1.type='attende' and t3.device_type='android' ";
        }elseif ($filter=="notLogin") {
            $filterQuery = "and t1.type='attende' and t1.fb_verfiy= 0";
        }
        else {
             $filterQuery = '';
        }
        //code for city filter
        if(!empty($data['cityFilter'])){
         $cityFilter = "and t2.city='".$data['cityFilter']."'";   
        }else{
         $cityFilter = "";   
        }
      if(!empty($data['fromDate']) && !empty($data['toDate'])){
        $filterQuery1 = "and DATE_FORMAT(FROM_UNIXTIME(t1.uts), '%Y-%m-%d') >='".$data['fromDate']."' and   DATE_FORMAT(FROM_UNIXTIME(t1.uts), '%Y-%m-%d') <= '".$data['toDate']."'";
      }else{
       $filterQuery1 = ""; 
      }
        //search
        if ($searchUser != '') {
           $searchnm = "and ((lower(t2.firstName) like \"%" . $searchUser . "%\") or (lower(t1.qrCode) like \"%" . $searchUser . "%\") or (lower(t2.email) like \"%" . $searchUser . "%\") or (lower(t2.subCategory) like \"%" . $searchUser . "%\") or (lower(t2.phone) like \"%" . $searchUser . "%\"))";
        } else {
           $searchnm = '';
        }
        $sql = new Sql($this->adapter);
        $select = $sql->select(array(
                  't1' =>'users'
                ));
        $select->columns(array(  
         'username',
         'uts',
         'type',
         'attendeType',
         'qrCode',
         'sms_code'  
        ));
        $select->join(
        array('t2' =>'vcard_search'),
        new Expression('t2.username = t1.username'),
        array(
       'countryCode',
       'firstName',
       'lastName',
       'email',
       'phone',
       'city',
       'country',
       'companyName',
       'designation',
       ),
        'LEFT'
    );
    $select->join(array('t3' =>'auth_session'), new Expression('t3.user_id = t1.username'), array('device_type'),'LEFT');
    $select->join(array('t4' =>'last'), new Expression('t4.username = t1.username'), array('seconds'),'LEFT');
    if ($data['firstNameOrder']=="asc"){
            $select->order(array(
          't2.firstName ASC'
         ));
    } elseif ($data['firstNameOrder']=="desc") {
            $select->order(array(
          't2.firstName DESC'
       ));
    } else{
       $select->order(array(
          't1.id DESC'
       ));
   }
   $select->where(array("t1.extra3=1 and t1.active=1 and t1.status =0 and t1.attendeType!='TechHR' and t1.testAccount='".$testAccount."'  $searchnm $filterQuery $cityFilter $filterQuery1"));
   if($pageing){
         $dbAdapter = new DbSelect($select, $this->getAdapter());
         $paginator = new Paginator($dbAdapter);
         return $paginator;
   } else {
         $smt = $sql->prepareStatementForSqlObject($select);
         $result = $this->resultSetPrototype->initialize($smt->execute())->toArray();
         return $result;
    }
  }
   //method for counter    
  public function getCounterOfUsers($data,$testAccount){
       $sql = new Sql($this->adapter);
       $select = $sql->select(array(
                        't1' =>'users'
                            ));
       $select->columns(array(
           'count' => new \Zend\Db\Sql\Expression('COUNT(t1.username)'),
       )); 
       $select->join(array('t2' =>'vcard_search'), new Expression('t2.username = t1.username'), array('firstName'),'LEFT');
       $select->join(array('t3' =>'auth_session'), new Expression('t3.user_id = t1.username'), array('device_type'),'LEFT');
      if($data =='all'){
       $select->where(array("t1.extra3=1 and t1.ticket=0 and  t1.testAccount='".$testAccount."' and t1.active=1  and t1.status =0  and t1.type='attende' and attendeType!='TechHR'"));
      }elseif($data=='VIP'){
       $select->where(array("t1.extra3=1 and t1.ticket=0 and t1.testAccount='".$testAccount."' and t1.type='attende'  and t1.active=1 and t1.status =0 and attendeType='VIP'"));
      }else if ($data=='HRPRAC'){
       $select->where(array("t1.extra3=1 and t1.ticket=0 and t1.testAccount='".$testAccount."' and t1.type='attende'  and t1.active=1 and t1.status =0 and attendeType='HRPRAC'"));
      }else if ($data=='SERVICEPRO'){
       $select->where(array("t1.extra3=1 and t1.ticket=0 and t1.testAccount='".$testAccount."' and t1.type='attende' and t1.active=1 and t1.status =0 and attendeType='SERVICEPRO'"));
      }else if ($data=='totalLogin'){
       $select->where(array("t1.extra3=1 and t1.ticket=0  and t1.fb_verfiy= 1 and t1.testAccount='".$testAccount."'  and t1.active=1 and t1.status =0 and t1.type='attende' and  attendeType!='TechHR'"));
      }else if ($data=='iosLogin'){
        $select->where(array("t1.status=0 and  t1.extra3=1 and t1.ticket=0 and t1.testAccount='".$testAccount."' and t1.active=1 and t1.type='attende' and t3.device_type='iOS' and attendeType!='TechHR'"));
      }else if ($data=='androidLogin'){
        $select->where(array("t1.status=0 and t1.extra3=1 and t1.ticket=0 and t1.testAccount='".$testAccount."'  and t1.active=1 and t1.type='attende'  and t3.device_type='android' and attendeType!='TechHR'"));
      }else if($data=='notLogin'){
       $select->where(array("t1.extra3=1 and t1.ticket=0  and t1.fb_verfiy= 0 and t1.testAccount='".$testAccount."'  and t1.active=1 and t1.status =0 and t1.type='attende' and  attendeType!='TechHR'"));
      }
      $smt = $sql->prepareStatementForSqlObject($select);
      $result = $this->resultSetPrototype->initialize($smt->execute())->toArray();
      return $result[0]['count']; 
  }
 //get Guest detail for edit 
 public function getUserDetailOnEdit($userId,$role,$child){
    $sql = new Sql($this->adapter);
    $select = $sql->select(array('t1' =>'users'));
     $select->columns(array(
        'username',
         'type'
        )); 
 $select->join(
        array('t2' =>'vcard_search'),
        new Expression('t2.username = t1.username'),
        array(     
       'countryCode',
       'firstName',
       'lastName',
       'email',
       'phone',
       'city',
       'country',
       'companyName',
       'designation',
       'gender','linkedinUrl'),
        'LEFT'
    );

    $select->where(array("t1.username='".$userId."'"));
    
    
  //$select->where(array("t1.active=1 and t1.status !=1 and t2.parentUserId ='' and t1.testAccount='".$testAccount."'  $searchnm $filterQuery"));
    
    $smt = $sql->prepareStatementForSqlObject($select);
//    echo $smt->getSql(); die;
    $result = $this->resultSetPrototype->initialize($smt->execute())->toArray();
    return $result;    
  }
public function getUserSkills($userId){
    $sql = new Sql($this->getAdapter());
          $select = new Select();
          $select->from(array(
              't1' => 'user_skills' 
          ));
          $select->columns(array(
           'id'=>'skill_id',
          ));
          $select->join(array(
            't2' => 'skills'
         ), 't2.id = t1.skill_id', array(
            'name'
         ), 'LEFT');
    $select->where(array("t1.user_id = '".$userId."'"));
    $select->order('t2.name ASC');
    $statement = $sql->prepareStatementForSqlObject($select);
        //   echo $statement->getSql(); die;
    $media = $this->getResultSetPrototype()->initialize($statement->execute())->toArray();
    $str = "";
    foreach($media as $skills){
       $str.=$skills['name'].","; 
    }
    return trim($str," ,");
  }
  public function getCityFilter($testAccount){
    $sql = new Sql($this->getAdapter());
    $select = new Select();
    $select->from(array(
              't1' => 'vcard_search' 
          ));
    $select->columns(array(
        'city'
    ));
    $select->join(array(
            't2' => 'users'
    ), 't2.username = t1.username',array(
    ), 'LEFT');
    $select->where(array("t2.extra3=1 and t2.ticket=0 and  t2.testAccount='".$testAccount."' and t2.active=1  and t2.status =0  and t2.type='attende' and t2.attendeType!='TechHR' and t1.city!=''"));
    $select->group('t1.city');
    $select->order('t1.city ASC');
    $statement = $sql->prepareStatementForSqlObject($select);
    $result = $this->getResultSetPrototype()->initialize($statement->execute())->toArray();
    return $result;
  }
//
 public function getDasBoardCounter($testAccount){
     $result['totalAttende'] = $this->getCountrForUser('attende',$testAccount);
     $result['totalSpeaker']  = $this->getCountrForUser('speaker',$testAccount);
     $result['totalSocialWall'] = $this->getProfileCounter('socialPost',"status=1",0);
     $result['totalChat'] = $this->getarchievecounter();
     $result['totalMeeting'] = $this->getProfileCounter('meeting',"status=1",0);
     $result['totalLogin'] = $this->getCountrForUser('totalLogin',$testAccount);
     $result['notLogin'] = $this->getCountrForUser('notLogin',$testAccount);
     $result['VIP'] = $this->getCountrForUser('VIP',$testAccount);
     $result['HRPRAC'] = $this->getCountrForUser('HRPRAC',$testAccount);
     $result['SERVICEPRO'] = $this->getCountrForUser('totalLogin',$testAccount);
     return $result;
}
 //method for counter    
  public function getCountrForUser($data,$testAccount){
       $sql = new Sql($this->adapter);
       $select = $sql->select(array(
                        't1' =>'users'
                            ));
       $select->columns(array(
           'count' => new \Zend\Db\Sql\Expression('COUNT(t1.username)'),
       )); 
      if($data =='attende'){
       $select->where(array("t1.extra3=1 and t1.ticket=0 and  t1.testAccount='".$testAccount."' and t1.active=1  and t1.status =0  and t1.type='attende' and attendeType!='TechHR'"));
      }else if ($data=='totalLogin'){
       $select->where(array("t1.extra3=1 and t1.ticket=0  and t1.fb_verfiy= 1 and t1.testAccount='".$testAccount."'  and t1.active=1 and t1.status =0 and t1.type='attende' and  attendeType!='TechHR'"));
      }else if($data=='notLogin'){
       $select->where(array("t1.extra3=1 and t1.ticket=0  and t1.fb_verfiy= 0 and t1.testAccount='".$testAccount."'  and t1.active=1 and t1.status =0 and t1.type='attende' and  attendeType!='TechHR'"));
      }elseif($data=='VIP'){
       $select->where(array("t1.extra3=1 and t1.ticket=0 and t1.testAccount='".$testAccount."' and t1.type='attende'  and t1.active=1 and t1.status =0 and attendeType='VIP'"));
      }else if ($data=='HRPRAC'){
       $select->where(array("t1.extra3=1 and t1.ticket=0 and t1.testAccount='".$testAccount."' and t1.type='attende'  and t1.active=1 and t1.status =0 and attendeType='HRPRAC'"));
      }else if ($data=='SERVICEPRO'){
       $select->where(array("t1.extra3=1 and t1.ticket=0 and t1.testAccount='".$testAccount."' and t1.type='attende' and t1.active=1 and t1.status =0 and attendeType='SERVICEPRO'"));
      }else{
       $select->where(array("t1.ticket=0 and  t1.testAccount='".$testAccount."' and t1.active=1  and t1.status =0  and t1.type='speaker' and attendeType!='TechHR'"));   
      }
      $smt = $sql->prepareStatementForSqlObject($select);
      $result = $this->resultSetPrototype->initialize($smt->execute())->toArray();
      return $result[0]['count']; 
  }
  //method for counter    
  public function getarchievecounter(){
       $sql = new Sql($this->adapter);
       $select = $sql->select(array(
                        't1' =>'archive_message'
                            ));
       $select->columns(array(
           'count' => new \Zend\Db\Sql\Expression('COUNT(t1.arc_to)'),
       )); 
      $select->where(array("t1.arc_to!=''"));   
      $smt = $sql->prepareStatementForSqlObject($select);
      $result = $this->resultSetPrototype->initialize($smt->execute())->toArray();
      return $result[0]['count']; 
  }
  public function getAllSpeakers($data=array(), $pageing = true, $testAccount,$type)
    {
        $searchUser = strtolower($data['search']);
        //search
        if ($searchUser != '') {
            $searchnm = "and ((lower(t2.firstName) like \"%" . $searchUser . "%\") or (lower(t2.email) like \"%" . $searchUser . "%\") or (lower(t2.subCategory) like \"%" . $searchUser . "%\") or (lower(t2.phone) like \"%" . $searchUser . "%\"))";
        } else {
            $searchnm = '';
        }
        $filter = $data['filter'];
       //Filters according to the dates
       if($filter=="all") {
            $filterQuery = "and t1.type='speaker'";
        }elseif ($filter=="totalLogin") {
            $filterQuery = "and t1.type='speaker' and t1.fb_verfiy= 1";
        } elseif ($filter=="iosLogin") {
             $filterQuery = "and t1.type='speaker' and t3.device_type='iOS' ";
        } elseif ($filter=="androidLogin") {
             $filterQuery = "and t1.type='speaker' and t3.device_type='android' ";
        }elseif ($filter=="notLogin") {
            $filterQuery = "and t1.type='speaker' and t1.fb_verfiy= 0";
        }
        else {
             $filterQuery = '';
        }
        //code for city filter
        if(!empty($data['cityFilter'])){
         $cityFilter = "and t2.city='".$data['cityFilter']."'";   
        }else{
         $cityFilter = "";   
        }
       if(!empty($data['fromDate']) && !empty($data['toDate'])){
         $filterQuery1 = "and DATE_FORMAT(FROM_UNIXTIME(t1.uts), '%Y-%m-%d') >='".$data['fromDate']."' and   DATE_FORMAT(FROM_UNIXTIME(t1.uts), '%Y-%m-%d') <= '".$data['toDate']."'";
       }else{
         $filterQuery1 = ""; 
       }
        
        $sql = new Sql($this->adapter);
        $select = $sql->select(array(
                  't1' =>'users'
                ));
        $select->columns(array(
         'username',
         'created_on',
         'timestamp',
         'status',
         'type',
         'uts'   
        ));
        $select->join(
        array('t2' =>'vcard_search'),
        new Expression('t2.username = t1.username'),
        array(
       'countryCode',
       'firstName',
       'lastName',
       'email',
       'phone',
       'city',
       'country',
       'companyName',
       'designation',
       'linkedinUrl',   
       'gender'),
        'LEFT'
    );
   $select->join(array('t3' =>'auth_session'), new Expression('t3.user_id = t1.username'), array('device_type'),'LEFT');     
   $select->where(array("t1.ticket=0 and t1.active=1 and t1.status !=1 and t1.testAccount='".$testAccount."' $searchnm $filterQuery $cityFilter $filterQuery1"));
   //$smt = $sql->prepareStatementForSqlObject($select);
   //echo $smt->getSql(); die;
   if ($data['firstNameOrder']=="asc"){
            $select->order(array(
          't2.firstName ASC'
         ));
    } elseif ($data['firstNameOrder']=="desc") {
            $select->order(array(
          't2.firstName DESC'
       ));
    } else{
       $select->order(array(
          't2.firstName ASC'
       ));
    } 
    if ($pageing) {
            $dbAdapter = new DbSelect($select, $this->getAdapter());
            $paginator = new Paginator($dbAdapter);
            return $paginator;
        } else {
            $smt = $sql->prepareStatementForSqlObject($select);
            $result = $this->resultSetPrototype->initialize($smt->execute())
                ->toArray();
            return $result;
        }
  }
  public function getCounterOfSpeaker($data,$testAccount){
       $sql = new Sql($this->adapter);
       $select = $sql->select(array(
                        't1' =>'users'
                            ));
       $select->columns(array(
           'count' => new \Zend\Db\Sql\Expression('COUNT(t1.username)'),
       )); 
       $select->join(array('t2' =>'vcard_search'), new Expression('t2.username = t1.username'), array('firstName'),'LEFT');
       $select->join(array('t3' =>'auth_session'), new Expression('t3.user_id = t1.username'), array('device_type'),'LEFT');
      if($data =='all'){
       $select->where(array("t1.ticket=0 and  t1.testAccount='".$testAccount."' and t1.active=1  and t1.status =0  and t1.type='speaker' and attendeType!='TechHR'"));
      }else if ($data=='totalLogin'){
       $select->where(array("t1.ticket=0  and t1.fb_verfiy= 1 and t1.testAccount='".$testAccount."'  and t1.active=1 and t1.status =0 and t1.type='speaker' and  attendeType!='TechHR'"));
      }else if ($data=='iosLogin'){
        $select->where(array("t1.status=0 and t1.ticket=0 and t1.testAccount='".$testAccount."' and t1.active=1 and t1.type='speaker' and t3.device_type='iOS' and attendeType!='TechHR'"));
      }else if ($data=='androidLogin'){
        $select->where(array("t1.status=0 and t1.ticket=0 and t1.testAccount='".$testAccount."'  and t1.active=1 and t1.type='speaker'  and t3.device_type='android' and attendeType!='TechHR'"));
      }else if($data=='notLogin'){
       $select->where(array("t1.ticket=0  and t1.fb_verfiy= 0 and t1.testAccount='".$testAccount."'  and t1.active=1 and t1.status =0 and t1.type='speaker' and  attendeType!='TechHR'"));
      }
      $smt = $sql->prepareStatementForSqlObject($select);
      $result = $this->resultSetPrototype->initialize($smt->execute())->toArray();
      return $result[0]['count']; 
 }
 public function getAllAgenda($data=array(), $pageing = true,$testAccount)
    {
        $searchUser = strtolower($data['search']);  
        //search
        if ($searchUser != '') {
            $searchnm = "and ((lower(t1.title) like \"%" . $searchUser . "%\") or (lower(t1.itemType) like \"%" . $searchUser . "%\") or (lower(t1.keyWord) like \"%" . $searchUser . "%\") or (lower(t1.keyWordDesc) like \"%" . $searchUser . "%\"))";
        } else {
            $searchnm = '';
        }
     if(!empty($data['datefilter'])){
       $filterQuery = "and DATE_FORMAT(FROM_UNIXTIME(t1.startDate), '%Y-%m-%d')='".$data['datefilter']."'";
     }else{
       $filterQuery = ""; 
     }
     if(empty($data['agendaLocation'])){
         $agenda = "";
     }else{
         $agenda = "and t1.location='".$data['agendaLocation']."'";  
     }
     if(empty($data['keyWordFilter'])){
         $keyWordFilter = "";
     }else{
         $keyWordFilter = "and t1.keyWord='".$data['keyWordFilter']."'";  
     }
        $sql = new Sql($this->adapter);
        $select = $sql->select(array(
                  't1' =>'agenda'
                ));
        $select->columns(array(
          'id',
         'location',
         'startDate',
         'endDate',
         'createdOn',
         'title',
         'description',
         'timeDuration',
          'color',
          'keyWord',
          'keyWordDesc',
          'itemType',
          'theme'
      ));
      $select->where(array("t1.testAccount='".$testAccount."' and t1.status=0 and (t1.itemType='agenda' || t1.itemType='invite')  $searchnm $agenda $filterQuery $keyWordFilter"));
      $select->order('t1.startDate ASC');
      $select->order('t1.endDate ASC');
      $select->order('t1.trackOrder ASC');
    //  $smt = $sql->prepareStatementForSqlObject($select);
     // echo $smt->getSql(); die;
        if ($pageing) {
            $dbAdapter = new DbSelect($select, $this->getAdapter());
            $paginator = new Paginator($dbAdapter);
            return $paginator;
        } else {
            $smt = $sql->prepareStatementForSqlObject($select);
            $result = $this->resultSetPrototype->initialize($smt->execute())
                ->toArray();
            return $result;
        }
    }
    public function getSpeakerList($masterClassId,$type,$itemType){
            $sql = new Sql($this->getAdapter());
            $select = new Select();
            $select->from(array(
                't1' => 'masterAgendaSpeaker' 
            ));
            $select->columns(array(
               'userId'
            ));
           
               $select->join(
               array('t2' =>'vcard_search'),
                 new Expression('t2.username = t1.userId'),
               array( 'firstName','lastName'
            ),'LEFT');
         
        if($type==1){       
        $select->where(array("t1.itemId= '".$masterClassId."'and t1.itemType= '".$itemType."'  "));
        }else{
         $select->where(array("t1.itemId= '".$masterClassId."' and t1.itemType='agenda'"));
           
        }
            
        $select->order(array("t1.id DESC"));
        $smt = $sql->prepareStatementForSqlObject($select);
//        echo $smt->getSql(); die;
        $result = $this->resultSetPrototype->initialize($smt->execute())
            ->toArray();
         $str="";
          foreach($result as $val){
              $str .= $val['firstName']." ,"; 
          }
         
         return rtrim($str," ,") ;
 
}
public function agendaOnEdit($masterClassId)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select(array(
                  't1' =>'agenda'
                 ));
        $select->columns(array(
         'id',
         'location',
         'startDate',
         'endDate',
         'createdOn',
         'title',
         'description',
         'timeDuration',
         'color',
         'keyWord',
         'keyWordDesc',
         'itemType','theme','companyName','vipCount','practorialCount','waitingListCount','waitingListCountUnpaid'   
        ));
       $select->where(array("t1.id= '".$masterClassId."'"));
       $smt = $sql->prepareStatementForSqlObject($select);
       $result = $this->resultSetPrototype->initialize($smt->execute())->toArray();
       return $result; 
   }
   //method for speaker list
 public function getSpeakersList($testAccount)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select(array(
                  't1' =>'users'
                ));
        $select->columns(array(
         'username'
        ));
        $select->join(
        array('t2' =>'vcard_search'),
        new Expression('t2.username = t1.username'),
        array(
       'firstName',
       'lastName'
       ),
       'LEFT'
    );
    $select->where(array("t1.ticket=0 and t1.active=1 and t1.status =0 and t1.type='speaker' and t1.testAccount='".$testAccount."'"));
    $select->order('t1.id asc');
    $smt = $sql->prepareStatementForSqlObject($select);
    $result = $this->resultSetPrototype->initialize($smt->execute())
                ->toArray();
    return $result;    
    }
    public function getSpeakerListonEdit($masterClassId,$type){
            $sql = new Sql($this->getAdapter());
            $select = new Select();
            $select->from(array(
                't1' => 'masterAgendaSpeaker' 
            ));
            $select->columns(array(
               'userId'
            ));
               $select->join(
               array('t2' =>'vcard_search'),
                 new Expression('t2.username = t1.userId'),
               array( 'firstName','lastName'
            ),'LEFT');       
        $select->where(array("t1.itemId= '".$masterClassId."'and t1.itemType='".$type."'"));
        $select->order(array("t1.id DESC"));
        $smt = $sql->prepareStatementForSqlObject($select);
        $result = $this->resultSetPrototype->initialize($smt->execute())
            ->toArray();
       return $result;
}
}