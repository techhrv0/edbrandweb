<?php
namespace Application\Model;
use Zend\Db\Sql\Sql;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Select;
use CustomLib\Model\CustomTable;
use Zend\Db\Sql\Predicate\Expression;

class UsersTable extends CustomTable
{
 protected $table = 'users';
 public function getUserFeedList($data,$testAccount,$pageing=true){
//     echo '<pre>'; print_r($testAccount); die;
    $searchUser = strtolower($data['search']);
    if ($searchUser != '') {
         $searchnm = "and ((lower(t1.firstName) like \"%" . $searchUser . "%\") or (lower(t1.phone) like \"%" . $searchUser . "%\") or (lower(t1.companyName) like \"%" . $searchUser . "%\")  or (lower(t1.designation) like \"%" . $searchUser . "%\"))";
    } else {
         $searchnm = '';
    }
    if(!empty($data['filter'])){
        $filterQuery = "and t2.attendeType='".$data['filter']."'";
    }else{
        $filterQuery = "";
    }
    $sql = new Sql($this->getAdapter());
    $select = new Select();
    $select->from(array(
           't1' => 'vcard_search'
    ));
    $select->columns(array(
            'userId'=>'username',
            'firstName' => new Expression('CONCAT(firstName," ",lastName)'),
            'phone',
            'email',
            'gender',
            'country','description','companyName','designation','city','linkedinUrl'
          ));
        $select->join(array(
           't2' => 'users'
        ), 't2.username = t1.username', array(
           'countryCode',
           'registeredOn'=> 'timestamp',
           'loginType'=>'type',
           'attendeType'=>new Expression("CASE WHEN t2.attendeType = 'VIP' THEN 'VIP'
                        WHEN t2.attendeType = 'HRPRAC' THEN 'HR Practitioners'
                        WHEN t2.attendeType = 'SERVICEPRO' THEN 'HR Service Provider'
                        WHEN t2.attendeType = 'TechHr' THEN 'TechHR'
                        WHEN t2.attendeType = 0 THEN ''
                        END")
        ), 'LEFT');
     if($data['action']=="speaker"){
      $select->where(array("t2.testAccount='".$testAccount."' AND t2.active = 1 and t2.adminType!=1 and t2.status=0 and t2.type='speaker' and t2.ticket=0 $searchnm $filterQuery"));   
      $select->order("t2.userPriority desc");
     }elseif($data['action']=="attende"){
      $select->where(array("t2.testAccount='".$testAccount."' and t1.firstName!=''  AND t2.active = 1 and t2.status=0 and t2.adminType!=1 and t2.type='attende' and t2.attendeType!='TechHR' and t2.ticket=0 $searchnm $filterQuery"));      
      $select->order("t1.firstName ASC");
     }elseif($data['action']=="investor"){
      $select->where(array("t2.testAccount='".$testAccount."' AND t2.active = 1 and t2.adminType!=1 and t2.status=0 and t2.type='investor'  and t2.ticket=0 $searchnm $filterQuery"));      
      $select->order("t1.firstName ASC");
     }
    
     if($pageing) {
          $dbAdapter = new DbSelect ( $select, $this->getAdapter () );
          $paginator = new Paginator ( $dbAdapter );
          return $paginator;
     } else{
        $statement = $sql->prepareStatementForSqlObject($select);
        $myEvent = $this->getResultSetPrototype()
            ->initialize($statement->execute())
            ->toArray();
         return $myEvent;
    }
  }
  public function getSpeakerMentorList($action,$testAccount,$type=0){
      
//echo '<pre>'; print_r($action); die(' ll');
    $sql = new Sql($this->getAdapter());
    $select = new Select();
    $select->from(array(
           't1' => 'vcard_search'
    ));
    $select->columns(array(
            'userId'=>'username',
            'firstName' => new Expression('CONCAT(firstName," ",lastName)'),
            'country','description','companyName','designation','city','linkedinUrl'
          ));
        $select->join(array(
           't2' => 'users'
        ), 't2.username = t1.username', array(
           
        ), 'LEFT');
     if($type==0){   
     if($action=="speaker"){
      $select->where(array("t2.testAccount='".$testAccount."' AND t2.active = 1 and t2.adminType!=1 and t2.type='speaker' and t2.ticket=0"));   
      $select->order("t2.userPriority desc");
     }elseif($action =="attende"){
      $select->where(array("t2.testAccount='".$testAccount."' and t1.firstName!=''  AND t2.active = 1 and t2.adminType!=1 and t2.type='attende' and t2.attendeType!='TechHR' and t2.ticket=0"));      
      $select->order("t1.firstName ASC");
     }elseif($action =="investor"){
      $select->where(array("t2.testAccount='".$testAccount."' AND t2.active = 1 and t2.adminType!=1 and t2.type='investor'  and t2.ticket=0"));      
      $select->order("t1.firstName ASC");
     }
      $select->limit(4);
     }else{
      $select->where(array("t1.username='".$type."' AND t2.active = 1 and t2.adminType!=1 and t2.ticket=0"));   
     }
     $statement = $sql->prepareStatementForSqlObject($select);
     $myEvent = $this->getResultSetPrototype()
            ->initialize($statement->execute())
            ->toArray();
         return $myEvent;
    
  }
  //method for master agenda
public function listofmasteragendaNew($data,$type,$testAccount){
    $searchUser = strtolower($data['search']);
    if ($searchUser != '') {
       $searchnm = "and ((lower(t1.title) like \"%" . $searchUser . "%\") or (lower(t1.description) like \"%" . $searchUser . "%\"))";
    } else {
       $searchnm = '';
    }  
    $sql = new Sql($this->getAdapter());
    if(!empty($data['keyword'])){
       $filterQuery = "and t1.keyWord='".$data['keyword']."'";
    }else{
       $filterQuery = ""; 
    }
    if(!empty($data['theme'])){
        $themeQuery = "and t1.theme='".$data['theme']."'";
    }else{
       $themeQuery = ""; 
    }
    if(empty($data['agendaLocation'])){
        $agenda = "";
    }else{
        $agenda = "and t1.location='".$data['agendaLocation']."'";  
    }
    if(empty($data['filterDate'])){
        $filterDate = "";
    }else{
        $filterDate = "and DATE_FORMAT(FROM_UNIXTIME(t1.startDate), '%b %d')='".$data['filterDate']."'";  
    }
    $select = new Select();
    $select->from(array(
        't1' =>'agenda'
    ));
    $select->columns(array(
           'id',
           'location','startDate','endDate','createdOn','updatedOn','icon','title','description','timeDuration','color','keyWord','keyWordDesc','itemType','status',
           'qrCode','vipCount','practorialCount','totalCount','isFor','link','price','companyName','currency','bookedColorCode','theme','userType','isFuturist','waitingListCount','waitingListCountUnpaid','meetingUrl','meetingNumber','userName','passWord' 
    ));
    $select->join(array(
       't2' => 'agendaLocation'
    ),'t2.location = t1.location', array(
    ), 'LEFT');
    if($type=="all"){  
      $select->where(array("t1.itemType='agenda' and t1.testAccount='".$testAccount."' and t1.status = 0 and isFor='all' $filterQuery $searchnm $agenda $themeQuery $filterDate"));
    }elseif($type=="vip"){
      $select->where(array("t1.itemType='agenda'  and t1.testAccount='".$testAccount."' and t1.status = 0 and (isFor='all' || isFor='vip') $filterQuery $searchnm $agenda $themeQuery $filterDate"));   
    }elseif($type=="prac"){
     $select->where(array("t1.itemType='agenda' and t1.testAccount='".$testAccount."' and t1.status = 0 and (isFor='all' || isFor='prac') $filterQuery $searchnm $agenda $themeQuery $filterDate"));   
    }    
    $select->order('t1.startDate ASC');
    $select->order('t1.endDate ASC');
    $select->order('t2.trackOrder ASC');
    $statement = $sql->prepareStatementForSqlObject($select);
    // echo $statement->getSql(); die;
    $result = $this->getResultSetPrototype()
        ->initialize($statement->execute())
        ->toArray();
        return $result; 
 }
 public function checkAgendaFav($userId,$agendaId){
        $sql = new Sql($this->getAdapter());
        $select = new Select();
        $select->from(array(
              't1' => 'agendaFav' 
          ));
          $select->columns(array(
           'isLike'
          ));   
          $select->where(array("t1.agendaId = '".$agendaId."' and t1.userId='".$userId."' AND isLike=1"));
          $statement = $sql->prepareStatementForSqlObject($select);
          $likeCounter = $this->getResultSetPrototype()
              ->initialize($statement->execute())
              ->toArray();
          return $likeCounter[0]['isLike'];
  }
   //method for spealer list name      
  public function getSpekAgName($data){
    $str = "";  
    $sql = new Sql($this->getAdapter());
    $select = new Select();
    $select->from(array(
           't1' => 'vcard_search'
    ));
    $select->columns(array(
            'userId'=>'username',
           'firstName' => new Expression('CONCAT(firstName," ",lastName)')
          ));
        $select->join(array(
            't3' => 'masterAgendaSpeaker'
         ), 't3.userId = t1.username', array(
         ), 'LEFT');
    $select->where(array("t3.itemId= '".$data['itemId']."'  AND t3.itemType = '".$data['itemType']."' and t3.status=1")); 
    $select->order("t1.id ASC");
    $statement = $sql->prepareStatementForSqlObject($select);
    $myEvent = $this->getResultSetPrototype()
            ->initialize($statement->execute())
            ->toArray();
   foreach($myEvent as $value){
       $str.=$value['firstName'].", ";
   }
     return rtrim($str,", ");
    }
//method for agenda detail    
  public function agendaDetail($agendaId){  
    $sql = new Sql($this->getAdapter());
    $select = new Select();
    $select->from(array(
        't1' =>'agenda'
    ));
    $select->columns(array(
           'id',
           'location','startDate','endDate','createdOn','updatedOn','icon','title','description','timeDuration','color','keyWord','keyWordDesc','itemType','status',
           'qrCode','vipCount','practorialCount','totalCount','isFor','link','price','companyName','currency','bookedColorCode','theme','userType','isFuturist','waitingListCount','waitingListCountUnpaid' 
    ));
    $select->join(array(
       't2' => 'agendaLocation'
    ),'t2.location = t1.location', array(
    ), 'LEFT');
   $select->where(array("t1.id='".$agendaId."' and t1.status = 0")); 
    $statement = $sql->prepareStatementForSqlObject($select);
    $result = $this->getResultSetPrototype()
        ->initialize($statement->execute())
        ->toArray();
        return $result; 
 }
 //method for spealer list name      
  public function getSpekAgNameNew($data){
    $sql = new Sql($this->getAdapter());
    $select = new Select();
    $select->from(array(
           't1' => 'vcard_search'
    ));
    $select->columns(array(
            'userId'=>'username',
           'firstName' => new Expression('CONCAT(firstName," ",lastName)')
          ));
        $select->join(array(
            't3' => 'masterAgendaSpeaker'
         ), 't3.userId = t1.username', array(
         ), 'LEFT');
    $select->where(array("t3.itemId= '".$data['itemId']."'  AND t3.itemType = '".$data['itemType']."' and t3.status=1")); 
    $select->order("t1.id ASC");
    $statement = $sql->prepareStatementForSqlObject($select);
    $myEvent = $this->getResultSetPrototype()
            ->initialize($statement->execute())
            ->toArray();
     return $myEvent;
 }
     public function checkFavforAgenda($data){
        $sql = new Sql($this->getAdapter());
          $select = new Select();
          $select->from(array(
              't1' => 'agendaFav' 
          ));
          $select->columns(array(
           'isLike'
          ));
          $select->where(array("t1.userId = '".$data['userId']."'AND agendaId = '".$data['agendaId']."'"));
          $statement = $sql->prepareStatementForSqlObject($select);
        //   echo $statement->getSql(); die;
          $checkLikeStatus = $this->getResultSetPrototype()
              ->initialize($statement->execute())
              ->toArray();
          return $checkLikeStatus;
    }
    public function getUserDetail($userId){
    $sql = new Sql($this->getAdapter());
    $select = new Select();
    $select->from(array(
           't1' => 'vcard_search'
    ));
    $select->columns(array(
            'userId'=>'username',
            'firstName' => new Expression('CONCAT(firstName," ",lastName)'),
            'country','description','companyName','designation','city','linkedinUrl','email','phone','countryCode'
          ));
        $select->join(array(
           't2' => 'users'
        ), 't2.username = t1.username', array('fb_verfiy','password','quickBloxId','chat_dialog_id'           
        ), 'LEFT');
    $select->where(array("t1.username='".$userId."'"));   
     $statement = $sql->prepareStatementForSqlObject($select);
     $myEvent = $this->getResultSetPrototype()
            ->initialize($statement->execute())
            ->toArray();
         return $myEvent;
    
  }
  public function getAgendaDate(){
    $sql = new Sql($this->getAdapter());
    $select = new Select();
    $select->from(array(
           't1' => 'agenda'
    ));
    $select->columns(array(
            'filterDate'=>new Expression("DATE_FORMAT(FROM_UNIXTIME(t1.startDate), '%b %d')")
          ));   
    $select->where(array("t1.itemType='agenda' and t1.testAccount='3' and t1.status = 0"));
    $select->group('filterDate');
    $select->order('filterDate asc');
    $statement = $sql->prepareStatementForSqlObject($select);
    $myEvent = $this->getResultSetPrototype()
            ->initialize($statement->execute())
            ->toArray();
         return $myEvent;
 }
 public function getUserDetailonEdit($userId){
    $sql = new Sql($this->getAdapter());
    $select = new Select();
    $select->from(array(
           't1' => 'vcard_search'
    ));
    $select->columns(array(
            'userId'=>'username',
            'firstName',
            'lastName','gender',
            'country','description','companyName','designation','city','linkedinUrl','email','phone','countryCode'
          ));
        $select->join(array(
           't2' => 'users'
        ), 't2.username = t1.username', array('attendeType',
           
        ), 'LEFT');
    $select->where(array("t1.username='".$userId."'"));   
     $statement = $sql->prepareStatementForSqlObject($select);
     $myEvent = $this->getResultSetPrototype()
            ->initialize($statement->execute())
            ->toArray();
         return $myEvent;
    
  }
  //method for user imatch list    
 public function getImacthList($data,$pageing=true){
    $searchUser = strtolower($data['search']);
    if ($searchUser != '') {
         $searchnm = "and ((lower(t2.firstName) like \"%" . $searchUser . "%\") or (lower(t2.phone) like \"%" . $searchUser . "%\") or (lower(t2.companyName) like \"%" . $searchUser . "%\")  or (lower(t2.designation) like \"%" . $searchUser . "%\"))";
    } else {
         $searchnm = '';
    }
    $sql = new Sql($this->getAdapter());
    $select = new Select();
    $select->from(array(
           't1' => 'user_match'
    ));
    $select->columns(array(
         'userId'=>'job_creator_id',
         'totalSkillMatch'=>'jobMatch',
         'isMatch'=>new Expression("CASE WHEN t1.isMatch = 0 THEN 'pending'
                        WHEN t1.isMatch = 1 THEN 'accepted'
                        WHEN t1.isMatch = 2 THEN 'rejected'
                        WHEN t1.isMatch = 3 THEN 'draft'
                        END")   
    ));
    $select->join(array(
           't2' => 'vcard_search'
    ), 't2.username = t1.job_creator_id', array(
        'firstName' => new Expression('CONCAT(firstName," ",lastName)'),'phone','gender',
       'status' => new Expression('IF(t2.status IS NOT NULL,t2.status,"")'),'country','description','companyName','designation','city'  
    ), 'LEFT');
    $select->join(array(
           't3' => 'users'
        ), 't3.username = t1.job_creator_id', array(
           'countryCode',
           'registeredOn'=> 'timestamp',
           'loginType'=>'type'
        ), 'LEFT');
   $select->where(array("t1.user_id = '".$data['userId']."' and t1.isMatch=0 $searchnm"));
   $select->order("t1.jobMatch DESC");
     if($pageing) {
          $dbAdapter = new DbSelect ( $select, $this->getAdapter () );
          $paginator = new Paginator ( $dbAdapter );
          return $paginator;
     } else{
        $statement = $sql->prepareStatementForSqlObject($select);
        $myEvent = $this->getResultSetPrototype()
            ->initialize($statement->execute())
            ->toArray();
         return $myEvent;
    }
  }
   // get matched  skills of candidate regarding any job
    public function getUserJobSkillData($jobCreator,$userId)
    {
        $sql = new Sql($this->getAdapter());
        $select = new Select();
        $select->from(array(
            't1' => 'user_skills'
        ));
        $select->columns(array(
        ));
        $select->join(array(
            'prof3' => 'user_match'
        ), 't1.user_id = prof3.job_creator_id', array(
        ), 'LEFT');
        $select->join(array(
            'prof2' => 'user_skills'
        ), 'prof3.user_id = prof2.user_id', array(
        ), 'LEFT');
        $select->join(array(
            'prof1' => 'skills'
        ), 't1.skill_id = prof1.id', array(
             'id',
            'name' => new Expression('IF(name IS NOT NULL,name,"")')
        ), 'LEFT');
       $select->where(array("t1.user_id = '".$jobCreator."' and t1.skill_id = prof2.skill_id and prof3.user_id='".$userId."' group by prof1.id"));
      $statement = $sql->prepareStatementForSqlObject($select);
      $jobTypeDetails = $this->getResultSetPrototype()
          ->initialize($statement->execute())
          ->toArray();
     return $jobTypeDetails;

   }
   public function getDeviceDetailNew($userId){
   $sql = new Sql($this->getAdapter());
   $select = new Select();
   $select->from(array(
               't1' => "users"
           ));
    $select->columns(array(
               'userId'=>'username','email'
      ));     
    $select->join(array(
           't2' => 'device_details'
       ), 't2.username = t1.username', array(
            'device_type','device_token','appType','apnsToken'
       ), 'LEFT');
    $select->join(array(
           't3' => 'vcard_search'
       ), 't3.username = t1.username', array(
          'firstName'  
       ), 'LEFT');
    $select->where(array("t1.username='".$userId."'"));
    $statement = $sql->prepareStatementForSqlObject($select);
    $result = $this->getResultSetPrototype()
               ->initialize($statement->execute())
               ->toArray();
     return $result; 
 }
 public function getLastMeeting($data){
    $sql = new Sql($this->getAdapter());
   $select = new Select();
   $select->from(array(
      't1' => 'meeting'
   ));
   $select->columns(array(
       'meetingId'=>'id',
       'userId',
       'description',
       'location',
        'title',
       'addLat',
       'addLong',
       'dateTime',
       'createdOn'
   ));
   $select->join(array(
           't2' => 'vcard_search'
        ), 't2.username = t1.userId', array(
           'firstName' => new Expression('CONCAT(firstName," ",lastName)')
           
        ), 'LEFT');
   $select->where(array("t1.id =".$data));
   $statement = $sql->prepareStatementForSqlObject($select);
   $myEvent = $this->getResultSetPrototype()
       ->initialize($statement->execute())
       ->toArray();
    return $myEvent;
}
public function listOfMeetingAttende($id,$type=0){
    $sql = new Sql($this->getAdapter());
    $select = new Select();
    $select->from(array(
           't1' => 'meetingAttendies'
    ));
    $select->columns(array(
            'userId',
            'isAttending'=>new Expression("CASE WHEN t1.isAttending = 0 THEN 'pending'
                        WHEN t1.isAttending = 1 THEN 'accepted'
                        WHEN t1.isAttending = 2 THEN 'rejected' 
                        WHEN t1.isAttending = 3 THEN 'cancelled'
                        END")
          ));
        $select->join(array(
           't2' => 'vcard_search'
        ), 't2.username = t1.userId', array(
           'firstName' => new Expression('CONCAT(firstName," ",lastName)')
           
        ), 'LEFT');
    if($type==0){
     $select->where(array("t1.meetingId='".$id."' "));
    }else{
     $select->where(array("t1.meetingId='".$id."' and t1.userId='".$type."'"));   
    }
     
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $this->getResultSetPrototype()
            ->initialize($statement->execute())
            ->toArray();
         return $result;
    
      
  }
  public function getUserSuitCaseData($data,$type,$testAccount){
    $searchUser = strtolower($data['search']);
    if ($searchUser != '') {
       $searchnm = "and ((lower(t1.title) like \"%" . $searchUser . "%\") or (lower(t1.description) like \"%" . $searchUser . "%\"))";
    } else {
       $searchnm = '';
    }  
    $sql = new Sql($this->getAdapter());
    if(!empty($data['keyword'])){
       $filterQuery = "and t1.keyWord='".$data['keyword']."'";
    }else{
       $filterQuery = ""; 
    }
    if(!empty($data['theme'])){
        $themeQuery = "and t1.theme='".$data['theme']."'";
    }else{
       $themeQuery = ""; 
    }
    if(empty($data['agendaLocation'])){
        $agenda = "";
    }else{
        $agenda = "and t1.location='".$data['agendaLocation']."'";  
    }
    if(empty($data['filterDate'])){
        $filterDate = "";
    }else{
        $filterDate = "and DATE_FORMAT(FROM_UNIXTIME(t1.startDate), '%b %d')='".$data['filterDate']."'";  
    }
    $select = new Select();
    $select->from(array(
        't1' =>'agenda'
    ));
    $select->columns(array(
           'id',
           'location','startDate','endDate','createdOn','updatedOn','icon','title','description','timeDuration','color','keyWord','keyWordDesc','itemType','status',
           'qrCode','vipCount','practorialCount','totalCount','isFor','link','price','companyName','currency','bookedColorCode','theme','userType','isFuturist','waitingListCount','waitingListCountUnpaid' 
    ));
    $select->join(array(
       't2' => 'agendaLocation'
    ),'t2.location = t1.location', array(
    ), 'LEFT');
    $select->join(array(
       't3' => 'userSuitcase'
    ),'t3.itemId = t1.id', array(
    ), 'LEFT');
    $select->where(array("t1.itemType='agenda' and t1.testAccount='".$testAccount."' and t1.status = 0 and t3.userId ='".$data['userId']."' $filterQuery $searchnm $agenda $themeQuery $filterDate"));
    $select->order('t1.startDate ASC');
    $select->order('t1.endDate ASC');
    $select->order('t2.trackOrder ASC');
    $statement = $sql->prepareStatementForSqlObject($select);
    // echo $statement->getSql(); die;
    $result = $this->getResultSetPrototype()
        ->initialize($statement->execute())
        ->toArray();
        return $result; 
 }
  
   public function getAllPartner()
    {
     $searchUser = strtolower($data['search']);
        //search
        if ($searchUser != '') {
            $searchnm = "and ((lower(t1.title) like \"%" . $searchUser . "%\") or (lower(t1.type) like \"%" . $searchUser . "%\"))";
        } else {
            $searchnm = '';
        }    
        $sql = new Sql($this->adapter);
        $select = $sql->select(array('t1' =>'partner'));
        $select->columns(array(
           'id',
           'title',
           'logo',
           'type',
           'website',
           'description', 
           'createdOn',
           'boothNumber' 
        ));
        $select->order(array(
          't1.id desc'
       ));
        
        
        $select->where(array("t1.status=1 and t1.testAccount='".$testAccount."' $searchnm"));
        
       
            $smt = $sql->prepareStatementForSqlObject($select);
            $result = $this->resultSetPrototype->initialize($smt->execute())
                ->toArray();
            return $result;
        
   }
   public function getListOfMeetings($data,$pageing=true){
       $searchUser = strtolower($data['search']);
    if ($searchUser != '') {
         $searchnm = "and ((lower(t1.description) like \"%" . $searchUser . "%\") or (lower(t2.firstName) like \"%" . $searchUser . "%\") or (lower(t1.title) like \"%" . $searchUser . "%\"))";
    } else {
         $searchnm = '';
    }
    $sql = new Sql($this->getAdapter());
     $select = new Select();
     $select->from(array(
           't1' => 'meeting'
     ));
     $select->columns(array(
       'meetingId'=>'id',
       'userId',
       'title',    
       'description',
       'location',
       'addLat',
       'addLong',
       'dateTime',
       'createdOn'
     ));
     $select->join(array(
           't2' => 'vcard_search'
     ),'t2.username = t1.userId', array(
      'firstName' => new Expression('CONCAT(firstName," ",lastName)')
     ), 'LEFT');
    $select->join(array(
     't3' => 'meetingAttendies'
    ), 't3.meetingId = t1.id', array(
       'isAttending'=> new Expression("CASE WHEN t3.isAttending = 0 THEN 'pending'
                        WHEN t3.isAttending = 1 THEN 'accepted'
                        WHEN t3.isAttending = 2 THEN 'rejected'
                        WHEN t3.isAttending = 3 THEN 'cancelled'
                        END")
    ),'LEFT');  
    $select->where(array("t1.status =1 and (t1.userId='".$data['userId']."' || t3.userId='".$data['userId']."')  $searchnm"));  
    $select->order('t1.id DESC');
    if($pageing) {
             $dbAdapter = new DbSelect ( $select, $this->getAdapter () );
             $paginator = new Paginator ( $dbAdapter );
             return $paginator;
     } else{
        $statement = $sql->prepareStatementForSqlObject($select);
        $myEvent = $this->getResultSetPrototype()
            ->initialize($statement->execute())
            ->toArray();
         return $myEvent;
     }
   }
   
   
    public function getSpeakerListForSessionFeedback($postData){
    $sql = new Sql($this->getAdapter());
        $select = new Select();
        $select->from(array(
            't1' => 'vcard_search'
        ));
        $select->columns(array(
            'userId' =>'username',
           'firstName' => new Expression('CONCAT(firstName," ",lastName)')

          ));
        $select->join(array(
           't2' => 'users'
        ), 't2.username = t1.username', array(
        ), 'LEFT');
         $select->join(array(
           't3' => 'masterAgendaSpeaker'
        ), 't3.userId = t1.username', array(
        ), 'LEFT');
        $select->where(array("t3.itemId='".$postData['itemId']."' and t3.itemType='".$postData['itemType']."' and t3.status=1")); 
        $select->order('t1.firstName asc');
      
        $smt = $sql->prepareStatementForSqlObject($select);
        $result = $this->resultSetPrototype->initialize($smt->execute())
            ->toArray();
        return $result;   
  }
  
  
   public function getAttendeListForMeeting($testAccount){
//     echo '<pre>'; print_r($testAccount); die;
    
    $sql = new Sql($this->getAdapter());
    $select = new Select();
    $select->from(array(
           't1' => 'vcard_search'
    ));
    $select->columns(array(
            'userId'=>'username',
            'firstName' => new Expression('CONCAT(firstName," ",lastName)'),
            'phone',
            'email',
            'gender',
            'country','description','companyName','designation','city','linkedinUrl'
          ));
        $select->join(array(
           't2' => 'users'
        ), 't2.username = t1.username', array(
           'countryCode',
           'registeredOn'=> 'timestamp',
           'loginType'=>'type',
           'attendeType'=>new Expression("CASE WHEN t2.attendeType = 'VIP' THEN 'VIP'
                        WHEN t2.attendeType = 'HRPRAC' THEN 'HR Practitioners'
                        WHEN t2.attendeType = 'SERVICEPRO' THEN 'HR Service Provider'
                        WHEN t2.attendeType = 'TechHr' THEN 'TechHR'
                        WHEN t2.attendeType = 0 THEN ''
                        END")
        ), 'LEFT');
   
      $select->where(array("t2.testAccount='".$testAccount."' and t1.firstName!=''  AND t2.active = 1 and t2.status=0 and t2.adminType!=1 and t2.type='attende' and t2.attendeType!='TechHR' and t2.ticket=0  "));      
      $select->order("t1.firstName ASC");
    
     if($pageing) {
          $dbAdapter = new DbSelect ( $select, $this->getAdapter () );
          $paginator = new Paginator ( $dbAdapter );
          return $paginator;
     } else{
        $statement = $sql->prepareStatementForSqlObject($select);
        $myEvent = $this->getResultSetPrototype()
            ->initialize($statement->execute())
            ->toArray();
         return $myEvent;
    }
  }
  
  
  public function getBoothDetail($type){
//      echo '<pre>'; print_r($type); die;
        $sql = new Sql($this->getAdapter());
        $select = new Select();
        $select->from(array(
              't1' => 'booth' 
          ));
          $select->columns(array(
           'meetingUrl'
          ));   
          if($type=="1"){
          $select->where(array("t1.status = '1' and t1.boothName='LinkedIn' "));
          }else if($type=="2"){
          $select->where(array("t1.status = '1' and t1.boothName='SAP' "));
              
          }else if($type=="3"){
          $select->where(array("t1.status = '1' and t1.boothName='Skillsoft' "));
              
          }else if($type=="4"){
           $select->where(array("t1.status = '1' and t1.boothName='ZingHR' "));
             
          }else if($type=="5"){
          $select->where(array("t1.status = '1' and t1.boothName = 'LinkedIn1' "));
              
          }else{
            $select->where(array("t1.status = '1' and t1.boothName= 'SAP1' "));
            
          }
          
           $statement = $sql->prepareStatementForSqlObject($select);
//           echo $statement->getSql(); die;
           $myEvent = $this->getResultSetPrototype()
            ->initialize($statement->execute())
            ->toArray();
         return $myEvent;
  }
   
   
 public function getBoothAllDetail(){
//      echo '<pre>'; print_r($type); die;
        $sql = new Sql($this->getAdapter());
        $select = new Select();
        $select->from(array(
              't1' => 'booth' 
          ));
          $select->columns(array(
              'id',
              'boothName',
              'meetingUrl'
          )); 
          
          
          $select->join(array(
           't2' => 'boothMedia'
        ), 't2.boothId = t1.id', array(
           'id',
           'media',
           'boothId',
            'type'
        ), 'LEFT'); 
          
          
        $select->where(array("t1.status=1 and t1.testAccount='3' and t2.boothMediaType=0  and t2.status=1"));
          $select->order(array(
          't1.boothorder ASC'
       )); 
          
           $statement = $sql->prepareStatementForSqlObject($select);
//           echo $statement->getSql(); die;
           $myEvent = $this->getResultSetPrototype()
            ->initialize($statement->execute())
            ->toArray();
         return $myEvent;
  } 
  
  
  
//   public function getBoothMedia($boothId)
//    {
//        $sql = new Sql($this->adapter);
//        $select = $sql->select(array('t1' =>'boothMedia'));
//        $select->columns(array(
//           'id',
//           'media',
//            'boothId'
//           
//        ));
//        $select->order(array(
//          't1.id desc'
//       ));
//        
//        $select->where(array("t1.status=1 and t1.testAccount='3' and t1.boothMediaType=0 "));
//            $smt = $sql->prepareStatementForSqlObject($select);
//            $result = $this->resultSetPrototype->initialize($smt->execute())
//                ->toArray();
//            return $result;
//        
//   }
   
   
   public function getBoothDetailView($boothId){
//      echo '<pre>'; print_r($type); die;
        $sql = new Sql($this->getAdapter());
        $select = new Select();
        $select->from(array(
              't1' => 'booth' 
          ));
          $select->columns(array(
              'id',
              'boothName',
              'meetingUrl',
              'title',
              'description'
          )); 
          
          
        $select->join(array(
           't2' => 'boothMedia'
        ), 't2.boothId = t1.id', array(
           'media',
            'type'
        ), 'LEFT');  
          
          
        $select->where(array("t1.status = '1' and t1.id ='".$boothId."' and t2.status=1 and t2.testAccount='3' and t2.boothMediaType=1 "));
          
          
           $statement = $sql->prepareStatementForSqlObject($select);
//           echo $statement->getSql(); die;
           $myEvent = $this->getResultSetPrototype()
            ->initialize($statement->execute())
            ->toArray();
         return $myEvent;
  } 
  
  
  
   public function getNextBoothId($boothId){
//      echo '<pre>'; print_r($type); die;
        $sql = new Sql($this->getAdapter());
        $select = new Select();
        $select->from(array(
              't1' => 'booth' 
          ));
          $select->columns(array(
              'id',
              'boothName',
              'meetingUrl'
          )); 
          
            $select->join(array(
           't2' => 'boothMedia'
        ), 't2.boothId = t1.id', array(
        ), 'LEFT');  
          
          
          
        $select->where(array(" t1.id > '".$boothId."' and  t1.status=1 and t1.testAccount='3' and t2.status = 1 and t2.boothMediaType =1 "));
          $select->order(array(
          't1.id ASC'
       )); 
       $select->limit(1);
           $statement = $sql->prepareStatementForSqlObject($select);
//           echo $statement->getSql(); die;
           $myEvent = $this->getResultSetPrototype()
            ->initialize($statement->execute())
            ->toArray();
         return $myEvent;
  } 
  
  
    public function getPrevBoothId($boothId){
//      echo '<pre>'; print_r($type); die;
        $sql = new Sql($this->getAdapter());
        $select = new Select();
        $select->from(array(
              't1' => 'booth' 
          ));
          $select->columns(array(
              'id',
          ));
          
           $select->join(array(
           't2' => 'boothMedia'
        ), 't2.boothId = t1.id', array(
        ), 'LEFT');  
          
          
          
        $select->where(array(" t1.id < '".$boothId."' and  t1.status=1 and t1.testAccount='3' and t2.status = 1  and t2.boothMediaType =1"));
          $select->order(array(
          't1.id desc'
       )); 
       $select->limit(1);
           $statement = $sql->prepareStatementForSqlObject($select);
//           echo $statement->getSql(); die;
           $myEvent = $this->getResultSetPrototype()
            ->initialize($statement->execute())
            ->toArray();
         return $myEvent;
  } 

 //method for spealer list name      
  public function getUserCallList($userId){  
   $sql = new Sql($this->getAdapter());
    $select = new Select();
    $select->from(array(
           't1' => 'vcard_search'
    ));
    $select->columns(array(
            'userId'=>'username',
           'firstName' => new Expression('CONCAT(firstName," ",lastName)'),
            'email'
          ));
        $select->join(array(
            't2' => 'users'
         ), 't2.username = t1.username', array('quickBloxId'
         ), 'LEFT');
    $select->where(array("t1.username!= '".$userId."'  AND t2.status = '0' and t2.active=1 and t2.quickBloxId!=''")); 
    $select->order("t1.id ASC");
    $statement = $sql->prepareStatementForSqlObject($select);
    $myEvent = $this->getResultSetPrototype()
            ->initialize($statement->execute())
            ->toArray();
   return $myEvent;
  }
  //method for spealer list name      
  public function getTableParticpantList($tableId,$tableNo){  
    $sql = new Sql($this->getAdapter());
    $select = new Select();
    $select->from(array(
           't1' => 'vcard_search'
    ));
    $select->columns(array(
            'userId'=>'username',
            'firstName' => new Expression('CONCAT(firstName," ",lastName)')
          ));
        $select->join(array(
            't2' => 'soicalTableParticapnts'
         ), 't2.userId = t1.username', array('quickbloxId','tableNo'
         ), 'LEFT');
    $select->where(array("t2.tableId= '".$tableId."'  AND t2.status = '1' and t2.tableNo='".$tableNo."'")); 
    $select->order("t2.tableNo ASC");
    $statement = $sql->prepareStatementForSqlObject($select);
    $myEvent = $this->getResultSetPrototype()
            ->initialize($statement->execute())
            ->toArray();
   return $myEvent;
 }
 //method for spealer list name      
  public function getTableParticpant($tableId,$userId){  
    $sql = new Sql($this->getAdapter());
    $select = new Select();
    $select->from(array(
           't1' => 'vcard_search'
    ));
    $select->columns(array(
            'userId'=>'username',
            'firstName' => new Expression('CONCAT(firstName," ",lastName)'),'designation','country'
          ));
        $select->join(array(
            't2' => 'soicalTableParticapnts'
         ), 't2.userId = t1.username', array('quickbloxId','tableNo'
         ), 'LEFT');
    $select->where(array("t2.tableId= '".$tableId."'  AND t2.status = '1' and t2.userId!='".$userId."'")); 
    $select->order("t2.tableNo ASC");
    $statement = $sql->prepareStatementForSqlObject($select);
    $myEvent = $this->getResultSetPrototype()
            ->initialize($statement->execute())
            ->toArray();
   return $myEvent;
 }
}
