<?php
namespace Application\Controller;
use CustomLib\Controller\CustomController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Filter\StripTags;
use Zend\Filter\StringTrim;
use Zend\View\Model\JsonModel;
use CustomLib\Service\UserPassword;
//use CustomLib\Service\ClickMeetingRestClient;
use CustomLib\Service\Quickblox;
class IndexController extends CustomController
{ 
//Admin home page   
 public function indexAction(){
    $uri = $this->getRequest()->getUri();
//    $baseUrl = $uri->getHost();
//    if(strstr( $baseUrl,'expoodle')){
//      $this->redirect()->toUrl("/application/index/client-index");
//    } 
    $usrTlb = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $speakerList = $usrTlb->getSpeakerMentorList('speaker',3);
    $mediaUrl = $this->getMediaUrl();
    $viewModel = new ViewModel(array('speakerList'=>$speakerList,'mediaUrl'=>$mediaUrl));
    return $viewModel;
 }
 public function clientIndexAction(){
    $usrTlb = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $speakerList = $usrTlb->getSpeakerMentorList('speaker',3);
    $mediaUrl = $this->getMediaUrl();
    $viewModel = new ViewModel(array('speakerList'=>$speakerList,'mediaUrl'=>$mediaUrl));
    return $viewModel;
 }
 
 
//User logout action 
 public function logoutAction(){
        $sess = new Container('User');
        $sess->getManager()->destroy();
        return $this->redirect()->toUrl('/');
 }
 //Set user time zone in session 
 public function settimezoneAction(){
       $request = $this->getRequest();
       $data = $request->getPost()->toArray();
       $sess = new Container('User');
       $sess->offsetSet('timeZone',$data['timezone']);
       return new JsonModel(array('message'=>"success"));
  }  
  // Employer Signup
  public function registration1Action(){
   try{       
    $request = $this->getRequest();
    $usrTlb = $this->getServiceLocator()->get('Application\Model\UsersTable');
// if($request->isPost()) {
    $postData = $request->getPost()->toArray();
    $files = $request->getFiles()->toArray();
    $checkNumber = $usrTlb->getDetail('users',array('username'),"email='".$postData['email']."'  and status=0 and testAccount='3'");
    if(!empty($checkNumber)){
     $send = array(
               'message' =>"Email already exist.",
               'status'=>0  
             );
             return new JsonModel(array('send'=>$send));
    }else{
    $vcardData=array();
    $cli= array();
    $userId = substr(time().ceil((rand(0,10000))),0, 5).$this->getUserId(6);
    $loginKey=$this->getToken(32);
    $passUtil = new UserPassword();
    $cli['username'] =   $userId;
    $usrPass = $passUtil->create($postData['password']);   
    $cli['password'] = $loginKey;
    if(!empty($postData['email'])){
      $cli['email'] = $postData['email'];
    }else{
     $cli['email'] = "";   
    }
    $cli['attendeType'] = $postData['userType'];    
    if(!empty($postData['countryCode'])){
      $cli['countryCode'] = $postData['countryCode'];
    }else{
     $cli['countryCode'] = "";   
    }
    if(!empty($postData['mobile'])){
      $cli['mobile'] = $postData['mobile'];
    }else{
     $cli['mobile'] = "";   
    }
  
    $cli['status'] = 0;
    $cli['type'] = 'attende';
    $cli['active'] =0;
    $cli['roleType'] = 0;
    $cli['testAccount']= 3;
    $cli['isPaymentDone']=0;
    $cli['timestamp']=time();
    $cli['uts'] = time();
    $cli['created_on'] = time();
    $cli['extra3'] =1;
    $cli['source'] = "web";
    $name = preg_replace("/[^a-zA-Z]+/", "",$postData['fname']);
    $qrName = $name.$this->getTicket(4);
    $cli['qrName'] = $qrName;
    $invitationId ="INVITE". mt_rand(1000, 9999);
    $cli['invitationId']=$invitationId;   
    
    if(!empty($postData['password'])){
     $cli['eventapPassword'] =$usrPass;
     $cli['eventapPasswordString'] = $postData['password'];
    }else{
     $cli['eventapPassword'] = "";
     $cli['eventapPasswordString'] = "";
    }
    $usrTlb->saveData('users', $cli);
    //$usrTlb->saveData('qrcodeSeries',array('userId'=>$userId,'lastValue'=>$qrCode,'createdOn'=> time(),'updatedOn'=> time()));
    $vcardData['username'] =   $userId;
    $vcardData['phone'] =     $cli['mobile'];
    
    $vcardData['firstName'] = $postData['fname'];
    if(!empty($postData['lname'])){
     $vcardData['lastName'] = $postData['lname'];
    }else{
     $vcardData['lastName'] = "";   
    }
    if(!empty($postData['email'])){
     $vcardData['email'] = $postData['email'];
    }else{
     $vcardData['email'] = "";   
    }
    if(!empty($postData['gender'])){
     $vcardData['gender'] =$postData['gender'];
    }else{
     $vcardData['gender'] = "";   
    }
    
    $vcardData['countryCode'] =$cli['countryCode'];
    $vcardData['status'] = '';
    $vcardData['createdAt'] = time();
    $vcardData['region'] ="r1";
    $vcardData['guestSide'] ="Bride";
    //code for admin userId
     $getAdminUserId = $usrTlb->getDetail('users',array('username'),"active=1 and status=0 and testAccount='3' and adminType=2 order by id asc limit 1");
  
    $vcardData['invitedBy'] = $getAdminUserId[0]['username'];
    if(!empty($postData['companyName'])){
     $vcardData['companyName'] =  $postData['companyName'];
    }else{
     $vcardData['companyName'] = "";   
    }
    
    if(!empty($postData['designation'])){
      $vcardData['designation'] =  $postData['designation'];
    }else{
      $vcardData['designation'] = "";   
    }
   if(!empty($postData['companyNameOnBadge'])){
      $vcardData['companyNameOnBadge'] =  $postData['companyNameOnBadge'];
    }else{
      $vcardData['companyNameOnBadge'] = "";   
    }
    
     if(!empty($postData['city'])){
      $vcardData['city'] =  $postData['city'];
    }else{
      $vcardData['city'] = "";   
    } 
    if(!empty($postData['role'])){
      $vcardData['description'] =  $postData['role'];
    }else{
      $vcardData['description'] = "";   
    }
  
    if(!empty($postData['country'])){
      $vcardData['country'] =  $postData['country'];
    }else{
      $vcardData['country'] = "";   
    } 
    $usrTlb->saveData('vcard_search',$vcardData);
  if (isset($postData['areaOfInterest']) && !empty($postData['areaOfInterest'])) {
                  $skillArray = $postData['areaOfInterest'];
               foreach($skillArray as $key=>$val){
                    $arr = array('user_id' => $userId , 'skill_id' => $val['id'] , 'updated_by' => $userId , 'created_on' => time(), 'updated_on'=>time() );
                    $usrTlb->saveData('user_skills',$arr);
                  }
               }  
    $usrTlb->saveData('userTravelDetail',array('travelType'=>'arrival','userId'=>$userId));
    $usrTlb->saveData('userTravelDetail',array('travelType'=>'departure','userId'=>$userId));
    $usrTlb->saveData('userDetail',array('userId'=>$userId));
    $usrTlb->saveData('kycMasterDocList',array('userId'=>$userId,'docName'=>'id','docLabel'=>'Enter your Id card number','caption'=>'Valid ID Proof','createdOn'=>time(),'updatedOn'=>time()));
   $usrTlb->saveData('kycMasterDocList',array('userId'=>$userId,'docName'=>'Travel','docLabel'=>'Enter Travel Detail','caption'=>'Travel','createdOn'=>time(),'updatedOn'=>time()));
    if(!empty($files['attachment'][0]['name'])){ 
          $mime = $files['attachment'][0]['type'];
          if(strstr($mime, "image/")){
            $image = "profile/".$userId.'.png';
            $this->putIntoS3Bucket($files['attachment'][0]['tmp_name'],$image,1);
            $usrTlb->updateData('users',array('lastAvtarUpdate'=> time()),array('username'=>$userId));
         }  
    }else{
     $data1['firstName']= $name;
     $data1['userId'] = $userId;
     $config = $this->getServiceLocator()->get('config');
     $this->createDefaultUserPic($data1,$config);
     $usrTlb->updateData('users',array('lastAvtarUpdate'=> time()),array('username'=>$userId));
    }
    $send = array(
               'message' =>"User Added Successfully",
               'status'=>1,
                'userId'=> base64_encode($userId)
              );
     return new JsonModel(array('send'=>$send));
    
  }
}catch(\Exception $e){
  $send = array(
               'message' =>"Something went wrong",
               'status'=>0  
              );
    return new JsonModel(array('send'=>$send));
}
$send = array(
               'message' =>"Something went wrong",
               'status'=>0  
              );
    return new JsonModel(array('send'=>$send));
}




public function paymentAction(){
   $userTable = $this->getServiceLocator()->get('Application\Model\UsersTable');
   $pera = $this->params()->fromroute('id', NULL);
   $userId= base64_decode($pera);
   $amount = $_GET['payment'];
   $userDetail = $userTable->getUserDetail($userId);
   
   $request = $this->getRequest();
   $msg = "";
   if($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $config = $this->getServiceLocator()->get('config');
            $StripeKey = $config['settings']['STRIPE_SECRET_KEY'];
            \Stripe\Stripe::setApiKey($StripeKey);
     try{
         
           //check entry exist or not
      
                 $charge =\Stripe\Charge::create(array(
                         "amount" =>$amount*100,
                         "currency" => "INR",
                         "source" => $postData['stripeToken'],
                         "description" => "EvenTap one time payment",
                         "receipt_email"=>$userDetail[0]['email']
                 ));
               $charge_array = $charge->__toArray(true); 

           
         $payment = array('userId'=>$userId,'id'=>$charge_array['id'],'object'=>$charge_array['object'],'amount'=>$charge_array['amount'],'amount_refunded'=>$charge_array['amount_refunded']
                           ,'application'=>$charge_array['application'],'application_fee'=>$charge_array['application_fee'],'balance_transaction'=>$charge_array['balance_transaction'],
                           'captured'=>$charge_array['captured'],'created'=>$charge_array['created'],'currency'=>$charge_array['currency'],'customer'=>$charge_array['customer'],'description'=>$charge_array['description'],
                           'destination'=>$charge_array['destination'],'dispute'=>$charge_array['dispute'],'failure_code'=>$charge_array['failure_code'],'failure_message'=>$charge_array['failure_message'],'invoice'=>$charge_array['invoice'],
                           'paid'=>$charge_array['paid'],'receipt_email'=>$charge_array['receipt_email'],'receipt_number'=>$charge_array['receipt_number'],'refunded'=>$charge_array['refunded'],
                           'review'=>$charge_array['review'],'shipping'=>$charge_array['shipping'],'status'=>$charge_array['status'],'updatedOn'=> time()
                         );
         $txnId = $userTable->saveData('payment',$payment);
           //save data in payment refund
         $pay_refund = array('txnId'=>$txnId,'object'=>$charge_array['refunds']['object'],'data'=> json_encode($charge_array['refunds']['data']),
                               'has_more'=>$charge_array['refunds']['has_more'],'total_count'=>$charge_array['refunds']['total_count'],
                               'url'=>$charge_array['refunds']['url']
                              );
       $userTable->saveData('payment_refund',$pay_refund);
       //save data in payment source
        $pay_source = array('txnId'=>$txnId,'id'=>$charge_array['source']['id'],'object'=>$charge_array['source']['object'],'address_city'=>$charge_array['source']['address_city'],
                                'address_country'=>$charge_array['source']['address_country'],'address_line1'=>$charge_array['source']['address_line1'],'address_line2'=>$charge_array['source']['address_line2'],
                                'address_state'=>$charge_array['source']['address_state'],'address_zip'=>$charge_array['source']['address_zip'],'brand'=>$charge_array['source']['brand'],'country'=>$charge_array['source']['country'],
                                'customer'=>$charge_array['source']['customer'],'cvc_check'=>$charge_array['source']['cvc_check'],'dynamic_last4'=>$charge_array['source']['dynamic_last4'],
                                'exp_month'=>$charge_array['source']['exp_month'],'exp_year'=>$charge_array['source']['exp_year'],'funding'=>$charge_array['source']['funding'],'last4'=>$charge_array['source']['last4'],
                                'name'=>$charge_array['source']['name'],'tokenization_method'=>$charge_array['source']['tokenization_method']
                             );
        $userTable->saveData('payment_source',$pay_source);
        
        //save data in user txn history
        $user_txn_his = array('username'=>$userId,'txnId'=>$txnId,'createdOn'=> time(),'updatedOn'=> time(),'status'=>1);
        $userTable->saveData('user_txn_history',$user_txn_his);
       //save original payment response
       $payment_res = array('txnId'=>$txnId,'userId'=>$userId,'response'=> json_encode($charge_array),'createdOn'=>time());
       $userTable->saveData('payment_reponse',$payment_res);
       //now code for register user on quick blox
       $quickBlox = new Quickblox();
       $tokenAuth = $quickBlox->quickAuth();
       $token = $tokenAuth['session']['token'];
       $quickAddUser = $quickBlox->quickAddUsers($token ,$userId,$userDetail[0]['password'],$userDetail[0]['email'],$userDetail[0]['firstName'],$userDetail[0]['countryCode'].$userDetail[0]['phone']);
       $userTable->updateData('users',array('isPaymentDone'=>'1','active'=>'1','quickBloxId'=>$quickAddUser['user']['id']),array('username'=>$userId));
       $sess = new Container('User');
       $sess->offsetSet('userId',$userId);
       $uri = $this->getRequest()->getUri();
       $baseUrl = $uri->getHost();
      /// if(strstr($baseUrl,'expoodle')){
         // $this->redirect()->toUrl("/users/client-hall");
        //}else{
          $this->redirect()->toUrl("/users/hall");      
       // }     
     }catch(\Stripe\Error\Card $e) {
            // Since it's a decline, \Stripe\Error\Card will be caught
        $msg = $e->getMessage();
    } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly
            $msg = $e->getMessage();  
    } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API
            $msg = $e->getMessage();
                  
    } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
          $msg = $e->getMessage();    
   } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed
            $msg = $e->getMessage();  
   } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            $msg = $e->getMessage();  
   }catch (Exception $ex) {
               $msg = $ex->getMessage();
   }
           
  }
   
  $view = new ViewModel(array('pera'=>$pera,'msg'=>$msg,'amount'=>$amount,'userDetail'=>$userDetail));
  $view->setTerminal(true);
  return $view; 
 } 
 
  //login action for employer
public function loginAction(){
    $request = $this->getRequest();
    $userTable = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $postData = $request->getPost()->toArray();
    $stringTrimFilter = new StringTrim();
    $stripTagFilter = new StripTags();
    $postData['username'] = $stripTagFilter->filter($stringTrimFilter->filter($postData['username']));
    $postData['password'] = $stripTagFilter->filter($stringTrimFilter->filter($postData['password']));
    $passUtil = new UserPassword();
    $usrPass = $passUtil->create($postData['password']);
    $userData = $userTable->getData('users',array('where' => array('email' =>$postData['username'],'edBrandPassword' =>$usrPass,'status' => '0','testAccount'=>3)));
    if(!empty($userData)){
        
        
//       if($userData[0]['active']==0 && $userData[0]['isPaymentDone']==0){
//           $send = array(
//               'message' => "Your account is not active due to payment.",
//               'status'=>0  
//             );
//           return new JsonModel(array('send'=>$send));
//       }
       
//       else{
        //setup session for user
        $sess = new Container('User');
        $sess->offsetSet('userId',$userData[0]['username']);
        $sess->offsetSet('testAccount', 3);
        $uri = $this->getRequest()->getUri();
        $baseUrl = $uri->getHost();
        $userDetail = $userTable->getUserDetail($userData[0]['username']);
        $profilePic = $this->getMediaUrl()."profile/".$userDetail[0]['userId'].".png";
       // if(strstr($baseUrl,'expoodle')){
         // $url = "/users/client-hall/". base64_encode($userDetail[0]['fb_verfiy']);   
        //}else{
          $url = "/users/hall/".base64_encode($userDetail[0]['fb_verfiy']);  
        //}
        $userTable->updateData('users',array('edBrandPasswordString'=>"",'ticket'=>0,'fb_verfiy'=>1),array('username'=>$userData[0]['username']));
        $send = array(
               'message' => "Success",
               'status'=>1,
               'url'=>$url,
               'firstName'=>$userDetail[0]['firstName'],
               'profilePic'=>$profilePic
              );
        return new JsonModel(array('send'=>$send));    
//     }
      
    } else {
             $send = array(
               'message' => "Wrong Username or password.",
               'status'=>0  
             );
           return new JsonModel(array('send'=>$send));
   }
}
 // Employer Signup
 public function emailcheckAction(){
 try{       
    $request = $this->getRequest();
    $usrTlb = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $postData = $request->getPost()->toArray();
    //echo "<pre>"; print_r($postData); die;
    if($postData['type']==1){
       $checkNumber = $usrTlb->getDetail('users',array('username'),"email='".$postData['email']."'  and status=0 and testAccount='3'");
       if(!empty($checkNumber[0]['username'])){
            $send = array(
               'message' =>"Email already exist.",
               'status'=>0  
             );
           return new JsonModel(array('send'=>$send));
       }else{
           $send = array(
               'message' =>"Success",
               'status'=>1  
            );
           return new JsonModel(array('send'=>$send));
      }
   }else{
       $checkNumber = $usrTlb->getDetail('users',array('username'),"countryCode='".$postData['countyCode']."' and mobile='".$postData['mobile']."' and status=0 and testAccount='3'");
       
       if(!empty($checkNumber[0]['username'])){
            $send = array(
               'message' =>"Mobile No already exist.",
               'status'=>0  
             );
           return new JsonModel(array('send'=>$send));
       }else{
           $send = array(
               'message' =>"Success",
               'status'=>1  
            );
          return new JsonModel(array('send'=>$send));
     }       
   }
}catch(\Exception $e){
   $send = array(
               'message' =>"Something went wrong",
               'status'=>0  
              );
    return new JsonModel(array('send'=>$send));
}
 $send = array(
               'message' =>"Something went wrong",
               'status'=>0  
              );
  return new JsonModel(array('send'=>$send));
}
//forget password
 public function forgetPasswordAction(){
 try{       
    $request = $this->getRequest();
    $usrTlb = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $postData = $request->getPost()->toArray();
    $checkNumber = $usrTlb->getDetail('users',array('username'),"email='".$postData['username']."'  and status=0 and active=1 and testAccount='3'");
    if(!empty($checkNumber[0]['username'])){
        $passUtil = new UserPassword();
        $userPassword = $this->getToken(6); 
        $usrPass = $passUtil->create($userPassword);
        $usrTlb->updateData('users',array('eventapPassword'=>$usrPass),array('username'=>$checkNumber[0]['username']));
        //code for email
        $mail['mailFromNickName'] = "EvenTap";
    // $mail['mailTo'] = "bhupendra@approutes.com";
        $mail['mailTo'] = $postData['username'];
        $mail['mailSubject'] = "Eventap-Forget Password";
        $mail['mailBody'] = "<html><body>Hi User,<br>Here is your new password: ".$userPassword."<br>Please user this password for login.<br>Thanks<br>Eventap </body></html>";
        $this->sendotpemail($mail);
        $send = array(
               'message' =>"Password is sent your mail.Please check",
               'status'=>1  
             );
           return new JsonModel(array('send'=>$send));
     }else{
           $send = array(
               'message' =>"This email id not exist",
               'status'=>0  
            );
           return new JsonModel(array('send'=>$send));
    }
   
}catch(\Exception $e){
   $send = array(
               'message' =>$e->getMessage(),
               'status'=>0  
              );
    return new JsonModel(array('send'=>$send));
}
 $send = array(
               'message' =>"Something went wrong",
               'status'=>0  
              );
  return new JsonModel(array('send'=>$send));
}
//method for meeting request accept or reject
public function meetingResponseAction(){
    $pera = $this->params()->fromroute('id', NULL);
    $idArray = explode("-", base64_decode($pera));
    $postData['meetingId'] = $idArray[1];
    $postData['userId'] = $idArray[0];
    $usrTlb = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $myLastEvents = $usrTlb->getLastMeeting($postData['meetingId']);
    $myLastEvents[0]['userTagList']  = $usrTlb->listOfMeetingAttende($postData['meetingId'],$postData['userId']);
    $myLastEvents[0]['dateTime']   = $this->getUserTimeZone(date("Y-m-d H:i:s",$myLastEvents[0]['dateTime']),"Asia/Kolkata",1);
    //echo "<pre>"; print_r($myLastEvents); die;
    $mediaUrl = $this->getMediaUrl();
    $view = new ViewModel(array(
            'myLastEvents' => $myLastEvents[0],
            'mediaUrl'=>$mediaUrl,'userId'=>$postData['userId'],'meetingId'=>$postData['meetingId']
    ));
    $view->setTerminal(true);
    return $view;
}
//accept reject meeting
 public function acceptRejectMeetingAction(){
 try{       
    $request = $this->getRequest();
    $userTable = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $postData = $request->getPost()->toArray();
    if($postData['action']== "accepted"){ 
        $userTable->updateData('meetingAttendies',array('isAttending'=>1,'updatedOn'=> time()),array('meetingId'=>$postData['meetingId'],'userId'=>$postData['userId']));
         //method to send push to creator
         $creatorId = $userTable->getDetail('meeting',array('userId','title','dateTime'),"id='".$postData['meetingId']."'");
         $creatorDetail = $userTable->getDeviceDetailNew($creatorId[0]['userId']);
         $where4 = "username='".$postData['userId']."'"; 
         $getUserName = $userTable->getDetail('vcard_search',array('firstName','lastName'),$where4);
         $text = $getUserName[0]['firstName']." "."has accepted your meeting request. Please Check.";
         $username = substr(time().ceil((rand(0,10000))),0, 5).$this->getUserId(6);
         $interData['title']="";
         $interData['updatedon']=time();
         $interData['message']= $text;
         $interData['notificationDataId'] = 0;
         $interData['notifyId']= $username;
         $interData['creatorId']= $postData['userId'];
         $interData['eventId'] =  $postData['meetingId'];
         $interData['type']="acceptRejectMeeting";
         $body1 = $interData;
         $body1['nickname'] = $getUserName[0]['firstName'];
         $body1['userId'] = $postData['userId'];
         $body1['eventName'] = "";
         $body1['bodyType'] = "acceptRejectMeeting";
         $body1['notificationMedia'] = array();
         $body1['chatAllowed'] = "";
         $body1['media'] = "";
         $body1['link'] = "";
         $body1['uiType'] = "";
         $body = json_encode($body1,true);
         $interData['body']=$body;
         $interData['username'] = $creatorId[0]['userId'];
         $userTable->saveData('notification',$interData);
         if(!empty($creatorDetail[0]['device_token'])){
            //first check android or ios
           if($creatorDetail[0]['device_type']=='android'){
                $pushData["deviceId"] = $creatorDetail[0]["device_token"];
                $pushData["message"] = $body;
                $pushData["pType"] = 'app';
                $pushData["subject"] ='FCM push'; 
                $this->sendAndroidPush($pushData,$creatorDetail[0]['appType']);  
           }else{   
            //now make json to send users
          //now code to get total counter of user
          $userCounter = $userTable->getDetail('userNotifictionCount',array('totalApp','totalChat','timelineCount'),"userId='".$creatorDetail[0]['userId']."' and testAccount='".$testAccount."'");
          if(!empty($userCounter)){
             $userTable->updateData('userNotifictionCount',array('totalApp'=>$userCounter[0]['totalApp']+1),array('userId'=>$creatorDetail[0]['userId'],'testAccount'=>$testAccount)); 
          }else{
             $userTable->saveData('userNotifictionCount',array('totalApp'=>1,'userId'=>$creatorDetail[0]['userId'],'testAccount'=>$testAccount));  
          }
          //now get user badge Count
          $userBadgeCount = $userTable->getDetail('userNotifictionCount',array('totalApp','totalChat','timelineCount'),"userId='".$creatorDetail[0]['userId']."' and testAccount='".$testAccount."'");
          //now make json to send users
          $message['badge'] = (int)($userBadgeCount[0]['totalApp']+ $userBadgeCount[0]['totalChat']+ $userBadgeCount[0]['timelineCount']);     
          
            $message['mutable-content'] =1;
            $message['sound'] ="default";
            $message['chatId'] = "";
            $message['fromUser'] ="";
            $message['toUser'] = "";
            $message['alert'] = array("title"=>"","body"=>$interData['message']);
            $message['bodyType'] = $interData['type'];
            $message['sender'] = "";
            $message['message']= $body;
            $message['timestamp'] = time();
            $payload['aps'] = $message;
            $payload['c'] = 1;
            $message1= "success";  
            $this->sendapnspush($payload,$message1,$creatorDetail[0]["apnsToken"],'dev',$creatorDetail[0]["appType"],'alert'); 
            $this->sendapnspush($payload,$message1,$creatorDetail[0]["apnsToken"],'dist',$creatorDetail[0]["appType"],'alert');
          }
        }
        $mail['mailFromNickName'] = "Expoodle";
        //$mail['mailTo'] = "bhupendra@approutes.com";
        $mail['mailTo'] = $creatorDetail[0]['email'];
        $mail['mailSubject'] = "Expoodle | Meeting Request";
        $mail['mailBody'] = "<html><body>Hi ".$creatorDetail[0]["firstName"].",<br>".$getUserName[0]['firstName']." ".$getUserName[0]['lastName']." has accepted your meeting request.<br><br>Please Check in My Meetings under My Profile section.<br>Thanks & Regards<br>Expoodle Team</body></html>";
        $this->sendotpemail($mail);
        $send = array(
               'message' =>"You accepted meeting request successfully.",
               'status'=>1  
              );
       return new JsonModel(array('send'=>$send));
  }else{
              $userTable->updateData('meetingAttendies',array('isAttending'=>2,'updatedOn'=> time()),array('meetingId'=>$postData['meetingId'],'userId'=>$postData['userId']));
              //method to send push to creator
               $creatorId = $userTable->getDetail('meeting',array('userId'),"id='".$postData['meetingId']."'");
               $creatorDetail = $userTable->getDeviceDetail($creatorId[0]['userId']);
               $where4 = "username='".$postData['userId']."'"; 
               $getUserName = $userTable->getDetail('vcard_search',array('firstName','lastName'),$where4);
               $text = $getUserName[0]['firstName']." "."has rejected your meeting request. Please Check.";
               $username = substr(time().ceil((rand(0,10000))),0, 5).$this->getUserId(6);
               $interData['title']="";
            $interData['updatedon']=time();
            $interData['message']= $text;
            $interData['notificationDataId'] = 0;
            $interData['notifyId']= $username;
            $interData['creatorId']= $postData['userId'];
            $interData['eventId'] =  $postData['meetingId'];
            $interData['type']="acceptRejectMeeting";
            $body1 = $interData;
            $body1['nickname'] = $getUserName[0]['firstName'];
            $body1['userId'] = $postData['userId'];
            $body1['eventName'] = "";
            $body1['bodyType'] = "acceptRejectMeeting";
            $body1['notificationMedia'] = array();
            $body1['chatAllowed'] = "";
            $body1['media'] = "";
            $body1['link'] = "";
            $body1['uiType'] = "";
            $body = json_encode($body1,true);
            $interData['body']=$body;
            $interData['username'] = $creatorId[0]['userId'];
            $userTable->saveData('notification',$interData);
            if(!empty($creatorDetail[0]['device_token'])){
            //first check android or ios
           if($creatorDetail[0]['device_type']=='android'){
                $pushData["deviceId"] = $creatorDetail[0]["device_token"];
                $pushData["message"] = $body;
                $pushData["pType"] = 'app';
                $pushData["subject"] ='FCM push'; 
                $this->sendAndroidPush($pushData,$creatorDetail[0]['appType']);  
           }else{  
            //now make json to send users
          //now code to get total counter of user
          $userCounter = $userTable->getDetail('userNotifictionCount',array('totalApp','totalChat','timelineCount'),"userId='".$creatorDetail[0]['userId']."' and testAccount='".$testAccount."'");
          if(!empty($userCounter)){
             $userTable->updateData('userNotifictionCount',array('totalApp'=>$userCounter[0]['totalApp']+1),array('userId'=>$creatorDetail[0]['userId'],'testAccount'=>$testAccount)); 
          }else{
             $userTable->saveData('userNotifictionCount',array('totalApp'=>1,'userId'=>$creatorDetail[0]['userId'],'testAccount'=>$testAccount));  
          }
          //now get user badge Count
          $userBadgeCount = $userTable->getDetail('userNotifictionCount',array('totalApp','totalChat','timelineCount'),"userId='".$creatorDetail[0]['userId']."' and testAccount='".$testAccount."'");
          //now make json to send users
          $message['badge'] = (int)($userBadgeCount[0]['totalApp']+ $userBadgeCount[0]['totalChat']+ $userBadgeCount[0]['timelineCount']);     
          
            $message['mutable-content'] =1;
            $message['sound'] ="default";
            $message['chatId'] = "";
            $message['fromUser'] ="";
            $message['toUser'] = "";
            $message['alert'] = array("title"=>"","body"=>$interData['message']);
            $message['bodyType'] = $interData['type'];
            $message['sender'] = "";
            $message['message']= $body;
            $message['timestamp'] = time();
            $payload['aps'] = $message;
            $payload['c'] = 1;
            $message1= "success";  
            $this->sendapnspush($payload,$message1,$creatorDetail[0]["apnsToken"],'dev',$creatorDetail[0]["appType"],'alert'); 
            $this->sendapnspush($payload,$message1,$creatorDetail[0]["apnsToken"],'dist',$creatorDetail[0]["appType"],'alert');
          }
        }
        
        $mail['mailFromNickName'] = "Expoodle";
        //$mail['mailTo'] = "bhupendra@approutes.com";
        $mail['mailTo'] = $creatorDetail[0]['email'];
        $mail['mailSubject'] = "Expoodle | Meeting Request";
        $mail['mailBody'] = "<html><body>Hi ".$creatorDetail[0]["firstName"].",<br>".$getUserName[0]['firstName']." ".$getUserName[0]['lastName']." has rejected your meeting request.<br>Please Check in My Meetings under My Profile section.<br>Thanks & Regards<br>Expoodle Team</body></html>";
        $this->sendotpemail($mail);
        $send = array(
               'message' =>"You rejected meeting request.",
               'status'=>2  
              );
       return new JsonModel(array('send'=>$send));
   }
         
}catch(\Exception $e){
   $send = array(
               //'message' =>$e->getMessage(),
               'status'=>0,
               "message"=>"Something went wrong.Please try again."
              );
    return new JsonModel(array('send'=>$send));
}
 $send = array(
               "message"=>"Something went wrong.Please try again.",
               'status'=>0  
             );
 return new JsonModel(array('send'=>$send));
}
public function quickbloxAction(){
   $quickBlox = new Quickblox();
   $userId = "15741r67xqg";
   $password = "MuhwtpSSBBiFKKB7wUL0B2IVPwZJoLKj";
   $tokenAuth = $quickBlox->quickUserAuth($userId,$password);
   //print_r($tokenAuth); die;
   $token = $tokenAuth['session']['token'];
   //method for create dialog
   $occupants_ids = "114276918";
   $dialogResponse = $quickBlox->createDialog($token,3,$occupants_ids);
   //now code for chat message
   $chat_dialog_id = $dialogResponse['_id'];
   $message ="Welcome to EvenTap Help Center!";
   $createMessageDetail = $quickBlox->createMessage($token, $chat_dialog_id, $message);
   print_r($createMessageDetail); die;
}


//public function testmeetingAction(){
// try {
//    $client = new ClickMeetingRestClient(array('api_key' => 'us89783b3b4d5774997f96883be1aefb805c81fb09'));
//    // Conferences
//    $params = array(
//        'lobby_enabled'     => true,
//        'lobby_description' => 'My meeting',
//        'name'              => 'Bhupi',
//        'room_type'         => 'meeting',
//        'permanent_room'    => 0,
//        'access_type'       => 2,
//        'password'=>"123456",
////        'registration' => array(
////            'template'=> 1,
////            'enabled' => true
////        ),
//       'settings' => array(
//            'show_on_personal_page'        => 1,
//            'thank_you_emails_enabled'     => 1,
//            'connection_tester_enabled'    => 1,
//            'phonegateway_enabled'         => 1,
//            'recorder_autostart_enabled'   => 1,
//            'room_invite_button_enabled'   => 1,
//            'social_media_sharing_enabled' => 1,
//            'connection_status_enabled'    => 1,
//            'thank_you_page_url'           => 'http://example.com/thank_you.html',
//        ),
//    );
//
//    //$conference = $client->addConference($params);
//    //$room_id = $conference->room->id;
//    $room_id = "3729044";
//  // echo "<pre>"; print_r($room_id);  echo "<pre>"; print_r($conference); die;
//
////    print_r($client->conference($room_id));
////
////    print_r($client->conferences());
////
////    print_r($client->editConference($room_id, array('name' => 'new_test_room')));
////
////    print_r($client->conferenceSkins());
////
////    print_r($client->addContact([
////        'email' => 'example@domain.com',
////        'firstname' => 'John',
////        'lastname' => 'Dee',
////        'phone' => '+1234567890',
////        'company' => 'My company',
////        'country' => 'US']));
////
//    print_r($client->conferenceAutologinHash($room_id, array(
//        'email' => 'iram@approutes.com',
//        'nickname' => 'Palak',
//        'role' => 'listener'
//    )));
//    die("siisis");
//
////    print_r($client->sendConferenceEmailInvitations($room_id, 'us', array(
////        'attendees' => array(
////            array('email' => 'example@domain.com')
////        ),
////        'template' => 'advanced', // basic | advanced
////        'role' => 'listener',
////    )));
////
////    // Tokens
////    print_r($client->generateConferenceTokens($room_id, array('how_many' => 2)));
////
////    print_r($client->conferenceTokens($room_id));
////
////    // Sessions
////    print_r($client->conferenceSessions($room_id));
////
////    $existing_room_id = 123;
////    $existing_session_id = 456;
////    print_r($client->conferenceSession($existing_room_id, $existing_session_id));
////
////    print_r($client->conferenceSessionAttendees($existing_room_id, $existing_session_id));
////
////    print_r($client->generateConferenceSessionPDF($existing_room_id, $existing_session_id, 'en'));
////
////    // Timezones
////    print_r($client->timeZoneList());
////
////    print_r($client->countryTimeZoneList('us'));
////
////    print_r($client->phoneGatewayList());
////
////    // Registrations
////    print_r($client->addConferenceRegistration($room_id, array(
////        'registration' => array(
////           1 => 'John',
////           2 => 'Dee',
////           3 => 'example@domain.com'
////        ),
////        'confirmation_email' => array(
////            'enabled' => 1,
////            'lang' => 'en',
////        )
////    )));
////
////    print_r($client->conferenceRegistrations($room_id, 'all'));
////
////    // File library
////    $file = $client->addFileLibraryFile('/my/file.png');
////    print_r($file);
////
////    $file_id = $file->id;
////
////    print_r($client->fileLibrary());
////
////    print_r($client->conferenceFileLibrary($room_id));
////
////    print_r($client->fileLibraryFile($file_id));
////
////    print_r($client->fileLibraryContent($file_id));
////
////    print_r($client->deleteFileLibraryFile($file_id));
////
////    // Recordings
////    print_r($client->conferenceRecordings($room_id));
////
////    print_r($client->deleteConferenceRecordings($room_id));
////
////    $recording_id = 123;
////    print_r($client->deleteConferenceRecording($existing_room_id, $recording_id));
////
////    // Chats
////    print_r($client->chats());
////
////    print_r($client->conferenceSessionChats($existing_session_id));
////
////    print_r($client->deleteConference($room_id));
//}
//catch (Exception $e)
//{
//    print_r(json_decode($e->getMessage()));
//}
//die("sususu");
//}


}