<?php
namespace Cron\Controller;
use Zend\Mvc\Controller\AbstractActionController;

class CronController extends AbstractActionController
{
  public function cronAction(){
   $task = $this->params('task');
   if($task=='updateuser'){
     $this->updateUser();
   }else if($task=="sendwelcomepush"){
      $this->sendUserNotification();  
   }
   die("process complete");
 }
 
 public function updateUser(){
     $usrTlb = $this->getServiceLocator()->get('Application\Model\UsersTable');
     $userList = $usrTlb->getUserListForUpdate();
     foreach($userList as $value){
         $userId = substr(time().ceil((rand(0,10000))),0, 5).$this->getUserId(6);
         $loginKey = $this->getToken(32);
         $qrCode = "QR".$this->getToken(40);
         $name = preg_replace("/[^a-zA-Z]+/", "",$value['firstName']);
         $qrName = $name.$this->getToken(4);
         $invitationId ="INVITE". mt_rand(1000, 9999);
         //first Update user table
         $usrTlb->updateData('users',array('username'=>$userId,'password'=>$loginKey,'qrCode'=>$qrCode,'qrName'=>$qrName,'invitationId'=>$invitationId,'status'=>0,'active'=>1,'uts'=> time(),'testAccount'=>2,'countryCode'=>"+".$value['countryCode']),array('mappingUserId'=>$value['mappingUserId']));
         //now update vcard search table
         $usrTlb->updateData('vcard_search',array('username'=>$userId,'invitedBy'=>'15602060wjm','region'=>'r1','guestSide'=>'Bride','countryCode'=>"+".$value['countryCode']),array('mappingUserId'=>$value['mappingUserId']));
     }
     die("done");
 }
 
 
 public function getUserId($length) {
        $token = "";
        $codeAlphabet= "abcdklmvwxnopeiifghijqrstuyz";
        $codeAlphabet.= "0142376895";
        $max = strlen($codeAlphabet) - 1;
        for ($i = 0; $i < $length; $i++) {
            $token .= $codeAlphabet[$this->crypto_rand_secure(0, $max)];
        }
        return $token;
    }
     //get Token    
     public function getToken($length) {
        $token = "";
        $codeAlphabet = "IXQAZDWSELRFCVSPOMBGLKTYUJHG";
        $codeAlphabet.= "abcdklmvwxnopeiifghijqrstuyz";
        $codeAlphabet.= "0142376895";
        //$codeAlphabet.= "$&*@!%";
        $max = strlen($codeAlphabet) - 1;
        for ($i = 0; $i < $length; $i++) {
            $token .= $codeAlphabet[$this->crypto_rand_secure(0, $max)];
        }
        return $token;
    }
    public function crypto_rand_secure($min, $max) {
        $range = $max - $min;
        if ($range < 1)
            return $min; // not so random...
        $log = ceil(log($range, 2));
        $bytes = (int) ($log / 8) + 1; // length in bytes
        $bits = (int) $log + 1; // length in bits
        $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter; // discard irrelevant bits
        } while ($rnd >= $range);
        return $min + $rnd;
    }
}