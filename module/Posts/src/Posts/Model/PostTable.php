<?php
namespace Posts\Model;
use Zend\Db\Sql\Insert;
use Zend\Db\Sql\Sql;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Where;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Predicate\Predicate;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Like;
use CustomLib\Model\CustomTable;
use Zend\Db\Sql\Predicate\Expression;

class PostTable extends CustomTable
{

    protected $table = 'event';
    
    
    
    
    
    
    
    
public function getAllEvents($data=array(), $pageing = true)
    { 
       $searchUser = $data['search'];       
       if ($searchUser != '') {
           $searchnm = "and (lower(t1.eventName) like \"%" . $searchUser . "%\") or (lower(t1.location) like \"%" . $searchUser . "%\") or (lower(t1.venue) like \"%" . $searchUser . "%\")";
       } else {
          $searchnm = '';
       }
     $sql = new Sql($this->adapter);
     $select = $sql->select(array(
                               't1' =>'event'
                                 ));
         $select->columns(array(
            'id',
           'eventName',
           'eventCategoryId',
           'eventCategory',
            'eventTypeId', 
           'description',
           'eventStartDate',
           'eventEndDate',
           'location',
           'venue',
           'createdOn',
        )); 
       $select->join(array('t2' => 'eventMedia'), new Expression('t2.eventId = t1.id'), array('media'),'LEFT');
        $select->join(array('t3' => 'eventType'), new Expression('t3.eventTypeId = t1.eventTypeId'), array('eventType'=>'eventName'),'LEFT');
      $select->order(array(
          't1.id DESC'
       )); 
    $select->where(array(" t1.status=0  $searchnm"));
    if(!empty($data['filter'])){
        $select->where(array("t1.eventTypeId='".$data['filter']."' and t1.status=0  $searchnm "));
    }
    
     if ($pageing) {
            $dbAdapter = new DbSelect($select, $this->getAdapter());
            $paginator = new Paginator($dbAdapter);
            return $paginator;
        } else {
            $smt = $sql->prepareStatementForSqlObject($select);
            $result = $this->resultSetPrototype->initialize($smt->execute())
                ->toArray();
            return $result;
        }
    }
    
    
  public function getOwnerDetail($eventId){
        $sql = new Sql($this->getAdapter());
        $select = new Select();
        $select->from(array(
                           't1' => 'userRole' 
                           ));
        $select->columns(array('roleId'));
       $select->join(array('t2' => 'roleType'), new Expression('t2.roleType = t1.roleId'), array('roleName'),'LEFT');
       $select->join(array('t3' => 'users'), new Expression('t3.username = t1.userId'), array('countryCode','mobile','email'),'LEFT');
       $select->join(array('t4' => 'vcard_search'), new Expression('t4.username = t1.userId'), array('firstName'),'LEFT');
      $select->where(array("t1.roleId=1 and t1.eventId =".$eventId));
          $smt = $sql->prepareStatementForSqlObject($select);
          $results = $this->resultSetPrototype->initialize($smt->execute())
            ->toArray();
           return $results; 
    }
    

    
 //method for Owner Event list of any event
  public function getEventListForOwner($data=array(), $pageing = true)
    {
       $searchUser = $data['search'];       
       if ($searchUser != '') {
           $searchnm = "and (lower(t2.eventName) like \"%" . $searchUser . "%\") or (lower(t2.eventCategory) like \"%" . $searchUser . "%\")";
       } else {
          $searchnm = '';
       }
     $sql = new Sql($this->adapter);
     $select = $sql->select(array('t1' =>'userRole'));
     $select->columns(array(
           'userRoleId',
           'roleId',
           'eventId',
          'updatedOn',
           'createdOn',
           'status'
        ));  
     //
  $select->join(array('t2' => 'event'), new Expression('t2.id = t1.eventId'), array('eventName','eventCategory','description','eventStartDate','eventEndDate','location','venue'),'LEFT');
  $select->join(array('t3' => 'eventType'), new Expression('t2.eventTypeId = t3.eventTypeId'), array('eventTypeName'=>'eventName','eventTypeStatus'=> 'status'),'LEFT');

//   $select->order(array(
//          't1.eventProgramId DESC'
//       )); 
       
        $select->where(array("t1.status=0 and t1.roleId != 3  and t1.userId = '".$data['userId']."' $searchnm"));
          if(!empty($data['filter'])){
        $select->where(array("t3.eventTypeId='".$data['filter']."' and t1.status=0 and t1.roleId != 3 and t1.userId = '".$data['userId']."' $searchnm "));
    }

     if ($pageing) {
            $dbAdapter = new DbSelect($select, $this->getAdapter());
            $paginator = new Paginator($dbAdapter);
            return $paginator;
        } else {
            $smt = $sql->prepareStatementForSqlObject($select);
            $result = $this->resultSetPrototype->initialize($smt->execute())
                ->toArray();
            return $result;
        }
    }  
    public function getEventProgramList($data=array(), $pageing = true)
    { 
       $searchUser = $data['search'];       
       if ($searchUser != '') {
           $searchnm = "and (lower(t1.programName) like \"%" . $searchUser . "%\") or (lower(t1.location) like \"%" . $searchUser . "%\")";
       } else {
          $searchnm = '';
       }
     $sql = new Sql($this->adapter);
     $select = $sql->select(array('t1' =>'eventProgram'));
     $select->columns(array(
           'eventProgramId',
           'eventId',
           'programName',
           'descrption',
           'startDate',
           'endDate',
           'location',
           'createdOn',
           'dressCode',
           'venue'
        ));  
      $select->order(array(
          't1.eventProgramId DESC'
       )); 
    $select->where(array("t1.status=0 and t1.eventId=".base64_decode($data['pera'])."  $searchnm"));
     if ($pageing) {
            $dbAdapter = new DbSelect($select, $this->getAdapter());
            $paginator = new Paginator($dbAdapter);
            return $paginator;
        } else {
            $smt = $sql->prepareStatementForSqlObject($select);
            $result = $this->resultSetPrototype->initialize($smt->execute())
                ->toArray();
            return $result;
        }
    } 
    
    
    
    
  
    
    public function getTotalCounter($data){
        $sql = new Sql($this->adapter);
        $select = $sql->select(array(
                        't1' =>'users'
                            ));
        $select->columns(array(
            'count' => new \Zend\Db\Sql\Expression('COUNT(t1.username)'),
            )); 
            $select->join(array('t2' =>'vcard_search'), new Expression('t2.username = t1.username'), array(),'LEFT');
        if($data=='all'){
            $select->where(array("testAccount=0 and t2.firstName != '' and type = 'Guest'"));
        }else{
            $select->where(array("t1.RSVP='".$data."' and testAccount=0 and t2.firstName != '' and type = 'Guest'"));
        }
        $smt = $sql->prepareStatementForSqlObject($select);
    //    echo $smt->getSql(); die;
        $result = $this->resultSetPrototype->initialize($smt->execute())->toArray();
        return $result[0]['count']; 
    }
    
    
     public function getAllGuestForExcel($action){
        $sql = new Sql($this->getAdapter());
            $select = new Select();
            $select->from(array(
                't1' => 'users'
            ));
            $select->columns(array(
           'id',
           'RSVP',
            'RSVPTime',       
           'invitationId',
           'created_on',
           'username',
           'type',
           'familyId',
           'designation',
           'comingFrom',
           'headCount',
           'qrName',
           'whereFrom',
           'salutation',
           'countryCode',
           'qrCode',
            'comment'     
        )); 
    //
    $select->join(array('t2' =>'vcard_search'), new Expression('t2.username = t1.username'), array('firstName','email','isMailSent','isSmsSent','towerNo','flatNo','corPh','phone','gender'),'LEFT');
   
    $select->order(array(
          't1.id DESC'
    ));     
                
    if($action== "all"){
      $select->where(array("t1.type = 'Guest' AND  t2.firstName != '' AND t1.testAccount=0"));
    }else if($action== "yes"){
          $select->where(array("t1.type = 'Guest' AND t1.RSVP='yes' AND  t2.firstName != '' AND t1.testAccount=0"));
    
    }else if($action== "no"){
          $select->where(array("t1.type = 'Guest' AND t1.RSVP='no' AND  t2.firstName != '' AND t1.testAccount=0"));
    }else if($action== "mayBe"){
           $select->where(array("t1.type = 'Guest' AND t1.RSVP='maybe' AND  t2.firstName != '' AND t1.testAccount=0"));
    }else{
        $select->where(array("t1.type= 'Guest'  AND  t2.firstName != '' AND t1.testAccount=0"));
        
    }
      $smt = $sql->prepareStatementForSqlObject($select);
      $result = $this->resultSetPrototype->initialize($smt->execute())
      ->toArray();
      return $result;
    }
    
    
    
        //method to get list of any guest by event 
public function getAllGuestForShreeRam($data=array(),$pageing = true){ 
//    echo '<pre>';    print_r($data); die(' ol');
       $searchUser = $data['search'];  
      $filter = $data['filter'];
       if(!empty($filter)){
           $filterQuery = "and t1.RSVP='".$filter."'";
       } else {
           $filterQuery = '';
       }
       
       if ($searchUser != '') {
           $searchnm = "and ( (lower(t2.firstName) like \"%" . $searchUser . "%\") or (lower(t1.email) like \"%" . $searchUser . "%\") or (lower(t2.flatNo) like \"%" . $searchUser . "%\") or (lower(t2.phone) like \"%" . $searchUser . "%\"))";
       } else {
          $searchnm = '';
       }
     $sql = new Sql($this->adapter);
     $select = $sql->select(array(
                               't1' =>'shriRamUsers'
                                 ));
     $select->columns(array(
           'id',
           'RSVP',
           'invitationId',
           'created_on',
           'username',
           'type',
           'familyId',
           'designation',
           'comingFrom',
           'headCount',
           'qrName',
           'whereFrom',
           'salutation',
           'countryCode',
           'RSVPTime',
           'qrCode'
        )); 
    //
    $select->join(array('t2' =>'shriRamVcard_search'), new Expression('t2.username = t1.username'), array('firstName','email','towerNo','flatNo','corPh','phone','gender'),'LEFT');
    $select->order(array(
          't1.id DESC'
    )); 
    if($data['filter']=="all"){
    $select->where(array(" t1.type = 'Guest' AND  t2.firstName != '' AND t1.testAccount=0   $searchnm"));
      }else{
    $select->where(array(" t1.type = 'Guest' AND  t2.firstName != '' AND t1.testAccount=0   $searchnm $filterQuery"));
    }
     if ($pageing) {
            $dbAdapter = new DbSelect($select, $this->getAdapter());
            $paginator = new Paginator($dbAdapter);
            return $paginator;
        } else {
            $smt = $sql->prepareStatementForSqlObject($select);
            $result = $this->resultSetPrototype->initialize($smt->execute())
                ->toArray();
            return $result;
        }
    }
    
    
    
     public function getTotalCounterForShreeRam($data){
        $sql = new Sql($this->adapter);
        $select = $sql->select(array(
                        't1' =>'shriRamUsers'
                            ));
        $select->columns(array(
            'count' => new \Zend\Db\Sql\Expression('COUNT(username)'),
            )); 
        if($data=='all'){
            $select->where(array("testAccount=0 and type = 'Guest'"));
        }else{
            $select->where(array("t1.RSVP='".$data."' and testAccount=0 and type = 'Guest'"));
        }
        $smt = $sql->prepareStatementForSqlObject($select);
    //    echo $smt->getSql(); die;
        $result = $this->resultSetPrototype->initialize($smt->execute())->toArray();
        return $result[0]['count']; 
    }

    public function getUserDetailInvite($pageing = false){
        $sql = new Sql($this->adapter);
         $select = $sql->select(array(
                   't1' =>'shriRamVcard_search'
                       ));
         $select->columns(array(
            'firstName',
            'username',
            'flatNo',
            'phone'
            )); 
       $select->join(array('t2' => 'shriRamUsers'), new Expression('t2.username = t1.username'), array('qrCode','qrName','rsvpUrl'),'LEFT');
       $select->where(array("t2.type='Guest' and t2.testAccount=0  and t1.phone!='' and t1.phone!=0 "));
    //    $select->order('t1.id desc');
       
      $select->limit(50);
       if ($pageing) {
                $dbAdapter = new DbSelect($select, $this->getAdapter());
                $paginator = new Paginator($dbAdapter);
                return $paginator;
       } else {
                $smt = $sql->prepareStatementForSqlObject($select);
                $result = $this->resultSetPrototype->initialize($smt->execute())
                    ->toArray();
                return $result;
        }   
      }
    
    
    
   }