<?php
namespace Posts\Controller;
use Zend\View\Model\ViewModel;
use CustomLib\Controller\CustomController;
use Zend\Filter\StripTags;
use Zend\Filter\StringTrim;
use Zend\View\Model\JsonModel;
use CustomLib\Service\Quickblox;
class IndexController extends CustomController
{
//method for root    
    public function indexAction()
    {
        return new ViewModel(array());
    } 
    public function chatAction(){
        $userTable = $this->getServiceLocator()->get('Application\Model\UsersTable');
        $loginDetail = $this->getLoggedInUserId();
        $userDetail = $userTable->getUserDetail($loginDetail['userId']);
        //first check chat dialogue id exist or not
        if(empty($userDetail[0]['chat_dialog_id'])){
          //first get help center user Detail
          $helpCenterDetail = $userTable->getDetail('users',array("username","password"),"username='15741r67xqg'");
          $quickBlox = new Quickblox();
          $tokenAuth = $quickBlox->quickUserAuth($helpCenterDetail[0]['username'],$helpCenterDetail[0]['password']);
          //print_r($tokenAuth); die;
          $token = $tokenAuth['session']['token'];
          //method for create dialog
          $occupants_ids = $userDetail[0]['quickBloxId'];
          $dialogResponse = $quickBlox->createDialog($token,3,$occupants_ids);
          //now code for chat message
          $chat_dialog_id = $dialogResponse['_id'];
          $message ="Welcome to the Help Center! How can I help you?";
          $createMessageDetail = $quickBlox->createMessage($token, $chat_dialog_id, $message);
          //now update in users table
          $userTable->updateData('users',array("chat_dialog_id"=>$chat_dialog_id),array('username'=>$loginDetail['userId']));
        }else{
         //nathing will happen   
        }
        $viewModel = new ViewModel(array('userDetail'=>$userDetail[0]));
        $viewModel->setTerminal(true);
        return $viewModel;
   }
   public function quickbloxAction(){
        $userTable = $this->getServiceLocator()->get('Application\Model\UsersTable');
        $loginDetail = $this->getLoggedInUserId();
        $userDetail = $userTable->getUserDetail($loginDetail['userId']);
        $userList = $userTable->getUserCallList($loginDetail['userId']);
        $viewModel = new ViewModel(array('userDetail'=>$userDetail[0],'userList'=>$userList));
        $viewModel->setTerminal(true);
        return $viewModel;
   }
//method for grap seat
public function grabseatAction(){
   $request = $this->getRequest();
   $userTable = $this->getServiceLocator()->get('Application\Model\UsersTable');
   $loginDetail = $this->getLoggedInUserId();
   $userDetail = $userTable->getUserDetail($loginDetail['userId']);
   $postData = $request->getPost()->toArray();
   $checkUserExist = $userTable->getDetail('soicalTableParticapnts',array('id'),"tableId='".$postData['tableId']."' and userId='".$loginDetail['userId']."'");
   if(empty($checkUserExist)){
     //booking seat counter
    $seatCounter = $userTable->getProfileCounter('soicalTableParticapnts',"tableId='".$postData['tableId']."'");
    if($seatCounter==8){
    $send = array(
               'message' => "Sorry! We are not able to book seat in this Table,Currently Table is full.",
               'status'=>0
              );
    }else{
        $userTable->saveData('soicalTableParticapnts',array('tableId'=>$postData['tableId'],'userId'=>$loginDetail['userId'],'quickbloxId'=>$userDetail[0]['quickBloxId'],'tableNo'=>$postData['seatNo'],'createdOn'=> time()));
        $send = array(
               'message' => "success",
               'status'=>1
             );
     }
    
   }else{
         $send = array(
               'message' => "You have already booked your seat",
               'status'=>0
              );  
    }
//   $seatCounter = $userTable->getProfileCounter('soicalTableParticapnts',"tableId='".$postData['tableId']."'");
//   if($seatCounter==1){
//    $send = array(
//               'message' => "Your Seat Book Successfully,But For Call atleast Two Member Needed.Wait for another member",
//               'status'=>0
//              );
//   }else{
       
  // }
  return new JsonModel(array('send'=>$send)); 
}
public function showparticpantlistAction(){
      $userTable = $this->getServiceLocator()->get('Application\Model\UsersTable');
      $loginDetail = $this->getLoggedInUserId();
      $userDetail = $userTable->getUserDetail($loginDetail['userId']);
      $request = $this->getRequest();
      $postData = $request->getPost()->toArray();
      $particpantList = $userTable->getTableParticpant($postData['tableId'],$loginDetail['userId']);
      $mediaUrl = $this->getMediaUrl();
      $callerId = $userDetail[0]['quickBloxId'];
      $particpantId = "";
      foreach($particpantList as $value){
       $particpantId.=$value['quickbloxId'].",";   
      }
      $particpantId = rtrim($particpantId,",");
      $viewModel = new ViewModel(array('userDetail'=>$userDetail[0],'particpantList'=>$particpantList,'mediaUrl'=>$mediaUrl,'callerId'=>$callerId,'particpantId'=>$particpantId,'postData'=>$postData));
      $viewModel->setTerminal(true);
      return $viewModel;
}
public function leavemeetingAction(){
   $request = $this->getRequest();
   $userTable = $this->getServiceLocator()->get('Application\Model\UsersTable');
   $loginDetail = $this->getLoggedInUserId();
   $userDetail = $userTable->getUserDetail($loginDetail['userId']);
   $postData = $request->getPost()->toArray();
   $userTable->deleteUserData('soicalTableParticapnts',array('tableId'=>$postData['tableId'],'userId'=>$loginDetail['userId']));
   $send = array(
               'message' => "success",
               'status'=>1,
               "firstName"=>$userDetail[0]['firstName']
               );
  return new JsonModel(array('send'=>$send)); 
}
public function networkingTableDataAction(){
        $userTable = $this->getServiceLocator()->get('Application\Model\UsersTable');
        $loginDetail = $this->getLoggedInUserId();
        //table1 data->code for 1st table particpant list
        $one = $userTable->getTableParticpantList(1,1);
        $two = $userTable->getTableParticpantList(1,2);
        $three = $userTable->getTableParticpantList(1,3);
        $four = $userTable->getTableParticpantList(1,4);
        $five = $userTable->getTableParticpantList(1,5);
        $six = $userTable->getTableParticpantList(1,6);
        $seven = $userTable->getTableParticpantList(1,7);
        $eight = $userTable->getTableParticpantList(1,8);
        $mediaUrl = $this->getMediaUrl();
        $checkUserSeatExist = $userTable->getDetail('soicalTableParticapnts',array('id'),"tableId=1 and userId='".$loginDetail['userId']."'");
        if(!empty($checkUserSeatExist)){
           $bookSeat = 1; 
        }else{
           $bookSeat = 0;
        }
        
        $viewModel = new ViewModel(array('mediaUrl'=>$mediaUrl,'one'=>$one,'two'=>$two,'three'=>$three,'four'=>$four,'five'=>$five,'six'=>$six,'seven'=>$seven,'eight'=>$eight,'bookSeat'=>$bookSeat));
        $viewModel->setTerminal(true);
        return $viewModel;
 }
 public function videochatAction(){
        $userTable = $this->getServiceLocator()->get('Application\Model\UsersTable');
        $loginDetail = $this->getLoggedInUserId();
        $userDetail = $userTable->getUserDetail($loginDetail['userId']);
        //firstTable detail
        $tableDetail = $userTable->getDetail('socialTables',array('chat_dialog_id','chatCreatorId'),"id=1 and status=1");
        //now code for add partipant in existing dialog
        //first get group creater detail
        $groupCreatorDetail = $userTable->getUserDetail($tableDetail[0]['chatCreatorId']);
        $quickBlox = new Quickblox();
        $tokenAuth = $quickBlox->quickUserAuth($groupCreatorDetail[0]['userId'],$groupCreatorDetail[0]['password']);
        $token = $tokenAuth['session']['token'];
        //method for create dialog
        $occupants_ids = (int)$userDetail[0]['quickBloxId'];
        $dialogResponse = $quickBlox->update_dialogue($token,"Table1",$occupants_ids,$tableDetail[0]['chat_dialog_id']);
        $viewModel = new ViewModel(array('userDetail'=>$userDetail[0],'chat_dialog'=>$tableDetail[0]['chat_dialog_id']));
        $viewModel->setTerminal(true);
        return $viewModel;
   }
}