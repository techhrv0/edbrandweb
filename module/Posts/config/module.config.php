<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'Posts\Controller\Posts' => 'Posts\Controller\IndexController',
        ),
    ),
    'router' => array(
        'routes' => array(
            'eventprogram' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/eventprogram[/][:action][/:id][/:val]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Posts\Controller\Posts',
                        'action'     => 'index',
                    ),
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'index' => __DIR__ . '/../view',
        ),
        'strategies' => array (
            'ViewJsonStrategy'
        )
    ),
);