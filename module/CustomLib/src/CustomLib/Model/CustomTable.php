<?php
/**
 * Description of CustomTable
 *
 * @author adminuser
 */
namespace CustomLib\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Select;
/**
 * Extends in other model class
 *
 * @category Admin
 * @package Model
 *         
 */
class CustomTable extends AbstractTableGateway implements 
    ServiceLocatorAwareInterface
{
    protected $_serviceLocator;
    
    /**
     * Set $_serviceLocator
     *
     * @access pubic
     * @param ServiceLocatorInterface $serviceLocator
     *            // ServiceLocatorInterface instance
     * @see \Zend\ServiceManager\ServiceLocatorAwareInterface::setServiceLocator()
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->_serviceLocator = $serviceLocator;
    }
    
    /**
     * Get $_serviceLocator
     *
     * @access pubic
     * @return \Zend\ServiceManager\ServiceLocatorAwareInterface
     * @see \Zend\ServiceManager\ServiceLocatorAwareInterface::getServiceLocator()
     */
    public function getServiceLocator()
    {
        return $this->_serviceLocator;
    }
    
    /**
     * Constructor
     *
     * @access pubic
     * @param Adapter $adapter
     *            // Adapter instance
     */
    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->initialize();
    }
    
    /**
     * Magic Method __Call
     *
     * @access pubic
     * @param <string> $method
     *            // Method name
     * @param <array> $arguments
     *            // Arguments
     * @return <mixed>
     */
    public function __call($method, $arguments)
    {
        if (preg_match('/^findAllBy/', $method)) {
            $criteria = substr($method, 9);
            
            /*
             * $criteria =
             * strtolower($this->getServiceLocator()->get('Service\Utility')
             * ->fromCamelCase($criteria));
             */
            $criteria = strtolower($criteria);
            
            $criteriaArr = explode('_and_', $criteria);
            
            $num = 0;
            
            $conditions = array();
            
            foreach ($criteriaArr as $val) {
                $conditions[$val] = (string) $arguments[$num];
                $num++;
            }
            
            return $this->select($conditions)->toArray();
        }
        
        //parent::__call($method, $arguments);
    }
    
    /**
     *
     * @param string $table
     * @param unknown $data
     */
    public function getData($mytable = '', $data = array())
    {
        try {
            $sql = new Sql($this->adapter);
            $select = $sql->select($mytable);
            if (! empty($data['where'])) {
                $select->where($data['where']);
            }
            if(!empty($data['limit'])){
                $select->limit($data['limit'][0])->offset((!empty($data['limit'][1]))?$data['limit'][1]:0);
            }
            if (! empty($data['order'])) {
                $select->order($data['order']);
            }
            $smt = $sql->prepareStatementForSqlObject($select);
            $result = $this->resultSetPrototype->initialize($smt->execute())
            ->toArray();
            return $result;
        } catch (\Exception $e) {
            echo $e->getMessage();
            return array();
        }
    }
    
    /**
     *
     * @param string $table
     * @param unknown $data
     */
    public function getDataCount($mytable = '', $data = array())
    {
        try {
            $sql = new Sql($this->adapter);
            $select = $sql->select($mytable);
            $select->columns(array(
                'count' => new Expression('COUNT(id)')
            ));
            if (! empty($data['where'])) {
                $select->where($data['where']);
            }
            if(!empty($data['limit'])){
                $select->limit($data['limit'][0])->offset((!empty($data['limit'][1]))?$data['limit'][1]:0);
            }
            if (! empty($data['order'])) {
                $select->order($data['order']);
            }    
            $smt = $sql->prepareStatementForSqlObject($select);
            $result = $this->resultSetPrototype->initialize($smt->execute())
            ->toArray();
            return $result;
        } catch (\Exception $e) {
            echo $e->getMessage();
            return array();
        }
    }
    
    /**
     *
     * @param string $myTable
     * @param unknown $data
     */
public function saveData($mytable = '', $data = array())
    {
        try {
            $sql = new Sql($this->adapter);
            $insert = $sql->insert($mytable);
            $insert->values($data);
            $smt = $sql->prepareStatementForSqlObject($insert);
            $this->resultSetPrototype->initialize($smt->execute());
            return $this->adapter->getDriver()->getLastGeneratedValue();
        } catch (\Exception $e) {
            echo $e->getMessage();
            return 0;
        }
    }
    
    /**
     *
     * @param string $myTable
     * @param unknown $data
     */
public function updateData($mytable = '', $data = array(), $where = array())
    {
        try {
            $sql = new Sql($this->adapter);
            $update = $sql->update($mytable);
            $update->set($data);
            $update->where($where);
            $smt = $sql->prepareStatementForSqlObject($update);
            $this->resultSetPrototype->initialize($smt->execute());
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }
    
    /**
     *
     * @param string $myTable
     * @param unknown $data
     */
public function deleteUserData($mytable = '', $where = array())
    {
        try {
            $sql = new Sql($this->adapter);
            $delete = $sql->delete($mytable);
            $delete->where($where);
            $smt = $sql->prepareStatementForSqlObject($delete);
            $this->resultSetPrototype->initialize($smt->execute());
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }
   /*
    * delete User
    */ 
   public function deleteUser($userId) {
        $adapter = $this->getAdapter();
        $stmt = $adapter->createStatement(); 
        $stmt->prepare('call deleteUser(:_UserId)'); 
        $stmt->getResource()->bindParam(':_UserId', $userId, \PDO::PARAM_INT); 
        $resultado=$stmt->execute();
   }
  //common fetch method by columns 
   public function getDetail($table,$columns,$where){
         $sql = new Sql($this->getAdapter());
        $select = new Select();
        $select->from(array(
            't1' => $table
        ));
        if(!empty($columns)){
            $select->columns($columns);
        }
        $select->where(array($where));
        $statement = $sql->prepareStatementForSqlObject($select);
        $loginDetails = $this->getResultSetPrototype()
            ->initialize($statement->execute())
            ->toArray();
       return $loginDetails;
  }
public function getProfileCounter($table,$where,$type){
        $sql = new Sql($this->getAdapter());
          $select = new Select();
          $select->from(array(
              't1' => $table
          ));
           $select->columns(array(
           'count'=>new Expression('COUNT(t1.id)')
          ));
         if($type==2){
             $select->join(array(
            't2' => 'users'
        ), 't2.username = t1.username', 
            array(), 'LEFT');
        }  
          $select->where(array($where));
          $statement = $sql->prepareStatementForSqlObject($select);

          $followCounter = $this->getResultSetPrototype()
              ->initialize($statement->execute())
              ->toArray();
          return $followCounter[0]['count'];
  }
  public function callSetUserMatchedJobProcedure($userId,$testAccount) {
      if($testAccount==1){
        $adapter = $this->getAdapter();
        $stmt = $adapter->createStatement();
        $stmt->prepare('CALL getUserJob(:_UserRefId)');
        $stmt->getResource()->bindParam(':_UserRefId', $userId, \PDO::PARAM_INT);
        $resultado=$stmt->execute();
        return "success";
      }else{
       $adapter = $this->getAdapter();
        $stmt = $adapter->createStatement();
        $stmt->prepare('CALL refreshSkillProcedure(:_UserRefId)');
        $stmt->getResource()->bindParam(':_UserRefId', $userId, \PDO::PARAM_INT);
        $resultado=$stmt->execute();
        return "success";   
      }
    }
  
}