<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace CustomLib\Service;

/**
 * Description of NotificationMessage
 *
 * @author varun
 */
class NotificationMessage {
    //put your code here

    // Ticket Create Notification Message
    const TICKET_CREATED_SUCCESSFULLY = ' Ticket Successfully Created.';
    const TICKET_ASSIGN_TO_USER = ' Assign TICKET To You.';
    const ADMIN_CREATED_TICKET  =' Ticket Created By Admin Notification';
    const TICKET_CREATED_NOTIFICATION = ' Ticket Created By ';
    
    // COMMON MESSAGES - Forward Ticket
    const USER_FORWARD_TO_USER= ' Has Forward Ticket To You With Ticket Number ';
    const USER_FORWARD_TO_ADMIN = ' Has Forward Ticket To Admin.';
    const ADMIN_FORWARD_TO_USER = ' Ticket is Forward By Admin To User.';
    const FORWARD_NOTIFICATION = ' Has Forward A Ticket.';
    
     // Close Ticket
    const CREATOR_CLOSE = ' Creator Of The Ticket Close The Ticket.';
    const USER_CLOSE = ' Has Close The Ticket Of TicketId ';
     const TICKET_REOPEN = ' Has ReOpen The Ticket Of TicketId';
    const ADMIN_CLLOSE = ' Admin Has Close The Ticket.';
    const TICKET_CLOSE_NOTIFICATION = " Ticket Closed Notification To Admin.";
    
    //Escalation
    const ESCALATION_TO_USER = ' In First 24 Hour Of The Ticket Creation The Escalation To User Is Done.';
    const ESCALATION_TO_ADMIN = ' In Next 24 Hour Of The Ticket Creation The Escalation To Admin Is Done.';
    const ESCALATION_NOTIFICATION = ' Escalation Notification To The Admin.';

    //On Hold
    const ON_HOLD = ' Ticket Has Put On Hold.';
}