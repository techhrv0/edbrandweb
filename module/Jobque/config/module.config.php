<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'Jobque\Controller\Jobque' => 'Jobque\Controller\JobqueController',
        ),
    ),
    'router' => array(
        'routes' => array(
            'Jobque' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/jobque[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobque\Controller\Jobque',
                        'action'     => 'index',
                    ),
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'notification\Controller\Notification' => __DIR__ . '/../view',
        ),
        'strategies' => array (
            'ViewJsonStrategy'
        )
    ),
);
