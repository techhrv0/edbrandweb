<?php
namespace Users\Controller;
use Zend\View\Model\ViewModel;
use CustomLib\Controller\CustomController;
use Zend\View\Model\JsonModel;
use Zend\Filter\StripTags;
use Zend\Filter\StringTrim;
use CustomLib\Service\UserPassword;
use CustomLib\Service\ClickMeetingRestClient;
class UsersController extends CustomController
{
  //default action control  
  public function agendaAction()
  {
       $userTable = $this->getServiceLocator()->get('Application\Model\UsersTable');
      //code for agenda location
       $agendaLocation  = $userTable->getDetail('agendaLocation',array('location'),"status=1 order by trackOrder asc"); 
       //code for agenda keyword
       $agendaKeyWord  = $userTable->getDetail('agenda',array('keyWord'),"status=0 and testAccount='3' and keyWord!='' and itemType='agenda' group by keyWord order by keyWord asc"); 
        //code for agenda theme
       $agendaTheme  = $userTable->getDetail('agenda',array('theme'),"status=0 and testAccount='3' and theme!='' and itemType='agenda'  group by theme order by theme asc");
       $agendDate = $userTable->getAgendaDate();
       $view = new ViewModel(array('agendaLocation'=>$agendaLocation,'agendaKeyWord'=>$agendaKeyWord,'agendaTheme'=>$agendaTheme,'agendDate'=>$agendDate));
       return $view;
  }
 public function agendaDataAction(){
    $userTable = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $request = $this->getRequest();
    $loginDetail = $this->getLoggedInUserId();
    $postData = $request->getPost()->toArray();
    if(empty($loginDetail['userId'])){
       $postData['userId'] = "123456"; 
    }else{
       $postData['userId'] = $loginDetail['userId'];   
    }
    $checkAttendeType = $userTable->getDetail('users',array('attendeType','isFutureStick','isPaid'),"username='".$postData['userId']."'");
     $checkGender= $userTable->getDetail('vcard_search',array('gender'),"username='".$postData['userId']."'");
    if(empty($checkAttendeType[0]['attendeType'])){
        $type ="all";
    }elseif($checkAttendeType[0]['attendeType']=="VIP"){
       $type = "vip";
    }elseif($checkAttendeType[0]['attendeType']=="HRPRAC"){
      $type = "prac";  
    }else{
      $type = "all";    
    }
    $getAgendaList = $userTable->listofmasteragendaNew($postData,$type,3);
      foreach($getAgendaList as $ke1=>$agenda){
            $getAgendaList[$ke1]['strStartDate'] = date('l, M d, Y',$agenda['startDate']);
            $getAgendaList[$ke1]['waitingMessageColorCode'] ="#FF9900";
           if($agenda['itemType']=='agenda'){
             $checkLike = $userTable->checkAgendaFav($postData['userId'],$agenda['id']);
            if(empty($checkLike)){
               $getAgendaList[$ke1]['isLike'] ="0";
            }else if($checkLike==0){
               $getAgendaList[$ke1]['isLike'] ="0";  
            }else{
              $getAgendaList[$ke1]['isLike'] ="1";  
            }
           //code for speaker name
           $data['itemId'] = $agenda['id'];
           $data['itemType'] = 'agenda';
           $speakerName = $userTable->getSpekAgName($data);
           $getAgendaList[$ke1]['speakersName'] = $speakerName;
       }  
     }
     
     $dateArray = array();
          //grouping result by date
          foreach($getAgendaList as $key=>$value){
              $dateArray[date('l, M d, Y',$value['startDate'])][] = $value;
          } 
          $record = array();
          //group result by time duration
          foreach($dateArray as $key1=>$value2){
              $timeArray = array(); 
             foreach($value2 as $key2=>$value3){     
               $timeArray[$value3['timeDuration']][] = $value3;  
             }
             //$finalArray = array();
             $finalArraynew = array();
             foreach($timeArray as $key3=>$value4){
               $finalArray['timeDuration'] = $key3;
               $finalArray['list'] = $value4;
               $finalArraynew [] = $finalArray;
             }
             //get Title of day
             $title = $userTable->getDetail('agendaTitle',array('title'),"date='".$key1."'");
             $final['title'] = $title[0]['title'];
             $final['date'] = $key1;
             $final['strStartDate'] = $key1;
             $final['timeStamp'] = $finalArraynew[0]['list'][0]['startDate'];
             $final['result'] = $finalArraynew;
             $record[] = $final;
         }
        // echo "<pre>"; print_r($record); die;
    $uri = $this->getRequest()->getUri();
    $mediaUrl = $this->getMediaUrl();
    $view = new ViewModel(array(
            'uri' => $uri,
            'record' => $record,
            'mediaUrl'=>$mediaUrl,
            'userId'=>$loginDetail['userId']
       ));
    $view->setTerminal(true);
    return $view;  
 }
public function agendaDetailAction(){
    $request = $this->getRequest();
    $data = $request->getPost()->toArray();
    $usrTlb = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $getAgendaDetail = $usrTlb->agendaDetail($data['agendaId']);
    $loginDetail = $this->getLoggedInUserId();
    if(empty($loginDetail['userId'])){
       $postData['userId'] = "0"; 
    }else{
       $postData['userId'] = $loginDetail['userId'];   
    }
    foreach($getAgendaDetail as $key=>$value){
       $getAgendaDetail[$key]['strStartDate'] = date('l, M d, Y',$value['startDate']); 
       $checkLike = $usrTlb->checkAgendaFav($postData['userId'],$value['id']);
        if(empty($checkLike)){
           $getAgendaDetail[$key]['isLike'] ="0";
        }else if($checkLike==0){
           $getAgendaDetail[$key]['isLike'] ="0";  
        }else{
          $getAgendaDetail[$key]['isLike'] ="1";  
       }
       //code for speaker name
        $data1['itemId'] = $value['id'];
        $data1['itemType'] = 'agenda';
        $getAgendaDetail[$key]['speakersName'] = $usrTlb->getSpekAgNameNew($data1);
    }
   // echo "<pre>"; print_r($getAgendaDetail); die;
    $view = new ViewModel(array(
      'getAgendaDetail' => $getAgendaDetail[0],
      'userId'=>$loginDetail['userId']  
    ));
    $view->setTerminal(true);
    return $view;   
}
 
 public function speakerAction()
  {
    $view = new ViewModel(array());
     return $view;
  }
  public function speakerDataListAction(){
    $usrTlb = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $request = $this->getRequest();
    $data = $request->getPost()->toArray();
    //$loginDetail = $this->getLoggedInUserId();
    
    $data['action'] = "speaker";
    $paginator = $usrTlb->getUserFeedList($data,3,true);
    $results = $this->paginationToArray($paginator,$data['page'],8);
    $uri = $this->getRequest()->getUri();
    $mediaUrl = $this->getMediaUrl();
    $view = new ViewModel(array(
            'paginator' => $paginator,
            'uri' => $uri,
            'results' => $results,
            'mediaUrl'=>$mediaUrl
       ));
        $view->setTerminal(true);
        return $view; 
  }
  
  public function partnersAction()
  {
  //MYSCript for speaker
  $usrTlb = $this->getServiceLocator()->get('Application\Model\UsersTable');
   $loginDetail = $this->getLoggedInUserId();
   
//   $userCounters = $this->getSpeakerCounter($loginDetail['testAccount']);
//   $pera = $this->params()->fromroute('id', NULL);
   //$cityFilter = $usrTlb->getCityFilter($loginDetail['testAccount']);
   
   
//  if(!empty($pera)){
//      $idArray = explode("-", base64_decode($pera));
//      $filter = $idArray[0];
//      $page = $idArray[1];
//      $lastUserId = $idArray[2];
//  }else{
//     $page =0;
//     $filter ='all';
//     $lastUserId = 0;
//  }
  
 
   //code for import data
//   $msg = "";
//   $request = $this->getRequest();
//   if($request->isPost()) {
//       $files = $request->getFiles()->toArray();
//     if(!empty($files['file']['name'])){
//           $allowedExtensions = array("xls","xlsx");
//           $ext = pathinfo($files['file']['name'], PATHINFO_EXTENSION);
//
//         if(in_array($ext, $allowedExtensions)) {
//               $file_size = $files['file']['size'] / 1024;
//
//              if($file_size < 1000) {   
//               $path="public/uploadedFiles/OstrichMedia/";
//                $file = $path.$files['file']['name'];
//                $isUploaded = copy($files['file']['tmp_name'], $file); 
//
//           if($isUploaded) {
//              try {
//                  $inputFileType = \PHPExcel_IOFactory::identify($file);
//                  $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
//                  $objPHPExcel = $objReader->load($file);
//              } catch(Exception $e) {
//                  die('Error loading file "'.pathinfo($file,PATHINFO_BASENAME).'": '.$e->getMessage());
//              }            
//            $sheet = $objPHPExcel->getActiveSheet();
//            $sData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
//            $sheetData = array_slice($sData, 0);
//            
//      // Loop through each row of the worksheet in turn
//        foreach($sheetData as $row){
//
//    if(trim($row['A']) != "First Name" && trim($row['A']) != ""){
//          //code for userTable
//          $userId = substr(time().ceil((rand(0,10000))),0, 5).$this->getUserId(6);
//            $loginKey = $this->getToken(32);   
//             $cli['username'] = $userId;
//             $cli['password'] = $loginKey;
//             $cli['attendeType'] = 0;  
//             $cli['status']=0; 
//             $cli['active'] = 1;
//             $cli['timestamp']=time();
//             $cli['uts']= time(); 
//             $cli['type']= "speaker"; 
//             $cli['roleType'] = 0;
//             $cli['testAccount']= 3;
//             $qrCode = "QR".$this->getTicket(40);
//             $name = preg_replace("/[^a-zA-Z]+/", "", trim($row['A']));
//             $qrName = $name.$this->getTicket(4);
//             $cli['qrCode'] = $qrCode;
//             $cli['qrName'] = $qrName;
//             $invitationId ="INVITE". mt_rand(1000, 9999);
//             $cli['invitationId']=$invitationId;
//             
//              if(!empty(trim($row['F']))){
//                 $cli['email'] = trim($row['F']);
//              }else{
//                 $cli['email'] = "";  
//              } 
//              
//              
//              if(!empty(trim($row['G']))){
//                $cli['moblie'] = trim($row['G']);
//             }else{
//                $cli['mobile'] = "" ;  
//             } 
//             
//             $usrTlb->saveData('users', $cli);   
//              //now entry in vcard search table
//             $val['username'] = $userId;
//             $val['status']="";
//             if(!empty(trim($row['A']))){
//               $val['firstName'] = trim($row['A']);
//             }else{
//               $val['firstName'] = "";  
//             }
//              if(!empty(trim($row['B']))){
//                 $val['lastName'] = trim($row['B']);
//              }else{
//                 $val['lastName'] = "";  
//              } 
//              
//           if(!empty(trim($row['C']))){
//                 $val['designation'] = trim($row['C']);
//              }else{
//                 $val['designation'] = "";  
//              }  
//              
//             if(!empty(trim($row['D']))){
//                 $val['companyName'] = trim($row['D']);
//              }else{
//                 $val['companyName'] = "";  
//              }   
//              
//              if(!empty(trim($row['E']))){
//                 $val['city'] = trim($row['E']);
//              }else{
//                 $val['city'] = "";  
//              }  
//              
//              
//            if(!empty(trim($row['F']))){
//                 $val['email'] = trim($row['F']);
//              }else{
//                 $val['email'] = "";  
//              } 
//              
//           
//            if(!empty(trim($row['G']))){
//                $val['phone'] = trim($row['G']);
//             }else{
//                $val['phone'] = "" ;  
//             } 
//             
//             
//            if(!empty(trim($row['H']))){
//                $val['description'] = trim($row['H']);
//             }else{
//                $val['description'] = "" ;  
//             }   
//             
//              
////            if(!empty(trim($row['F']))){
////                $val['linkedinUrl'] = trim($row['F']);
////             }else{
////                $val['linkedinUrl'] = "" ;  
////             } 
////             if(!empty(trim($row['G']))){
////                $val['city'] = trim($row['G']);
////             }else{
////                $val['city'] = "" ;  
////             } 
//            
//            $val['countryCode'] =""; 
//            $val['country'] ="";         
////            $val['parentUserId'] = 0;
////            $val['total_participants'] = 0;  
//            $val['gender'] = "NA";
//            $val['region'] ="r1";
//            $val['guestSide'] ="Bride";
//           //code for admin userId
//            $getAdminUserId = $usrTlb->getDetail('users',array('username'),"active=1 and status=0 and testAccount='3' and adminType=2 order by id asc limit 1");
//            $val['invitedBy'] = $getAdminUserId[0]['username'];
//            
//            
//            $usrTlb->saveData('vcard_search', $val);
//            $usrTlb->saveData('userTravelDetail',array('travelType'=>'arrival','userId'=>$userId));
//           $usrTlb->saveData('userTravelDetail',array('travelType'=>'departure','userId'=>$userId));
//           $usrTlb->saveData('userDetail',array('userId'=>$userId));
//           $usrTlb->saveData('kycMasterDocList',array('userId'=>$userId,'docName'=>'id','docLabel'=>'Enter your Id card number','caption'=>'Valid ID Proof','createdOn'=>time(),'updatedOn'=>time()));
//           $usrTlb->saveData('kycMasterDocList',array('userId'=>$userId,'docName'=>'Travel','docLabel'=>'Enter Travel Detail','caption'=>'Travel','createdOn'=>time(),'updatedOn'=>time()));
//           }
//         } 
//         
//         
//         die('success');
//           
//      }else{
//                  $msg ="File not uploaded"   ;   
//            }     
//                        
//          }else{
//                      $msg ="Maximum file size should not cross 50 KB on size!";
//               }
//                  
//                  
//            } else{
//                   $msg ="This type of file is not allowed!";
//             }
//              
//        }else{
//              $msg ="Please select a file first"  ;
//         }  
////     }else{
////         $msg ="Something went wrong";
////     }      
//  }
      
      
      
      
  //END here    
      
      
      
      
    $view = new ViewModel(array());
    //$view->setTerminal(true);
     return $view;
  }
   public function userDetailAction(){
    $request = $this->getRequest();
    $data = $request->getPost()->toArray();
    $usrTlb = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $getGuestDetail = $usrTlb->getSpeakerMentorList('speaker',3,base64_decode($data['username']));
    $mediaUrl = $this->getMediaUrl();
    $view = new ViewModel(array(
            'userDetail' => $getGuestDetail[0],
            'action'=>$data['type'],
            'mediaUrl'=>$mediaUrl
     ));
    $view->setTerminal(true);
    return $view;
 }
 
 
 
 public function hallAction()
  {
    $pera = $this->params()->fromroute('id', NULL);
    $fbVerfiy = base64_decode($pera);
    if($fbVerfiy==""){
      $fbVerfiy = "1";  
    }
    $userTable = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $loginDetail = $this->getLoggedInUserId();
    $userDetail = $userTable->getUserDetail($loginDetail['userId']);
    $mediaUrl = $this->getMediaUrl();
      //code for agenda location
        $agendaLocation  = $userTable->getDetail('agendaLocation',array('location'),"status=1 order by trackOrder asc"); 
       //code for agenda keyword
       $agendaKeyWord  = $userTable->getDetail('agenda',array('keyWord'),"status=0 and testAccount='3' and keyWord!='' and itemType='agenda' group by keyWord order by keyWord asc"); 
        //code for agenda theme
       $agendaTheme  = $userTable->getDetail('agenda',array('theme'),"status=0 and testAccount='3' and theme!='' and itemType='agenda'  group by theme order by theme asc");
       $agendDate = $userTable->getAgendaDate();
       //$getPartnerDetail = $userTable->getAllPartner();  
       $peopleWalking  = $userTable->getDetail('videos',array('media'),"status=1 and sectionType='Exterior' ");
       $entryVideo  = $userTable->getDetail('videos',array('media'),"status=1 and sectionType ='Entry desk'");
       $lobby  = $userTable->getDetail('videos',array('media'),"status=1 and sectionType='Lobby' ");
       $auditorium  = $userTable->getDetail('videos',array('media'),"status=1 and sectionType='Auditorium' ");
       $networking  = $userTable->getDetail('videos',array('media'),"status=1 and sectionType='Networking' ");
       $experienceZone  = $userTable->getDetail('videos',array('media'),"status=1 and sectionType='experienceZone'");
       
       $meetingLinkForBooth1  = $userTable->getBoothDetail('1');
       $meetingLinkForBooth2  = $userTable->getBoothDetail('2');
       $meetingLinkForBooth3  = $userTable->getBoothDetail('3');
       $meetingLinkForBooth4  = $userTable->getBoothDetail('4');
       $meetingLinkForBooth5  = $userTable->getBoothDetail('5');
       
       $getBoothMedia= $userTable->getBoothAllDetail(); 
       $view = new ViewModel(array( 'getBoothMedia'=>$getBoothMedia,'meetingLinkForBooth1'=>$meetingLinkForBooth1, 'meetingLinkForBooth2'=>$meetingLinkForBooth2,'meetingLinkForBooth3'=>$meetingLinkForBooth3, 'meetingLinkForBooth4'=>$meetingLinkForBooth4,  'meetingLinkForBooth5'=>$meetingLinkForBooth5,'peopleWalking'=>$peopleWalking,'experienceZone'=>$experienceZone,'networking'=>$networking, 'auditorium'=>$auditorium, 'entryVideo'=>$entryVideo, 'lobby'=>$lobby, 'userDetail'=>$userDetail,'mediaUrl'=>$mediaUrl,'agendaLocation'=>$agendaLocation,'agendaKeyWord'=>$agendaKeyWord,'agendaTheme'=>$agendaTheme,'agendDate'=>$agendDate,'fbVerfiy'=>$fbVerfiy));
       $view->setTerminal(true);
       return $view;
  }
   //login action for employer
public function bookAgendaAction(){
    $request = $this->getRequest();
    $userTable = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $loginDetail = $this->getLoggedInUserId();
    $postData = $request->getPost()->toArray();
    $postData['userId'] = $loginDetail['userId'];
    $likeDislikeStatus = $userTable->checkFavforAgenda($postData);
    if(!empty($likeDislikeStatus)){
       $authArray = array('isLike'=>$postData['isLike'],'updatedOn'=>time(),'status'=>1);
       $isLikeDislike=  $userTable->updateData('agendaFav',$authArray, array('agendaId '=>$postData['agendaId'],'userId'=> $postData['userId']));                    
     }else{
       $authArray = array('agendaId'=>$postData['agendaId'],'userId'=>$postData['userId'],'isLike'=>$postData['isLike'],'createdOn'=> time(),'updatedOn'=> time());
       $isLikeDislike = $userTable->saveData('agendaFav',$authArray);
    }
    //new code for saving agenda in user suitcase
            if($postData['isLike']==1){
                //check agenda exist or not
               $checkAgenda = $userTable->getDetail('userSuitcase',array('id'),"itemId='".$postData['agendaId']."' and userId='".$postData['userId']."' and itemType='agenda'"); 
               if(empty($checkAgenda)){
                 $getAgendaDetail = $userTable->getDetail('agenda',"",'id='.$postData['agendaId']);
                 $userTable->saveData('userSuitcase',array('location'=>$getAgendaDetail[0]['location'],'startDate'=>$getAgendaDetail[0]['startDate'],'endDate'=>$getAgendaDetail[0]['endDate'],'createdOn'=>time(),'updatedOn'=>time(),'icon'=>$getAgendaDetail[0]['icon'],'title'=>$getAgendaDetail[0]['title'],'description'=>$getAgendaDetail[0]['description'],'timeDuration'=>$getAgendaDetail[0]['timeDuration'],'color'=>$getAgendaDetail[0]['color'],'keyWord'=>$getAgendaDetail[0]['keyWord'],'keyWordDesc'=>$getAgendaDetail[0]['keyWordDesc'],'itemId'=>$postData['agendaId'],'itemType'=>'agenda','userId'=>$postData['userId']));
               } 
            }else{
              //delete from user suitcase
              $userTable->deleteUserData('userSuitcase',array('itemId'=>$postData['agendaId'],'itemType'=>'agenda','userId'=>$postData['userId'])); 
            }
            if($postData['isLike']==1){
                  $message = "You have successfully favouritise the agenda";
              }else{
                  $message = "You have  unfavouritise the agenda"; 
              }
      $send = array(
               'message' => $message,
               'status'=>1  
             );
           return new JsonModel(array('send'=>$send));
}
public function userSuitcaseAction()
 {
    
       $userTable = $this->getServiceLocator()->get('Application\Model\UsersTable');
       $view = new ViewModel(array());
       return $view;
 }
 public function speakerDataListLoginAction(){
    $usrTlb = $this->getServiceLocator()->get('Application\Model\UsersTable');
//    $pera = $this->params()->fromroute('id', NULL);
    
//    $search= base64_decode($pera);
 
    $request = $this->getRequest();
    $data = $request->getPost()->toArray();

//    $data['search']=$search;
    //$loginDetail = $this->getLoggedInUserId();

    $data['action'] = "speaker";
    $paginator = $usrTlb->getUserFeedList($data,3,true);
    $results = $this->paginationToArray($paginator,$data['page'],8);
    //echo "<pre>"; print_r($results); die;
    $uri = $this->getRequest()->getUri();
    $mediaUrl = $this->getMediaUrl();
    $view = new ViewModel(array(
            'paginator' => $paginator,
            'uri' => $uri,
            'results' => $results,
            'mediaUrl'=>$mediaUrl
       ));
        $view->setTerminal(true);
        return $view; 
 }
 
 public function userDetailLoginAction(){
    $request = $this->getRequest();
    $data = $request->getPost()->toArray();
    $usrTlb = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $getGuestDetail = $usrTlb->getSpeakerMentorList($data['type'],3,base64_decode($data['username']));
    $mediaUrl = $this->getMediaUrl();
    $view = new ViewModel(array(
            'userDetail' => $getGuestDetail[0],
            'action'=>$data['type'],
            'mediaUrl'=>$mediaUrl
     ));
    $view->setTerminal(true);
    return $view;
 }
 
 
 public function chatAction(){
            $viewModel = new ViewModel(array());
            $viewModel->setTerminal(true);
            return $viewModel;
     } 



public function mentorDataListLoginAction(){
    $usrTlb = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $request = $this->getRequest();
    $data = $request->getPost()->toArray();
    //$loginDetail = $this->getLoggedInUserId();
//    $data['action'] = "investor";
    $data['action'] = "speaker";
    $paginator = $usrTlb->getUserFeedList($data,3,true);
    $results = $this->paginationToArray($paginator,$data['page'],8);
//    echo "<pre>"; print_r($results); die;
    $uri = $this->getRequest()->getUri();
    $mediaUrl = $this->getMediaUrl();
    $view = new ViewModel(array(
            'paginator' => $paginator,
            'uri' => $uri,
            'results' => $results,
            'mediaUrl'=>$mediaUrl
       ));
        $view->setTerminal(true);
        return $view; 
 }
 
 
 
 
public function attendeDataListLoginAction(){
    $usrTlb = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $request = $this->getRequest();
    $data = $request->getPost()->toArray();
    //$loginDetail = $this->getLoggedInUserId();
    $data['action'] = "attende";
    $paginator = $usrTlb->getUserFeedList($data,3,true);
    $results = $this->paginationToArray($paginator,$data['page'],8);
//    echo "<pre>"; print_r($results); die;
    $uri = $this->getRequest()->getUri();
    $mediaUrl = $this->getMediaUrl();
    $view = new ViewModel(array(
            'paginator' => $paginator,
            'uri' => $uri,
            'results' => $results,
            'mediaUrl'=>$mediaUrl
       ));
        $view->setTerminal(true);
        return $view; 
 }
 



public function agendaDataLoginAction(){
    $userTable = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $request = $this->getRequest();
    $loginDetail = $this->getLoggedInUserId();
    $postData = $request->getPost()->toArray();
    if(empty($loginDetail['userId'])){
       $postData['userId'] = "123456"; 
    }else{
       $postData['userId'] = $loginDetail['userId'];   
    }
    $checkAttendeType = $userTable->getDetail('users',array('attendeType','isFutureStick','isPaid'),"username='".$postData['userId']."'");
     //$checkGender= $userTable->getDetail('vcard_search',array('gender'),"username='".$postData['userId']."'");
    if(empty($checkAttendeType[0]['attendeType'])){
        $type ="all";
    }elseif($checkAttendeType[0]['attendeType']=="VIP"){
       $type = "vip";
    }elseif($checkAttendeType[0]['attendeType']=="HRPRAC"){
      $type = "prac";  
    }else{
      $type = "all";    
    }
    if($postData['type']==1){
     $getAgendaList = $userTable->listofmasteragendaNew($postData,$type,3); 
    }else{
     $getAgendaList = $userTable->getUserSuitCaseData($postData,$type,3);
    //$getAgendaList = $userTable->listofmasteragendaNew($postData,$type,3);     
    }
      foreach($getAgendaList as $ke1=>$agenda){
            $getAgendaList[$ke1]['strStartDate'] = date('l, M d, Y',$agenda['startDate']);
            $getAgendaList[$ke1]['waitingMessageColorCode'] ="#FF9900";
           if($agenda['itemType']=='agenda'){
             $checkLike = $userTable->checkAgendaFav($postData['userId'],$agenda['id']);
            if(empty($checkLike)){
               $getAgendaList[$ke1]['isLike'] ="0";
            }else if($checkLike==0){
               $getAgendaList[$ke1]['isLike'] ="0";  
            }else{
              $getAgendaList[$ke1]['isLike'] ="1";  
            }
           //code for speaker name
           $data['itemId'] = $agenda['id'];
           $data['itemType'] = 'agenda';
           $speakerName = $userTable->getSpekAgName($data);
           $getAgendaList[$ke1]['speakersName'] = $speakerName;
       }  
     }
     
     $dateArray = array();
          //grouping result by date
          foreach($getAgendaList as $key=>$value){
              $dateArray[date('l, M d, Y',$value['startDate'])][] = $value;
          } 
          $record = array();
          //group result by time duration
          foreach($dateArray as $key1=>$value2){
              $timeArray = array(); 
             foreach($value2 as $key2=>$value3){     
               $timeArray[$value3['timeDuration']][] = $value3;  
             }
             //$finalArray = array();
             $finalArraynew = array();
             foreach($timeArray as $key3=>$value4){
               $finalArray['timeDuration'] = $key3;
               $finalArray['list'] = $value4;
               $finalArraynew [] = $finalArray;
             }
             //get Title of day
             $title = $userTable->getDetail('agendaTitle',array('title'),"date='".$key1."'");
             $final['title'] = $title[0]['title'];
             $final['date'] = $key1;
             $final['strStartDate'] = $key1;
             $final['timeStamp'] = $finalArraynew[0]['list'][0]['startDate'];
             $final['result'] = $finalArraynew;
             $record[] = $final;
         }
        // echo "<pre>"; print_r($record); die;
    $uri = $this->getRequest()->getUri();
    $mediaUrl = $this->getMediaUrl();
    $view = new ViewModel(array(
            'uri' => $uri,
            'record' => $record,
            'mediaUrl'=>$mediaUrl,
            'userId'=>$loginDetail['userId']
       ));
    $view->setTerminal(true);
    return $view;  
 }
 public function editUserAction()
 {
      $request = $this->getRequest();
      $postData = $request->getPost()->toArray();
      $userTable = $this->getServiceLocator()->get('Application\Model\UsersTable');
      $loginDetail = $this->getLoggedInUserId();
      $userDetail = $userTable->getUserDetailonEdit($loginDetail['userId']);
      $userskill = $userTable->getDetail('user_skills',array('skill_id'),"status=0 and user_id='".$loginDetail['userId']."'");
      $userskills = array();
      foreach($userskill as $skill){
         $userskills[] = $skill['skill_id']; 
      }
      $skills = $userTable->getDetail('skills',array('name','id'),"status='0'");
      $countryList = $userTable->getDetail('country',array('nicename','phonecode'),"phonecode!=0 and phonecode!='91'");
      $mediaUrl = $this->getMediaUrl();
      $view = new ViewModel(array('userDetail'=>$userDetail[0],'userskills'=>$userskills,'skills'=>$skills,'countryList'=>$countryList,'mediaUrl'=>$mediaUrl,'edituser'=>$postData['agendaId']));
      $view->setTerminal(true);
      return $view;
 }
 public function editDetailAction(){
 try{       
   $request = $this->getRequest();
    $usrTlb = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $loginDetail = $this->getLoggedInUserId();
// if($request->isPost()) {
    $postData = $request->getPost()->toArray();
    $files = $request->getFiles()->toArray();
    //echo "<pre>"; print_r($postData); echo "<pre>"; print_r($files); die;
    $vcardData=array();
    $cli= array();
    $passUtil = new UserPassword();
    $usrPass = $passUtil->create($postData['password']);   
//    if(!empty($postData['email'])){
//      $cli['email'] = $postData['email'];
//    }else{
//     $cli['email'] = "";   
//    }
    $cli['attendeType'] = $postData['userType'];    
//    if(!empty($postData['countryCode'])){
//      $cli['countryCode'] = $postData['countryCode'];
//    }else{
//     $cli['countryCode'] = "";   
//    }
//    if(!empty($postData['mobile'])){
//      $cli['mobile'] = $postData['mobile'];
//    }else{
//     $cli['mobile'] = "";   
//    }
    $cli['timestamp']=time();
    $cli['uts'] = time();
    if(!empty($postData['password'])){
     $cli['eventapPassword'] =$usrPass;
     $cli['eventapPasswordString'] = $postData['password'];
    }
    $usrTlb->updateData('users',$cli,array('username'=>$loginDetail['userId']));
    //$usrTlb->saveData('qrcodeSeries',array('userId'=>$userId,'lastValue'=>$qrCode,'createdOn'=> time(),'updatedOn'=> time()));
    //$vcardData['username'] =   $userId;
    //$vcardData['phone'] =     $cli['mobile'];
    
    $vcardData['firstName'] = $postData['fname'];
    if(!empty($postData['lname'])){
     $vcardData['lastName'] = $postData['lname'];
    }else{
     $vcardData['lastName'] = "";   
    }
//    if(!empty($postData['email'])){
//     $vcardData['email'] = $postData['email'];
//    }else{
//     $vcardData['email'] = "";   
//    }
    if(!empty($postData['gender'])){
     $vcardData['gender'] =$postData['gender'];
    }else{
     $vcardData['gender'] = "";   
    }
   // $vcardData['countryCode'] =$cli['countryCode'];
    if(!empty($postData['companyName'])){
     $vcardData['companyName'] =  $postData['companyName'];
    }else{
     $vcardData['companyName'] = "";   
    }
    if(!empty($postData['designation'])){
      $vcardData['designation'] =  $postData['designation'];
    }else{
      $vcardData['designation'] = "";   
    }
   if(!empty($postData['companyNameOnBadge'])){
      $vcardData['companyNameOnBadge'] =  $postData['companyNameOnBadge'];
    }else{
      $vcardData['companyNameOnBadge'] = "";   
    }
    
     if(!empty($postData['city'])){
      $vcardData['city'] =  $postData['city'];
    }else{
      $vcardData['city'] = "";   
    } 
    
    if(!empty($postData['role'])){
      $vcardData['description'] =  $postData['role'];
    }else{
      $vcardData['description'] = "";   
    }
    if(!empty($postData['country'])){
      $vcardData['country'] =  $postData['country'];
    }else{
      $vcardData['country'] = "";   
    }
    $usrTlb->updateData('vcard_search',$vcardData,array('username'=>$loginDetail['userId']));
    if (isset($postData['areaOfInterest']) && !empty($postData['areaOfInterest'])) {
        $usrTlb->deleteUserData('user_skills',array('user_id'=>$loginDetail['userId']));
        $skillArray = $postData['areaOfInterest'];
               foreach($skillArray as $key=>$val){
                    $arr = array('user_id' => $loginDetail['userId'] , 'skill_id' => $val['id'] , 'updated_by' => $loginDetail['userId'] , 'created_on' => time(), 'updated_on'=>time() );
                    $usrTlb->saveData('user_skills',$arr);
               }
     //call the procedure
     $usrTlb->callSetUserMatchedJobProcedure($loginDetail['userId'],3);          
    }  
    
    if(!empty($files['attachment'][0]['name'])){ 
          $mime = $files['attachment'][0]['type'];
          if(strstr($mime, "image/")){
            $image = "profile/".$loginDetail['userId'].'.png';
            $this->putIntoS3Bucket($files['attachment'][0]['tmp_name'],$image,1);
            $usrTlb->updateData('users',array('lastAvtarUpdate'=> time()),array('username'=>$loginDetail['userId']));
         }  
    }
    $send = array(
               'message' =>"User info updated Successfully",
               'status'=>1,
               //'userId'=> base64_encode($userId)
              );
     return new JsonModel(array('send'=>$send));
    
}catch(\Exception $e){
  $send = array(
               'message' =>$e->getMessage(),
               'status'=>0  
              );
    return new JsonModel(array('send'=>$send));
}
$send = array(
               'message' =>"Something went wrong",
               'status'=>0  
              );
    return new JsonModel(array('send'=>$send));
}
//method for imatch
public function imatchAction(){
    $request = $this->getRequest();
    //$data = $request->getPost()->toArray();
    $userTable = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $loginDetail = $this->getLoggedInUserId();
    $postData['userId'] = $loginDetail['userId']; 
    $getTotalLikes = $userTable->getImacthList($postData, false);
    foreach($getTotalLikes as $key=>$value){
          $skillList = $userTable->getUserJobSkillData($value['userId'],$postData['userId']);
          $getTotalLikes[$key]['skills'] = $skillList;
    }
    $totalCounter = count($getTotalLikes);
    $mediaUrl = $this->getMediaUrl();
    $view = new ViewModel(array(
            'imatch' => $getTotalLikes,
            'mediaUrl'=>$mediaUrl,
            'totalCounter'=>$totalCounter
    ));
    $view->setTerminal(true);
    return $view;     
}
//code for request meeting model popup
//method for imatch
public function requestMeetingAction(){
    $request = $this->getRequest();
    $data = $request->getPost()->toArray();
    $userTable = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $userName  = $userTable->getDetail('vcard_search',array("firstName","lastName"),"username='".$data['imatchUserId']."'");
    $view = new ViewModel(array(
            'name' => $userName[0]['firstName']." ".$userName[0]['lastName']
    ));
    $view->setTerminal(true);
    return $view;     
}
//method for add meeting
public function addMeetingAction(){
    $request = $this->getRequest();
    $userTable = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $loginDetail = $this->getLoggedInUserId();
    $postData = $request->getPost()->toArray();
    $postData['userId'] = $loginDetail['userId'];
    $stringTrimFilter = new StringTrim();
    $stripTagFilter = new StripTags();
    if(isset($postData['description']) && !empty($postData['description'])) {
            $enterData['description'] = $stripTagFilter->filter(
                $stringTrimFilter->filter($postData['description']));
    }
    if (isset($postData['location']) && !empty($postData['location'])) {
            $enterData['location'] = $stripTagFilter->filter(
                $stringTrimFilter->filter($postData['location']));
    }else{
          $enterData['location']="";  
    }
    if (isset($postData['title']) && !empty($postData['title'])) {
        $enterData['title'] = $stripTagFilter->filter(
                $stringTrimFilter->filter($postData['title']));
    }else{
       $enterData['title']="";  
    }
    $enterData['userId'] =  $postData['userId'];
    $enterData['dateTime'] = $this->timzeconvertion($postData['dateTime']);
    $enterData['createdOn']=time();
    $enterData['updatedOn']=time();
    $enterData['testAccount'] = 3;
    $enterData['status'] =1;
    $meetingId = $userTable->saveData('meeting',$enterData);
    $arr = array('meetingId' => $meetingId , 'userId' => $postData['imatchUserId'], 'createdBy'=> $postData['userId'], 'createdOn' => time(),'updatedOn'=>time(),'status'=>0 );
    $userTable->saveData('meetingAttendies',$arr);
    //now code for push meeting
    //method for sending post to tag users
    $where3 = "meetingId='".$meetingId."'";
    $getTagUserList = $userTable->getDetail('meetingAttendies',array('userId'),$where3);
    $creatorDetail = $userTable->getDeviceDetailNew($getTagUserList[0]['userId']);
    if(!empty($getTagUserList)){ 
            $where4 = "username='".$postData['userId']."'"; 
            $getUserName = $userTable->getDetail('vcard_search',array('firstName','lastName'),$where4);
            $text = $getUserName[0]['firstName']." "."invited you for meeting. Please check.";
            $username = substr(time().ceil((rand(0,10000))),0, 5).$this->getUserId(6);
            $interData['title']="";
            $interData['updatedon']=time();
            $interData['message']= $text;
            $interData['notificationDataId'] = 0;
            $interData['notifyId']= $username;
            $interData['creatorId']= $postData['userId'];
            $interData['eventId'] =  $meetingId;
            $interData['type']="addMeeting";
            $body1 = $interData;
            $body1['nickname'] = $getUserName[0]['firstName'];
            $body1['userId'] = $postData['userId'];
            $body1['eventName'] = "";
            $body1['bodyType'] = "addMeeting";
            $body1['notificationMedia'] = array();
            $body1['chatAllowed'] = "";
            $body1['media'] = "";
            $body1['link'] = "";
            $body1['uiType'] = "";
            $body = json_encode($body1,true);
            $interData['body']=$body;
            $interData['username'] = $getTagUserList[0]['userId'];
            $userTable->saveData('notification',$interData);
            if(!empty($creatorDetail[0]['device_token'])){
            //first check android or ios
           if($creatorDetail[0]['device_type']=='android'){
                $pushData["deviceId"] = $creatorDetail[0]["device_token"];
                $pushData["message"] = $body;
                $pushData["pType"] = 'app';
                $pushData["subject"] ='FCM push'; 
                $this->sendAndroidPush($pushData,$creatorDetail[0]['appType']);  
           }else{  
           //now make json to send users
          //now code to get total counter of user
          $userCounter = $userTable->getDetail('userNotifictionCount',array('totalApp','totalChat','timelineCount'),"userId='".$creatorDetail[0]['userId']."' and testAccount='3'");
          if(!empty($userCounter)){
             $userTable->updateData('userNotifictionCount',array('totalApp'=>$userCounter[0]['totalApp']+1),array('userId'=>$creatorDetail[0]['userId'],'testAccount'=>3)); 
          }else{
             $userTable->saveData('userNotifictionCount',array('totalApp'=>1,'userId'=>$creatorDetail[0]['userId'],'testAccount'=>3));  
          }
          //now get user badge Count
          $userBadgeCount = $userTable->getDetail('userNotifictionCount',array('totalApp','totalChat','timelineCount'),"userId='".$creatorDetail[0]['userId']."' and testAccount='3'");
          //now make json to send users
          $message['badge'] = (int)($userBadgeCount[0]['totalApp']+ $userBadgeCount[0]['totalChat']+ $userBadgeCount[0]['timelineCount']);     
          
            $message['mutable-content'] =1;
            $message['sound'] ="default";
            $message['chatId'] = "";
            $message['fromUser'] ="";
            $message['toUser'] = "";
            $message['alert'] = array("title"=>"","body"=>$interData['message']);
            $message['bodyType'] = $interData['type'];
            $message['sender'] = "";
            $message['message']= $body;
            $message['timestamp'] = time();
            $payload['aps'] = $message;
            $payload['c'] = 1;
            $message1= "success";  
            $this->sendapnspush($payload,$message1,$creatorDetail[0]["apnsToken"],'dev',$creatorDetail[0]["appType"],'alert'); 
            $this->sendapnspush($payload,$message1,$creatorDetail[0]["apnsToken"],'dist',$creatorDetail[0]["appType"],'alert');
         }
         
        }
        //code for send email invitaion
       //code for email
        if(!empty( $creatorDetail[0]["email"])){
        $config = $this->getServiceLocator()->get('config');
        $url = $config['settings']['quickPole']."meeting-response/". base64_encode($postData['imatchUserId']."-".$meetingId);
        $uri = $this->getRequest()->getUri();
        $baseUrl = $uri->getHost();
        if(strstr( $baseUrl,'expoodle')){
        $mail['mailFromNickName'] = "Expoodle";
        //$mail['mailTo'] = "bhupendra@approutes.com";
        $mail['mailTo'] = $creatorDetail[0]["email"];
        $mail['mailSubject'] = "Expoodle | Meeting Request";
        $mail['mailBody'] = "<html><body>Hi ".$creatorDetail[0]["firstName"].",<br>".$getUserName[0]['firstName']." ".$getUserName[0]['lastName']." requested you for a meeting.<br>Here are Meeting Details: <br>Title: ".$enterData['title']." <br>Date&Time: ".$postData['dateTime']."<br><br>Please click on below link for Accept or Reject this meeting request: <a href='".$url."'>Click Here</a> <br>Thanks & Regards<br>Expoodle Team</body></html>";
        $this->sendotpemail($mail);   
       }else{
        $mail['mailFromNickName'] = "EvenTap";
        //$mail['mailTo'] = "bhupendra@approutes.com";
        $mail['mailTo'] = $creatorDetail[0]["email"];
        $mail['mailSubject'] = "EvenTap | Meeting Request";
        $mail['mailBody'] = "<html><body>Hi ".$creatorDetail[0]["firstName"].",<br>".$getUserName[0]['firstName']." ".$getUserName[0]['lastName']." requested you for a meeting.<br>Here are Meeting Details: <br>Title: ".$enterData['title']." <br>Date&Time: ".$postData['dateTime']."<br><br>Please click on below link for Accept or Reject this meeting request: <a href='".$url."'>Click Here</a> <br>Thanks & Regards<br>EvenTap Team</body></html>";
        $this->sendotpemail($mail);
       } 
        }       
      }
       //now code for imatch
       //$userTable->updateData('user_match',array('isMatch'=>1,'updatedOn'=> time()),array('user_id'=>$postData['userId'],'job_creator_id'=>$postData['imatchUserId'])); 
       $send = array(
               'message' => "Your Meeting Request has been sent, we will notify you as soon as request is accepted. You can check meeting request status in My Meetings under My Profile.",
               'status'=>1  
        );
       return new JsonModel(array('send'=>$send));
}
public function mymeetingDataListLoginAction(){
    $usrTlb = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $request = $this->getRequest();
    $data = $request->getPost()->toArray();
    $loginDetail = $this->getLoggedInUserId();
    $data['userId'] = $loginDetail['userId'];
    $paginator = $usrTlb->getListOfMeetings($data,true);
    $results = $this->paginationToArray($paginator,$data['page'],8);
    foreach($results['allData'] as $key=>$val){
                $results['allData'][$key]['userTagList']  = $usrTlb->listOfMeetingAttende($val['meetingId']);
                //code for sending opposite person profile
                if($data['userId'] == $val['userId']){
                  $results['allData'][$key]['userId'] = $results['allData'][$key]['userTagList'][0]['userId'];
                  $results['allData'][$key]['firstName'] = $results['allData'][$key]['userTagList'][0]['firstName'];
                }else{
                  $results['allData'][$key]['userId'] = $val['userId'];
                  $results['allData'][$key]['firstName'] = $val['firstName'];
               }
       $results['allData'][$key]['dateTime']   = $this->getUserTimeZone(date("Y-m-d H:i:s",$results['allData'][$key]['dateTime']),"Asia/Kolkata",1);        
    }
    $uri = $this->getRequest()->getUri();
    $mediaUrl = $this->getMediaUrl();
    $view = new ViewModel(array(
            'paginator' => $paginator,
            'uri' => $uri,
            'results' => $results,
            'mediaUrl'=>$mediaUrl
       ));
    $view->setTerminal(true);
    return $view; 
 }
 //method for meeting request accept or reject
public function meetingDetailAction(){
    $request = $this->getRequest();
    $postData = $request->getPost()->toArray();
    $postData['meetingId'] = base64_decode($postData['meetingId']);
   // $loginDetail = $this->getLoggedInUserId();
    $usrTlb = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $myLastEvents = $usrTlb->getLastMeeting($postData['meetingId']);
    $myLastEvents[0]['userTagList']  = $usrTlb->listOfMeetingAttende($postData['meetingId']);
    $myLastEvents[0]['dateTime']   = $this->getUserTimeZone(date("Y-m-d H:i:s",$myLastEvents[0]['dateTime']),"Asia/Kolkata",1);
   // echo "<pre>"; print_r($myLastEvents); die;
    $mediaUrl = $this->getMediaUrl();
    $view = new ViewModel(array(
            'myLastEvents' => $myLastEvents[0],
            'mediaUrl'=>$mediaUrl
    ));
    $view->setTerminal(true);
    return $view;
}



public function auditoriumBannerAction(){
    $usrTlb = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $request = $this->getRequest();
    $data = $request->getPost()->toArray();
    //$loginDetail = $this->getLoggedInUserId();
//    $data['action'] = "attende";
//    $paginator = $usrTlb->getUserFeedList($data,3,true);
//    $results = $this->paginationToArray($paginator,$data['page'],8);
//    echo "<pre>"; print_r($results); die;
    $uri = $this->getRequest()->getUri();
    $mediaUrl = $this->getMediaUrl();
    $view = new ViewModel(array(
            'paginator' => $paginator,
            'uri' => $uri,
//            'results' => $results,
//            'mediaUrl'=>$mediaUrl
       ));
        $view->setTerminal(true);
        return $view; 
 }
 public function meetingAction(){
  try { 
     $pera = $this->params()->fromroute('id', NULL);
     $agendaId = base64_decode($pera);
     $userTable = $this->getServiceLocator()->get('Application\Model\UsersTable');
     $agendaDetail = $userTable->getDetail('agenda',array('meetingUrl','userName','meetingNumber','passWord','meetingType'),"id='".$agendaId."'");
     $loginDetail = $this->getLoggedInUserId();
     $userDetail = $userTable->getUserDetail($loginDetail['userId']);
     if($userDetail[0]['meetingType']=="clickMeeting"){
     $client = new ClickMeetingRestClient(array('api_key' => 'us89783b3b4d5774997f96883be1aefb805c81fb09'));
     $room_id = $agendaDetail[0]['meetingNumber'];
     $clientHost = $client->conferenceAutologinHash($room_id, array(
        'email' => $userDetail[0]['email'],
        'nickname' => $userDetail[0]['firstName'],
        'role' => 'listener'
    ));
     $url = $agendaDetail[0]['meetingUrl'].'?l='.$clientHost->autologin_hash;
     return $this->redirect()->toUrl($url); 
    }else{
     $url = $agendaDetail[0]['meetingUrl'];
     return $this->redirect()->toUrl($url);     
    }
    
   }
  catch (\Exception $e)
  {
    //print_r(json_decode($e->getMessage()));
    die("Something went wrong");
  }
     //$viewModel = new ViewModel(array('agendaDetail'=>$agendaDetail[0]));
     //$viewModel->setTerminal(true);
     //return $viewModel;
 }
public function leaveMeetingAction(){
        $viewModel = new ViewModel(array());
        $viewModel->setTerminal(true);
        return $viewModel;
 }
 public function clientHallAction()
  {
    $pera = $this->params()->fromroute('id', NULL);
    $fbVerfiy = base64_decode($pera);
    if($fbVerfiy==""){
      $fbVerfiy = "1";  
    }
    $userTable = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $loginDetail = $this->getLoggedInUserId();
    $userDetail = $userTable->getUserDetail($loginDetail['userId']);
    $mediaUrl = $this->getMediaUrl();
      //code for agenda location
    $agendaLocation  = $userTable->getDetail('agendaLocation',array('location'),"status=1 order by trackOrder asc"); 
       //code for agenda keyword
       $agendaKeyWord  = $userTable->getDetail('agenda',array('keyWord'),"status=0 and testAccount='3' and keyWord!='' and itemType='agenda' group by keyWord order by keyWord asc"); 
        //code for agenda theme
       $agendaTheme  = $userTable->getDetail('agenda',array('theme'),"status=0 and testAccount='3' and theme!='' and itemType='agenda'  group by theme order by theme asc");
       $agendDate = $userTable->getAgendaDate();
       
     $getPartnerDetail = $userTable->getAllPartner();  
     
        $peopleWalking  = $userTable->getDetail('videos',array('media'),"status=1 and sectionType='Exterior' ");
        $entryVideo  = $userTable->getDetail('videos',array('media'),"status=1 and sectionType ='Entry desk'");
        $lobby  = $userTable->getDetail('videos',array('media'),"status=1 and sectionType='Lobby' ");
        $auditorium  = $userTable->getDetail('videos',array('media'),"status=1 and sectionType='Auditorium' ");
        $networking  = $userTable->getDetail('videos',array('media'),"status=1 and sectionType='Networking' ");
        
//        echo '<pre>'; print_r($peopleWalking);
      
    $view = new ViewModel(array('peopleWalking'=>$peopleWalking,  'networking'=>$networking, 'auditorium'=>$auditorium, 'entryVideo'=>$entryVideo, 'lobby'=>$lobby, 'userDetail'=>$userDetail,'mediaUrl'=>$mediaUrl,'agendaLocation'=>$agendaLocation,'agendaKeyWord'=>$agendaKeyWord,'agendaTheme'=>$agendaTheme,'agendDate'=>$agendDate,"fbVerfiy"=>$fbVerfiy));
    $view->setTerminal(true);
    return $view;
  }
  
  
  public function showQuickPollAction(){
        $viewModel = new ViewModel(array());
        $viewModel->setTerminal(true);
        return $viewModel;
 }
 
 
 
 public function quickPollQrAction(){ 
    $loginDetail = $this->getLoggedInUserId();
    $userTable = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $request = $this->getRequest();
    $postData = $request->getPost()->toArray();
    $checkQuickPoll = $userTable->getDetail('quickPole',array('id'),"qrCode='".$postData['qrCode']."' and status=1 ");
    if(!empty($checkQuickPoll)){
       $id=base64_encode($loginDetail['userId'].'-'.$checkQuickPoll[0]['id']);
       $send = array(
               'message' => "",
               'status'=>1 ,
               'id'=>$id,
               'quickPollDetail'=>$quickPollDetail
        );
       return new JsonModel(array('send'=>$send));
    }else{
     $send = array(
               'message' => "Wrong QuickPoll",
               'status'=>0  
        );
       return new JsonModel(array('send'=>$send)); 
    }     
  }
  
   public function quickPollAction(){
   $request = $this->getRequest();
   $postData = $request->getPost()->toArray();    
     $userTable = $this->getServiceLocator()->get('Application\Model\UsersTable');
//      
     $quickPollArr= base64_decode($postData['idArr']); 
     $explodeArr= explode("-", $quickPollArr);
     
     $userId=$explodeArr[0];
     $quickPollId=$explodeArr[1];
     $quickPollDetail = $userTable->getDetail('quickPole','','id='.$quickPollId);
//       echo '<pre>'; print_r($quickPollDetail);die;
      $view = new ViewModel(array(
                                'quickPollDetail'=>$quickPollDetail,
                                 'quickPollId'=>$quickPollId
                                ));
        $view->setTerminal(true);
        return $view;  
          
   
    }   
    
    
 
public function useranswerAction(){  
    $usrTlb = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $request = $this->getRequest();
    $loginDetail = $this->getLoggedInUserId();
//      echo '<pre>'; print_r($loginDetail); die(' kjk');
    $data = $request->getPost()->toArray();
//    $decryptedArr= base64_decode($data['id']);
//    $explodeArr= explode("-", $decryptedArr);
    $quickPollId=$data['id'];
  
    $checkAnswer = $usrTlb->getDetail('userQuickPoleResult',array('id'),"userId='".$userId."' and questionId=".$quickPollId);
    
    if(!empty($checkAnswer)){
     $usrTlb->updateData('userQuickPoleResult',array('answer'=>$data['useranswer']),array('userId'=>$loginDetail['userId'],'questionId'=>$quickPollId));
    // $usrTlb->updateData('assignQuickPoll',array('status'=>0),array('userId'=>$data['userId'],'questionId'=>$data['questionId']));
    }else{
     $usrTlb->saveData('userQuickPoleResult',array('userId'=>$loginDetail['userId'],'questionId'=>$quickPollId,'answer'=>$data['useranswer'],'createdOn'=> time(),'updatedOn'=> time(),'status'=>1));   
    }
    echo "success"; exit;
 }   
 
 
 
  //method for quick poll
public function quickpollgraphAction(){
    $request = $this->getRequest();
    $data = $request->getPost()->toArray();
    $quickPollId =$data['id'];
//    $pera = $this->params()->fromroute('id', NULL);
    $usrTlb = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $quickPollDetail = $usrTlb->getDetail('quickPole','','id='.$quickPollId);
//    echo "<pre>"; print_r($quickPollDetail); die;
    $option1 =$usrTlb->getProfileCounter('userQuickPoleResult',"questionId=".$quickPollId." and answer='option1' and recordStatus=1",0,0);
    $option2 = $usrTlb->getProfileCounter('userQuickPoleResult',"questionId=".$quickPollId." and answer='option2' and recordStatus=1",0,0);
    $option3 =$usrTlb->getProfileCounter('userQuickPoleResult',"questionId=".$quickPollId." and answer='option3' and recordStatus=1",0,0);
    $option4 = $usrTlb->getProfileCounter('userQuickPoleResult',"questionId=".$quickPollId." and answer='option4' and recordStatus=1",0,0);
    $option5 =$usrTlb->getProfileCounter('userQuickPoleResult',"questionId=".$quickPollId." and answer='option5' and recordStatus=1",0,0);
    $option6 = $usrTlb->getProfileCounter('userQuickPoleResult',"questionId=".$quickPollId." and answer='option6' and recordStatus=1",0,0);
    $view = new ViewModel(array('pera'=>$quickPollId,'option1'=>$option1,'option2'=>$option2,'option3'=>$option3,'option4'=>$option4,'option5'=>$option5,'option6'=>$option6,'quickPollDetail'=>$quickPollDetail,'chartType'=>$data['chartType']));
    $view->setTerminal(true);
    return $view;
}

//method for append the graph :-
 public function quickpolltimeoutAction(){
    $request = $this->getRequest();
    $data = $request->getPost()->toArray();
//    $idArray = explode("-",base64_decode($data['pera']));
    $quickPollId = $data['pera'];
//    echo '<pre>'; print_r($quickPollId);die(' jjk');
    $usrTlb = $this->getServiceLocator()->get('Application\Model\UsersTable');
 //   $quickPollDetail = $usrTlb->getDetail('quickPole','','id='.$quickPollId);
    $option1 =$usrTlb->getProfileCounter('userQuickPoleResult',"questionId=".$quickPollId." and answer='option1' and recordStatus=1",0,0);
    $option2 = $usrTlb->getProfileCounter('userQuickPoleResult',"questionId=".$quickPollId." and answer='option2' and recordStatus=1",0,0);
    $option3 =$usrTlb->getProfileCounter('userQuickPoleResult',"questionId=".$quickPollId." and answer='option3' and recordStatus=1",0,0);
    $option4 = $usrTlb->getProfileCounter('userQuickPoleResult',"questionId=".$quickPollId." and answer='option4' and recordStatus=1",0,0);
   // $option5 =$usrTlb->getProfileCounter('userQuickPoleResult',"questionId=".$quickPollId." and answer='option5' and recordStatus=1",0,0);
   // $option6 = $usrTlb->getProfileCounter('userQuickPoleResult',"questionId=".$quickPollId." and answer='option6' and recordStatus=1",0,0);
    return new JsonModel(array('option1'=>(int)$option1,'option2'=>(int)$option2,'option3'=>(int)$option3,'option4'=>(int)$option4));
 }
 
 
 //Check session feedback exsist or not :-
  public function checkSessionIsExsistAction(){ 
    $loginDetail = $this->getLoggedInUserId();
    $userTable = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $request = $this->getRequest();
    $postData = $request->getPost()->toArray();
    $checkQuickPoll = $userTable->getDetail('agenda',array('id'),"id='".$postData['itemId']."' and status=0 ");
//   echo '<pre>'; print_r($checkQuickPoll); die(' jjk');
    if(!empty($checkQuickPoll)){
       $id=base64_encode($postData['itemId'].'-'.$postData['itemType']);
       $send = array(
               'message' => "",
               'status'=>1 ,
               'id'=>$id,
        );
       return new JsonModel(array('send'=>$send));
    }else{
     $send = array(
               'message' => "There is no session available",
               'status'=>0  
        );
       return new JsonModel(array('send'=>$send)); 
    }     
  }
 
 
 //method for user session feedback
public function usersessionfeedbackAction(){
     $userTable = $this->getServiceLocator()->get('Application\Model\UsersTable');
      $loginDetail = $this->getLoggedInUserId();

     $request = $this->getRequest();
     $idArr = $request->getPost()->toArray();
      
      $decodedArr= base64_decode($idArr['idArr']);
      $explodeArr= explode("-", $decodedArr);
     
      
      $getRequest['itemId']=$explodeArr[0];
      $getRequest['itemType']=$explodeArr[1];
      
//    echo '<pre>'; print_r($$getRequest); die(' ol');
    if($getRequest['itemType']=="agenda"){
      $agendaDetail = $userTable->getDetail('agenda','','id='.$getRequest['itemId']);
      $speakerList = $userTable->getSpeakerListForSessionFeedback($getRequest);
    }else{
     $agendaDetail = $userTable->getDetail('agenda','','id='.$getRequest['itemId']);
     $speakerList = $userTable->getSpeakerListForSessionFeedback($getRequest);
    }
 
     
    $view = new ViewModel(array('agendaDetail'=>$agendaDetail,'speakerList'=>$speakerList,'getRequest'=>$getRequest,'itemType'=>$getRequest['itemType'],'idArr'=>$idArr['idArr'],'userId'=>$loginDetail['userId']));
    $view->setTerminal(true);
    return $view;
}
    
 
  
 public function saveSessionFeedbackAction(){
     $userTable = $this->getServiceLocator()->get('Application\Model\UsersTable');
     $loginDetail = $this->getLoggedInUserId();
     $request = $this->getRequest();
     
    
     if($request->isPost()){
     $postData = $request->getPost()->toArray();
//     echo '<pre>'; print_r($postData); die(' okl');
     
     $getRequest['itemType']="agenda";
     $getRequest['itemId']=$postData['itemId'];
     
     if($getRequest['itemType']=="agenda"){
       if(!empty($postData['agenda'])){
           $userTable->saveData('sessionRating',array('itemId'=>$getRequest['itemId'],'itemType'=>$getRequest['itemType'],'userId'=>$loginDetail['userId'],'rating'=>$postData['agenda'],'createdOn'=> time(),'updatedOn'=>time()));
       }  
     foreach($postData['rate'] as $key=>$val){
         $userTable->saveData('sessionFeedBack',array('itemId'=>$getRequest['itemId'],'itemType'=>$getRequest['itemType'],'userId'=>$loginDetail['userId'],'rating'=>$val,'speakerUserId'=>$key,'createdOn'=> time(),'updatedOn'=>time()));
     }
     }else{
        if(!empty($postData['agenda'])){
           $userTable->saveData('sessionRating',array('itemId'=>$getRequest['itemId'],'itemType'=>$getRequest['itemType'],'userId'=>$loginDetail['userId'],'rating'=>$postData['agenda'],'createdOn'=> time(),'updatedOn'=>time()));
       }  
         
     foreach($postData['rate'] as $key=>$val){
         $userTable->saveData('sessionFeedBack',array('itemId'=>$getRequest['itemId'],'itemType'=>$getRequest['itemType'],'userId'=>$loginDetail['userId'],'rating'=>$val,'speakerUserId'=>$key,'createdOn'=> time(),'updatedOn'=>time()));
     }    
   } 
   
     }
      
     
     $send = array(
               'status'=>1  
           );
       return new JsonModel(array('send'=>$send)); 
}
public function agendaDataAuditoriumAction(){
    $userTable = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $request = $this->getRequest();
    $loginDetail = $this->getLoggedInUserId();
    $postData = $request->getPost()->toArray();
    //echo "<pre>"; print_r($postData); die;
    if(empty($loginDetail['userId'])){
       $postData['userId'] = "123456"; 
    }else{
       $postData['userId'] = $loginDetail['userId'];   
    }
    $checkAttendeType = $userTable->getDetail('users',array('attendeType','isFutureStick','isPaid'),"username='".$postData['userId']."'");
    if(empty($checkAttendeType[0]['attendeType'])){
        $type ="all";
    }elseif($checkAttendeType[0]['attendeType']=="VIP"){
       $type = "vip";
    }elseif($checkAttendeType[0]['attendeType']=="HRPRAC"){
      $type = "prac";  
    }else{
      $type = "all";    
    }
     $getAgendaList = $userTable->listofmasteragendaNew($postData,$type,3); 
      foreach($getAgendaList as $ke1=>$agenda){
            $getAgendaList[$ke1]['strStartDate'] = date('l, M d, Y',$agenda['startDate']);
            $getAgendaList[$ke1]['waitingMessageColorCode'] ="#FF9900";
           if($agenda['itemType']=='agenda'){
             $checkLike = $userTable->checkAgendaFav($postData['userId'],$agenda['id']);
            if(empty($checkLike)){
               $getAgendaList[$ke1]['isLike'] ="0";
            }else if($checkLike==0){
               $getAgendaList[$ke1]['isLike'] ="0";  
            }else{
              $getAgendaList[$ke1]['isLike'] ="1";  
            }
           //code for speaker name
           $data['itemId'] = $agenda['id'];
           $data['itemType'] = 'agenda';
           $speakerName = $userTable->getSpekAgName($data);
           $getAgendaList[$ke1]['speakersName'] = $speakerName;
       }  
     }
     
     $dateArray = array();
          //grouping result by date
          foreach($getAgendaList as $key=>$value){
              $dateArray[date('l, M d, Y',$value['startDate'])][] = $value;
          } 
          $record = array();
          //group result by time duration
          foreach($dateArray as $key1=>$value2){
              $timeArray = array(); 
             foreach($value2 as $key2=>$value3){     
               $timeArray[$value3['timeDuration']][] = $value3;  
             }
             //$finalArray = array();
             $finalArraynew = array();
             foreach($timeArray as $key3=>$value4){
               $finalArray['timeDuration'] = $key3;
               $finalArray['list'] = $value4;
               $finalArraynew [] = $finalArray;
             }
             //get Title of day
             $title = $userTable->getDetail('agendaTitle',array('title'),"date='".$key1."'");
             $final['title'] = $title[0]['title'];
             $final['date'] = $key1;
             $final['strStartDate'] = $key1;
             $final['timeStamp'] = $finalArraynew[0]['list'][0]['startDate'];
             $final['result'] = $finalArraynew;
             $record[] = $final;
         }
    $uri = $this->getRequest()->getUri();
    $mediaUrl = $this->getMediaUrl();
    $view = new ViewModel(array(
            'uri' => $uri,
            'record' => $record,
            'mediaUrl'=>$mediaUrl,
            'userId'=>$loginDetail['userId']
       ));
    $view->setTerminal(true);
    return $view;  
 }
 
 //method for meeting request accept or reject
public function appendAgendaVideoAction(){
    $request = $this->getRequest();
    $postData = $request->getPost()->toArray();
    $userTable = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $agendaDetail = $userTable->getDetail('agenda',array('title','description','videoUrl'),"id='".$postData['agendaId']."'");
    $mediaUrl = $this->getMediaUrl();
    $view = new ViewModel(array(
            'agendaDetail' => $agendaDetail[0],
            'mediaUrl'=>$mediaUrl
    ));
    $view->setTerminal(true);
    return $view;
}


public function attendeListForMeetingAction(){
    $usrTlb = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $request = $this->getRequest();
    $data = $request->getPost()->toArray();
    //$loginDetail = $this->getLoggedInUserId();
//    $paginator = $usrTlb->getAttendeListForMeeting($data,3,true);
    $results = $usrTlb->getAttendeListForMeeting(3);
//    echo "<pre>"; print_r($results); die;
    $mediaUrl = $this->getMediaUrl();
    $view = new ViewModel(array(
            'results' => $results,
             'mediaUrl'=>$mediaUrl
       ));
        $view->setTerminal(true);
        return $view; 
 }
 
 
 //method for add meeting
public function addMeetingNewAction(){
    $request = $this->getRequest();
    $userTable = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $loginDetail = $this->getLoggedInUserId();
    $postData = $request->getPost()->toArray();
    $postData['userId'] = $loginDetail['userId'];
//    echo '<pre>';print_r($postData); die;
    $stringTrimFilter = new StringTrim();
    $stripTagFilter = new StripTags();
    if(isset($postData['description']) && !empty($postData['description'])) {
            $enterData['description'] = $stripTagFilter->filter(
                $stringTrimFilter->filter($postData['description']));
    }
    if (isset($postData['location']) && !empty($postData['location'])) {
            $enterData['location'] = $stripTagFilter->filter(
                $stringTrimFilter->filter($postData['location']));
    }else{
          $enterData['location']="";  
    }
    if (isset($postData['title']) && !empty($postData['title'])) {
        $enterData['title'] = $stripTagFilter->filter(
                $stringTrimFilter->filter($postData['title']));
    }else{
       $enterData['title']="";  
    }
    $enterData['userId'] =  $postData['userId'];
    $enterData['dateTime'] = $this->timzeconvertion($postData['dateTime']);
    $enterData['createdOn']=time();
    $enterData['updatedOn']=time();
    $enterData['testAccount'] = 3;
    $enterData['status'] =1;
    $meetingId = $userTable->saveData('meeting',$enterData);
    $arr = array('meetingId' => $meetingId , 'userId' => $postData['imatchUserId'], 'createdBy'=> $postData['userId'], 'createdOn' => time(),'updatedOn'=>time(),'status'=>0 );
    $userTable->saveData('meetingAttendies',$arr);
//    echo '<pre>';    print_r($a); die('kjhj');
    //now code for push meeting
    //method for sending post to tag users
    $where3 = "meetingId='".$meetingId."'";
    $getTagUserList = $userTable->getDetail('meetingAttendies',array('userId'),$where3);
    $creatorDetail = $userTable->getDeviceDetailNew($getTagUserList[0]['userId']);
    if(!empty($getTagUserList)){ 
            $where4 = "username='".$postData['userId']."'"; 
            $getUserName = $userTable->getDetail('vcard_search',array('firstName',"lastName"),$where4);
            $text = $getUserName[0]['firstName']." "."invited you for meeting. Please check.";
            $username = substr(time().ceil((rand(0,10000))),0, 5).$this->getUserId(6);
            $interData['title']="";
            $interData['updatedon']=time();
            $interData['message']= $text;
            $interData['notificationDataId'] = 0;
            $interData['notifyId']= $username;
            $interData['creatorId']= $postData['userId'];
            $interData['eventId'] =  $meetingId;
            $interData['type']="addMeeting";
            $body1 = $interData;
            $body1['nickname'] = $getUserName[0]['firstName'];
            $body1['userId'] = $postData['userId'];
            $body1['eventName'] = "";
            $body1['bodyType'] = "addMeeting";
            $body1['notificationMedia'] = array();
            $body1['chatAllowed'] = "";
            $body1['media'] = "";
            $body1['link'] = "";
            $body1['uiType'] = "";
            $body = json_encode($body1,true);
            $interData['body']=$body;
            $interData['username'] = $getTagUserList[0]['userId'];
            $userTable->saveData('notification',$interData);
            if(!empty($creatorDetail[0]['device_token'])){
            //first check android or ios
           if($creatorDetail[0]['device_type']=='android'){
                $pushData["deviceId"] = $creatorDetail[0]["device_token"];
                $pushData["message"] = $body;
                $pushData["pType"] = 'app';
                $pushData["subject"] ='FCM push'; 
                $this->sendAndroidPush($pushData,$creatorDetail[0]['appType']);  
           }else{  
           //now make json to send users
          //now code to get total counter of user
          $userCounter = $userTable->getDetail('userNotifictionCount',array('totalApp','totalChat','timelineCount'),"userId='".$creatorDetail[0]['userId']."' and testAccount='3'");
          if(!empty($userCounter)){
             $userTable->updateData('userNotifictionCount',array('totalApp'=>$userCounter[0]['totalApp']+1),array('userId'=>$creatorDetail[0]['userId'],'testAccount'=>3)); 
          }else{
             $userTable->saveData('userNotifictionCount',array('totalApp'=>1,'userId'=>$creatorDetail[0]['userId'],'testAccount'=>3));  
          }
          //now get user badge Count
          $userBadgeCount = $userTable->getDetail('userNotifictionCount',array('totalApp','totalChat','timelineCount'),"userId='".$creatorDetail[0]['userId']."' and testAccount='3'");
          //now make json to send users
          $message['badge'] = (int)($userBadgeCount[0]['totalApp']+ $userBadgeCount[0]['totalChat']+ $userBadgeCount[0]['timelineCount']);     
          
            $message['mutable-content'] =1;
            $message['sound'] ="default";
            $message['chatId'] = "";
            $message['fromUser'] ="";
            $message['toUser'] = "";
            $message['alert'] = array("title"=>"","body"=>$interData['message']);
            $message['bodyType'] = $interData['type'];
            $message['sender'] = "";
            $message['message']= $body;
            $message['timestamp'] = time();
            $payload['aps'] = $message;
            $payload['c'] = 1;
            $message1= "success";  
            $this->sendapnspush($payload,$message1,$creatorDetail[0]["apnsToken"],'dev',$creatorDetail[0]["appType"],'alert'); 
            $this->sendapnspush($payload,$message1,$creatorDetail[0]["apnsToken"],'dist',$creatorDetail[0]["appType"],'alert');
         }
         
        }
        //code for send email invitaion
       ///code for email
        if(!empty( $creatorDetail[0]["email"])){
        $config = $this->getServiceLocator()->get('config');
        $url = $config['settings']['quickPole']."meeting-response/". base64_encode($postData['imatchUserId']."-".$meetingId);
        $uri = $this->getRequest()->getUri();
        $baseUrl = $uri->getHost();
        if(strstr( $baseUrl,'expoodle')){
        $mail['mailFromNickName'] = "Expoodle";
        //$mail['mailTo'] = "bhupendra@approutes.com";
        $mail['mailTo'] = $creatorDetail[0]["email"];
        $mail['mailSubject'] = "Expoodle | Meeting Request";
        $mail['mailBody'] = "<html><body>Hi ".$creatorDetail[0]["firstName"].",<br>".$getUserName[0]['firstName']." ".$getUserName[0]['lastName']." requested you for a meeting.<br>Here are Meeting Details: <br>Title: ".$enterData['title']." <br>Date&Time: ".$postData['dateTime']."<br><br>Please click on below link for Accept or Reject this meeting request: <a href='".$url."'>Click Here</a> <br>Thanks & Regards<br>Expoodle Team</body></html>";
        $this->sendotpemail($mail);   
       }else{
        $mail['mailFromNickName'] = "EvenTap";
        //$mail['mailTo'] = "bhupendra@approutes.com";
        $mail['mailTo'] = $creatorDetail[0]["email"];
        $mail['mailSubject'] = "EvenTap | Meeting Request";
        $mail['mailBody'] = "<html><body>Hi ".$creatorDetail[0]["firstName"].",<br>".$getUserName[0]['firstName']." ".$getUserName[0]['lastName']." requested you for a meeting.<br>Here are Meeting Details: <br>Title: ".$enterData['title']." <br>Date&Time: ".$postData['dateTime']."<br><br>Please click on below link for Accept or Reject this meeting request: <a href='".$url."'>Click Here</a> <br>Thanks & Regards<br>EvenTap Team</body></html>";
        $this->sendotpemail($mail);
       }       
       }
    }
       //now code for imatch
       //$userTable->updateData('user_match',array('isMatch'=>1,'updatedOn'=> time()),array('user_id'=>$postData['userId'],'job_creator_id'=>$postData['imatchUserId'])); 
       $send = array(
               'message' => "Your Meeting Request has been sent, we will notify you as soon as request is accepted. You can check meeting request status in My Meeting under My Profile.",
               'status'=>1  
        );
       return new JsonModel(array('send'=>$send));
}
public function clientExperienceZoneAction()
 {
    $userTable = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $loginDetail = $this->getLoggedInUserId();
    $userDetail = $userTable->getUserDetail($loginDetail['userId']);
    $mediaUrl = $this->getMediaUrl();
      //code for agenda location
    $agendaLocation  = $userTable->getDetail('agendaLocation',array('location'),"status=1 order by trackOrder asc"); 
       //code for agenda keyword
       $agendaKeyWord  = $userTable->getDetail('agenda',array('keyWord'),"status=0 and testAccount='3' and keyWord!='' and itemType='agenda' group by keyWord order by keyWord asc"); 
        //code for agenda theme
       $agendaTheme  = $userTable->getDetail('agenda',array('theme'),"status=0 and testAccount='3' and theme!='' and itemType='agenda'  group by theme order by theme asc");
       $agendDate = $userTable->getAgendaDate();
       
     $getPartnerDetail = $userTable->getAllPartner();  
     
        $peopleWalking  = $userTable->getDetail('videos',array('media'),"status=1 and sectionType='Exterior' ");
        $entryVideo  = $userTable->getDetail('videos',array('media'),"status=1 and sectionType ='Entry desk'");
        $lobby  = $userTable->getDetail('videos',array('media'),"status=1 and sectionType='Lobby' ");
        $auditorium  = $userTable->getDetail('videos',array('media'),"status=1 and sectionType='Auditorium' ");
        $networking  = $userTable->getDetail('videos',array('media'),"status=1 and sectionType='Networking' ");
        
//        echo '<pre>'; print_r($peopleWalking);
      
    $view = new ViewModel(array('peopleWalking'=>$peopleWalking,  'networking'=>$networking, 'auditorium'=>$auditorium, 'entryVideo'=>$entryVideo, 'lobby'=>$lobby, 'userDetail'=>$userDetail,'mediaUrl'=>$mediaUrl,'agendaLocation'=>$agendaLocation,'agendaKeyWord'=>$agendaKeyWord,'agendaTheme'=>$agendaTheme,'agendDate'=>$agendDate));
    $view->setTerminal(true);
    return $view;
  }
  //code for request meeting model popup
//method for imatch
public function requestMeetingAttendeAction(){
    $request = $this->getRequest();
    $data = $request->getPost()->toArray();
    $userTable = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $userName  = $userTable->getDetail('vcard_search',array("firstName","lastName"),"username='".$data['imatchUserId']."'");
    $view = new ViewModel(array(
            'name' => $userName[0]['firstName']." ".$userName[0]['lastName']
    ));
    $view->setTerminal(true);
    return $view;     
}

public function editUserNewAction()
 {
      $request = $this->getRequest();
      $postData = $request->getPost()->toArray();
      $userTable = $this->getServiceLocator()->get('Application\Model\UsersTable');
      $loginDetail = $this->getLoggedInUserId();
      $userDetail = $userTable->getUserDetailonEdit($loginDetail['userId']);
      $userskill = $userTable->getDetail('user_skills',array('skill_id'),"status=0 and user_id='".$loginDetail['userId']."'");
      $userskills = array();
      foreach($userskill as $skill){
         $userskills[] = $skill['skill_id']; 
      }
      $skills = $userTable->getDetail('skills',array('name','id'),"status='0'");
      $countryList = $userTable->getDetail('country',array('nicename','phonecode'),"phonecode!=0 and phonecode!='91'");
      $mediaUrl = $this->getMediaUrl();
      $view = new ViewModel(array('userDetail'=>$userDetail[0],'userskills'=>$userskills,'skills'=>$skills,'countryList'=>$countryList,'mediaUrl'=>$mediaUrl,'edituser'=>$postData['agendaId']));
      $view->setTerminal(true);
      return $view;
 }
public function experienceZoneAction()
 {
    $userTable = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $loginDetail = $this->getLoggedInUserId();
    $userDetail = $userTable->getUserDetail($loginDetail['userId']);
    $mediaUrl = $this->getMediaUrl();
      //code for agenda location
    $agendaLocation  = $userTable->getDetail('agendaLocation',array('location'),"status=1 order by trackOrder asc"); 
       //code for agenda keyword
       $agendaKeyWord  = $userTable->getDetail('agenda',array('keyWord'),"status=0 and testAccount='3' and keyWord!='' and itemType='agenda' group by keyWord order by keyWord asc"); 
        //code for agenda theme
       $agendaTheme  = $userTable->getDetail('agenda',array('theme'),"status=0 and testAccount='3' and theme!='' and itemType='agenda'  group by theme order by theme asc");
       $agendDate = $userTable->getAgendaDate();
       
     $getPartnerDetail = $userTable->getAllPartner();  
     
        $peopleWalking  = $userTable->getDetail('videos',array('media'),"status=1 and sectionType='Exterior' ");
        $entryVideo  = $userTable->getDetail('videos',array('media'),"status=1 and sectionType ='Entry desk'");
        $lobby  = $userTable->getDetail('videos',array('media'),"status=1 and sectionType='Lobby' ");
        $auditorium  = $userTable->getDetail('videos',array('media'),"status=1 and sectionType='Auditorium' ");
        $networking  = $userTable->getDetail('videos',array('media'),"status=1 and sectionType='Networking' ");
        
        
       $meetingLinkForBooth1  = $userTable->getBoothDetail('1');
       $getBoothMedia= $userTable->getBoothAllDetail(); 
//        echo '<pre>'; print_r($getBoothMedia);
    $view = new ViewModel(array('getBoothMedia'=>$getBoothMedia,'meetingLinkForBooth1'=>$meetingLinkForBooth1,'peopleWalking'=>$peopleWalking,  'networking'=>$networking, 'auditorium'=>$auditorium, 'entryVideo'=>$entryVideo, 'lobby'=>$lobby, 'userDetail'=>$userDetail,'mediaUrl'=>$mediaUrl,'agendaLocation'=>$agendaLocation,'agendaKeyWord'=>$agendaKeyWord,'agendaTheme'=>$agendaTheme,'agendDate'=>$agendDate));
    $view->setTerminal(true);
    return $view;
  }
  
  
  public function networkingAction(){
        $viewModel = new ViewModel(array());
        $viewModel->setTerminal(true);
        return $viewModel;
 }
 
 
 
 
 
//  public function exhibitorDetailViewAction(){
//    $userTable = $this->getServiceLocator()->get('Application\Model\UsersTable');
//    $loginDetail = $this->getLoggedInUserId();     
//    $request = $this->getRequest();
//    $data = $request->getPost()->toArray();
//    $getBoothDetail = $userTable->getBoothDetailView($data['boothId']);
//    $getBoothMenuItems=$userTable->getDetail('boothMenuItem',array('id','menuName','url'),"boothId='".$data['boothId']."' and status=1");
//    $userTable->saveData('boothHitLogs',array('boothId'=>$data['boothId'],'type'=>0,'userId'=>$loginDetail['userId'],'testAccount'=>3,'createdOn'=>time(),'updatedOn'=>time()));  
// 
//   $getBoothVideos=$userTable->getDetail('boothVideos',array('boothVideoTitle','mediaName','media','boothvideoUrl','boothId','id'),"boothId='".$data['boothId']."' and status=1");
//   $getBoothDocument=$userTable->getDetail('boothDocument',array('media','mediaName','Title','boothId','id'),"boothId='".$data['boothId']."' and status=1");
//    $mediaUrl = $this->getMediaUrl();
//        $viewModel = new ViewModel(array('getBoothDetail'=>$getBoothDetail, 'getBoothVideos'=>$getBoothVideos, 'getBoothDocument'=>$getBoothDocument, 'mediaUrl'=>$mediaUrl,'menuItems'=>$getBoothMenuItems,));
//        $viewModel->setTerminal(true);
//        return $viewModel;
//     }
 
 
 
  public function exhibitorDetailView1Action(){
    $userTable = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $loginDetail = $this->getLoggedInUserId();     
    $request = $this->getRequest();
    $data = $request->getPost()->toArray();
//    echo '<pre>'; print_r($data); die;
    if($data['type']=="next"){
     $getGreterActiveIdFromDb=$userTable->getNextBoothId($data['boothId']);
//     echo '<pre>';   print_r($getGreterActiveIdFromDb); die( 'khkh');  
    $getBoothDetail = $userTable->getBoothDetailView($getGreterActiveIdFromDb[0]['id']);
    $getBoothMenuItems=$userTable->getDetail('boothMenuItem',array('id','menuName','url'),"boothId='".$getGreterActiveIdFromDb[0]['boothId']."' and status=1");
    $userTable->saveData('boothHitLogs',array('boothId'=>$getGreterActiveIdFromDb[0]['id'],'type'=>0,'userId'=>$loginDetail['userId'],'testAccount'=>3,'createdOn'=>time(),'updatedOn'=>time()));  
 
   $getBoothVideos=$userTable->getDetail('boothVideos',array('boothVideoTitle','mediaName','media','boothvideoUrl','boothId','id'),"boothId='".$getGreterActiveIdFromDb[0]['id']."' and status=1");
   $getBoothDocument=$userTable->getDetail('boothDocument',array('media','mediaName','Title','boothId','id'),"boothId='".$getGreterActiveIdFromDb[0]['id']."' and status=1");
    
   
    }else if($data['type']=="prev") {
//            echo '<pre>'; print_r($data); die;

    $getGreterActiveIdFromDb=$userTable->getPrevBoothId($data['boothId']);
//     echo '<pre>';   print_r($getGreterActiveIdFromDb); die( 'khkh');  
    $getBoothDetail = $userTable->getBoothDetailView($getGreterActiveIdFromDb[0]['id']);
    $getBoothMenuItems=$userTable->getDetail('boothMenuItem',array('id','menuName','url'),"boothId='".$getGreterActiveIdFromDb[0]['boothId']."' and status=1");
    $userTable->saveData('boothHitLogs',array('boothId'=>$getGreterActiveIdFromDb[0]['id'],'type'=>0,'userId'=>$loginDetail['userId'],'testAccount'=>3,'createdOn'=>time(),'updatedOn'=>time()));  
 
   $getBoothVideos=$userTable->getDetail('boothVideos',array('boothVideoTitle','mediaName','media','boothvideoUrl','boothId','id'),"boothId='".$getGreterActiveIdFromDb[0]['id']."' and status=1");
   $getBoothDocument=$userTable->getDetail('boothDocument',array('media','mediaName','Title','boothId','id'),"boothId='".$getGreterActiveIdFromDb[0]['id']."' and status=1"); 
    
   
    }else{
    
    $getBoothDetail = $userTable->getBoothDetailView($data['boothId']);
    $getBoothMenuItems=$userTable->getDetail('boothMenuItem',array('id','menuName','url'),"boothId='".$data['boothId']."' and status=1");
    $userTable->saveData('boothHitLogs',array('boothId'=>$data['boothId'],'type'=>0,'userId'=>$loginDetail['userId'],'testAccount'=>3,'createdOn'=>time(),'updatedOn'=>time()));  
 
   $getBoothVideos=$userTable->getDetail('boothVideos',array('boothVideoTitle','mediaName','media','boothvideoUrl','boothId','id'),"boothId='".$data['boothId']."' and status=1");
   $getBoothDocument=$userTable->getDetail('boothDocument',array('media','mediaName','Title','boothId','id'),"boothId='".$data['boothId']."' and status=1");
    }
   $mediaUrl = $this->getMediaUrl();
        $viewModel = new ViewModel(array('getBoothDetail'=>$getBoothDetail, 'getBoothVideos'=>$getBoothVideos, 'getBoothDocument'=>$getBoothDocument, 'mediaUrl'=>$mediaUrl,'menuItems'=>$getBoothMenuItems,));
        $viewModel->setTerminal(true);
        return $viewModel;
     }
     
     
     
      public function exhibitorDetailViewAction(){
    $userTable = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $loginDetail = $this->getLoggedInUserId();     
    $request = $this->getRequest();
    $data = $request->getPost()->toArray();
//    echo '<pre>'; print_r($data); die;
    if($data['type']=="next"){
//     echo '<pre>';   print_r($data); die( 'khkh');  
    $getBoothDetail = $userTable->getBoothDetailView($data['boothId']);
    $getBoothMenuItems=$userTable->getDetail('boothMenuItem',array('id','menuName','url'),"boothId='".$data['boothId']."' and status=1");
    $userTable->saveData('boothHitLogs',array('boothId'=>$data['boothId'],'type'=>0,'userId'=>$loginDetail['userId'],'testAccount'=>3,'createdOn'=>time(),'updatedOn'=>time()));  
    $getBoothVideos=$userTable->getDetail('boothVideos',array('boothVideoTitle','mediaName','media','boothvideoUrl','boothId','id'),"boothId='".$data['boothId']."' and status=1");
    $getBoothDocument=$userTable->getDetail('boothDocument',array('media','mediaName','Title','boothId','id'),"boothId='".$data['boothId']."' and status=1");
    }else if($data['type']=="prev") {
//            echo '<pre>'; print_r($data); die;
    $getBoothDetail = $userTable->getBoothDetailView($data['boothId']);
    $getBoothMenuItems=$userTable->getDetail('boothMenuItem',array('id','menuName','url'),"boothId='".$data['boothId']."' and status=1");
    $userTable->saveData('boothHitLogs',array('boothId'=>$data['boothId'],'type'=>0,'userId'=>$loginDetail['userId'],'testAccount'=>3,'createdOn'=>time(),'updatedOn'=>time()));  
 
   $getBoothVideos=$userTable->getDetail('boothVideos',array('boothVideoTitle','mediaName','media','boothvideoUrl','boothId','id'),"boothId='".$data['boothId']."' and status=1");
   $getBoothDocument=$userTable->getDetail('boothDocument',array('media','mediaName','Title','boothId','id'),"boothId='".$data['boothId']."' and status=1");
    }else{
    $getBoothDetail = $userTable->getBoothDetailView($data['boothId']);
    $getBoothMenuItems=$userTable->getDetail('boothMenuItem',array('id','menuName','url'),"boothId='".$data['boothId']."' and status=1");
    $userTable->saveData('boothHitLogs',array('boothId'=>$data['boothId'],'type'=>0,'userId'=>$loginDetail['userId'],'testAccount'=>3,'createdOn'=>time(),'updatedOn'=>time()));  
   $getBoothVideos=$userTable->getDetail('boothVideos',array('boothVideoTitle','mediaName','media','boothvideoUrl','boothId','id'),"boothId='".$data['boothId']."' and status=1");
   $getBoothDocument=$userTable->getDetail('boothDocument',array('media','mediaName','Title','boothId','id'),"boothId='".$data['boothId']."' and status=1");
    }
   $mediaUrl = $this->getMediaUrl();
        $viewModel = new ViewModel(array('getBoothDetail'=>$getBoothDetail, 'getBoothVideos'=>$getBoothVideos, 'getBoothDocument'=>$getBoothDocument, 'mediaUrl'=>$mediaUrl,'menuItems'=>$getBoothMenuItems,));
        $viewModel->setTerminal(true);
        return $viewModel;
     }
     
     
     
     
     public function saveUserCountForMeetingAction(){
    $userTable = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $loginDetail = $this->getLoggedInUserId();     
    $request = $this->getRequest();
    $data = $request->getPost()->toArray();
    $userTable->saveData('boothHitLogs',array('boothId'=>$data['boothId'],'type'=>1,'userId'=>$loginDetail['userId'],'testAccount'=>3,'createdOn'=>time(),'updatedOn'=>time()));  
    $send = array(
//               'message' => "",
               'status'=>1  
        );
       return new JsonModel(array('send'=>$send));
     }
     
  public function exhibitorsBoothVideoAction(){
    $usrTlb = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $loginDetail = $this->getLoggedInUserId();
    $request = $this->getRequest();
    $data = $request->getPost()->toArray();
   //update data in booth tbl :-
   $getBoothVideos=$usrTlb->getDetail('boothVideos',array('media','boothVideoTitle','mediaName'),"boothId='".$data['boothId']."' and id= '".$data['videoId']."' and status=1");
   $usrTlb->saveData('boothContentLogs',array('boothId'=>$data['boothId'],'itemId'=>$data['videoId'],'type'=>1,'userId'=>$loginDetail['userId'],'testAccount'=>3,'createdOn'=>time(),'updatedOn'=>time()));  
   $mediaUrl = $this->getMediaUrl();
    $view = new ViewModel(array(
            'getBoothVideos'=>$getBoothVideos,
            'mediaUrl'=>$mediaUrl,
       ));
    $view->setTerminal(true);
    return $view;
 }   
 
 
 
 public function exhibitorVideoAction(){
    $usrTlb = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $loginDetail = $this->getLoggedInUserId();
    $request = $this->getRequest();
    $data = $request->getPost()->toArray();
    
    if(empty($data['boothId'])){ 
   $getExhibitorVideos=$usrTlb->getDetail('boothVideos',array('id','mediaName' ,'media','boothVideoTitle'),"  status=1");
    }else{
     $getExhibitorVideos=$usrTlb->getDetail('boothVideos',array('id','mediaName', 'media','boothVideoTitle'),"  status=1 and boothId='".$data['boothId']."'");
      
    }
//      echo '<pre>';     print_r($getExhibitorVideos); die(' lol');
  
    $mediaUrl = $this->getMediaUrl();
    $view = new ViewModel(array(
            'getExhibitorVideos'=>$getExhibitorVideos,
            'mediaUrl'=>$mediaUrl,
       ));
    $view->setTerminal(true);
    return $view;
 }  
 
 
   public function saveUserCountForBoothDocAction(){
    $userTable = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $loginDetail = $this->getLoggedInUserId();     
    $request = $this->getRequest();
    $data = $request->getPost()->toArray();
    
    $encodeArr= base64_decode($data['itemId']);
    $explodArr= explode("-", $encodeArr);   
    $itemId=  $explodArr[0];
    $boothId=  $explodArr[1];
    
    $userTable->saveData('boothContentLogs',array('boothId'=>$boothId,'itemId'=>$itemId,'type'=>2,'userId'=>$loginDetail['userId'],'testAccount'=>3,'createdOn'=>time(),'updatedOn'=>time()));  

    $send = array(
//               'message' => "",
               'status'=>1  
        );
       return new JsonModel(array('send'=>$send));
     }
     
     

          
     
 public function checkIsBoothExsitAction(){ 
    $loginDetail = $this->getLoggedInUserId();
    $userTable = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $request = $this->getRequest();
    $data = $request->getPost()->toArray();
//        echo '<pre>'; print_r($data); die(' lol');
 
   $getBoothActiveIdFromDb=$userTable->getNextBoothId($data['boothId']);
    if(!empty($getBoothActiveIdFromDb)){
       $send = array(
               'message' => "",
               'status'=>1 ,
               'id'=>$getBoothActiveIdFromDb[0]['id'],
        );
       return new JsonModel(array('send'=>$send));
    }else{
     $send = array(
               'message' => "",
               'status'=>0,
                'id'=>"", 
        );
       return new JsonModel(array('send'=>$send)); 
    }     
  }
  
  
  
  public function checkIsPrevBoothExsitAction(){ 
    $loginDetail = $this->getLoggedInUserId();
    $userTable = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $request = $this->getRequest();
    $data = $request->getPost()->toArray();
    
   $getBoothActiveIdFromDb=$userTable->getPrevBoothId($data['boothId']);
//     echo '<pre>'; print_r($getBoothActiveIdFromDb); die(' lol');
    if(!empty($getBoothActiveIdFromDb)){
       $send = array(
               'message' => "",
               'status'=>1 ,
               'id'=>$getBoothActiveIdFromDb[0]['id'],
        );
       return new JsonModel(array('send'=>$send));
    }else{
     $send = array(
               'message' => "",
               'status'=>0,
                'id'=>"", 
        );
       return new JsonModel(array('send'=>$send)); 
    }     
  }
  
  
  public function clickBoothVideoForResourceAction(){
    $usrTlb = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $loginDetail = $this->getLoggedInUserId();
    $request = $this->getRequest();
    $data = $request->getPost()->toArray();
   //update data in booth tbl :-
   $getBoothVideos=$usrTlb->getDetail('boothVideos',array('media','boothVideoTitle','mediaName')," id= '".$data['videoId']."' and status=1");
   $mediaUrl = $this->getMediaUrl();
    $view = new ViewModel(array(
            'getBoothVideos'=>$getBoothVideos,
            'mediaUrl'=>$mediaUrl,
       ));
    $view->setTerminal(true);
    return $view;
 }  
 
 
  public function exhibitorDocAction(){
    $usrTlb = $this->getServiceLocator()->get('Application\Model\UsersTable');
    $loginDetail = $this->getLoggedInUserId();
    $request = $this->getRequest();
    $data = $request->getPost()->toArray();
    
    if(empty($data['boothId'])){ 
   $getExhibitorDoc=$usrTlb->getDetail('boothDocument',array('id','mediaName' ,'media','Title'),"  status=1");
    }else{
     $getExhibitorDoc=$usrTlb->getDetail('boothDocument',array('id','mediaName', 'media','Title'),"  status=1 and boothId='".$data['boothId']."'");
      
    }
//      echo '<pre>';     print_r($getExhibitorVideos); die(' lol');
  
    $mediaUrl = $this->getMediaUrl();
    $view = new ViewModel(array(
            'getExhibitorDoc'=>$getExhibitorDoc,
            'mediaUrl'=>$mediaUrl,
       ));
    $view->setTerminal(true);
    return $view;
 } 


//script for delete InactiveUser  :-
public function deleteInactiveUserAction(){
    $userTable = $this->getServiceLocator()->get('Application\Model\UsersTable');    
    $where="(t1.type = 'Admin' || t1.type='investor') and t1.status=1";
    $getInactiveUser=$userTable->getDetail('users',array('username','testAccount'),$where); 
//    echo "<pre>"; print_r($getInactiveUser); die;
 foreach ($getInactiveUser as $key=>$val){    
    $userTable->deleteUserData('auth_session',array("user_id"=>$val['username']));  
    $userTable->deleteUserData('vcard_search',array("username"=>$val['username']));  
    $userTable->deleteUserData('userTravelDetail',array("userId"=>$val['username']));  
    $userTable->deleteUserData('userDetail',array("userId"=>$val['username'])); 
    $userTable->deleteUserData('notification',array("username"=>$val['username']));
    $userTable->deleteUserData('notification',array("creatorId"=>$val['username'])); 
    $userTable->deleteUserData('selectedFoodPreferences',array("userId"=>$val['username'])); 
    $userTable->deleteUserData('kycMasterDocList',array("userId"=>$val['username'])); 
    $userTable->deleteUserData('kycMedia',array("userId"=>$val['username'])); 
    $userTable->deleteUserData('device_details',array("username"=>$val['username'])); 
    $userTable->deleteUserData('agendaFav',array("userId"=>$val['username'])); 
    $userTable->deleteUserData('attendeFav',array("userId"=>$val['username'])); 
    $userTable->deleteUserData('attendeFav',array("otherUserId"=>$val['username'])); 
    $userTable->deleteUserData('isLikeDislike',array("userId"=>$val['username'])); 
    $userTable->deleteUserData('meetingAttendies',array("userId"=>$val['username'])); 
    $userTable->deleteUserData('scannedLogs',array("userId"=>$val['username'])); 
    $userTable->deleteUserData('speakerComment',array("otherUserId"=>$val['username'])); 
    $userTable->deleteUserData('speakerComment',array("userId"=>$val['username'])); 
    $userTable->deleteUserData('speakerFav',array("otherUserId"=>$val['username'])); 
    $userTable->deleteUserData('speakerFav',array("userId"=>$val['username'])); 
    $userTable->deleteUserData('speakerLikes',array("otherUserId"=>$val['username'])); 
    $userTable->deleteUserData('speakerLikes',array("userId"=>$val['username'])); 
//    $userTable->deleteUserData('trialBooking',array("userId"=>$val['username'])); 
//    $userTable->deleteUserData('trialWaiting',array("userId"=>$val['username'])); 
//    $userTable->deleteUserData('userAnswer',array("userId"=>$val['username'])); 
    $userTable->deleteUserData('userChatAdmins',array("adminId"=>$val['username'])); 
    $userTable->deleteUserData('userChatAdmins',array("username"=>$val['username'])); 
    $userTable->deleteUserData('userGameHistory',array("requestBy"=>$val['username'])); 
    $userTable->deleteUserData('userGameHistory',array("requestedTo"=>$val['username'])); 
    $userTable->deleteUserData('userGameTikTok',array("createdBy"=>$val['username'])); 
//    $userTable->deleteUserData('userQuestionAnswer',array("userId"=>$val['username'])); 
//    $userTable->deleteUserData('userRoomMatch',array("userId"=>$val['username'])); 
    $userTable->deleteUserData('user_match',array("user_id"=>$val['username'])); 
    $userTable->deleteUserData('user_match',array("job_creator_id"=>$val['username'])); 
    $userTable->deleteUserData('user_skills',array("user_id"=>$val['username']));
    $userTable->deleteUserData('users',array("username"=>$val['username']));
    }
    die("success");  
 }
 
 
 
 
 //script for delete socialPost and all :-
//  public function deleteSocialMediaAction(){
//    $userTable = $this->getServiceLocator()->get('Application\Model\UsersTable');
//     $where="t1.status = 4";
//     $getInactiveSocialPost=$userTable->getDetail('socialPost',array('userId','id'),$where);
//   foreach ($getInactiveSocialPost as $key=>$val){  
//    $userTable->deleteUserData('socialPostMedia',array("socialPostId"=>$val['id'])); 
//    $userTable->deleteUserData('socialPostUserTag',array("socialPostId"=>$val['id']));  
//    $userTable->deleteUserData('event_comment',array("eventId"=>$val['id']));  
//    $userTable->deleteUserData('isLikeDislike',array("eventId"=>$val['id'])); 
//    $userTable->deleteUserData('notification',array("eventId"=>$val['id'])); 
//    $userTable->deleteUserData('reportSpam',array("socialPostId"=>$val['id'])); 
//    $userTable->deleteUserData('totalViews',array("eventId"=>$val['id']));
//    }
//    $where1="t1.status = 4";
//    $getInactiveStory=$userTable->getDetail('story',array('id'),$where1);
//  foreach ($getInactiveStory as $key1=>$value){  
//    $userTable->deleteUserData('storyLikeDislike',array("storyId"=>$value['id'])); 
//    $userTable->deleteUserData('storyMedia',array("storyId"=>$value['id']));  
//    $userTable->deleteUserData('storyUserTag',array("storyId"=>$value['id']));  
//    $userTable->deleteUserData('storyViews',array("storyId"=>$value['id'])); 
//  }
//  die("success");  
//}
  
  
}